function toJalaali(n, t, i) {
    return Object.prototype.toString.call(n) === "[object Date]" && (i = n.getDate(), t = n.getMonth() + 1, n = n.getFullYear()), d2j(g2d(n, t, i))
}

function toGregorian(n, t, i) {
    return d2g(j2d(n, t, i))
}

function isValidJalaaliDate(n, t, i) {
    return n >= -61 && n <= 3177 && t >= 1 && t <= 12 && i >= 1 && i <= jalaaliMonthLength(n, t)
}

function isLeapJalaaliYear(n) {
    return jalCal(n).leap === 0
}

function jalaaliMonthLength(n, t) {
    return t <= 6 ? 31 : t <= 11 ? 30 : isLeapJalaaliYear(n) ? 30 : 29
}

function jalCal(n) {
    var u = [-61, 9, 38, 199, 426, 686, 756, 818, 1111, 1181, 1210, 1635, 2060, 2097, 2192, 2262, 2324, 2394, 2456, 3178],
        c = u.length, h = n + 621, r = -14, f = u[0], e, t, o, l, a, i, s;
    if (n < f || n >= u[c - 1]) throw new Error("Invalid Jalaali year " + n);
    for (s = 1; s < c; s += 1) {
        if (e = u[s], t = e - f, n < e) break;
        r = r + div(t, 33) * 8 + div(mod(t, 33), 4);
        f = e
    }
    return i = n - f, r = r + div(i, 33) * 8 + div(mod(i, 33) + 3, 4), mod(t, 33) === 4 && t - i == 4 && (r += 1), l = div(h, 4) - div((div(h, 100) + 1) * 3, 4) - 150, a = 20 + r - l, t - i < 6 && (i = i - t + div(t + 4, 33) * 33), o = mod(mod(i + 1, 33) - 1, 4), o === -1 && (o = 4), {
        leap: o,
        gy: h,
        march: a
    }
}

function j2d(n, t, i) {
    var r = jalCal(n);
    return g2d(r.gy, 3, r.march) + (t - 1) * 31 - div(t, 7) * (t - 7) + i - 1
}

function d2j(n) {
    var f = d2g(n).gy, i = f - 621, e = jalCal(i), o = g2d(f, 3, e.march), r, u, t;
    if (t = n - o, t >= 0) {
        if (t <= 185) return u = 1 + div(t, 31), r = mod(t, 31) + 1, {jy: i, jm: u, jd: r};
        t -= 186
    } else i -= 1, t += 179, e.leap === 1 && (t += 1);
    return u = 7 + div(t, 30), r = mod(t, 30) + 1, {jy: i, jm: u, jd: r}
}

function g2d(n, t, i) {
    var r = div((n + div(t - 8, 6) + 100100) * 1461, 4) + div(153 * mod(t + 9, 12) + 2, 5) + i - 34840408;
    return r - div(div(n + 100100 + div(t - 8, 6), 100) * 3, 4) + 752
}

function d2g(n) {
    var t, i, u, r, f;
    return t = 4 * n + 139361631, t = t + div(div(4 * n + 183187720, 146097) * 3, 4) * 4 - 3908, i = div(mod(t, 1461), 4) * 5 + 308, u = div(mod(i, 153), 5) + 1, r = mod(div(i, 153), 12) + 1, f = div(t, 1461) - 100100 + div(8 - r, 6), {
        gy: f,
        gm: r,
        gd: u
    }
}

function div(n, t) {
    return ~~(n / t)
}

function mod(n, t) {
    return n - ~~(n / t) * t
}

function jd_to_gregorian(n) {
    var t = d2g(n);
    return [t.gy, t.gm, t.gd]
}

function gregorian_to_jd(n, t, i) {
    return g2d(n, t, i)
}

function jd_to_persian(n) {
    var t = d2j(n);
    return [t.jy, t.jm, t.jd]
}

function persian_to_jd(n, t, i) {
    return j2d(n, t, i)
}

function persian_to_jd_fixed(n, t, i) {
    if (t > 12 || t <= 0) {
        var r = Math.floor((t - 1) / 12);
        n += r;
        t = t - r * 12
    }
    return persian_to_jd(n, t, i)
}

function digits_fa2en(n) {
    for (var t, r, i = "", u = 0; u < n.length; u++) t = n.charCodeAt(u), t >= 1776 && t <= 1785 ? (r = t - 1728, i = i + String.fromCharCode(r)) : t >= 1632 && t <= 1641 ? (r = t - 1584, i = i + String.fromCharCode(r)) : i = i + String.fromCharCode(t);
    return i
}

function digits_en2fa(n) {
    var i, r, t, u;
    if (n) {
        for (n = n + "", i = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"], r = ["۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"], t = 0, u = i.length; t < u; t++) n = n.replace(new RegExp(i[t], "g"), r[t]);
        return n
    }
}

function pad2(n) {
    return n < 10 ? `0${n}` : n
}

function parseDateString(n) {
    var t = /^(شنبه|یکشنبه|دوشنبه|سه شنبه|چهارشنبه|پنجشنبه|جمعه)\s(\d\d)\s*(فروردین|اردیبهشت|خرداد|تیر|مرداد|شهریور|مهر|آبان|آذر|دی|بهمن|اسفند)\s*(\d\d\d\d)$/.exec(n),
        r, u, f, i;
    if (t) return r = parseInt(t[4]), u = parseInt(JDate.i18n.months.indexOf(t[3])) + 1, f = parseInt(t[2]), i = jd_to_gregorian(persian_to_jd_fixed(r, u, f)), new Date(i[0], i[1] - 1, i[2])
}

function parseDate(n, t) {
    var i = /^(\d|\d\d|\d\d\d\d)(?:([-\/])(\d{1,2})(?:\2(\d|\d\d|\d\d\d\d))?)?(([ T])(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d+))?)?(Z|([+-])(\d{2})(?::?(\d{2}))?)?)?$/.exec(n),
        f, o, c, u, e, r, s, l, a, v, y, h, p, w;
    if (i && (o = i[2], c = i[6], u = +i[1], e = +i[3] || 1, r = +i[4] || 1, s = o !== "/" && i[6] !== " ", l = +i[7] || 0, a = +i[8] || 0, v = +i[9] || 0, y = +`0.${i[10] || "0"}` * 1e3, h = i[11], p = s && (h || !i[5]), w = (i[12] === "-" ? -1 : 1) * ((+i[13] || 0) * 60 + (+i[14] || 0)), !h && c !== "T" || s) && r >= 1e3 != u >= 1e3) {
        if (r >= 1e3) {
            if (o === "-") return;
            r = +i[1];
            u = r
        }
        if (t) {
            const n = jd_to_gregorian(persian_to_jd_fixed(u, e, r));
            u = n[0];
            e = n[1];
            r = n[2]
        }
        return f = new Date(u, e - 1, r, l, a, v, y), p && f.setUTCMinutes(f.getUTCMinutes() - f.getTimezoneOffset() + w), f
    }
}

function JDate(n, t, i, r, u, f, e) {
    if (n === "") this._d = new Date; else if (typeof n == "string" && parseInt(n) != n) n = digits_fa2en(n), this._d = parseDate(n, !0), isDate(this._d) || (this._d = parseDateString(n), isDate(this._d) || (this._d = new Date(n), isDate(this._d) || (this._d = new Date))); else if (arguments.length === 0) this._d = new Date; else if (arguments.length === 1) this._d = new Date(n instanceof JDate ? n._d : n); else {
        const o = jd_to_gregorian(persian_to_jd_fixed(parseInt(n), (parseInt(t) || 0) + 1, parseInt(i) || 1));
        this._d = new Date(o[0], o[1] - 1, o[2], r || 0, u || 0, f || 0, e || 0)
    }
    this._date = this._d;
    this._cached_date_ts = null;
    this._cached_date = [0, 0, 0];
    this._cached_utc_date_ts = null;
    this._cached_utc_date = [0, 0, 0]
}

function generateImageOPT() {
    const n = new XMLHttpRequest;
    n.open("POST", "http://melrio.com/api/v1/generateImageOPT", !0);
    n.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
    n.onreadystatechange = function () {
        var t, i, r;
        n.readyState == 4 && n.status == 200 && (t = readBody(n), t != undefined && t != null && (i = JSON.parse(t), authenticatedCode = i.code, r = document.getElementById(prefex + "authenticatedCode"), r.innerHTML = authenticatedCode))
    };
    n.send("submissionid=" + formId + "&form_token=" + form_token)
}

function addCSS(n) {
    var t = document.createElement("style");
    t.setAttribute("type", "text/css");
    t.innerHTML = n;
    document.getElementsByTagName("head")[0].appendChild(t)
}

function onIdentityTypeInserted(n, t, i) {
    var r = ["FirstName", "LastName", "Gender", "CellNumber", "Email", "BirthDate", "RegNumber", "Marriage", "SpouseName", "Children", "NationalID", "PassportNumber", "DrivingLicence", "FatherName", "BirthPlace", "RegIssuePlace", "MilitaryService"],
        u = ["OrganType", "CompanyType", "CompanyName", "NationalIDCode", "DirectorFirstName", "DirectorLastName", "AgentFirstName", "AgentLastName", "AgentCellNumber", "AgentEmail", "PREmail"],
        f = ["PhoneNumber", "BankCardNumber"], e = r.concat(u).concat(f), o, s;
    for (o in e) if (s = document.getElementById(prefex + e[o] + "-wrapper"), s != undefined) {
        s.style.display = "none";
        let n = getFieldById(e[o]);
        n.orgRequired = n.required;
        n.required = !1
    }
    document.getElementById(prefex + i.id).addEventListener("change", function () {
        var n = this.querySelector("input:checked").value;
        n == "natural" ? (setITRelatedInputsState(r, !0), setITRelatedInputsState(u, !1), setITRelatedInputsState(f, !0)) : (setITRelatedInputsState(u, !0), setITRelatedInputsState(r, !1), setITRelatedInputsState(f, !0));
        document.querySelector("." + prefex + "form-wrapper").style.height = pages[0].el.offsetHeight
    });
    setPageWidth()
}

function onMarriageTypeInserted(n, t, i) {
    var r = ["SpouseName", ""], u = ["Children"];
    document.getElementById(prefex + i.id).addEventListener("change", function () {
        var n = this.querySelector("input:checked").value;
        n == "married" ? (setITRelatedInputsState(r, !0), setITRelatedInputsState(u, !0)) : n == "single" ? (setITRelatedInputsState(r, !1), setITRelatedInputsState(u, !1)) : n == "divorced" && (setITRelatedInputsState(r, !1), setITRelatedInputsState(u, !0));
        document.querySelector("." + prefex + "form-wrapper").style.height = pages[0].el.offsetHeight
    });
    setPageWidth()
}

function onGenderTypeInserted(n, t, i) {
    var r = ["MilitaryService"];
    document.getElementById(prefex + i.id).addEventListener("change", function () {
        var n = this.querySelector("input:checked").value;
        n == "male" ? setITRelatedInputsState(r, !0) : setITRelatedInputsState(r, !1);
        document.querySelector("." + prefex + "form-wrapper").style.height = pages[0].el.offsetHeight
    });
    setPageWidth()
}

function setITRelatedInputsState(n, t) {
    n.forEach(function (n) {
        var o = document.getElementById(prefex + n + "-wrapper"), i, u, f, r, e;
        if (o != undefined && (o.style.display = t ? "" : "none", i = getFieldById(n), u = getCustomFieldById(n), i.required = t ? i.orgRequired : !1, f = document.getElementById(prefex + i.id), f != undefined)) if (t) {
            if (u.Required) if (i.type == "masktext") for (r = 0; r < u.max; r++) e = document.getElementById(prefex + i.id + r), e.setAttribute("required", ""); else f.setAttribute("required", "")
        } else if (i.type == "masktext") for (r = 0; r < u.max; r++) e = document.getElementById(prefex + i.id + r), e.removeAttribute("required"); else f.removeAttribute("required")
    })
}

function mapInit(n) {
    L.mapbox.accessToken = "pk.eyJ1IjoibmFtaW5hYmlsemFkZWgiLCJhIjoiY2s4b3Y3YWJyMDV2cTNmcHJrMHFiejl1ZCJ9.FTlM39MR4Ob3HUTX2CPbtw";
    var t = L.mapbox.map(n).setView([35.7148197, 51.3411997], 15).addLayer(L.mapbox.styleLayer("mapbox://styles/mapbox/streets-v11"));
    marker = L.marker([35.7148197, 51.3411997], {
        icon: L.mapbox.marker.icon({
            "marker-size": "large",
            "marker-symbol": "star",
            "marker-color": "#ff5b5c"
        }), draggable: !0
    });
    marker.addTo(t)
}

function ajax(n) {
    var t = new XMLHttpRequest;
    t.onreadystatechange = function () {
        this.readyState == 4 && (this.status == 200 ? n.success && n.success(this) : n.error && n.error(this))
    };
    t.open(n.method, n.url, !0);
    t.setRequestHeader("Content-type", "application/json");
    n.token && t.setRequestHeader("Authorization", "Token " + n.token);
    n.method == "post" ? t.send(JSON.stringify(n.data)) : t.send()
}

function getFieldById(n) {
    return fields.find(function (t) {
        return t.id == n
    })
}

function getCustomFieldById(n) {
    return selectedfields.find(function (t) {
        return t.id == n
    })
}

function setPageWidth() {
    document.querySelector("." + prefex + "form-wrapper").style.height = theme.template == "paginated" ? pages[currPage - 1].el.offsetHeight : "auto"
}

function checkPageValidation(n) {
    var u, t, r, i;
    for (u in n.fields) if (t = n.fields[u], typeof t.hasError == "undefined") {
        r = document.getElementById(prefex + t.id);
        i = document.getElementById(prefex + t.id + "-wrapper");
        switch (t.type) {
            case"text":
                castFieldEvents(t.id) ? (ev = castFieldEvents(t.id).find(function (n) {
                    return n.event == "validation"
                }), ev ? ev.handler(r, i, t, !1) : textInputValidation(r, i, t, !1)) : textInputValidation(r, i, t, !1);
                break;
            case"masktext":
                castFieldEvents(t.id) ? (ev = castFieldEvents(t.id).find(function (n) {
                    return n.event == "validation"
                }), ev ? ev.handler(r, i, t, !1) : masktextInputValidation(r, i, t, !1)) : masktextInputValidation(r, i, t, !1);
                break;
            case"select":
                selectInputValidation(r, i, t, !1);
                break;
            case"radio":
                checkInputValidation(i, t, !1);
                break;
            case"checkbox":
                checkInputValidation(i, t, !1);
                break;
            case"image":
                imageInputValidation(i, t)
        }
    }
}

function emailValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(n.value) === !1 ? "ایمیل صحیح نیست" : !1
    })
}

function phoneValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return /^09\d{9}$/.test(n.value) === !1 ? "شماره تلفن همراه صحیح نیست" : !1
    })
}

function tempNationalIdValidation(n, t, i, r) {
    console.log(n)
    textInputValidation(n, t, i, r, function () {
        return /\d{10}/.test(n.value) === !1 ? "کد ملی صحیح نیست" : !1
    })
}

function nationalIDValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        var n = getMaskTextValue(i);
        return /\d{10}/.test(n) === !1 ? "کد ملی صحیح نیست" : !1
    })
}

function nationalIDCodeValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        var n = getMaskTextValue(i);
        return /\d{11}/.test(n) === !1 ? "کد شناسه ملی صحیح نیست" : !1
    })
}

function passportNumberValidation1(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        var n = getMaskTextValue(i);
        return /\d{11}/.test(n) === !1 ? "کد شناسه ملی صحیح نیست" : !1
    })
}

function passportNumberValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return /^(?!^0+$)[a-zA-Z0-9]{4,10}$/.test(n.value) === !1 ? "شماره پاسپورت صحیح نیست" : !1
    })
}

function persianRegexValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return persianRegex.test(n.value) === !1 ? "فقط حروف فارسی مجاز هستند" : !1
    })
}

function regionValidation(n, t, i, r) {
    var u = document.getElementById(prefex + "RegionType").value;
    textInputValidation(n, t, i, r, function () {
        return addressValidation(n, t, i, u)
    })
}

function roadValidation(n, t, i, r) {
    var u = document.getElementById(prefex + i.id.replace("Name", "Type")).value;
    textInputValidation(n, t, i, r, function () {
        return addressValidation(n, t, i, u)
    })
}

function unitValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return addressValidation(n, t, i, "unit")
    })
}

function postCodeValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return /\b(?!(\d)\1{3})[13-9]{4}[1346-9][013-9]{5}\b/.test(n.value) === !1 ? "کد پستی صحیح نیست" : !1
    })
}

function houseNumberValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        return addressValidation(n, t, i, "house_number")
    })
}

function buildingNameValidation(n, t, i, r) {
    textInputValidation(n, t, i, r, function () {
        var r = document.getElementById(prefex + "noHouseNumber").checked;
        return r && isEmpty(n) ? i.label + " نمی‌تواند خالی باشد" : addressValidation(n, t, i, "building")
    })
}

function addressValidation(n, t, i, r) {
    var u, f, e;
    return n.value == "" && i.required ? i.label + " نمی‌تواند خالی باشد" : (u = getFieldById(i.id.replace("Name", "Type")), f = u ? document.getElementById(prefex + u.id) : null, f != null && i.required && (f.value == "" || f.value == null)) ? "لطفا " + u.label + " را انتخاب کنید" : !n.required && n.value == "" ? void 0 : (e = "type=" + r + "&q=" + n.value, e = encodeURI(e), ajax({
        url: apiUrl + e,
        method: "get",
        token: apiToken,
        success: function (r) {
            var u = JSON.parse(r.responseText), e = t.querySelector("." + prefex + "form-help"), f, o;
            if (u.error == 0) setInputError("", t, i, n.value), n.value = u.final_text, e.classList.remove("show"); else if (setInputError(u.message, t, i, n.value), u.poison.length > 0) {
                f = n.value;
                for (o in u.poison) f = f.replace(u.poison[o], "<span class='strike-word'>" + u.poison[o] + "<\/span>");
                f = "راهنما: " + f;
                e.innerHTML = f;
                e.classList.add("show")
            } else e.classList.remove("show")
        },
        error: function () {
            setInputError("", t, i, value.input)
        }
    }), "in_progress")
}

function masktextInputValidation(n, t, i, r, u = null) {
    function f() {
        var r = "", f = getMaskTextValue(i);
        i.required && isEmptyMasktext(f) ? r = i.label + " نمی‌تواند خالی باشد." : f.length < i.minLength ? r = i.label + " نمی‌تواند کمتر از " + i.minLength + " کاراکتر باشد" : f.length > i.maxLength ? r = i.label + " نمی‌تواند بیشتر از " + i.maxLength + " کاراکتر باشد" : u != null && (r = u());
        r != "" ? (i.hasError = !0, setInputError(r, t, i, n.value)) : (i.hasError = !1, setInputError(r, t, i, n.value))
    }

    i.validationTimeout && clearTimeout(i.validationTimeout);
    r ? i.validationTimeout = setTimeout(function () {
        f()
    }, 900) : f()
}

function textInputValidation(n, t, i, r, u = null) {
    function f() {
        var r = "";
        i.required && isEmpty(n) ? r = i.label + " نمی‌تواند خالی باشد." : n.value.length < i.minLength ? r = i.label + " نمی‌تواند کمتر از " + i.minLength + " کاراکتر باشد" : n.value.length > i.maxLength ? r = i.label + " نمی‌تواند بیشتر از " + i.maxLength + " کاراکتر باشد" : u != null && (r = u());
        r != "" ? (i.hasError = !0, setInputError(r, t, i, n.value)) : (i.hasError = !1, setInputError(r, t, i, n.value))
    }

    if (i.type == "masktext") {
        masktextInputValidation(n, t, i, r, u);
        return
    }
    n.addEventListener('focusout', (event) => {
        f()
    });
    // i.validationTimeout && clearTimeout(i.validationTimeout);
    // r ? i.validationTimeout = setTimeout(function () {
    //     f()
    // }, 2500) : f()
}

function selectInputValidation(n, t, i) {
    var r = !1;
    i.required && n.value == "" && (r = i.label + " را انتخاب کنید");
    r ? setInputError(r, t, i, n.value) : setInputError(r, t, i, n.value)
}

function checkInputValidation(n, t) {
    var i = !1, r = document.querySelector("input[name='" + t.id + "']:checked");
    t.required && !r && (i = t.label + " را انتخاب کنید");
    i ? setInputError(i, n, t) : setInputError(i, n, t)
}

function imageInputValidation(n, t) {
    var i = !1, r = document.getElementById(prefex + t.id);
    t.required && r.files.length == 0 ? i = t.label + " را انتخاب کنید" : r.files[0].size > t.maxLength && (i = "حجم فایل انتخاب شده بیشتر از حد مجاز است");
    i ? setInputError(i, n, t) : setInputError(i, n, t)
}

function getMaskTextValue(n) {
    for (var t, r = "", i = 0; i < n.maxLength; i++)

        t = document.getElementById(prefex + n.id + i)
        r += t.value
        console.log(r)
    return r
}

function isEmptyMasktext(n) {
    return n.trim() == "" ? !0 : !1
}

function isEmpty(n) {
    return n.value.trim() == "" ? !0 : !1
}

function setInputError(n, t, i, r) {
    var u = getValidationElement(t);
    u.innerHTML = n && n != "in_progress" ? n : "";
    n == "" ? (i.hasError = !1, i.required || r != null && r != undefined && r.trim() ? t.classList.add("validated") : t.classList.remove("validated"), document.getElementById(prefex + i.id).classList.remove("error")) : n != "in_progress" && (i.hasError = !0, t.classList.remove("validated"), document.getElementById(prefex + i.id).classList.add("error"))
}

function getValidationElement(n) {
    return document.getElementById(n.id).getElementsByClassName(prefex + "form-validation")[0]
}

function addEvents() {
    console.log("Caled")
    var f, e, l, p, n, r, u, o, s, h, c, v, y;
    for (f in pages) {
        e = pages[f];
        l = e.fields;
        for (p in l) if (n = l[p], r = selectedfields.find(t => t.id == n.id), r != undefined) {
            var a = document.getElementById(prefex + n.id + "-wrapper"), t = document.getElementById(prefex + n.id),
                i = castFieldEvents(n.id), w = !1;
            if (n.Required = r.Required, n.minLength = r.min, n.maxLength = r.max, n.lengthStatus = r.required, n.type == "map") {
                mapInit(prefex + "map");
                continue
            }
            if (i != null) for (u in i) o = "", i[u].event == "validation" ? (o = "input", w = !0) : o = i[u].event, s = t, i[u].target && (s = document.getElementById(i[u].target)), s.addEventListener(o, i[u].handler.bind(null, s, n.el, n, !0, null));
            if (t.addEventListener("focusout", function (n) {
                n.getElementsByClassName(prefex + "form-help")[0].classList.remove("show")
            }.bind(null, a)), n.type == "image" ? (document.getElementById(t.id + "-btn").addEventListener("click", onImageButtonClick.bind(null, t)), t.addEventListener("change", onImageChange.bind(null, t, n.el, n))) : n.type == "cardnumber" && Array.from(document.querySelectorAll("#" + prefex + n.id + " input"), function (t) {
                t.addEventListener("input", onCardnumberChange.bind(null, t, a, n));
                t.addEventListener("keydown", onCardnumberChange.bind(null, t, a, n))
            }), !w) switch (n.type) {
                case"text":
                    console.log("here2")
                    t.addEventListener("input", textInputValidation.bind(null, t, n.el, n, !0, null));
                    break;
                case"masktext":
                    for (h = 0; h < n.maxLength; h++) c = document.getElementById(prefex + n.id + h), c != undefined && c != null && c.addEventListener("input", masktextInputValidation.bind(null, t, n.el, n, !0, null));
                    break;
                case"select":
                    t.addEventListener("change", selectInputValidation.bind(null, t, n.el, n, !0));
                    break;
                case"radio":
                    Array.from(t.getElementsByTagName("input")).map(function (t) {
                        t.addEventListener("change", checkInputValidation.bind(null, n.el, n, !1))
                    });
                    break;
                case"checkbox":
                    Array.from(t.getElementsByTagName("input")).map(function (t) {
                        t.addEventListener("change", checkInputValidation.bind(null, n.el, n, !1))
                    });
                    break;
                case"textarea":
                    t.addEventListener("input", textInputValidation.bind(null, t, n.el, n, !0, null))
            }
            switch (n.type) {
                case"text":
                    autocomplete(t, n);
                    autocorrection(t, n)
            }
        }
        v = document.querySelector("." + prefex + "page:nth-child(" + (parseInt(f) + 1) + ") ." + prefex + "next-button");
        y = document.querySelector("." + prefex + "page:nth-child(" + (parseInt(f) + 1) + ") ." + prefex + "prev-button");
        v && v.addEventListener("click", nextHandler.bind(e));
        y && y.addEventListener("click", prevHandler.bind(e))
    }
    document.getElementsByClassName(prefex + "form")[0].addEventListener("submit", submitHandler);
    window.addEventListener("resize", onWindowResize)
}

function onImageButtonClick(n) {
    n.click()
}

function onImageChange(n, t, i) {
    var r = "";
    r = n.value.split("\\").pop();
    t.querySelector("." + prefex + "file-name").innerHTML = r ? r : "";
    imageInputValidation(t, i)
}

function onCardnumberChange(n, t, i, r, u) {
    n.value = n.value.replace(/[^\d]/g, "");
    var f = n.id.slice(-1);
    n.value.length == 4 && f != "4" && document.getElementById(prefex + i.id + "-" + (parseInt(f) + 1)).focus();
    n.value.length == 0 && f != "1" && u.keyCode && u.keyCode == 8 && document.getElementById(prefex + i.id + "-" + (parseInt(f) - 1)).focus()
}

function onNoHouseNumberChange(n) {
    n.checked ? document.getElementById(prefex + "HouseNumber").setAttribute("disabled", "") : document.getElementById(prefex + "HouseNumber").removeAttribute("disabled")
}

function nextHandler(n) {
    var u, f, i, e, r, t;
    n.preventDefault();
    checkPageValidation(pages[currPage - 1]);
    for (u in pages[currPage - 1].fields) if (f = pages[currPage - 1].fields[u], f.hasError) return;
    i = document.getElementsByClassName(prefex + "form-wrapper")[0];
    e = i.offsetWidth;
    i.scrollLeft -= e;
    currPage++;
    for (r in pages) t = pages[r], parseInt(r) + 1 == currPage ? (t.el.style.opacity = 1, t.el.style.transform = "scale(1)") : (t.el.style.opacity = 0, t.el.style.transform = "scale(0.7)");
    i.scrollIntoView();
    setPageWidth()
}

function prevHandler(n) {
    var i, u, r, t;
    n.preventDefault();
    i = document.getElementsByClassName(prefex + "form-wrapper")[0];
    u = i.offsetWidth;
    i.scrollLeft += u;
    currPage--;
    for (r in pages) t = pages[r], parseInt(r) + 1 == currPage ? (t.el.style.opacity = 1, t.el.style.transform = "scale(1)") : (t.el.style.opacity = 0, t.el.style.transform = "scale(0.7)");
    i.scrollIntoView();
    setPageWidth()
}

function submitHandler(n) {
    var r = [], h = {}, e, c, t, o, s, l, a, f, i, v, y, p;
    h = {fieldId: "AuthenticatedCode", value: authenticatedCode};
    r.push(h);
    e = controlsType;
    for (c in e) t = e[c], t.id != "Location" ? t.type == "radio" ? (o = document.getElementById(prefex + t.id), o != undefined && (s = o.querySelector("input:checked"), s != undefined && (i = {}, i = {
        fieldId: t.id,
        value: s.value
    }, r.push(i)))) : t.type == "masktext" ? (f = selectedfields.find(n => n.id == t.id), f != undefined && (l = getMaskTextValue(t), i = {
        fieldId: t.id,
        value: l
    }, r.push(i))) : t.type == "image" ? (f = selectedfields.find(n => n.id == t.id), f != undefined && (a = document.getElementById(prefex + t.id), i = {
        fieldId: t.id,
        value: a.files[0]
    }, r.push(i))) : (f = selectedfields.find(n => n.id == t.id), f != undefined && (i = {}, v = document.getElementById(prefex + t.id).value, i = {
        fieldId: t.id,
        value: v
    }, r.push(i))) : (y = document.getElementById(prefex + "map"), y != null && (i = {
        fieldId: t.id,
        value: marker.getLatLng()
    }, r.push(i)));
    const u = new XMLHttpRequest;
    u.open("POST", "http://melrio.com/api/v1/submitForm", !0);
    p = "application/x-www-form-urlencoded; charset=utf-8";
    u.setRequestHeader("Content-Type", p);
    u.onreadystatechange = function () {
        var n, t;
        u.readyState == 4 && u.status == 200 && (n = readBody(u), n != undefined && n != null && (t = JSON.parse(n)))

    };
    u.send("submission_id=" + formId + "&data=" + JSON.stringify(r));
    alert("d");
    n.preventDefault()
}

function onWindowResize() {
    var n = document.getElementsByClassName(prefex + "form-wrapper")[0];
    n.scrollLeft = n.scrollWidth - n.scrollWidth / pages.length * currPage;
    setPageWidth()
}

function provinceChanged(n, t, i) {
    var r = castFieldOptions(i.options).find(function (t) {
        return t.value == n.value
    });
    r.citiesLoaded || ajax({
        url: "http://api.geocoding.ir/rest/cities/?province=" + r.id,
        method: "get",
        success: function (n) {
            var o = JSON.parse(n.responseText), t = [], e, u, i, r, f;
            for (o.results.map(function (n) {
                var i = {label: n.normalized_name, value: n.normalized_name, "data-id": n.id};
                n.is_province_center ? t.unshift(i) : t.push(i)
            }), e = getFieldById("City"), e.options = t, u = document.querySelector("#" + prefex + "City-wrapper option"); u;) u.remove(), u = document.querySelector("#" + prefex + "City-wrapper option");
            for (i = 0; i < t.length; i++) r = document.createElement("option"), r.value = t[i].value, r.setAttribute("data-id", t[i]["data-id"]), r.innerHTML = t[i].label, document.querySelector("#" + prefex + "City-wrapper select").appendChild(r);
            f = document.createEvent("HTMLEvents");
            f.initEvent("change", !1, !0);
            document.getElementById(prefex + "City").dispatchEvent(f)
        }
    })
}

function childrenChanged() {
}

function readBody(n) {
    return n.responseType && n.responseType !== "text" ? n.responseType === "document" ? n.responseXML : n.response : n.responseText
}

function autocomplete(n, t) {
    function r(n) {
        if (!n) return !1;
        f(n);
        i >= n.length && (i = 0);
        i < 0 && (i = n.length - 1);
        n[i].classList.add("autocomplete-active")
    }

    function f(n) {
        for (var t = 0; t < n.length; t++) n[t].classList.remove("autocomplete-active")
    }

    function u(t) {
        for (var r = document.getElementsByClassName("autocomplete-items"), i = 0; i < r.length; i++) t != r[i] && t != n && r[i].parentNode.removeChild(r[i])
    }

    var i;
    n.addEventListener("input", function () {
        function r() {
            var r, f, e;
            if (!isEmpty(n)) {
                if (r = "", f = "", t.id == "MainRoadName" || t.id == "PrimaryRoadName" || t.id == "SecondaryRoadName") r = "http://melrio.com/api/v1/autocomplete?prefix=" + n.value.trim() + "&count=10", f = "color:green;"; else return;
                const e = new XMLHttpRequest;
                e.open("GET", r);
                e = "application/x-www-form-urlencoded; charset=utf-8";
                e.overrideMimeType("text/json; UTF-8");
                e.responseType = "json";
                e.setRequestHeader("Authorization", "Token " + _token);
                e.onreadystatechange = function () {
                    var h, o, s, t, r, c;
                    if (e.readyState == 4 && e.status == 200 && (h = readBody(e), h != undefined && h != null)) {
                        if (o = h.result, c = n.value, u(), !c) return !1;
                        for (i = -1, s = document.createElement("DIV"), s.setAttribute("id", n.id + "autocomplete-list"), s.setAttribute("class", "autocomplete-items"), n.parentNode.appendChild(s), r = 0; r < o.length; r++) t = document.createElement("DIV"), t.innerHTML = "<strong style='" + f + "'>" + o[r].substr(0, c.length) + "<\/strong>", t.innerHTML += o[r].substr(c.length), t.innerHTML += "<input type='hidden' value='" + o[r] + "'>", t.addEventListener("click", function () {
                            n.value = this.getElementsByTagName("input")[0].value;
                            u()
                        }), s.appendChild(t)
                    }
                };
                e.send()
            }
        }

        t.validationTimeout = setTimeout(function () {
            r()
        }, 900)
    });
    n.addEventListener("keydown", function (t) {
        var u = document.getElementById(n.id + "autocomplete-list");
        u && (u = u.getElementsByTagName("div"));
        t.keyCode == 40 ? (i++, r(u)) : t.keyCode == 38 ? (i--, r(u)) : t.keyCode == 13 && (t.preventDefault(), i > -1 && u && u[i].click())
    })
}

function autocorrection(n, t) {
    function r(n) {
        if (!n) return !1;
        u(n);
        i >= n.length && (i = 0);
        i < 0 && (i = n.length - 1);
        n[i].classList.add("autocomplete-active")
    }

    function u(n) {
        for (var t = 0; t < n.length; t++) n[t].classList.remove("autocomplete-active")
    }

    function f(t) {
        for (var r = document.getElementsByClassName("autocomplete-items"), i = 0; i < r.length; i++) t != r[i] && t != n && r[i].parentNode.removeChild(r[i])
    }

    var i;
    n.addEventListener("focusout", function () {
        function r() {
            var r, u, e;
            if (!isEmpty(n)) {
                if (r = "", u = "", t.id == "MainRoadName" || t.id == "PrimaryRoadName" || t.id == "SecondaryRoadName") r = "http://melrio.com/api/v1/autocorrection?phrase=" + n.value.trim(), u = "color:yellow;"; else return;
                const e = new XMLHttpRequest;
                e.open("GET", r);
                e = "application/x-www-form-urlencoded; charset=utf-8";
                e.overrideMimeType("text/json; UTF-8");
                e.responseType = "json";
                e.setRequestHeader("Authorization", "Token " + _token);
                e.onreadystatechange = function () {
                    var l, r, h, o, s, c;
                    if (e.readyState == 4 && e.status == 200 && (l = readBody(e), l != undefined && l != null)) {
                        if (r = l.result, c = n.value, t.separator == "address") if (r[c].changed == "true") r = r[c].corrected; else return !1;
                        for (i = -1, h = document.createElement("DIV"), h.setAttribute("id", n.id + "autocomplete-list"), h.setAttribute("class", "autocomplete-items"), n.parentNode.appendChild(h), s = 0; s < r.length; s++) o = document.createElement("DIV"), o.innerHTML = "<strong style='" + u + "'>" + r[s].substr(0, c.length) + "<\/strong>", o.innerHTML += r[s].substr(c.length), o.innerHTML += "<input type='hidden' value='" + r[s] + "'>", o.addEventListener("click", function () {
                            n.value = this.getElementsByTagName("input")[0].value;
                            f()
                        }), h.appendChild(o)
                    }
                };
                e.send()
            }
        }

        t.validationTimeout = setTimeout(function () {
            r()
        }, 900)
    });
    n.addEventListener("keydown", function (t) {
        var u = document.getElementById(n.id + "autocomplete-list");
        u && (u = u.getElementsByTagName("div"));
        t.keyCode == 40 ? (i++, r(u)) : t.keyCode == 38 ? (i--, r(u)) : t.keyCode == 13 && (t.preventDefault(), i > -1 && u && u[i].click())
    })
}

function selfPortraitDescription() {
    var r = document.createElement("DIV"), t, i;
    r.className = prefex + "selfportrait-desc";
    var u = document.createElement("DIV"), f = document.createElement("DIV"), n = document.createElement("DIV");
    return n.id = prefex + "authenticatedCode", n.className = "code", n.innerHTML = authenticatedCode, t = document.createElement("SPAN"), t.className = "help", t.innerHTML = "لطفا کد 4 رقمی را بصورت بزرگ و خوانا روی یک برگه سفید نوشته و در کنار صورت خود نگهدارید. سپس یک عکس سلفی از خود به همراه این برگه بگیرید و در اینجا آپلود نمایید. دقت نمایید صورت شما شفاف، بدون عینک و یا گریم و کلاه باشد.", i = document.createElement("img"), i.className = "image", i.src = "data:image/png;base64," + selfportraitAsBase64, u.innerHTML = n.outerHTML + t.outerHTML, f.innerHTML = i.outerHTML, r.innerHTML = u.outerHTML + f.outerHTML, r
}

function addressStatusOptions() {
    return [{
        label: "محل دائمی سکونت | مالک هستم | بیش از دو سال در این مکان هستم",
        value: 1
    }, {
        label: "محل دائمی سکونت | مالک هستم | بیش از یک سال و کمتر از دو سال در این مکان هستم",
        value: 2
    }, {
        label: "محل دائمی سکونت | مالک هستم | به زودی تغییر مکان می‌دهم",
        value: 3
    }, {
        label: "محل دائمی سکونت | مستاجر هستم | بیش از یک سال در این مکان هستم",
        value: 4
    }, {
        label: "محل دائمی سکونت | مستاجر هستم | کمتر از یک سال در این مکان هستم",
        value: 5
    }, {
        label: "محل موقت سکونت | در ایران ساکن نیستم و ساکن کشور دیگری هستم",
        value: 6
    }, {
        label: "محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم",
        value: 7
    }, {
        label: "محل موقت سکونت | نه مالک و نه مستاجر هستم | به زودری تغییر مکان می‌دهم",
        value: 8
    }, {
        label: "محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم",
        value: 9
    }, {
        label: "محل دائمی کار و تجارت | مالک یا مستاجر هستم | به تازگی در این مکان هستم (کمتر از دو ماه)",
        value: 10
    }, {
        label: "محل دائمی کار و تجارت | مالک هستم | بیش از یک سال در این مکان هستم",
        value: 11
    }, {
        label: "محل دائمی کار و تجارت | مالک هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)",
        value: 12
    }, {
        label: "محل دائمی کار و تجارت | مستاجر هستم | بیش از یک سال در این مکان هستم",
        value: 13
    }, {label: "محل دائمی کار و تجارت | مستاجر هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)", value: 14}]
}

function identityTypeOptions() {
    return [{label: "حقیقی", value: "natural"}, {label: "حقوقی", value: "legal"}]
}

function genderOptions() {
    return [{label: "زن", value: "female"}, {label: "مرد", value: "male"}]
}

function childrenOptions() {
    return [{id: 0, label: "0", value: "0"}, {id: 1, label: "1", value: "1"}, {id: 2, label: "2", value: "2"}, {
        id: 3,
        label: "3",
        value: "3"
    }, {id: 4, label: "4", value: "5"}, {id: 5, label: "5", value: "5"}, {id: 6, label: "6", value: "6"}, {
        id: 7,
        label: "7",
        value: "7"
    }, {id: 8, label: "8", value: "8"}, {id: 9, label: "9", value: "9"}, {id: 10, label: "10", value: "10"}, {
        id: 11,
        label: "11",
        value: "11"
    }, {id: 12, label: "12", value: "12"}, {id: 13, label: "13", value: "13"}, {
        id: 14,
        label: "14",
        value: "14"
    }, {id: 15, label: "15", value: "15"}]
}

function provinceOptions() {
    return [{id: 3, label: "آذربایجان شرقی", value: "آذربایجان شرقی"}, {
        id: 16,
        label: "آذربایجان غربی",
        value: "آذربایجان غربی"
    }, {id: 15, label: "اردبیل", value: "اردبیل"}, {id: 6, label: "اصفهان", value: "اصفهان"}, {
        id: 31,
        label: "البرز",
        value: "البرز"
    }, {id: 27, label: "ایلام", value: "ایلام"}, {id: 21, label: "بوشهر", value: "بوشهر"}, {
        id: 1,
        label: "تهران",
        value: "تهران"
    }, {id: 24, label: "چهارمحال و بختیاری", value: "چهارمحال و بختیاری"}, {
        id: 30,
        label: "خراسان جنوبی",
        value: "خراسان جنوبی"
    }, {id: 7, label: "خراسان رضوی", value: "خراسان رضوی"}, {
        id: 29,
        label: "خراسان شمالی",
        value: "خراسان شمالی"
    }, {id: 4, label: "خوزستان", value: "خوزستان"}, {id: 12, label: "زنجان", value: "زنجان"}, {
        id: 9,
        label: "سمنان",
        value: "سمنان"
    }, {id: 26, label: "سیستان و بلوچستان", value: "سیستان و بلوچستان"}, {id: 5, label: "فارس", value: "فارس"}, {
        id: 8,
        label: "قزوین",
        value: "قزوین"
    }, {id: 10, label: "قم", value: "قم"}, {id: 18, label: "کردستان", value: "کردستان"}, {
        id: 22,
        label: "کرمان",
        value: "کرمان"
    }, {id: 19, label: "کرمانشاه", value: "کرمانشاه"}, {id: 0, label: "کشورهای خارجه", value: "کشورهای خارجه"}, {
        id: 28,
        label: "کهگیلویه و بویراحمد",
        value: "کهگیلویه و بویراحمد"
    }, {id: 14, label: "گلستان", value: "گلستان"}, {id: 2, label: "گیلان", value: "گیلان"}, {
        id: 20,
        label: "لرستان",
        value: "لرستان"
    }, {id: 13, label: "مازندران", value: "مازندران"}, {id: 11, label: "مرکزی", value: "مرکزی"}, {
        id: 23,
        label: "هرمزگان",
        value: "هرمزگان"
    }, {id: 17, label: "همدان", value: "همدان"}, {id: 25, label: "یزد", value: "یزد"}]
}

function regionTypeOptions() {
    return [{id: "1", label: "بخش", value: "district"}, {id: "2", label: "محله", value: "neighborhood"}, {
        id: "3",
        label: "دهستان",
        value: "rural district"
    }, {id: "4", label: "شهرک", value: "township"}, {id: "5", label: "منطقه شهرداری", value: "municipality region"}]
}

function roadTypeOptions() {
    return [{id: "1", label: "خیابان", value: "street"}, {id: "2", label: "کوچه", value: "alley"}, {
        id: "3",
        label: "بن بست",
        value: "dead end"
    }, {id: "4", label: "بلوار", value: "boulvard"}, {id: "5", label: " بزرگراه", value: "highway"}, {
        id: "6",
        label: "آزادراه",
        value: "freeway"
    }, {id: "7", label: "میدان", value: "square"}, {id: "8", label: "پل", value: "bridge"}, {
        id: "9",
        label: "زیرگذر",
        value: "underpass"
    }, {id: "10", label: "کنارگذر", value: "Bypass"}, {id: "11", label: "جاده", value: "road"}, {
        id: "12",
        label: "مسیر اختصاصی",
        value: "assigned route"
    }, {id: "13", label: "کیلومتر", value: "kilometer"}, {id: "14", label: "چهارراه", value: "fourway"}, {
        id: "15",
        label: "سه راه",
        value: "threeway"
    }, {id: "16", label: "دو راه", value: "dualway"}]
}

function floorOptions() {
    for (var t = [], n = 1; n < 41; n++) t.push({id: n, label: "طبقه " + n, value: n});
    return t
}

function presentDaysOptions() {
    return [{label: "شنبه", value: "SAT"}, {label: "یکشنبه", value: "SUN"}, {
        label: "دوشنبه",
        value: "MON"
    }, {label: "سه‌شنبه", value: "TUE"}, {label: "چهارشنبه", value: "WED"}, {
        label: "پنج‌شنبه",
        value: "THU"
    }, {label: "جمعه", value: "FRI"}]
}

function presentHoursOptions() {
    return [{label: "۹ الی ۱۲", value: "09-12"}, {label: "۱۲ الی ۱۵", value: "12-15"}, {
        label: "۱۵ الی ۱۸",
        value: "15-18"
    }, {label: "۱۸ الی ۲۱", value: "18-21"}]
}

function deliveryToSecuirtyOptions() {
    return [{label: "بله", value: "1"}, {label: "خیر", value: "0"}]
}

function organTypeOptions() {
    return [{label: "دولتی", value: "government"}, {label: "خصوصی", value: "private"}, {
        label: "نیمه‌خصوصی",
        value: "semiprivate"
    }]
}

function companyTypeOptions() {
    return [{label: "مسئولیت محدود", value: "limited"}, {label: "سهامی خاص", value: "private"}, {
        label: "سهامی عام",
        value: "public"
    }, {label: "غیر انتفاعی", value: "nonprofit"}]
}

function marriageOptions() {
    return [{label: "مجرد", value: "single"}, {label: "متاهل", value: "married"}, {label: "جداشده", value: "divorced"}]
}

function militaryServiceOptions() {
    return [{label: "معافیت دائم", value: "1"}, {label: "مشمول", value: "2"}, {
        label: "مشغول خدمت",
        value: "3"
    }, {label: "دارای کارت پایان خدمت", value: "4"}, {label: "غایب", value: "5"}]
}

function noHouseNumber() {
    var i = document.createElement("DIV"), t, n;
    return i.classList.add(prefex + "group-append", prefex + "form-checkbox"), t = document.createElement("LABEL"), t.classList.add(prefex + "group-text"), n = document.createElement("INPUT"), n.setAttribute("type", "checkbox"), n.name = "noHouseNumber", n.id = prefex + "noHouseNumber", t.innerHTML = n.outerHTML + '<span class="check"><\/span>بدون پلاک', i.appendChild(t), i
}

function castFieldOptions(n) {
    switch (n) {
        case"identityTypeOptions":
            return identityTypeOptions();
        case"organTypeOptions":
            return organTypeOptions();
        case"companyTypeOptions":
            return companyTypeOptions();
        case"genderOptions":
            return genderOptions();
        case"marriageOptions":
            return marriageOptions();
        case"militaryServiceOptions":
            return militaryServiceOptions();
        case"addressStatusOptions":
            return addressStatusOptions();
        case"childrenOptions":
            return childrenOptions();
        case"provinceOptions":
            return provinceOptions();
        case"regionTypeOptions":
            return regionTypeOptions();
        case"roadTypeOptions":
            return roadTypeOptions();
        case"floorOptions":
            return floorOptions();
        case"presentDaysOptions":
            return presentDaysOptions();
        case"presentHoursOptions":
            return presentHoursOptions();
        case"deliveryToSecuirtyOptions":
            return deliveryToSecuirtyOptions();
        default:
            return null
    }
}

function castFieldEvents(n) {
    switch (n) {
        case"IdentityType":
            return [{
                event: "myformready", handler: onIdentityTypeInserted, get target() {
                    return parentId
                }
            }];
        case"Gender":
            return [{
                event: "myformready", handler: onGenderTypeInserted, get target() {
                    return parentId
                }
            }];
        case"Marriage":
            return [{
                event: "myformready", handler: onMarriageTypeInserted, get target() {
                    return parentId
                }
            }];
        case"NationalID":

            return [{event: "validation", handler: tempNationalIdValidation}];
        case"NationalIDCode":
            return [{event: "validation", handler: nationalIDCodeValidation}];
        case"PassportNumber":
            return [{event: "validation", handler: passportNumberValidation}];
        case"FirstName":
        case"CompanyName":
        case"LastName":
        case"SpouseName":
        case"FatherName":
        case"RegIssuePlace":
        case"DirectorFirstName":
        case"DirectorLastName":
        case"AgentFirstName":
        case"AgentLastName":
        case"ExtraDeliveryNotes":
        case"DoorColor":
        case"BuildingName":
        case"SecondaryRoadName":
        case"PrimaryRoadName":
        case"MainRoadName":
        case"RegionName":
            return [{event: "validation", handler: persianRegexValidation}];
        case"CellNumber":
            return [{event: "validation", handler: phoneValidation}];
        case"AgentCellNumber":
            return [{event: "validation", handler: phoneValidation}];
        case"Email":
            return [{event: "validation", handler: emailValidation}];
        case"AgentEmail":
            return [{event: "validation", handler: emailValidation}];
        case"PREmail":
            return [{event: "validation", handler: emailValidation}];
        case"PostCode":
            return [{event: "validation", handler: postCodeValidation}];
        case"Province":
            return [{event: "change", handler: provinceChanged}];
        case"children":
            return [{event: "change", handler: childrenChanged}];
        case"RegionName":
            return [{event: "validation", handler: regionValidation}];
        case"MainRoadName":
            return [{event: "validation", handler: roadValidation}];
        case"PrimaryRoadName":
            return [{event: "validation", handler: roadValidation}];
        case"SecondaryRoadName":
            return [{event: "validation", handler: roadValidation}];
        case"HouseNumber":
            return [{event: "validation", handler: houseNumberValidation}, {
                event: "change",
                target: prefex + "noHouseNumber",
                handler: onNoHouseNumberChange
            }];
        case"Unit":
            return [{event: "validation", handler: unitValidation}];
        case"BuildingName":
            return [{event: "validation", handler: buildingNameValidation}];
        default:
            return null
    }
}

function castFieldAppends(n) {
    switch (n) {
        case"HouseNumber":
            return noHouseNumber();
        case"SelfPortraitImage":
            return selfPortraitDescription();
        default:
            return null
    }
}

function setDatePicker() {
    var r, n, f, i, t, u;
    for (r in fields) {
        if (n = fields[r], n.type == "date") t = document.getElementById(prefex + n.id), t != undefined && (t.setAttribute("class", prefex + "input"), f = new Pikaday({
            field: document.getElementById(prefex + n.id),
            firstDay: 1,
            minDate: new Date(1890, 12, 31),
            maxDate: new Date,
            yearRange: [1890, 2020]
        })); else if (n.type == "masktext") for (i = 0; i < n.maxLength; i++) t = document.getElementById(prefex + n.id + i), t != undefined && t != null && (u = function (n, t) {
            try {
                var i = document.getElementById(prefex + n.id + "-wrapper");
                masktextInputValidation(t, i, n, !1)
            } catch ({}) {
                t.addEventListener("keypress", n => n.keyCode >= 48 && n.keyCode <= 57 || n.keyCode >= 96 && n.keyCode <= 105 ? (n.target.value = n.key, n.target.nextElementSibling.focus(), !1) : !1);
                t.addEventListener("keypress", u.bind(null, n, t));
                t.addEventListener("keydown", n => {
                    n.keyCode == 8 && (n.target.value = "", n.target.previousElementSibling.focus())
                })
            }
        });


    }
}
function startRegister() {
            const n = new XMLHttpRequest;
            n.open("POST", "http://melrio.com/api/v1/startRegister", !0);
            n.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
            n.onreadystatechange = function () {
                var t, i;
                n.readyState == 4 && n.status == 200 && (t = readBody(n), t != undefined && t != null && (i = JSON.parse(t), formId = i.id, generateImageOPT()))
            };
            n.send("form_token=" + form_token)
        }

var isDate, Data, proto, fields;
(function () {
    function n(t, i, r) {
        function u(f, o) {
            var h, c, s;
            if (!i[f]) {
                if (!t[f]) {
                    if (h = "function" == typeof require && require, !o && h) return h(f, !0);
                    if (e) return e(f, !0);
                    c = new Error("Cannot find module '" + f + "'");
                    throw c.code = "MODULE_NOT_FOUND", c;
                }
                s = i[f] = {exports: {}};
                t[f][0].call(s.exports, function (n) {
                    var i = t[f][1][n];
                    return u(i || n)
                }, s, s.exports, n, t, i, r)
            }
            return i[f].exports
        }

        for (var e = "function" == typeof require && require, f = 0; f < r.length; f++) u(r[f]);
        return u
    }

    return n
})()({
    1: [function (n, t) {
        function i(n, t, i) {
            function s(n) {
                return n >= 200 && n < 300 || 304 === n
            }

            function e() {
                void 0 === r.status || s(r.status) ? t.call(r, null, r) : t.call(r, r, null)
            }

            var o = !1, u, r, f;
            return void 0 === window.XMLHttpRequest ? t(Error("Browser not supported")) : (void 0 === i && (u = n.match(/^\s*https?:\/\/[^\/]*/), i = u && u[0] !== location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "")), r = new window.XMLHttpRequest, !i || "withCredentials" in r || (r = new window.XDomainRequest, f = t, t = function () {
                if (o) f.apply(this, arguments); else {
                    var n = this, t = arguments;
                    setTimeout(function () {
                        f.apply(n, t)
                    }, 0)
                }
            }), "onload" in r ? r.onload = e : r.onreadystatechange = function () {
                4 === r.readyState && e()
            }, r.onerror = function (n) {
                t.call(this, n || !0, null);
                t = function () {
                }
            }, r.onprogress = function () {
            }, r.ontimeout = function (n) {
                t.call(this, n, null);
                t = function () {
                }
            }, r.onabort = function (n) {
                t.call(this, n, null);
                t = function () {
                }
            }, r.open("GET", n, !0), r.send(null), o = !0, r)
        }

        "undefined" != typeof t && (t.exports = i)
    }, {}], 2: [function (n, t) {
        function i(n) {
            "use strict";
            return /^https?/.test(n.getScheme()) ? n.toString() : /^mailto?/.test(n.getScheme()) ? n.toString() : "data" == n.getScheme() && /^image/.test(n.getPath()) ? n.toString() : void 0
        }

        function r(n) {
            return n
        }

        var u = n("./sanitizer-bundle.js");
        t.exports = function (n) {
            return n ? u(n, i, r) : ""
        }
    }, {"./sanitizer-bundle.js": 3}], 3: [function (n, t) {
        var f = function () {
            function u(i) {
                var r = ("" + i).match(p);
                return r ? new n(t(r[1]), t(r[2]), t(r[3]), t(r[4]), t(r[5]), t(r[6]), t(r[7])) : null
            }

            function l(t, r, u, f, h, l, a) {
                var v = new n(i(t, e), i(r, e), o(u), f > 0 ? f.toString() : null, i(h, c), null, o(a));
                return l && ("string" == typeof l ? v.setRawQuery(l.replace(/[^?&=0-9A-Za-z_\-~.%]/g, s)) : v.setAllParameters(l)), v
            }

            function o(n) {
                return "string" == typeof n ? encodeURIComponent(n) : null
            }

            function i(n, t) {
                return "string" == typeof n ? encodeURI(n).replace(t, s) : null
            }

            function s(n) {
                var t = n.charCodeAt(0);
                return "%" + "0123456789ABCDEF".charAt(t >> 4 & 15) + "0123456789ABCDEF".charAt(15 & t)
            }

            function a(n) {
                return n.replace(/(^|\/)\.(?:\/|$)/g, "$1").replace(/\/{2,}/g, "/")
            }

            function r(n) {
                if (null === n) return null;
                for (var i, t = a(n), r = y; (i = t.replace(r, "$1")) != t; t = i) ;
                return t
            }

            function h(n, t) {
                var e = n.clone(), i = t.hasScheme(), o, u, s, h;
                return i ? e.setRawScheme(t.getRawScheme()) : i = t.hasCredentials(), i ? e.setRawCredentials(t.getRawCredentials()) : i = t.hasDomain(), i ? e.setRawDomain(t.getRawDomain()) : i = t.hasPort(), o = t.getRawPath(), u = r(o), i ? (e.setPort(t.getPort()), u = u && u.replace(f, "")) : (i = !!o) ? 47 !== u.charCodeAt(0) && (s = r(e.getRawPath() || "").replace(f, ""), h = s.lastIndexOf("/") + 1, u = r((h ? s.substring(0, h) : "") + r(o)).replace(f, "")) : (u = u && u.replace(f, "")) !== o && e.setRawPath(u), i ? e.setRawPath(u) : i = t.hasQuery(), i ? e.setRawQuery(t.getRawQuery()) : i = t.hasFragment(), i && e.setRawFragment(t.getRawFragment()), e
            }

            function n(n, t, i, r, u, f, e) {
                this.scheme_ = n;
                this.credentials_ = t;
                this.domain_ = i;
                this.port_ = r;
                this.path_ = u;
                this.query_ = f;
                this.fragment_ = e;
                this.paramCache_ = null
            }

            function t(n) {
                return "string" == typeof n && n.length > 0 ? n : null
            }

            var v = new RegExp("(/|^)(?:[^./][^/]*|\\.{2,}(?:[^./][^/]*)|\\.{3,}[^/]*)/\\.\\.(?:/|$)"),
                y = new RegExp(v), f = /^(?:\.\.\/)*(?:\.\.$)?/;
            n.prototype.toString = function () {
                var n = [];
                return null !== this.scheme_ && n.push(this.scheme_, ":"), null !== this.domain_ && (n.push("//"), null !== this.credentials_ && n.push(this.credentials_, "@"), n.push(this.domain_), null !== this.port_ && n.push(":", this.port_.toString())), null !== this.path_ && n.push(this.path_), null !== this.query_ && n.push("?", this.query_), null !== this.fragment_ && n.push("#", this.fragment_), n.join("")
            };
            n.prototype.clone = function () {
                return new n(this.scheme_, this.credentials_, this.domain_, this.port_, this.path_, this.query_, this.fragment_)
            };
            n.prototype.getScheme = function () {
                return this.scheme_ && decodeURIComponent(this.scheme_).toLowerCase()
            };
            n.prototype.getRawScheme = function () {
                return this.scheme_
            };
            n.prototype.setScheme = function (n) {
                return this.scheme_ = i(n, e), this
            };
            n.prototype.setRawScheme = function (n) {
                return this.scheme_ = n || null, this
            };
            n.prototype.hasScheme = function () {
                return null !== this.scheme_
            };
            n.prototype.getCredentials = function () {
                return this.credentials_ && decodeURIComponent(this.credentials_)
            };
            n.prototype.getRawCredentials = function () {
                return this.credentials_
            };
            n.prototype.setCredentials = function (n) {
                return this.credentials_ = i(n, e), this
            };
            n.prototype.setRawCredentials = function (n) {
                return this.credentials_ = n || null, this
            };
            n.prototype.hasCredentials = function () {
                return null !== this.credentials_
            };
            n.prototype.getDomain = function () {
                return this.domain_ && decodeURIComponent(this.domain_)
            };
            n.prototype.getRawDomain = function () {
                return this.domain_
            };
            n.prototype.setDomain = function (n) {
                return this.setRawDomain(n && encodeURIComponent(n))
            };
            n.prototype.setRawDomain = function (n) {
                return this.domain_ = n || null, this.setRawPath(this.path_)
            };
            n.prototype.hasDomain = function () {
                return null !== this.domain_
            };
            n.prototype.getPort = function () {
                return this.port_ && decodeURIComponent(this.port_)
            };
            n.prototype.setPort = function (n) {
                if (n) {
                    if ((n = Number(n)) !== (65535 & n)) throw new Error("Bad port number " + n);
                    this.port_ = "" + n
                } else this.port_ = null;
                return this
            };
            n.prototype.hasPort = function () {
                return null !== this.port_
            };
            n.prototype.getPath = function () {
                return this.path_ && decodeURIComponent(this.path_)
            };
            n.prototype.getRawPath = function () {
                return this.path_
            };
            n.prototype.setPath = function (n) {
                return this.setRawPath(i(n, c))
            };
            n.prototype.setRawPath = function (n) {
                return n ? (n = String(n), this.path_ = !this.domain_ || /^\//.test(n) ? n : "/" + n) : this.path_ = null, this
            };
            n.prototype.hasPath = function () {
                return null !== this.path_
            };
            n.prototype.getQuery = function () {
                return this.query_ && decodeURIComponent(this.query_).replace(/\+/g, " ")
            };
            n.prototype.getRawQuery = function () {
                return this.query_
            };
            n.prototype.setQuery = function (n) {
                return this.paramCache_ = null, this.query_ = o(n), this
            };
            n.prototype.setRawQuery = function (n) {
                return this.paramCache_ = null, this.query_ = n || null, this
            };
            n.prototype.hasQuery = function () {
                return null !== this.query_
            };
            n.prototype.setAllParameters = function (n) {
                var r, u, i, t;
                if ("object" == typeof n && !(n instanceof Array) && (n instanceof Object || "[object Array]" !== Object.prototype.toString.call(n))) {
                    r = [];
                    u = -1;
                    for (i in n) t = n[i], "string" == typeof t && (r[++u] = i, r[++u] = t);
                    n = r
                }
                this.paramCache_ = null;
                for (var f = [], o = "", e = 0; e < n.length;) i = n[e++], t = n[e++], f.push(o, encodeURIComponent(i.toString())), o = "&", t && f.push("=", encodeURIComponent(t.toString()));
                return this.query_ = f.join(""), this
            };
            n.prototype.checkParameterCache_ = function () {
                var n, r;
                if (!this.paramCache_) if (n = this.query_, n) {
                    for (var u = n.split(/[&\?]/), t = [], f = -1, i = 0; i < u.length; ++i) r = u[i].match(/^([^=]*)(?:=(.*))?$/), t[++f] = decodeURIComponent(r[1]).replace(/\+/g, " "), t[++f] = decodeURIComponent(r[2] || "").replace(/\+/g, " ");
                    this.paramCache_ = t
                } else this.paramCache_ = []
            };
            n.prototype.setParameterValues = function (n, t) {
                "string" == typeof t && (t = [t]);
                this.checkParameterCache_();
                for (var r = 0, u = this.paramCache_, f = [], i = 0; i < u.length; i += 2) n === u[i] ? r < t.length && f.push(n, t[r++]) : f.push(u[i], u[i + 1]);
                for (; r < t.length;) f.push(n, t[r++]);
                return this.setAllParameters(f), this
            };
            n.prototype.removeParameter = function (n) {
                return this.setParameterValues(n, [])
            };
            n.prototype.getAllParameters = function () {
                return this.checkParameterCache_(), this.paramCache_.slice(0, this.paramCache_.length)
            };
            n.prototype.getParameterValues = function (n) {
                this.checkParameterCache_();
                for (var i = [], t = 0; t < this.paramCache_.length; t += 2) n === this.paramCache_[t] && i.push(this.paramCache_[t + 1]);
                return i
            };
            n.prototype.getParameterMap = function () {
                var n, t, i, r;
                for (this.checkParameterCache_(), n = {}, t = 0; t < this.paramCache_.length; t += 2) i = this.paramCache_[t++], r = this.paramCache_[t++], i in n ? n[i].push(r) : n[i] = [r];
                return n
            };
            n.prototype.getParameterValue = function (n) {
                this.checkParameterCache_();
                for (var t = 0; t < this.paramCache_.length; t += 2) if (n === this.paramCache_[t]) return this.paramCache_[t + 1];
                return null
            };
            n.prototype.getFragment = function () {
                return this.fragment_ && decodeURIComponent(this.fragment_)
            };
            n.prototype.getRawFragment = function () {
                return this.fragment_
            };
            n.prototype.setFragment = function (n) {
                return this.fragment_ = n ? encodeURIComponent(n) : null, this
            };
            n.prototype.setRawFragment = function (n) {
                return this.fragment_ = n || null, this
            };
            n.prototype.hasFragment = function () {
                return null !== this.fragment_
            };
            var p = new RegExp("^(?:([^:/?#]+):)?(?://(?:([^/?#]*)@)?([^/?#:@]*)(?::([0-9]+))?)?([^?#]+)?(?:\\?([^#]*))?(?:#(.*))?$"),
                e = /[#\/\?@]/g, c = /[\#\?]/g;
            return n.parse = u, n.create = l, n.resolve = h, n.collapse_dots = r, n.utils = {
                mimeTypeOf: function (n) {
                    return /\.html$/.test(u(n).getPath()) ? "text/html" : "application/javascript"
                }, resolve: function (n, t) {
                    return n ? h(u(n), u(t)).toString() : "" + t
                }
            }, n
        }(), i = {}, r, u;
        if (i.atype = {
            NONE: 0,
            URI: 1,
            URI_FRAGMENT: 11,
            SCRIPT: 2,
            STYLE: 3,
            HTML: 12,
            ID: 4,
            IDREF: 5,
            IDREFS: 6,
            GLOBAL_NAME: 7,
            LOCAL_NAME: 8,
            CLASSES: 9,
            FRAME_TARGET: 10,
            MEDIA_QUERY: 13
        }, i.atype = i.atype, i.ATTRIBS = {
            "*::class": 9,
            "*::dir": 0,
            "*::draggable": 0,
            "*::hidden": 0,
            "*::id": 4,
            "*::inert": 0,
            "*::itemprop": 0,
            "*::itemref": 6,
            "*::itemscope": 0,
            "*::lang": 0,
            "*::onblur": 2,
            "*::onchange": 2,
            "*::onclick": 2,
            "*::ondblclick": 2,
            "*::onfocus": 2,
            "*::onkeydown": 2,
            "*::onkeypress": 2,
            "*::onkeyup": 2,
            "*::onload": 2,
            "*::onmousedown": 2,
            "*::onmousemove": 2,
            "*::onmouseout": 2,
            "*::onmouseover": 2,
            "*::onmouseup": 2,
            "*::onreset": 2,
            "*::onscroll": 2,
            "*::onselect": 2,
            "*::onsubmit": 2,
            "*::onunload": 2,
            "*::spellcheck": 0,
            "*::style": 3,
            "*::title": 0,
            "*::translate": 0,
            "a::accesskey": 0,
            "a::coords": 0,
            "a::href": 1,
            "a::hreflang": 0,
            "a::name": 7,
            "a::onblur": 2,
            "a::onfocus": 2,
            "a::shape": 0,
            "a::tabindex": 0,
            "a::target": 10,
            "a::type": 0,
            "area::accesskey": 0,
            "area::alt": 0,
            "area::coords": 0,
            "area::href": 1,
            "area::nohref": 0,
            "area::onblur": 2,
            "area::onfocus": 2,
            "area::shape": 0,
            "area::tabindex": 0,
            "area::target": 10,
            "audio::controls": 0,
            "audio::loop": 0,
            "audio::mediagroup": 5,
            "audio::muted": 0,
            "audio::preload": 0,
            "bdo::dir": 0,
            "blockquote::cite": 1,
            "br::clear": 0,
            "button::accesskey": 0,
            "button::disabled": 0,
            "button::name": 8,
            "button::onblur": 2,
            "button::onfocus": 2,
            "button::tabindex": 0,
            "button::type": 0,
            "button::value": 0,
            "canvas::height": 0,
            "canvas::width": 0,
            "caption::align": 0,
            "col::align": 0,
            "col::char": 0,
            "col::charoff": 0,
            "col::span": 0,
            "col::valign": 0,
            "col::width": 0,
            "colgroup::align": 0,
            "colgroup::char": 0,
            "colgroup::charoff": 0,
            "colgroup::span": 0,
            "colgroup::valign": 0,
            "colgroup::width": 0,
            "command::checked": 0,
            "command::command": 5,
            "command::disabled": 0,
            "command::icon": 1,
            "command::label": 0,
            "command::radiogroup": 0,
            "command::type": 0,
            "data::value": 0,
            "del::cite": 1,
            "del::datetime": 0,
            "details::open": 0,
            "dir::compact": 0,
            "div::align": 0,
            "dl::compact": 0,
            "fieldset::disabled": 0,
            "font::color": 0,
            "font::face": 0,
            "font::size": 0,
            "form::accept": 0,
            "form::action": 1,
            "form::autocomplete": 0,
            "form::enctype": 0,
            "form::method": 0,
            "form::name": 7,
            "form::novalidate": 0,
            "form::onreset": 2,
            "form::onsubmit": 2,
            "form::target": 10,
            "h1::align": 0,
            "h2::align": 0,
            "h3::align": 0,
            "h4::align": 0,
            "h5::align": 0,
            "h6::align": 0,
            "hr::align": 0,
            "hr::noshade": 0,
            "hr::size": 0,
            "hr::width": 0,
            "iframe::align": 0,
            "iframe::frameborder": 0,
            "iframe::height": 0,
            "iframe::marginheight": 0,
            "iframe::marginwidth": 0,
            "iframe::width": 0,
            "img::align": 0,
            "img::alt": 0,
            "img::border": 0,
            "img::height": 0,
            "img::hspace": 0,
            "img::ismap": 0,
            "img::name": 7,
            "img::src": 1,
            "img::usemap": 11,
            "img::vspace": 0,
            "img::width": 0,
            "input::accept": 0,
            "input::accesskey": 0,
            "input::align": 0,
            "input::alt": 0,
            "input::autocomplete": 0,
            "input::checked": 0,
            "input::disabled": 0,
            "input::inputmode": 0,
            "input::ismap": 0,
            "input::list": 5,
            "input::max": 0,
            "input::maxlength": 0,
            "input::min": 0,
            "input::multiple": 0,
            "input::name": 8,
            "input::onblur": 2,
            "input::onchange": 2,
            "input::onfocus": 2,
            "input::onselect": 2,
            "input::placeholder": 0,
            "input::readonly": 0,
            "input::required": 0,
            "input::size": 0,
            "input::src": 1,
            "input::step": 0,
            "input::tabindex": 0,
            "input::type": 0,
            "input::usemap": 11,
            "input::value": 0,
            "ins::cite": 1,
            "ins::datetime": 0,
            "label::accesskey": 0,
            "label::for": 5,
            "label::onblur": 2,
            "label::onfocus": 2,
            "legend::accesskey": 0,
            "legend::align": 0,
            "li::type": 0,
            "li::value": 0,
            "map::name": 7,
            "menu::compact": 0,
            "menu::label": 0,
            "menu::type": 0,
            "meter::high": 0,
            "meter::low": 0,
            "meter::max": 0,
            "meter::min": 0,
            "meter::value": 0,
            "ol::compact": 0,
            "ol::reversed": 0,
            "ol::start": 0,
            "ol::type": 0,
            "optgroup::disabled": 0,
            "optgroup::label": 0,
            "option::disabled": 0,
            "option::label": 0,
            "option::selected": 0,
            "option::value": 0,
            "output::for": 6,
            "output::name": 8,
            "p::align": 0,
            "pre::width": 0,
            "progress::max": 0,
            "progress::min": 0,
            "progress::value": 0,
            "q::cite": 1,
            "select::autocomplete": 0,
            "select::disabled": 0,
            "select::multiple": 0,
            "select::name": 8,
            "select::onblur": 2,
            "select::onchange": 2,
            "select::onfocus": 2,
            "select::required": 0,
            "select::size": 0,
            "select::tabindex": 0,
            "source::type": 0,
            "table::align": 0,
            "table::bgcolor": 0,
            "table::border": 0,
            "table::cellpadding": 0,
            "table::cellspacing": 0,
            "table::frame": 0,
            "table::rules": 0,
            "table::summary": 0,
            "table::width": 0,
            "tbody::align": 0,
            "tbody::char": 0,
            "tbody::charoff": 0,
            "tbody::valign": 0,
            "td::abbr": 0,
            "td::align": 0,
            "td::axis": 0,
            "td::bgcolor": 0,
            "td::char": 0,
            "td::charoff": 0,
            "td::colspan": 0,
            "td::headers": 6,
            "td::height": 0,
            "td::nowrap": 0,
            "td::rowspan": 0,
            "td::scope": 0,
            "td::valign": 0,
            "td::width": 0,
            "textarea::accesskey": 0,
            "textarea::autocomplete": 0,
            "textarea::cols": 0,
            "textarea::disabled": 0,
            "textarea::inputmode": 0,
            "textarea::name": 8,
            "textarea::onblur": 2,
            "textarea::onchange": 2,
            "textarea::onfocus": 2,
            "textarea::onselect": 2,
            "textarea::placeholder": 0,
            "textarea::readonly": 0,
            "textarea::required": 0,
            "textarea::rows": 0,
            "textarea::tabindex": 0,
            "textarea::wrap": 0,
            "tfoot::align": 0,
            "tfoot::char": 0,
            "tfoot::charoff": 0,
            "tfoot::valign": 0,
            "th::abbr": 0,
            "th::align": 0,
            "th::axis": 0,
            "th::bgcolor": 0,
            "th::char": 0,
            "th::charoff": 0,
            "th::colspan": 0,
            "th::headers": 6,
            "th::height": 0,
            "th::nowrap": 0,
            "th::rowspan": 0,
            "th::scope": 0,
            "th::valign": 0,
            "th::width": 0,
            "thead::align": 0,
            "thead::char": 0,
            "thead::charoff": 0,
            "thead::valign": 0,
            "tr::align": 0,
            "tr::bgcolor": 0,
            "tr::char": 0,
            "tr::charoff": 0,
            "tr::valign": 0,
            "track::default": 0,
            "track::kind": 0,
            "track::label": 0,
            "track::srclang": 0,
            "ul::compact": 0,
            "ul::type": 0,
            "video::controls": 0,
            "video::height": 0,
            "video::loop": 0,
            "video::mediagroup": 5,
            "video::muted": 0,
            "video::poster": 1,
            "video::preload": 0,
            "video::width": 0
        }, i.ATTRIBS = i.ATTRIBS, i.eflags = {
            OPTIONAL_ENDTAG: 1,
            EMPTY: 2,
            CDATA: 4,
            RCDATA: 8,
            UNSAFE: 16,
            FOLDABLE: 32,
            SCRIPT: 64,
            STYLE: 128,
            VIRTUALIZED: 256
        }, i.eflags = i.eflags, i.ELEMENTS = {
            a: 0,
            abbr: 0,
            acronym: 0,
            address: 0,
            applet: 272,
            area: 2,
            article: 0,
            aside: 0,
            audio: 0,
            b: 0,
            base: 274,
            basefont: 274,
            bdi: 0,
            bdo: 0,
            big: 0,
            blockquote: 0,
            body: 305,
            br: 2,
            button: 0,
            canvas: 0,
            caption: 0,
            center: 0,
            cite: 0,
            code: 0,
            col: 2,
            colgroup: 1,
            command: 2,
            data: 0,
            datalist: 0,
            dd: 1,
            del: 0,
            details: 0,
            dfn: 0,
            dialog: 272,
            dir: 0,
            div: 0,
            dl: 0,
            dt: 1,
            em: 0,
            fieldset: 0,
            figcaption: 0,
            figure: 0,
            font: 0,
            footer: 0,
            form: 0,
            frame: 274,
            frameset: 272,
            h1: 0,
            h2: 0,
            h3: 0,
            h4: 0,
            h5: 0,
            h6: 0,
            head: 305,
            header: 0,
            hgroup: 0,
            hr: 2,
            html: 305,
            i: 0,
            iframe: 16,
            img: 2,
            input: 2,
            ins: 0,
            isindex: 274,
            kbd: 0,
            keygen: 274,
            label: 0,
            legend: 0,
            li: 1,
            link: 274,
            map: 0,
            mark: 0,
            menu: 0,
            meta: 274,
            meter: 0,
            nav: 0,
            nobr: 0,
            noembed: 276,
            noframes: 276,
            noscript: 276,
            object: 272,
            ol: 0,
            optgroup: 0,
            option: 1,
            output: 0,
            p: 1,
            param: 274,
            pre: 0,
            progress: 0,
            q: 0,
            s: 0,
            samp: 0,
            script: 84,
            section: 0,
            select: 0,
            small: 0,
            source: 2,
            span: 0,
            strike: 0,
            strong: 0,
            style: 148,
            sub: 0,
            summary: 0,
            sup: 0,
            table: 0,
            tbody: 1,
            td: 1,
            textarea: 8,
            tfoot: 1,
            th: 1,
            thead: 1,
            time: 0,
            title: 280,
            tr: 1,
            track: 2,
            tt: 0,
            u: 0,
            ul: 0,
            "var": 0,
            video: 0,
            wbr: 2
        }, i.ELEMENTS = i.ELEMENTS, i.ELEMENT_DOM_INTERFACES = {
            a: "HTMLAnchorElement",
            abbr: "HTMLElement",
            acronym: "HTMLElement",
            address: "HTMLElement",
            applet: "HTMLAppletElement",
            area: "HTMLAreaElement",
            article: "HTMLElement",
            aside: "HTMLElement",
            audio: "HTMLAudioElement",
            b: "HTMLElement",
            base: "HTMLBaseElement",
            basefont: "HTMLBaseFontElement",
            bdi: "HTMLElement",
            bdo: "HTMLElement",
            big: "HTMLElement",
            blockquote: "HTMLQuoteElement",
            body: "HTMLBodyElement",
            br: "HTMLBRElement",
            button: "HTMLButtonElement",
            canvas: "HTMLCanvasElement",
            caption: "HTMLTableCaptionElement",
            center: "HTMLElement",
            cite: "HTMLElement",
            code: "HTMLElement",
            col: "HTMLTableColElement",
            colgroup: "HTMLTableColElement",
            command: "HTMLCommandElement",
            data: "HTMLElement",
            datalist: "HTMLDataListElement",
            dd: "HTMLElement",
            del: "HTMLModElement",
            details: "HTMLDetailsElement",
            dfn: "HTMLElement",
            dialog: "HTMLDialogElement",
            dir: "HTMLDirectoryElement",
            div: "HTMLDivElement",
            dl: "HTMLDListElement",
            dt: "HTMLElement",
            em: "HTMLElement",
            fieldset: "HTMLFieldSetElement",
            figcaption: "HTMLElement",
            figure: "HTMLElement",
            font: "HTMLFontElement",
            footer: "HTMLElement",
            form: "HTMLFormElement",
            frame: "HTMLFrameElement",
            frameset: "HTMLFrameSetElement",
            h1: "HTMLHeadingElement",
            h2: "HTMLHeadingElement",
            h3: "HTMLHeadingElement",
            h4: "HTMLHeadingElement",
            h5: "HTMLHeadingElement",
            h6: "HTMLHeadingElement",
            head: "HTMLHeadElement",
            header: "HTMLElement",
            hgroup: "HTMLElement",
            hr: "HTMLHRElement",
            html: "HTMLHtmlElement",
            i: "HTMLElement",
            iframe: "HTMLIFrameElement",
            img: "HTMLImageElement",
            input: "HTMLInputElement",
            ins: "HTMLModElement",
            isindex: "HTMLUnknownElement",
            kbd: "HTMLElement",
            keygen: "HTMLKeygenElement",
            label: "HTMLLabelElement",
            legend: "HTMLLegendElement",
            li: "HTMLLIElement",
            link: "HTMLLinkElement",
            map: "HTMLMapElement",
            mark: "HTMLElement",
            menu: "HTMLMenuElement",
            meta: "HTMLMetaElement",
            meter: "HTMLMeterElement",
            nav: "HTMLElement",
            nobr: "HTMLElement",
            noembed: "HTMLElement",
            noframes: "HTMLElement",
            noscript: "HTMLElement",
            object: "HTMLObjectElement",
            ol: "HTMLOListElement",
            optgroup: "HTMLOptGroupElement",
            option: "HTMLOptionElement",
            output: "HTMLOutputElement",
            p: "HTMLParagraphElement",
            param: "HTMLParamElement",
            pre: "HTMLPreElement",
            progress: "HTMLProgressElement",
            q: "HTMLQuoteElement",
            s: "HTMLElement",
            samp: "HTMLElement",
            script: "HTMLScriptElement",
            section: "HTMLElement",
            select: "HTMLSelectElement",
            small: "HTMLElement",
            source: "HTMLSourceElement",
            span: "HTMLSpanElement",
            strike: "HTMLElement",
            strong: "HTMLElement",
            style: "HTMLStyleElement",
            sub: "HTMLElement",
            summary: "HTMLElement",
            sup: "HTMLElement",
            table: "HTMLTableElement",
            tbody: "HTMLTableSectionElement",
            td: "HTMLTableDataCellElement",
            textarea: "HTMLTextAreaElement",
            tfoot: "HTMLTableSectionElement",
            th: "HTMLTableHeaderCellElement",
            thead: "HTMLTableSectionElement",
            time: "HTMLTimeElement",
            title: "HTMLTitleElement",
            tr: "HTMLTableRowElement",
            track: "HTMLTrackElement",
            tt: "HTMLElement",
            u: "HTMLElement",
            ul: "HTMLUListElement",
            "var": "HTMLElement",
            video: "HTMLVideoElement",
            wbr: "HTMLElement"
        }, i.ELEMENT_DOM_INTERFACES = i.ELEMENT_DOM_INTERFACES, i.ueffects = {
            NOT_LOADED: 0,
            SAME_DOCUMENT: 1,
            NEW_DOCUMENT: 2
        }, i.ueffects = i.ueffects, i.URIEFFECTS = {
            "a::href": 2,
            "area::href": 2,
            "blockquote::cite": 0,
            "command::icon": 1,
            "del::cite": 0,
            "form::action": 2,
            "img::src": 1,
            "input::src": 1,
            "ins::cite": 0,
            "q::cite": 0,
            "video::poster": 1
        }, i.URIEFFECTS = i.URIEFFECTS, i.ltypes = {
            UNSANDBOXED: 2,
            SANDBOXED: 1,
            DATA: 0
        }, i.ltypes = i.ltypes, i.LOADERTYPES = {
            "a::href": 2,
            "area::href": 2,
            "blockquote::cite": 2,
            "command::icon": 1,
            "del::cite": 2,
            "form::action": 2,
            "img::src": 1,
            "input::src": 1,
            "ins::cite": 2,
            "q::cite": 2,
            "video::poster": 1
        }, i.LOADERTYPES = i.LOADERTYPES, "i" !== "I".toLowerCase()) throw"I/i problem";
        r = function (n) {
            function ot(n) {
                var t, i;
                return o.hasOwnProperty(n) ? o[n] : (t = n.match(kt), t) ? String.fromCharCode(parseInt(t[1], 10)) : (t = n.match(dt)) ? String.fromCharCode(parseInt(t[1], 16)) : s && gt.test(n) ? (s.innerHTML = "&" + n + ";", i = s.textContent, o[n] = i, i) : "&" + n + ";"
            }

            function st(n, t) {
                return ot(t)
            }

            function ht(n) {
                return n.replace(ni, "")
            }

            function c(n) {
                return n.replace(ti, st)
            }

            function l(n) {
                return ("" + n).replace(ri, "&amp;").replace(ut, "&lt;").replace(ft, "&gt;").replace(fi, "&#34;")
            }

            function a(n) {
                return n.replace(ui, "&amp;$1").replace(ut, "&lt;").replace(ft, "&gt;")
            }

            function v(n) {
                var t = {
                    cdata: n.cdata || n.cdata,
                    comment: n.comment || n.comment,
                    endDoc: n.endDoc || n.endDoc,
                    endTag: n.endTag || n.endTag,
                    pcdata: n.pcdata || n.pcdata,
                    rcdata: n.rcdata || n.rcdata,
                    startDoc: n.startDoc || n.startDoc,
                    startTag: n.startTag || n.startTag
                };
                return function (n, i) {
                    return ct(n, t, i)
                }
            }

            function ct(n, t, i) {
                y(t, lt(n), 0, {noMoreGT: !1, noMoreEndComments: !1}, i)
            }

            function i(n, t, i, r, u) {
                return function () {
                    y(n, t, i, r, u)
                }
            }

            function y(t, u, f, e, o) {
                var a, h, v, s, c, y, l, w, k, b;
                try {
                    for (t.startDoc && 0 == f && t.startDoc(o), s = f, c = u.length; s < c;) {
                        y = u[s++];
                        l = u[s];
                        switch (y) {
                            case"&":
                                ii.test(l) ? (t.pcdata && t.pcdata("&" + l, o, r, i(t, u, s, e, o)), s++) : t.pcdata && t.pcdata("&amp;", o, r, i(t, u, s, e, o));
                                break;
                            case"<\/":
                                (a = /^([-\w:]+)[^\'\"]*/.exec(l)) ? a[0].length === l.length && ">" === u[s + 1] ? (s += 2, v = a[1].toLowerCase(), t.endTag && t.endTag(v, o, r, i(t, u, s, e, o))) : s = at(u, s, t, o, r, e) : t.pcdata && t.pcdata("&lt;/", o, r, i(t, u, s, e, o));
                                break;
                            case"<":
                                (a = /^([-\w:]+)\s*\/?/.exec(l)) ? a[0].length === l.length && ">" === u[s + 1] ? (s += 2, v = a[1].toLowerCase(), t.startTag && t.startTag(v, [], o, r, i(t, u, s, e, o)), w = n.ELEMENTS[v], w & et && (k = {
                                    name: v,
                                    next: s,
                                    eflags: w
                                }, s = p(u, k, t, o, r, e))) : s = vt(u, s, t, o, r, e) : t.pcdata && t.pcdata("&lt;", o, r, i(t, u, s, e, o));
                                break;
                            case"<!--":
                                if (!e.noMoreEndComments) {
                                    for (h = s + 1; h < c && (">" !== u[h] || !/--$/.test(u[h - 1])); h++) ;
                                    h < c ? (t.comment && (b = u.slice(s, h).join(""), t.comment(b.substr(0, b.length - 2), o, r, i(t, u, h + 1, e, o))), s = h + 1) : e.noMoreEndComments = !0
                                }
                                e.noMoreEndComments && t.pcdata && t.pcdata("&lt;!--", o, r, i(t, u, s, e, o));
                                break;
                            case"<!":
                                if (/^\w/.test(l)) {
                                    if (!e.noMoreGT) {
                                        for (h = s + 1; h < c && ">" !== u[h]; h++) ;
                                        h < c ? s = h + 1 : e.noMoreGT = !0
                                    }
                                    e.noMoreGT && t.pcdata && t.pcdata("&lt;!", o, r, i(t, u, s, e, o))
                                } else t.pcdata && t.pcdata("&lt;!", o, r, i(t, u, s, e, o));
                                break;
                            case"<?":
                                if (!e.noMoreGT) {
                                    for (h = s + 1; h < c && ">" !== u[h]; h++) ;
                                    h < c ? s = h + 1 : e.noMoreGT = !0
                                }
                                e.noMoreGT && t.pcdata && t.pcdata("&lt;?", o, r, i(t, u, s, e, o));
                                break;
                            case">":
                                t.pcdata && t.pcdata("&gt;", o, r, i(t, u, s, e, o));
                                break;
                            case"":
                                break;
                            default:
                                t.pcdata && t.pcdata(y, o, r, i(t, u, s, e, o))
                        }
                    }
                    t.endDoc && t.endDoc(o)
                } catch (n) {
                    if (n !== r) throw n;
                }
            }

            function lt(n) {
                var u = /(<\/|<\!--|<[!?]|[&<>])/g, t, i, r;
                if (n += "", oi) return n.split(u);
                for (i = [], r = 0; null !== (t = u.exec(n));) i.push(n.substring(r, t.index)), i.push(t[0]), r = t.index + t[0].length;
                return i.push(n.substring(r)), i
            }

            function at(n, t, r, u, f, e) {
                var o = w(n, t);
                return o ? (r.endTag && r.endTag(o.name, u, f, i(r, n, t, e, u)), o.next) : n.length
            }

            function vt(n, t, r, u, f, e) {
                var o = w(n, t);
                return o ? (r.startTag && r.startTag(o.name, o.attrs, u, f, i(r, n, o.next, e, u)), o.eflags & et ? p(n, o, r, u, f, e) : o.next) : n.length
            }

            function p(t, r, u, f, e, o) {
                var l = t.length, c;
                h.hasOwnProperty(r.name) || (h[r.name] = new RegExp("^" + r.name + "(?:[\\s\\/]|$)", "i"));
                for (var v = h[r.name], y = r.next, s = r.next + 1; s < l && ("<\/" !== t[s - 1] || !v.test(t[s])); s++) ;
                if (s < l && (s -= 1), c = t.slice(y, s).join(""), r.eflags & n.eflags.CDATA) u.cdata && u.cdata(c, f, e, i(u, t, s, o, f)); else {
                    if (!(r.eflags & n.eflags.RCDATA)) throw new Error("bug");
                    u.rcdata && u.rcdata(a(c), f, e, i(u, t, s, o, f))
                }
                return s
            }

            function w(t, i) {
                var r = /^([-\w:]+)/.exec(t[i]), e = {}, s, l, a;
                e.name = r[1].toLowerCase();
                e.eflags = n.ELEMENTS[e.name];
                for (var f = t[i].substr(r[0].length), u = i + 1, o = t.length; u < o && ">" !== t[u]; u++) f += t[u];
                if (!(o <= u)) {
                    for (s = []; "" !== f;) if (r = ei.exec(f)) {
                        if (r[4] && !r[5] || r[6] && !r[7]) {
                            for (var v = r[4] || r[6], h = !1, c = [f, t[u++]]; u < o; u++) {
                                if (h) {
                                    if (">" === t[u]) break
                                } else 0 <= t[u].indexOf(v) && (h = !0);
                                c.push(t[u])
                            }
                            if (o <= u) break;
                            f = c.join("");
                            continue
                        }
                        l = r[1].toLowerCase();
                        a = r[2] ? yt(r[3]) : "";
                        s.push(l, a);
                        f = f.substr(r[0].length)
                    } else f = f.replace(/^[\s\S][^a-z\s]*/, "");
                    return e.attrs = s, e.next = u + 1, e
                }
            }

            function yt(n) {
                var t = n.charCodeAt(0);
                return 34 !== t && 39 !== t || (n = n.substr(1, n.length - 2)), c(ht(n))
            }

            function b(t) {
                var i, r, u = function (n, t) {
                    r || t.push(n)
                };
                return v({
                    startDoc: function () {
                        i = [];
                        r = !1
                    }, startTag: function (u, f, e) {
                        var s, h, y, o, c, a, p, w, v;
                        if (!r && n.ELEMENTS.hasOwnProperty(u) && (s = n.ELEMENTS[u], !(s & n.eflags.FOLDABLE))) {
                            if (h = t(u, f), !h) return void (r = !(s & n.eflags.EMPTY));
                            if ("object" != typeof h) throw new Error("tagPolicy did not return object (old API?)");
                            if (!("attribs" in h)) throw new Error("tagPolicy gave no attribs");
                            for (f = h.attribs, ("tagName" in h ? (o = h.tagName, y = n.ELEMENTS[o]) : (o = u, y = s), s & n.eflags.OPTIONAL_ENDTAG) && (c = i[i.length - 1], c && c.orig === u && (c.rep !== o || u !== o) && e.push("<\/", c.rep, ">")), s & n.eflags.EMPTY || i.push({
                                orig: u,
                                rep: o
                            }), e.push("<", o), a = 0, p = f.length; a < p; a += 2) w = f[a], v = f[a + 1], null !== v && void 0 !== v && e.push(" ", w, '="', l(v), '"');
                            e.push(">");
                            s & n.eflags.EMPTY && !(y & n.eflags.EMPTY) && e.push("<\/", o, ">")
                        }
                    }, endTag: function (t, u) {
                        var e, f, o, s, h;
                        if (r) return void (r = !1);
                        if (n.ELEMENTS.hasOwnProperty(t) && (e = n.ELEMENTS[t], !(e & (n.eflags.EMPTY | n.eflags.FOLDABLE)))) {
                            if (e & n.eflags.OPTIONAL_ENDTAG) for (f = i.length; --f >= 0;) {
                                if (o = i[f].orig, o === t) break;
                                if (!(n.ELEMENTS[o] & n.eflags.OPTIONAL_ENDTAG)) return
                            } else for (f = i.length; --f >= 0 && i[f].orig !== t;) ;
                            if (f < 0) return;
                            for (s = i.length; --s > f;) h = i[s].rep, n.ELEMENTS[h] & n.eflags.OPTIONAL_ENDTAG || u.push("<\/", h, ">");
                            f < i.length && (t = i[f].rep);
                            i.length = f;
                            u.push("<\/", t, ">")
                        }
                    }, pcdata: u, rcdata: u, cdata: u, endDoc: function (n) {
                        for (; i.length; i.length--) n.push("<\/", i[i.length - 1].rep, ">")
                    }
                })
            }

            function k(n, t, i, r, u) {
                var e, o;
                if (!u) return null;
                try {
                    if (e = f.parse("" + n), e && (!e.hasScheme() || si.test(e.getScheme()))) return o = u(e, t, i, r), o ? o.toString() : null
                } catch (n) {
                    return null
                }
                return null
            }

            function u(n, t, i, r, u) {
                if (i || n(t + " removed", {change: "removed", tagName: t}), r !== u) {
                    var f = "changed";
                    r && !u ? f = "removed" : !r && u && (f = "added");
                    n(t + "." + i + " " + f, {change: f, tagName: t, attribName: i, oldValue: r, newValue: u})
                }
            }

            function d(n, t, i) {
                var r;
                return r = t + "::" + i, n.hasOwnProperty(r) ? n[r] : (r = "*::" + i, n.hasOwnProperty(r) ? n[r] : void 0)
            }

            function pt(t, i) {
                return d(n.LOADERTYPES, t, i)
            }

            function wt(t, i) {
                return d(n.URIEFFECTS, t, i)
            }

            function g(t, i, r, f, o) {
                for (var v, l = 0; l < i.length; l += 2) {
                    var a, h = i[l], s = i[l + 1], c = s, y = null;
                    if (a = t + "::" + h, (n.ATTRIBS.hasOwnProperty(a) || (a = "*::" + h, n.ATTRIBS.hasOwnProperty(a))) && (y = n.ATTRIBS[a]), null !== y) switch (y) {
                        case n.atype.NONE:
                            break;
                        case n.atype.SCRIPT:
                            s = null;
                            o && u(o, t, h, c, s);
                            break;
                        case n.atype.STYLE:
                            if (void 0 === e) {
                                s = null;
                                o && u(o, t, h, c, s);
                                break
                            }
                            v = [];
                            e(s, {
                                declaration: function (t, i) {
                                    var u = t.toLowerCase(), f = rt[u];
                                    f && (it(u, f, i, r ? function (t) {
                                        return k(t, n.ueffects.SAME_DOCUMENT, n.ltypes.SANDBOXED, {
                                            TYPE: "CSS",
                                            CSS_PROP: u
                                        }, r)
                                    } : null), v.push(t + ": " + i.join(" ")))
                                }
                            });
                            s = v.length > 0 ? v.join(" ; ") : null;
                            o && u(o, t, h, c, s);
                            break;
                        case n.atype.ID:
                        case n.atype.IDREF:
                        case n.atype.IDREFS:
                        case n.atype.GLOBAL_NAME:
                        case n.atype.LOCAL_NAME:
                        case n.atype.CLASSES:
                            s = f ? f(s) : s;
                            o && u(o, t, h, c, s);
                            break;
                        case n.atype.URI:
                            s = k(s, wt(t, h), pt(t, h), {TYPE: "MARKUP", XML_ATTR: h, XML_TAG: t}, r);
                            o && u(o, t, h, c, s);
                            break;
                        case n.atype.URI_FRAGMENT:
                            s && "#" === s.charAt(0) ? (s = s.substring(1), null !== (s = f ? f(s) : s) && void 0 !== s && (s = "#" + s)) : s = null;
                            o && u(o, t, h, c, s);
                            break;
                        default:
                            s = null;
                            o && u(o, t, h, c, s)
                    } else s = null, o && u(o, t, h, c, s);
                    i[l + 1] = s
                }
                return i
            }

            function nt(t, i, r) {
                return function (f, e) {
                    if (!(n.ELEMENTS[f] & n.eflags.UNSAFE)) return {attribs: g(f, e, t, i, r)};
                    r && u(r, f, void 0, void 0, void 0)
                }
            }

            function tt(n, t) {
                var i = [];
                return b(t)(n, i), i.join("")
            }

            function bt(n, t, i, r) {
                return tt(n, nt(t, i, r))
            }

            var e, it, rt;
            "undefined" != typeof window && (e = window.parseCssDeclarations, it = window.sanitizeCssProperty, rt = window.cssSchema);
            var o = {lt: "<", LT: "<", gt: ">", GT: ">", amp: "&", AMP: "&", quot: '"', apos: "'", nbsp: " "},
                kt = /^#(\d+)$/, dt = /^#x([0-9A-Fa-f]+)$/, gt = /^[A-Za-z][A-za-z0-9]+$/,
                s = "undefined" != typeof window && window.document ? window.document.createElement("textarea") : null,
                ni = /\0/g, ti = /&(#[0-9]+|#[xX][0-9A-Fa-f]+|\w+);/g, ii = /^(#[0-9]+|#[xX][0-9A-Fa-f]+|\w+);/,
                ri = /&/g, ui = /&([^a-z#]|#(?:[^0-9x]|x(?:[^0-9a-f]|$)|$)|$)/gi, ut = /[<]/g, ft = />/g, fi = /\"/g,
                ei = new RegExp("^\\s*([-.:\\w]+)(?:\\s*(=)\\s*((\")[^\"]*(\"|$)|(')[^']*('|$)|(?=[a-z][-\\w]*\\s*=)|[^\"'\\s]*))?", "i"),
                oi = 3 === "a,b".split(/(,)/).length, et = n.eflags.CDATA | n.eflags.RCDATA, r = {}, h = {},
                si = /^(?:https?|mailto|data)$/i, t = {};
            return t.escapeAttrib = t.escapeAttrib = l, t.makeHtmlSanitizer = t.makeHtmlSanitizer = b, t.makeSaxParser = t.makeSaxParser = v, t.makeTagPolicy = t.makeTagPolicy = nt, t.normalizeRCData = t.normalizeRCData = a, t.sanitize = t.sanitize = bt, t.sanitizeAttribs = t.sanitizeAttribs = g, t.sanitizeWithPolicy = t.sanitizeWithPolicy = tt, t.unescapeEntities = t.unescapeEntities = c, t
        }(i);
        u = r.sanitize;
        i.ATTRIBS["*::style"] = 0;
        i.ELEMENTS.style = 0;
        i.ATTRIBS["a::target"] = 0;
        i.ELEMENTS.video = 0;
        i.ATTRIBS["video::src"] = 0;
        i.ATTRIBS["video::poster"] = 0;
        i.ATTRIBS["video::controls"] = 0;
        i.ELEMENTS.audio = 0;
        i.ATTRIBS["audio::src"] = 0;
        i.ATTRIBS["video::autoplay"] = 0;
        i.ATTRIBS["video::controls"] = 0;
        "undefined" != typeof t && (t.exports = u)
    }, {}], 4: [function (n, t, i) {
        !function (n, r) {
            "object" == typeof i && "undefined" != typeof t ? r(i) : "function" == typeof define && define.amd ? define(["exports"], r) : r(n.L = {})
        }(this, function (n) {
            "use strict";

            function s(n) {
                for (var i, r, t = 1, u = arguments.length; t < u; t++) {
                    r = arguments[t];
                    for (i in r) n[i] = r[i]
                }
                return n
            }

            function c(n, t) {
                var i = Array.prototype.slice, r;
                return n.bind ? n.bind.apply(n, i.call(arguments, 1)) : (r = i.call(arguments, 2), function () {
                    return n.apply(t, r.length ? r.concat(i.call(arguments)) : arguments)
                })
            }

            function o(n) {
                return n._leaflet_id = n._leaflet_id || ++th, n._leaflet_id
            }

            function uf(n, t, i) {
                var u, r, f, e;
                return e = function () {
                    u = !1;
                    r && (f.apply(i, r), r = !1)
                }, f = function () {
                    u ? r = arguments : (n.apply(i, arguments), setTimeout(e, t), u = !0)
                }
            }

            function hr(n, t, i) {
                var f = t[1], r = t[0], u = f - r;
                return n === f && i ? n : ((n - r) % u + u) % u + r
            }

            function g() {
                return !1
            }

            function at(n, t) {
                var i = Math.pow(10, void 0 === t ? 6 : t);
                return Math.round(n * i) / i
            }

            function ff(n) {
                return n.trim ? n.trim() : n.replace(/^\s+|\s+$/g, "")
            }

            function yi(n) {
                return ff(n).split(/\s+/)
            }

            function l(n, t) {
                n.hasOwnProperty("options") || (n.options = n.options ? yu(n.options) : {});
                for (var i in t) n.options[i] = t[i];
                return n.options
            }

            function go(n, t, i) {
                var u = [];
                for (var r in n) u.push(encodeURIComponent(i ? r.toUpperCase() : r) + "=" + encodeURIComponent(n[r]));
                return (t && -1 !== t.indexOf("?") ? "&" : "?") + u.join("&")
            }

            function ns(n, t) {
                return n.replace(al, function (n, i) {
                    var r = t[i];
                    if (void 0 === r) throw new Error("No value provided for variable " + n);
                    return "function" == typeof r && (r = r(t)), r
                })
            }

            function ts(n, t) {
                for (var i = 0; i < n.length; i++) if (n[i] === t) return i;
                return -1
            }

            function ef(n) {
                return window["webkit" + n] || window["moz" + n] || window["ms" + n]
            }

            function is(n) {
                var t = +new Date, i = Math.max(0, 16 - (t - ih));
                return ih = t + i, window.setTimeout(n, i)
            }

            function d(n, t, i) {
                if (!i || re !== is) return re.call(window, c(n, t));
                n.call(t)
            }

            function nt(n) {
                n && rh.call(window, n)
            }

            function kt() {
            }

            function oc(n) {
                if ("undefined" != typeof L && L && L.Mixin) {
                    n = pt(n) ? n : [n];
                    for (var t = 0; t < n.length; t++) n[t] === L.Mixin.Events && console.warn("Deprecated include of L.Mixin.Events: this property will be removed in future releases, please inherit from L.Evented instead.", (new Error).stack)
                }
            }

            function t(n, t, i) {
                this.x = i ? Math.round(n) : n;
                this.y = i ? Math.round(t) : t
            }

            function i(n, i, r) {
                return n instanceof t ? n : pt(n) ? new t(n[0], n[1]) : void 0 === n || null === n ? n : "object" == typeof n && "x" in n && "y" in n ? new t(n.x, n.y) : new t(n, i, r)
            }

            function v(n, t) {
                if (n) for (var r = t ? [n, t] : n, i = 0, u = r.length; i < u; i++) this.extend(r[i])
            }

            function ot(n, t) {
                return !n || n instanceof v ? n : new v(n, t)
            }

            function tt(n, t) {
                if (n) for (var r = t ? [n, t] : n, i = 0, u = r.length; i < u; i++) this.extend(r[i])
            }

            function k(n, t) {
                return n instanceof tt ? n : new tt(n, t)
            }

            function h(n, t, i) {
                if (isNaN(n) || isNaN(t)) throw new Error("Invalid LatLng object: (" + n + ", " + t + ")");
                this.lat = +n;
                this.lng = +t;
                void 0 !== i && (this.alt = +i)
            }

            function y(n, t, i) {
                return n instanceof h ? n : pt(n) && "object" != typeof n[0] ? 3 === n.length ? new h(n[0], n[1], n[2]) : 2 === n.length ? new h(n[0], n[1]) : null : void 0 === n || null === n ? n : "object" == typeof n && "lat" in n ? new h(n.lat, "lng" in n ? n.lng : n.lon, n.alt) : void 0 === t ? null : new h(n, t, i)
            }

            function of(n, t, i, r) {
                if (pt(n)) return this._a = n[0], this._b = n[1], this._c = n[2], void (this._d = n[3]);
                this._a = n;
                this._b = t;
                this._c = i;
                this._d = r
            }

            function cr(n, t, i, r) {
                return new of(n, t, i, r)
            }

            function rs(n) {
                return document.createElementNS("http://www.w3.org/2000/svg", n)
            }

            function us(n, t) {
                for (var i, s, u, f, e = "", r = 0, o = n.length; r < o; r++) {
                    for (u = n[r], i = 0, s = u.length; i < s; i++) f = u[i], e += (i ? "L" : "M") + f.x + " " + f.y;
                    e += t ? bu ? "z" : "x" : ""
                }
                return e || "M0 0"
            }

            function vt(n) {
                return navigator.userAgent.toLowerCase().indexOf(n) >= 0
            }

            function sc(n, t, i, r) {
                return "touchstart" === t ? cc(n, i, r) : "touchmove" === t ? vc(n, i, r) : "touchend" === t && yc(n, i, r), this
            }

            function hc(n, t, i) {
                var r = n["_leaflet_" + t + i];
                return "touchstart" === t ? n.removeEventListener(ye, r, !1) : "touchmove" === t ? n.removeEventListener(pe, r, !1) : "touchend" === t && (n.removeEventListener(we, r, !1), n.removeEventListener(be, r, !1)), this
            }

            function cc(n, t, i) {
                var r = c(function (n) {
                    if ("mouse" !== n.pointerType && n.MSPOINTER_TYPE_MOUSE && n.pointerType !== n.MSPOINTER_TYPE_MOUSE) {
                        if (!(gl.indexOf(n.target.tagName) < 0)) return;
                        ft(n)
                    }
                    sf(n, t)
                });
                n["_leaflet_touchstart" + i] = r;
                n.addEventListener(ye, r, !1);
                ah || (document.documentElement.addEventListener(ye, lc, !0), document.documentElement.addEventListener(pe, ac, !0), document.documentElement.addEventListener(we, fs, !0), document.documentElement.addEventListener(be, fs, !0), ah = !0)
            }

            function lc(n) {
                tr[n.pointerId] = n;
                ke++
            }

            function ac(n) {
                tr[n.pointerId] && (tr[n.pointerId] = n)
            }

            function fs(n) {
                delete tr[n.pointerId];
                ke--
            }

            function sf(n, t) {
                n.touches = [];
                for (var i in tr) n.touches.push(tr[i]);
                n.changedTouches = [n];
                t(n)
            }

            function vc(n, t, i) {
                var r = function (n) {
                    (n.pointerType !== n.MSPOINTER_TYPE_MOUSE && "mouse" !== n.pointerType || 0 !== n.buttons) && sf(n, t)
                };
                n["_leaflet_touchmove" + i] = r;
                n.addEventListener(pe, r, !1)
            }

            function yc(n, t, i) {
                var r = function (n) {
                    sf(n, t)
                };
                n["_leaflet_touchend" + i] = r;
                n.addEventListener(we, r, !1);
                n.addEventListener(be, r, !1)
            }

            function es(n, t, i) {
                function f(n) {
                    var i, t, f;
                    if (st) {
                        if (!wr || "mouse" === n.pointerType) return;
                        i = ke
                    } else i = n.touches.length;
                    i > 1 || (t = Date.now(), f = t - (u || t), r = n.touches ? n.touches[0] : n, o = f > 0 && f <= s, u = t)
                }

                function e(n) {
                    if (o && !r.cancelBubble) {
                        if (st) {
                            if (!wr || "mouse" === n.pointerType) return;
                            var i, f, e = {};
                            for (f in r) i = r[f], e[f] = i && i.bind ? i.bind(r) : i;
                            r = e
                        }
                        r.type = "dblclick";
                        t(r);
                        u = null
                    }
                }

                var u, r, o = !1, s = 250;
                return n[ir + du + i] = f, n[ir + gu + i] = e, n[ir + "dblclick" + i] = t, n.addEventListener(du, f, !1), n.addEventListener(gu, e, !1), n.addEventListener("dblclick", t, !1), this
            }

            function os(n, t) {
                var i = n[ir + du + t], r = n[ir + gu + t], u = n[ir + "dblclick" + t];
                return n.removeEventListener(du, i, !1), n.removeEventListener(gu, r, !1), wr || n.removeEventListener("dblclick", u, !1), this
            }

            function ss(n) {
                return "string" == typeof n ? document.getElementById(n) : n
            }

            function lr(n, t) {
                var i = n.style[t] || n.currentStyle && n.currentStyle[t], r;
                return (!i || "auto" === i) && document.defaultView && (r = document.defaultView.getComputedStyle(n, null), i = r ? r[t] : null), "auto" === i ? null : i
            }

            function e(n, t, i) {
                var r = document.createElement(n);
                return r.className = t || "", i && i.appendChild(r), r
            }

            function a(n) {
                var t = n.parentNode;
                t && t.removeChild(n)
            }

            function hf(n) {
                for (; n.firstChild;) n.removeChild(n.firstChild)
            }

            function pi(n) {
                var t = n.parentNode;
                t && t.lastChild !== n && t.appendChild(n)
            }

            function wi(n) {
                var t = n.parentNode;
                t && t.firstChild !== n && t.insertBefore(n, t.firstChild)
            }

            function cf(n, t) {
                if (void 0 !== n.classList) return n.classList.contains(t);
                var i = fu(n);
                return i.length > 0 && new RegExp("(^|\\s)" + t + "(\\s|$)").test(i)
            }

            function r(n, t) {
                var r;
                if (void 0 !== n.classList) for (var u = yi(t), i = 0, f = u.length; i < f; i++) n.classList.add(u[i]); else cf(n, t) || (r = fu(n), lf(n, (r ? r + " " : "") + t))
            }

            function p(n, t) {
                void 0 !== n.classList ? n.classList.remove(t) : lf(n, ff((" " + fu(n) + " ").replace(" " + t + " ", " ")))
            }

            function lf(n, t) {
                void 0 === n.className.baseVal ? n.className = t : n.className.baseVal = t
            }

            function fu(n) {
                return n.correspondingElement && (n = n.correspondingElement), void 0 === n.className.baseVal ? n.className : n.className.baseVal
            }

            function ut(n, t) {
                "opacity" in n.style ? n.style.opacity = t : "filter" in n.style && pc(n, t)
            }

            function pc(n, t) {
                var i = !1, r = "DXImageTransform.Microsoft.Alpha";
                try {
                    i = n.filters.item(r)
                } catch (n) {
                    if (1 === t) return
                }
                t = Math.round(100 * t);
                i ? (i.Enabled = 100 !== t, i.Opacity = t) : n.style.filter += " progid:" + r + "(opacity=" + t + ")"
            }

            function eu(n) {
                for (var i = document.documentElement.style, t = 0; t < n.length; t++) if (n[t] in i) return n[t];
                return !1
            }

            function si(n, i, r) {
                var u = i || new t(0, 0);
                n.style[de] = (ae ? "translate(" + u.x + "px," + u.y + "px)" : "translate3d(" + u.x + "px," + u.y + "px,0)") + (r ? " scale(" + r + ")" : "")
            }

            function b(n, t) {
                n._leaflet_pos = t;
                et ? si(n, t) : (n.style.left = t.x + "px", n.style.top = t.y + "px")
            }

            function ri(n) {
                return n._leaflet_pos || new t(0, 0)
            }

            function af() {
                u(window, "dragstart", ft)
            }

            function vf() {
                w(window, "dragstart", ft)
            }

            function yf(n) {
                for (; -1 === n.tabIndex;) n = n.parentNode;
                n.style && (ou(), nf = n, ge = n.style.outline, n.style.outline = "none", u(window, "keydown", ou))
            }

            function ou() {
                nf && (nf.style.outline = ge, nf = void 0, ge = void 0, w(window, "keydown", ou))
            }

            function hs(n) {
                do n = n.parentNode; while (!(n.offsetWidth && n.offsetHeight || n === document.body));
                return n
            }

            function pf(n) {
                var t = n.getBoundingClientRect();
                return {x: t.width / n.offsetWidth || 1, y: t.height / n.offsetHeight || 1, boundingClientRect: t}
            }

            function u(n, t, i, r) {
                var f, u, e;
                if ("object" == typeof t) for (f in t) su(n, f, t[f], i); else for (t = yi(t), u = 0, e = t.length; u < e; u++) su(n, t[u], i, r);
                return this
            }

            function w(n, t, i, r) {
                var f, u, o, e;
                if ("object" == typeof t) for (f in t) wf(n, f, t[f], i); else if (t) for (t = yi(t), u = 0, o = t.length; u < o; u++) wf(n, t[u], i, r); else {
                    for (e in n[ht]) wf(n, e, n[ht][e]);
                    delete n[ht]
                }
                return this
            }

            function su(n, t, i, r) {
                var f = t + o(i) + (r ? "_" + o(r) : ""), u, e;
                if (n[ht] && n[ht][f]) return this;
                u = function (t) {
                    return i.call(r || n, t || window.event)
                };
                e = u;
                st && 0 === t.indexOf("touch") ? sc(n, t, u, f) : !wt || "dblclick" !== t || !es || st && kr ? "addEventListener" in n ? "mousewheel" === t ? n.addEventListener("onwheel" in n ? "wheel" : "mousewheel", u, !1) : "mouseenter" === t || "mouseleave" === t ? (u = function (t) {
                    t = t || window.event;
                    hu(n, t) && e(t)
                }, n.addEventListener("mouseenter" === t ? "mouseover" : "mouseout", u, !1)) : ("click" === t && gi && (u = function (n) {
                    wc(n, e)
                }), n.addEventListener(t, u, !1)) : "attachEvent" in n && n.attachEvent("on" + t, u) : es(n, u, f);
                n[ht] = n[ht] || {};
                n[ht][f] = u
            }

            function wf(n, t, i, r) {
                var u = t + o(i) + (r ? "_" + o(r) : ""), f = n[ht] && n[ht][u];
                if (!f) return this;
                st && 0 === t.indexOf("touch") ? hc(n, t, u) : !wt || "dblclick" !== t || !os || st && kr ? "removeEventListener" in n ? "mousewheel" === t ? n.removeEventListener("onwheel" in n ? "wheel" : "mousewheel", f, !1) : n.removeEventListener("mouseenter" === t ? "mouseover" : "mouseleave" === t ? "mouseout" : t, f, !1) : "detachEvent" in n && n.detachEvent("on" + t, f) : os(n, u);
                n[ht][u] = null
            }

            function hi(n) {
                return n.stopPropagation ? n.stopPropagation() : n.originalEvent ? n.originalEvent._stopped = !0 : n.cancelBubble = !0, df(n), this
            }

            function bf(n) {
                return su(n, "mousewheel", hi), this
            }

            function ar(n) {
                return u(n, "mousedown touchstart dblclick", hi), su(n, "click", kf), this
            }

            function ft(n) {
                return n.preventDefault ? n.preventDefault() : n.returnValue = !1, this
            }

            function yt(n) {
                return ft(n), hi(n), this
            }

            function cs(n, i) {
                if (!i) return new t(n.clientX, n.clientY);
                var r = pf(i), u = r.boundingClientRect;
                return new t((n.clientX - u.left) / r.x - i.clientLeft, (n.clientY - u.top) / r.y - i.clientTop)
            }

            function ls(n) {
                return wr ? n.wheelDeltaY / 2 : n.deltaY && 0 === n.deltaMode ? -n.deltaY / ta : n.deltaY && 1 === n.deltaMode ? 20 * -n.deltaY : n.deltaY && 2 === n.deltaMode ? 60 * -n.deltaY : n.deltaX || n.deltaZ ? 0 : n.wheelDelta ? (n.wheelDeltaY || n.wheelDelta) / 2 : n.detail && Math.abs(n.detail) < 32765 ? 20 * -n.detail : n.detail ? n.detail / -32765 * 60 : 0
            }

            function kf(n) {
                to[n.type] = !0
            }

            function df(n) {
                var t = to[n.type];
                return to[n.type] = !1, t
            }

            function hu(n, t) {
                var i = t.relatedTarget;
                if (!i) return !0;
                try {
                    for (; i && i !== n;) i = i.parentNode
                } catch (n) {
                    return !1
                }
                return i !== n
            }

            function wc(n, t) {
                var r = n.timeStamp || n.originalEvent && n.originalEvent.timeStamp, i = no && r - no;
                if (i && i > 100 && i < 500 || n.target._simulatedClick && !n._simulated) return void yt(n);
                no = r;
                t(n)
            }

            function bc(n, t) {
                return new f(n, t)
            }

            function as(n, t) {
                if (!t || !n.length) return n.slice();
                var i = t * t;
                return n = gc(n, i), n = dc(n, i)
            }

            function vs(n, t, i) {
                return Math.sqrt(vr(n, t, i, !0))
            }

            function kc(n, t, i) {
                return vr(n, t, i)
            }

            function dc(n, t) {
                var r = n.length, e = typeof Uint8Array != void 0 + "" ? Uint8Array : Array, u = new e(r), i, f;
                for (u[0] = u[r - 1] = 1, gf(n, u, t, 0, r - 1), f = [], i = 0; i < r; i++) u[i] && f.push(n[i]);
                return f
            }

            function gf(n, t, i, r, u) {
                for (var e, s, o = 0, f = r + 1; f <= u - 1; f++) (s = vr(n[f], n[r], n[u], !0)) > o && (e = f, o = s);
                o > i && (t[e] = 1, gf(n, t, i, r, e), gf(n, t, i, e, u))
            }

            function gc(n, t) {
                for (var r = [n[0]], i = 1, u = 0, f = n.length; i < f; i++) nl(n[i], n[u]) > t && (r.push(n[i]), u = i);
                return u < f - 1 && r.push(n[f - 1]), r
            }

            function ys(n, t, i, r, u) {
                var s, o, h, f = r ? kh : ci(n, i), e = ci(t, i);
                for (kh = e; ;) {
                    if (!(f | e)) return [n, t];
                    if (f & e) return !1;
                    s = f || e;
                    o = cu(n, t, s, i, u);
                    h = ci(o, i);
                    s === f ? (n = o, f = h) : (t = o, e = h)
                }
            }

            function cu(n, i, r, u, f) {
                var e, o, s = i.x - n.x, h = i.y - n.y, c = u.min, l = u.max;
                return 8 & r ? (e = n.x + s * (l.y - n.y) / h, o = l.y) : 4 & r ? (e = n.x + s * (c.y - n.y) / h, o = c.y) : 2 & r ? (e = l.x, o = n.y + h * (l.x - n.x) / s) : 1 & r && (e = c.x, o = n.y + h * (c.x - n.x) / s), new t(e, o, f)
            }

            function ci(n, t) {
                var i = 0;
                return n.x < t.min.x ? i |= 1 : n.x > t.max.x && (i |= 2), n.y < t.min.y ? i |= 4 : n.y > t.max.y && (i |= 8), i
            }

            function nl(n, t) {
                var i = t.x - n.x, r = t.y - n.y;
                return i * i + r * r
            }

            function vr(n, i, r, u) {
                var h, o = i.x, s = i.y, f = r.x - o, e = r.y - s, c = f * f + e * e;
                return c > 0 && (h = ((n.x - o) * f + (n.y - s) * e) / c, h > 1 ? (o = r.x, s = r.y) : h > 0 && (o += f * h, s += e * h)), f = n.x - o, e = n.y - s, u ? f * f + e * e : new t(o, s)
            }

            function dt(n) {
                return !pt(n[0]) || "object" != typeof n[0][0] && void 0 !== n[0][0]
            }

            function ps(n) {
                return console.warn("Deprecated use of _flat, please use L.LineUtil.isFlat instead."), dt(n)
            }

            function ws(n, t, i) {
                for (var e, l, c, o, s, f, u, a = [1, 4, 2, 8], r = 0, h = n.length; r < h; r++) n[r]._code = ci(n[r], t);
                for (c = 0; c < 4; c++) {
                    for (f = a[c], e = [], r = 0, h = n.length, l = h - 1; r < h; l = r++) o = n[r], s = n[l], o._code & f ? s._code & f || (u = cu(s, o, f, t, i), u._code = ci(u, t), e.push(u)) : (s._code & f && (u = cu(s, o, f, t, i), u._code = ci(u, t), e.push(u)), e.push(o));
                    n = e
                }
                return n
            }

            function tl(n) {
                return new fr(n)
            }

            function il(n, t) {
                return new tu(n, t)
            }

            function rl(n, t) {
                return new tf(n, t)
            }

            function ul(n, t, i) {
                return new oo(n, t, i)
            }

            function fl(n, t) {
                return new ni(n, t)
            }

            function el(n, t) {
                return new er(n, t)
            }

            function ne(n, t) {
                var u, e, i, o, r = "Feature" === n.type ? n.geometry : n, f = r ? r.coordinates : null, s = [],
                    h = t && t.pointToLayer, c = t && t.coordsToLatLng || te, l;
                if (!f && !r) return null;
                switch (r.type) {
                    case"Point":
                        return u = c(f), h ? h(n, u) : new tu(u);
                    case"MultiPoint":
                        for (i = 0, o = f.length; i < o; i++) u = c(f[i]), s.push(h ? h(n, u) : new tu(u));
                        return new ai(s);
                    case"LineString":
                    case"MultiLineString":
                        return e = lu(f, "LineString" === r.type ? 0 : 1, c), new ni(e, t);
                    case"Polygon":
                    case"MultiPolygon":
                        return e = lu(f, "Polygon" === r.type ? 1 : 2, c), new er(e, t);
                    case"GeometryCollection":
                        for (i = 0, o = r.geometries.length; i < o; i++) l = ne({
                            geometry: r.geometries[i],
                            type: "Feature",
                            properties: n.properties
                        }, t), l && s.push(l);
                        return new ai(s);
                    default:
                        throw new Error("Invalid GeoJSON object.");
                }
            }

            function te(n) {
                return new h(n[1], n[0], n[2])
            }

            function lu(n, t, i) {
                for (var u, f = [], r = 0, e = n.length; r < e; r++) u = t ? lu(n[r], t - 1, i) : (i || te)(n[r]), f.push(u);
                return f
            }

            function ie(n, t) {
                return t = "number" == typeof t ? t : 6, void 0 !== n.alt ? [at(n.lng, t), at(n.lat, t), at(n.alt, t)] : [at(n.lng, t), at(n.lat, t)]
            }

            function au(n, t, i, r) {
                for (var u = [], f = 0, e = n.length; f < e; f++) u.push(t ? au(n[f], t - 1, i, r) : ie(n[f], r));
                return !t && i && u.push(u[0]), u
            }

            function bi(n, t) {
                return n.feature ? s({}, n.feature, {geometry: t}) : vu(t)
            }

            function vu(n) {
                return "Feature" === n.type || "FeatureCollection" === n.type ? n : {
                    type: "Feature",
                    properties: {},
                    geometry: n
                }
            }

            function bs(n, t) {
                return new ti(n, t)
            }

            function ol(n, t, i) {
                return new ic(n, t, i)
            }

            function sl(n) {
                return new co(n)
            }

            function hl(n) {
                return new iu(n)
            }

            function ks(n, t) {
                return new sr(n, t)
            }

            function cl(n, t) {
                return new uc(n, t)
            }

            function ds(n) {
                return lh ? new fc(n) : null
            }

            function gs(n) {
                return bu || ku ? new uu(n) : null
            }

            function ll(n, t) {
                return new lo(n, t)
            }

            var nh = Object.freeze, it, ki, ue, rr, bh, lt, rt, vi, rc, co, lo, ao, vo, yo, po, wo, bo, ko, ec;
            Object.freeze = function (n) {
                return n
            };
            var yu = Object.create || function () {
                    function n() {
                    }

                    return function (t) {
                        return n.prototype = t, new n
                    }
                }(), th = 0, al = /\{ *([\w_-]+) *\}/g, pt = Array.isArray || function (n) {
                    return "[object Array]" === Object.prototype.toString.call(n)
                }, pu = "data:image/gif;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=", ih = 0,
                re = window.requestAnimationFrame || ef("RequestAnimationFrame") || is,
                rh = window.cancelAnimationFrame || ef("CancelAnimationFrame") || ef("CancelRequestAnimationFrame") || function (n) {
                    window.clearTimeout(n)
                }, vl = (Object.freeze || Object)({
                    freeze: nh,
                    extend: s,
                    create: yu,
                    bind: c,
                    lastId: th,
                    stamp: o,
                    throttle: uf,
                    wrapNum: hr,
                    falseFn: g,
                    formatNum: at,
                    trim: ff,
                    splitWords: yi,
                    setOptions: l,
                    getParamString: go,
                    template: ns,
                    isArray: pt,
                    indexOf: ts,
                    emptyImageUrl: pu,
                    requestFn: re,
                    cancelFn: rh,
                    requestAnimFrame: d,
                    cancelAnimFrame: nt
                });
            kt.extend = function (n) {
                var i = function () {
                    this.initialize && this.initialize.apply(this, arguments);
                    this.callInitHooks()
                }, u = i.__super__ = this.prototype, t = yu(u), r;
                t.constructor = i;
                i.prototype = t;
                for (r in this) this.hasOwnProperty(r) && "prototype" !== r && "__super__" !== r && (i[r] = this[r]);
                return n.statics && (s(i, n.statics), delete n.statics), n.includes && (oc(n.includes), s.apply(null, [t].concat(n.includes)), delete n.includes), t.options && (n.options = s(yu(t.options), n.options)), s(t, n), t._initHooks = [], t.callInitHooks = function () {
                    if (!this._initHooksCalled) {
                        u.callInitHooks && u.callInitHooks.call(this);
                        this._initHooksCalled = !0;
                        for (var n = 0, i = t._initHooks.length; n < i; n++) t._initHooks[n].call(this)
                    }
                }, i
            };
            kt.include = function (n) {
                return s(this.prototype, n), this
            };
            kt.mergeOptions = function (n) {
                return s(this.prototype.options, n), this
            };
            kt.addInitHook = function (n) {
                var t = Array.prototype.slice.call(arguments, 1), i = "function" == typeof n ? n : function () {
                    this[n].apply(this, t)
                };
                return this.prototype._initHooks = this.prototype._initHooks || [], this.prototype._initHooks.push(i), this
            };
            it = {
                on: function (n, t, i) {
                    var u, r, f;
                    if ("object" == typeof n) for (u in n) this._on(u, n[u], t); else for (n = yi(n), r = 0, f = n.length; r < f; r++) this._on(n[r], t, i);
                    return this
                }, off: function (n, t, i) {
                    var u, r, f;
                    if (n) if ("object" == typeof n) for (u in n) this._off(u, n[u], t); else for (n = yi(n), r = 0, f = n.length; r < f; r++) this._off(n[r], t, i); else delete this._events;
                    return this
                }, _on: function (n, t, i) {
                    var r;
                    this._events = this._events || {};
                    r = this._events[n];
                    r || (r = [], this._events[n] = r);
                    i === this && (i = void 0);
                    for (var e = {
                        fn: t,
                        ctx: i
                    }, u = r, f = 0, o = u.length; f < o; f++) if (u[f].fn === t && u[f].ctx === i) return;
                    u.push(e)
                }, _off: function (n, t, i) {
                    var r, u, f, e;
                    if (this._events && (r = this._events[n])) {
                        if (!t) {
                            for (u = 0, f = r.length; u < f; u++) r[u].fn = g;
                            return void delete this._events[n]
                        }
                        if (i === this && (i = void 0), r) for (u = 0, f = r.length; u < f; u++) if (e = r[u], e.ctx === i && e.fn === t) return e.fn = g, this._firingCount && (this._events[n] = r = r.slice()), void r.splice(u, 1)
                    }
                }, fire: function (n, t, i) {
                    var f, r, u, o, e;
                    if (!this.listens(n, i)) return this;
                    if (f = s({}, t, {
                        type: n,
                        target: this,
                        sourceTarget: t && t.sourceTarget || this
                    }), this._events && (r = this._events[n], r)) {
                        for (this._firingCount = this._firingCount + 1 || 1, u = 0, o = r.length; u < o; u++) e = r[u], e.fn.call(e.ctx || this, f);
                        this._firingCount--
                    }
                    return i && this._propagateEvent(f), this
                }, listens: function (n, t) {
                    var i = this._events && this._events[n], r;
                    if (i && i.length) return !0;
                    if (t) for (r in this._eventParents) if (this._eventParents[r].listens(n, t)) return !0;
                    return !1
                }, once: function (n, t, i) {
                    var r, u;
                    if ("object" == typeof n) {
                        for (r in n) this.once(r, n[r], t);
                        return this
                    }
                    u = c(function () {
                        this.off(n, t, i).off(n, u, i)
                    }, this);
                    return this.on(n, t, i).on(n, u, i)
                }, addEventParent: function (n) {
                    return this._eventParents = this._eventParents || {}, this._eventParents[o(n)] = n, this
                }, removeEventParent: function (n) {
                    return this._eventParents && delete this._eventParents[o(n)], this
                }, _propagateEvent: function (n) {
                    for (var t in this._eventParents) this._eventParents[t].fire(n.type, s({
                        layer: n.target,
                        propagatedFrom: n.target
                    }, n), !0)
                }
            };
            it.addEventListener = it.on;
            it.removeEventListener = it.clearAllEventListeners = it.off;
            it.addOneTimeEventListener = it.once;
            it.fireEvent = it.fire;
            it.hasEventListeners = it.listens;
            ki = kt.extend(it);
            ue = Math.trunc || function (n) {
                return n > 0 ? Math.floor(n) : Math.ceil(n)
            };
            t.prototype = {
                clone: function () {
                    return new t(this.x, this.y)
                }, add: function (n) {
                    return this.clone()._add(i(n))
                }, _add: function (n) {
                    return this.x += n.x, this.y += n.y, this
                }, subtract: function (n) {
                    return this.clone()._subtract(i(n))
                }, _subtract: function (n) {
                    return this.x -= n.x, this.y -= n.y, this
                }, divideBy: function (n) {
                    return this.clone()._divideBy(n)
                }, _divideBy: function (n) {
                    return this.x /= n, this.y /= n, this
                }, multiplyBy: function (n) {
                    return this.clone()._multiplyBy(n)
                }, _multiplyBy: function (n) {
                    return this.x *= n, this.y *= n, this
                }, scaleBy: function (n) {
                    return new t(this.x * n.x, this.y * n.y)
                }, unscaleBy: function (n) {
                    return new t(this.x / n.x, this.y / n.y)
                }, round: function () {
                    return this.clone()._round()
                }, _round: function () {
                    return this.x = Math.round(this.x), this.y = Math.round(this.y), this
                }, floor: function () {
                    return this.clone()._floor()
                }, _floor: function () {
                    return this.x = Math.floor(this.x), this.y = Math.floor(this.y), this
                }, ceil: function () {
                    return this.clone()._ceil()
                }, _ceil: function () {
                    return this.x = Math.ceil(this.x), this.y = Math.ceil(this.y), this
                }, trunc: function () {
                    return this.clone()._trunc()
                }, _trunc: function () {
                    return this.x = ue(this.x), this.y = ue(this.y), this
                }, distanceTo: function (n) {
                    n = i(n);
                    var t = n.x - this.x, r = n.y - this.y;
                    return Math.sqrt(t * t + r * r)
                }, equals: function (n) {
                    return n = i(n), n.x === this.x && n.y === this.y
                }, contains: function (n) {
                    return n = i(n), Math.abs(n.x) <= Math.abs(this.x) && Math.abs(n.y) <= Math.abs(this.y)
                }, toString: function () {
                    return "Point(" + at(this.x) + ", " + at(this.y) + ")"
                }
            };
            v.prototype = {
                extend: function (n) {
                    return n = i(n), this.min || this.max ? (this.min.x = Math.min(n.x, this.min.x), this.max.x = Math.max(n.x, this.max.x), this.min.y = Math.min(n.y, this.min.y), this.max.y = Math.max(n.y, this.max.y)) : (this.min = n.clone(), this.max = n.clone()), this
                }, getCenter: function (n) {
                    return new t((this.min.x + this.max.x) / 2, (this.min.y + this.max.y) / 2, n)
                }, getBottomLeft: function () {
                    return new t(this.min.x, this.max.y)
                }, getTopRight: function () {
                    return new t(this.max.x, this.min.y)
                }, getTopLeft: function () {
                    return this.min
                }, getBottomRight: function () {
                    return this.max
                }, getSize: function () {
                    return this.max.subtract(this.min)
                }, contains: function (n) {
                    var r, u;
                    return n = "number" == typeof n[0] || n instanceof t ? i(n) : ot(n), n instanceof v ? (r = n.min, u = n.max) : r = u = n, r.x >= this.min.x && u.x <= this.max.x && r.y >= this.min.y && u.y <= this.max.y
                }, intersects: function (n) {
                    n = ot(n);
                    var t = this.min, i = this.max, r = n.min, u = n.max, f = u.x >= t.x && r.x <= i.x,
                        e = u.y >= t.y && r.y <= i.y;
                    return f && e
                }, overlaps: function (n) {
                    n = ot(n);
                    var t = this.min, i = this.max, r = n.min, u = n.max, f = u.x > t.x && r.x < i.x,
                        e = u.y > t.y && r.y < i.y;
                    return f && e
                }, isValid: function () {
                    return !(!this.min || !this.max)
                }
            };
            tt.prototype = {
                extend: function (n) {
                    var t, i, r = this._southWest, u = this._northEast;
                    if (n instanceof h) t = n, i = n; else {
                        if (!(n instanceof tt)) return n ? this.extend(y(n) || k(n)) : this;
                        if (t = n._southWest, i = n._northEast, !t || !i) return this
                    }
                    return r || u ? (r.lat = Math.min(t.lat, r.lat), r.lng = Math.min(t.lng, r.lng), u.lat = Math.max(i.lat, u.lat), u.lng = Math.max(i.lng, u.lng)) : (this._southWest = new h(t.lat, t.lng), this._northEast = new h(i.lat, i.lng)), this
                }, pad: function (n) {
                    var t = this._southWest, i = this._northEast, r = Math.abs(t.lat - i.lat) * n,
                        u = Math.abs(t.lng - i.lng) * n;
                    return new tt(new h(t.lat - r, t.lng - u), new h(i.lat + r, i.lng + u))
                }, getCenter: function () {
                    return new h((this._southWest.lat + this._northEast.lat) / 2, (this._southWest.lng + this._northEast.lng) / 2)
                }, getSouthWest: function () {
                    return this._southWest
                }, getNorthEast: function () {
                    return this._northEast
                }, getNorthWest: function () {
                    return new h(this.getNorth(), this.getWest())
                }, getSouthEast: function () {
                    return new h(this.getSouth(), this.getEast())
                }, getWest: function () {
                    return this._southWest.lng
                }, getSouth: function () {
                    return this._southWest.lat
                }, getEast: function () {
                    return this._northEast.lng
                }, getNorth: function () {
                    return this._northEast.lat
                }, contains: function (n) {
                    n = "number" == typeof n[0] || n instanceof h || "lat" in n ? y(n) : k(n);
                    var t, i, r = this._southWest, u = this._northEast;
                    return n instanceof tt ? (t = n.getSouthWest(), i = n.getNorthEast()) : t = i = n, t.lat >= r.lat && i.lat <= u.lat && t.lng >= r.lng && i.lng <= u.lng
                }, intersects: function (n) {
                    n = k(n);
                    var t = this._southWest, i = this._northEast, r = n.getSouthWest(), u = n.getNorthEast(),
                        f = u.lat >= t.lat && r.lat <= i.lat, e = u.lng >= t.lng && r.lng <= i.lng;
                    return f && e
                }, overlaps: function (n) {
                    n = k(n);
                    var t = this._southWest, i = this._northEast, r = n.getSouthWest(), u = n.getNorthEast(),
                        f = u.lat > t.lat && r.lat < i.lat, e = u.lng > t.lng && r.lng < i.lng;
                    return f && e
                }, toBBoxString: function () {
                    return [this.getWest(), this.getSouth(), this.getEast(), this.getNorth()].join(",")
                }, equals: function (n, t) {
                    return !!n && (n = k(n), this._southWest.equals(n.getSouthWest(), t) && this._northEast.equals(n.getNorthEast(), t))
                }, isValid: function () {
                    return !(!this._southWest || !this._northEast)
                }
            };
            h.prototype = {
                equals: function (n, t) {
                    return !!n && (n = y(n), Math.max(Math.abs(this.lat - n.lat), Math.abs(this.lng - n.lng)) <= (void 0 === t ? 1e-9 : t))
                }, toString: function (n) {
                    return "LatLng(" + at(this.lat, n) + ", " + at(this.lng, n) + ")"
                }, distanceTo: function (n) {
                    return ui.distance(this, y(n))
                }, wrap: function () {
                    return ui.wrapLatLng(this)
                }, toBounds: function (n) {
                    var t = 180 * n / 40075017, i = t / Math.cos(Math.PI / 180 * this.lat);
                    return k([this.lat - t, this.lng - i], [this.lat + t, this.lng + i])
                }, clone: function () {
                    return new h(this.lat, this.lng, this.alt)
                }
            };
            var gt = {
                latLngToPoint: function (n, t) {
                    var i = this.projection.project(n), r = this.scale(t);
                    return this.transformation._transform(i, r)
                }, pointToLatLng: function (n, t) {
                    var i = this.scale(t), r = this.transformation.untransform(n, i);
                    return this.projection.unproject(r)
                }, project: function (n) {
                    return this.projection.project(n)
                }, unproject: function (n) {
                    return this.projection.unproject(n)
                }, scale: function (n) {
                    return 256 * Math.pow(2, n)
                }, zoom: function (n) {
                    return Math.log(n / 256) / Math.LN2
                }, getProjectedBounds: function (n) {
                    if (this.infinite) return null;
                    var t = this.projection.bounds, i = this.scale(n);
                    return new v(this.transformation.transform(t.min, i), this.transformation.transform(t.max, i))
                }, infinite: !1, wrapLatLng: function (n) {
                    var t = this.wrapLng ? hr(n.lng, this.wrapLng, !0) : n.lng;
                    return new h(this.wrapLat ? hr(n.lat, this.wrapLat, !0) : n.lat, t, n.alt)
                }, wrapLatLngBounds: function (n) {
                    var t = n.getCenter(), e = this.wrapLatLng(t), i = t.lat - e.lat, r = t.lng - e.lng, u, f;
                    return 0 === i && 0 === r ? n : (u = n.getSouthWest(), f = n.getNorthEast(), new tt(new h(u.lat - i, u.lng - r), new h(f.lat - i, f.lng - r)))
                }
            }, ui = s({}, gt, {
                wrapLng: [-180, 180], R: 6371e3, distance: function (n, t) {
                    var i = Math.PI / 180, e = n.lat * i, o = t.lat * i, r = Math.sin((t.lat - n.lat) * i / 2),
                        u = Math.sin((t.lng - n.lng) * i / 2), f = r * r + Math.cos(e) * Math.cos(o) * u * u,
                        s = 2 * Math.atan2(Math.sqrt(f), Math.sqrt(1 - f));
                    return this.R * s
                }
            }), fe = {
                R: 6378137, MAX_LATITUDE: 85.0511287798, project: function (n) {
                    var i = Math.PI / 180, r = this.MAX_LATITUDE, f = Math.max(Math.min(r, n.lat), -r),
                        u = Math.sin(f * i);
                    return new t(this.R * n.lng * i, this.R * Math.log((1 + u) / (1 - u)) / 2)
                }, unproject: function (n) {
                    var t = 180 / Math.PI;
                    return new h((2 * Math.atan(Math.exp(n.y / this.R)) - Math.PI / 2) * t, n.x * t / this.R)
                }, bounds: function () {
                    var n = 6378137 * Math.PI;
                    return new v([-n, -n], [n, n])
                }()
            };
            of.prototype = {
                transform: function (n, t) {
                    return this._transform(n.clone(), t)
                }, _transform: function (n, t) {
                    return t = t || 1, n.x = t * (this._a * n.x + this._b), n.y = t * (this._c * n.y + this._d), n
                }, untransform: function (n, i) {
                    return i = i || 1, new t((n.x / i - this._b) / this._a, (n.y / i - this._d) / this._c)
                }
            };
            var yr, pr, ee, oe = s({}, ui, {
                    code: "EPSG:3857", projection: fe, transformation: function () {
                        var n = .5 / (Math.PI * fe.R);
                        return cr(n, .5, -n, .5)
                    }()
                }), yl = s({}, oe, {code: "EPSG:900913"}), se = document.documentElement.style,
                wu = "ActiveXObject" in window, di = wu && !document.addEventListener,
                wr = "msLaunchUri" in navigator && !("documentMode" in document), he = vt("webkit"), gi = vt("android"),
                br = vt("android 2") || vt("android 3"),
                pl = parseInt(/WebKit\/([0-9]+)|$/.exec(navigator.userAgent)[1], 10),
                uh = gi && vt("Google") && pl < 537 && !("AudioNode" in window), ce = !!window.opera, kr = vt("chrome"),
                le = vt("gecko") && !he && !ce && !wu, fh = !kr && vt("safari"), eh = vt("phantom"),
                oh = "OTransition" in se, sh = 0 === navigator.platform.indexOf("Win"), ae = wu && "transition" in se,
                ve = "WebKitCSSMatrix" in window && "m11" in new window.WebKitCSSMatrix && !br,
                hh = "MozPerspective" in se, et = !window.L_DISABLE_3D && (ae || ve || hh) && !oh && !eh,
                nr = "undefined" != typeof orientation || vt("mobile"), wl = nr && he, bl = nr && ve,
                fi = !window.PointerEvent && window.MSPointerEvent, st = !(!window.PointerEvent && !fi),
                wt = !window.L_NO_TOUCH && (st || "ontouchstart" in window || window.DocumentTouch && document instanceof window.DocumentTouch),
                ch = nr && ce, kl = nr && le,
                ei = (window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI) > 1,
                lh = function () {
                    return !!document.createElement("canvas").getContext
                }(), bu = !(!document.createElementNS || !rs("svg").createSVGRect), ku = !bu && function () {
                    var t, n;
                    try {
                        return t = document.createElement("div"), t.innerHTML = '<v:shape adj="1"/>', n = t.firstChild, n.style.behavior = "url(#default#VML)", n && "object" == typeof n.adj
                    } catch (t) {
                        return !1
                    }
                }(), dl = (Object.freeze || Object)({
                    ie: wu,
                    ielt9: di,
                    edge: wr,
                    webkit: he,
                    android: gi,
                    android23: br,
                    androidStock: uh,
                    opera: ce,
                    chrome: kr,
                    gecko: le,
                    safari: fh,
                    phantom: eh,
                    opera12: oh,
                    win: sh,
                    ie3d: ae,
                    webkit3d: ve,
                    gecko3d: hh,
                    any3d: et,
                    mobile: nr,
                    mobileWebkit: wl,
                    mobileWebkit3d: bl,
                    msPointer: fi,
                    pointer: st,
                    touch: wt,
                    mobileOpera: ch,
                    mobileGecko: kl,
                    retina: ei,
                    canvas: lh,
                    svg: bu,
                    vml: ku
                }), ye = fi ? "MSPointerDown" : "pointerdown", pe = fi ? "MSPointerMove" : "pointermove",
                we = fi ? "MSPointerUp" : "pointerup", be = fi ? "MSPointerCancel" : "pointercancel",
                gl = ["INPUT", "SELECT", "OPTION"], tr = {}, ah = !1, ke = 0,
                du = fi ? "MSPointerDown" : st ? "pointerdown" : "touchstart",
                gu = fi ? "MSPointerUp" : st ? "pointerup" : "touchend", ir = "_leaflet_",
                de = eu(["transform", "webkitTransform", "OTransform", "MozTransform", "msTransform"]),
                dr = eu(["webkitTransition", "transition", "OTransition", "MozTransition", "msTransition"]),
                vh = "webkitTransition" === dr || "OTransition" === dr ? dr + "End" : "transitionend";
            "onselectstart" in document ? (yr = function () {
                u(window, "selectstart", ft)
            }, pr = function () {
                w(window, "selectstart", ft)
            }) : (rr = eu(["userSelect", "WebkitUserSelect", "OUserSelect", "MozUserSelect", "msUserSelect"]), yr = function () {
                if (rr) {
                    var n = document.documentElement.style;
                    ee = n[rr];
                    n[rr] = "none"
                }
            }, pr = function () {
                rr && (document.documentElement.style[rr] = ee, ee = void 0)
            });
            var nf, ge, no, na = (Object.freeze || Object)({
                    TRANSFORM: de,
                    TRANSITION: dr,
                    TRANSITION_END: vh,
                    get: ss,
                    getStyle: lr,
                    create: e,
                    remove: a,
                    empty: hf,
                    toFront: pi,
                    toBack: wi,
                    hasClass: cf,
                    addClass: r,
                    removeClass: p,
                    setClass: lf,
                    getClass: fu,
                    setOpacity: ut,
                    testProp: eu,
                    setTransform: si,
                    setPosition: b,
                    getPosition: ri,
                    disableTextSelection: yr,
                    enableTextSelection: pr,
                    disableImageDrag: af,
                    enableImageDrag: vf,
                    preventOutline: yf,
                    restoreOutline: ou,
                    getSizedParentNode: hs,
                    getScale: pf
                }), ht = "_leaflet_events", ta = sh && kr ? 2 * window.devicePixelRatio : le ? window.devicePixelRatio : 1,
                to = {}, ia = (Object.freeze || Object)({
                    on: u,
                    off: w,
                    stopPropagation: hi,
                    disableScrollPropagation: bf,
                    disableClickPropagation: ar,
                    preventDefault: ft,
                    stop: yt,
                    getMousePosition: cs,
                    getWheelDelta: ls,
                    fakeStop: kf,
                    skipped: df,
                    isExternalTarget: hu,
                    addListener: u,
                    removeListener: w
                }), yh = ki.extend({
                    run: function (n, t, i, r) {
                        this.stop();
                        this._el = n;
                        this._inProgress = !0;
                        this._duration = i || .25;
                        this._easeOutPower = 1 / Math.max(r || .5, .2);
                        this._startPos = ri(n);
                        this._offset = t.subtract(this._startPos);
                        this._startTime = +new Date;
                        this.fire("start");
                        this._animate()
                    }, stop: function () {
                        this._inProgress && (this._step(!0), this._complete())
                    }, _animate: function () {
                        this._animId = d(this._animate, this);
                        this._step()
                    }, _step: function (n) {
                        var t = +new Date - this._startTime, i = 1e3 * this._duration;
                        t < i ? this._runFrame(this._easeOut(t / i), n) : (this._runFrame(1), this._complete())
                    }, _runFrame: function (n, t) {
                        var i = this._startPos.add(this._offset.multiplyBy(n));
                        t && i._round();
                        b(this._el, i);
                        this.fire("step")
                    }, _complete: function () {
                        nt(this._animId);
                        this._inProgress = !1;
                        this.fire("end")
                    }, _easeOut: function (n) {
                        return 1 - Math.pow(1 - n, this._easeOutPower)
                    }
                }), f = ki.extend({
                    options: {
                        crs: oe,
                        center: void 0,
                        zoom: void 0,
                        minZoom: void 0,
                        maxZoom: void 0,
                        layers: [],
                        maxBounds: void 0,
                        renderer: void 0,
                        zoomAnimation: !0,
                        zoomAnimationThreshold: 4,
                        fadeAnimation: !0,
                        markerZoomAnimation: !0,
                        transform3DLimit: 8388608,
                        zoomSnap: 1,
                        zoomDelta: 1,
                        trackResize: !0
                    },
                    initialize: function (n, t) {
                        t = l(this, t);
                        this._handlers = [];
                        this._layers = {};
                        this._zoomBoundLayers = {};
                        this._sizeChanged = !0;
                        this._initContainer(n);
                        this._initLayout();
                        this._onResize = c(this._onResize, this);
                        this._initEvents();
                        t.maxBounds && this.setMaxBounds(t.maxBounds);
                        void 0 !== t.zoom && (this._zoom = this._limitZoom(t.zoom));
                        t.center && void 0 !== t.zoom && this.setView(y(t.center), t.zoom, {reset: !0});
                        this.callInitHooks();
                        this._zoomAnimated = dr && et && !ch && this.options.zoomAnimation;
                        this._zoomAnimated && (this._createAnimProxy(), u(this._proxy, vh, this._catchTransitionEnd, this));
                        this._addLayers(this.options.layers)
                    },
                    setView: function (n, t, i) {
                        return (t = void 0 === t ? this._zoom : this._limitZoom(t), n = this._limitCenter(y(n), t, this.options.maxBounds), i = i || {}, this._stop(), this._loaded && !i.reset && !0 !== i) && (void 0 !== i.animate && (i.zoom = s({animate: i.animate}, i.zoom), i.pan = s({
                            animate: i.animate,
                            duration: i.duration
                        }, i.pan)), this._zoom !== t ? this._tryAnimatedZoom && this._tryAnimatedZoom(n, t, i.zoom) : this._tryAnimatedPan(n, i.pan)) ? (clearTimeout(this._sizeTimer), this) : (this._resetView(n, t), this)
                    },
                    setZoom: function (n, t) {
                        return this._loaded ? this.setView(this.getCenter(), n, {zoom: t}) : (this._zoom = n, this)
                    },
                    zoomIn: function (n, t) {
                        return n = n || (et ? this.options.zoomDelta : 1), this.setZoom(this._zoom + n, t)
                    },
                    zoomOut: function (n, t) {
                        return n = n || (et ? this.options.zoomDelta : 1), this.setZoom(this._zoom - n, t)
                    },
                    setZoomAround: function (n, i, r) {
                        var f = this.getZoomScale(i), u = this.getSize().divideBy(2),
                            e = n instanceof t ? n : this.latLngToContainerPoint(n),
                            o = e.subtract(u).multiplyBy(1 - 1 / f), s = this.containerPointToLatLng(u.add(o));
                        return this.setView(s, i, {zoom: r})
                    },
                    _getBoundsCenterZoom: function (n, t) {
                        t = t || {};
                        n = n.getBounds ? n.getBounds() : k(n);
                        var u = i(t.paddingTopLeft || t.padding || [0, 0]),
                            f = i(t.paddingBottomRight || t.padding || [0, 0]), r = this.getBoundsZoom(n, !1, u.add(f));
                        if ((r = "number" == typeof t.maxZoom ? Math.min(t.maxZoom, r) : r) === 1 / 0) return {
                            center: n.getCenter(),
                            zoom: r
                        };
                        var e = f.subtract(u).divideBy(2), o = this.project(n.getSouthWest(), r),
                            s = this.project(n.getNorthEast(), r);
                        return {center: this.unproject(o.add(s).divideBy(2).add(e), r), zoom: r}
                    },
                    fitBounds: function (n, t) {
                        if (n = k(n), !n.isValid()) throw new Error("Bounds are not valid.");
                        var i = this._getBoundsCenterZoom(n, t);
                        return this.setView(i.center, i.zoom, t)
                    },
                    fitWorld: function (n) {
                        return this.fitBounds([[-90, -180], [90, 180]], n)
                    },
                    panTo: function (n, t) {
                        return this.setView(n, this._zoom, {pan: t})
                    },
                    panBy: function (n, t) {
                        if (n = i(n).round(), t = t || {}, !n.x && !n.y) return this.fire("moveend");
                        if (!0 !== t.animate && !this.getSize().contains(n)) return this._resetView(this.unproject(this.project(this.getCenter()).add(n)), this.getZoom()), this;
                        if (this._panAnim || (this._panAnim = new yh, this._panAnim.on({
                            step: this._onPanTransitionStep,
                            end: this._onPanTransitionEnd
                        }, this)), t.noMoveStart || this.fire("movestart"), !1 !== t.animate) {
                            r(this._mapPane, "leaflet-pan-anim");
                            var u = this._getMapPanePos().subtract(n).round();
                            this._panAnim.run(this._mapPane, u, t.duration || .25, t.easeLinearity)
                        } else this._rawPanBy(n), this.fire("move").fire("moveend");
                        return this
                    },
                    flyTo: function (n, t, i) {
                        function a(n) {
                            var u = n ? -1 : 1, f = n ? l : r, e = l * l - r * r + u * h * h * s * s, o = 2 * f * h * s,
                                t = e / o, i = Math.sqrt(t * t + 1) - t;
                            return i < 1e-9 ? -18 : Math.log(i)
                        }

                        function v(n) {
                            return (Math.exp(n) - Math.exp(-n)) / 2
                        }

                        function e(n) {
                            return (Math.exp(n) + Math.exp(-n)) / 2
                        }

                        function g(n) {
                            return v(n) / e(n)
                        }

                        function nt(n) {
                            return r * (e(u) / e(u + f * n))
                        }

                        function tt(n) {
                            return r * (e(u) * g(u + f * n) - v(u)) / h
                        }

                        function it(n) {
                            return 1 - Math.pow(1 - n, 1.5)
                        }

                        function p() {
                            var i = (Date.now() - rt) / ut, u = it(i) * k;
                            i <= 1 ? (this._flyToFrame = d(p, this), this._move(this.unproject(c.add(w.subtract(c).multiplyBy(tt(u) / s)), o), this.getScaleZoom(r / nt(u), o), {flyTo: !0})) : this._move(n, t)._moveEnd(!0)
                        }

                        if (i = i || {}, !1 === i.animate || !et) return this.setView(n, t, i);
                        this._stop();
                        var c = this.project(this.getCenter()), w = this.project(n), b = this.getSize(), o = this._zoom;
                        n = y(n);
                        t = void 0 === t ? o : t;
                        var r = Math.max(b.x, b.y), l = r * this.getZoomScale(o, t), s = w.distanceTo(c) || 1, f = 1.42,
                            h = f * f, u = a(0), rt = Date.now(), k = (a(1) - u) / f,
                            ut = i.duration ? 1e3 * i.duration : 1e3 * k * .8;
                        return this._moveStart(!0, i.noMoveStart), p.call(this), this
                    },
                    flyToBounds: function (n, t) {
                        var i = this._getBoundsCenterZoom(n, t);
                        return this.flyTo(i.center, i.zoom, t)
                    },
                    setMaxBounds: function (n) {
                        return n = k(n), n.isValid() ? (this.options.maxBounds && this.off("moveend", this._panInsideMaxBounds), this.options.maxBounds = n, this._loaded && this._panInsideMaxBounds(), this.on("moveend", this._panInsideMaxBounds)) : (this.options.maxBounds = null, this.off("moveend", this._panInsideMaxBounds))
                    },
                    setMinZoom: function (n) {
                        var t = this.options.minZoom;
                        return this.options.minZoom = n, this._loaded && t !== n && (this.fire("zoomlevelschange"), this.getZoom() < this.options.minZoom) ? this.setZoom(n) : this
                    },
                    setMaxZoom: function (n) {
                        var t = this.options.maxZoom;
                        return this.options.maxZoom = n, this._loaded && t !== n && (this.fire("zoomlevelschange"), this.getZoom() > this.options.maxZoom) ? this.setZoom(n) : this
                    },
                    panInsideBounds: function (n, t) {
                        this._enforcingBounds = !0;
                        var i = this.getCenter(), r = this._limitCenter(i, this._zoom, k(n));
                        return i.equals(r) || this.panTo(r, t), this._enforcingBounds = !1, this
                    },
                    panInside: function (n, t) {
                        var f, u;
                        t = t || {};
                        var s = i(t.paddingTopLeft || t.padding || [0, 0]),
                            h = i(t.paddingBottomRight || t.padding || [0, 0]), a = this.getCenter(), c = this.project(a),
                            r = this.project(n), l = this.getPixelBounds(), o = l.getSize().divideBy(2),
                            e = ot([l.min.add(s), l.max.subtract(h)]);
                        return e.contains(r) || (this._enforcingBounds = !0, f = c.subtract(r), u = i(r.x + f.x, r.y + f.y), (r.x < e.min.x || r.x > e.max.x) && (u.x = c.x - f.x, f.x > 0 ? u.x += o.x - s.x : u.x -= o.x - h.x), (r.y < e.min.y || r.y > e.max.y) && (u.y = c.y - f.y, f.y > 0 ? u.y += o.y - s.y : u.y -= o.y - h.y), this.panTo(this.unproject(u), t), this._enforcingBounds = !1), this
                    },
                    invalidateSize: function (n) {
                        var i;
                        if (!this._loaded) return this;
                        n = s({animate: !1, pan: !0}, !0 === n ? {animate: !0} : n);
                        i = this.getSize();
                        this._sizeChanged = !0;
                        this._lastCenter = null;
                        var r = this.getSize(), u = i.divideBy(2).round(), f = r.divideBy(2).round(), t = u.subtract(f);
                        return t.x || t.y ? (n.animate && n.pan ? this.panBy(t) : (n.pan && this._rawPanBy(t), this.fire("move"), n.debounceMoveend ? (clearTimeout(this._sizeTimer), this._sizeTimer = setTimeout(c(this.fire, this, "moveend"), 200)) : this.fire("moveend")), this.fire("resize", {
                            oldSize: i,
                            newSize: r
                        })) : this
                    },
                    stop: function () {
                        return this.setZoom(this._limitZoom(this._zoom)), this.options.zoomSnap || this.fire("viewreset"), this._stop()
                    },
                    locate: function (n) {
                        if (n = this._locateOptions = s({
                            timeout: 1e4,
                            watch: !1
                        }, n), !("geolocation" in navigator)) return this._handleGeolocationError({
                            code: 0,
                            message: "Geolocation not supported."
                        }), this;
                        var t = c(this._handleGeolocationResponse, this), i = c(this._handleGeolocationError, this);
                        return n.watch ? this._locationWatchId = navigator.geolocation.watchPosition(t, i, n) : navigator.geolocation.getCurrentPosition(t, i, n), this
                    },
                    stopLocate: function () {
                        return navigator.geolocation && navigator.geolocation.clearWatch && navigator.geolocation.clearWatch(this._locationWatchId), this._locateOptions && (this._locateOptions.setView = !1), this
                    },
                    _handleGeolocationError: function (n) {
                        var t = n.code,
                            i = n.message || (1 === t ? "permission denied" : 2 === t ? "position unavailable" : "timeout");
                        this._locateOptions.setView && !this._loaded && this.fitWorld();
                        this.fire("locationerror", {code: t, message: "Geolocation error: " + i + "."})
                    },
                    _handleGeolocationResponse: function (n) {
                        var o = n.coords.latitude, s = n.coords.longitude, i = new h(o, s),
                            e = i.toBounds(2 * n.coords.accuracy), r = this._locateOptions, u, f, t;
                        r.setView && (u = this.getBoundsZoom(e), this.setView(i, r.maxZoom ? Math.min(u, r.maxZoom) : u));
                        f = {latlng: i, bounds: e, timestamp: n.timestamp};
                        for (t in n.coords) "number" == typeof n.coords[t] && (f[t] = n.coords[t]);
                        this.fire("locationfound", f)
                    },
                    addHandler: function (n, t) {
                        if (!t) return this;
                        var i = this[n] = new t(this);
                        return this._handlers.push(i), this.options[n] && i.enable(), this
                    },
                    remove: function () {
                        if (this._initEvents(!0), this._containerId !== this._container._leaflet_id) throw new Error("Map container is being reused by another instance");
                        try {
                            delete this._container._leaflet_id;
                            delete this._containerId
                        } catch (n) {
                            this._container._leaflet_id = void 0;
                            this._containerId = void 0
                        }
                        void 0 !== this._locationWatchId && this.stopLocate();
                        this._stop();
                        a(this._mapPane);
                        this._clearControlPos && this._clearControlPos();
                        this._resizeRequest && (nt(this._resizeRequest), this._resizeRequest = null);
                        this._clearHandlers();
                        this._loaded && this.fire("unload");
                        for (var n in this._layers) this._layers[n].remove();
                        for (n in this._panes) a(this._panes[n]);
                        return this._layers = [], this._panes = [], delete this._mapPane, delete this._renderer, this
                    },
                    createPane: function (n, t) {
                        var r = "leaflet-pane" + (n ? " leaflet-" + n.replace("Pane", "") + "-pane" : ""),
                            i = e("div", r, t || this._mapPane);
                        return n && (this._panes[n] = i), i
                    },
                    getCenter: function () {
                        return this._checkIfLoaded(), this._lastCenter && !this._moved() ? this._lastCenter : this.layerPointToLatLng(this._getCenterLayerPoint())
                    },
                    getZoom: function () {
                        return this._zoom
                    },
                    getBounds: function () {
                        var n = this.getPixelBounds();
                        return new tt(this.unproject(n.getBottomLeft()), this.unproject(n.getTopRight()))
                    },
                    getMinZoom: function () {
                        return void 0 === this.options.minZoom ? this._layersMinZoom || 0 : this.options.minZoom
                    },
                    getMaxZoom: function () {
                        return void 0 === this.options.maxZoom ? void 0 === this._layersMaxZoom ? 1 / 0 : this._layersMaxZoom : this.options.maxZoom
                    },
                    getBoundsZoom: function (n, t, r) {
                        n = k(n);
                        r = i(r || [0, 0]);
                        var u = this.getZoom() || 0, c = this.getMinZoom(), l = this.getMaxZoom(), a = n.getNorthWest(),
                            v = n.getSouthEast(), e = this.getSize().subtract(r),
                            o = ot(this.project(v, u), this.project(a, u)).getSize(), f = et ? this.options.zoomSnap : 1,
                            s = e.x / o.x, h = e.y / o.y, y = t ? Math.max(s, h) : Math.min(s, h);
                        return u = this.getScaleZoom(y, u), f && (u = Math.round(u / (f / 100)) * (f / 100), u = t ? Math.ceil(u / f) * f : Math.floor(u / f) * f), Math.max(c, Math.min(l, u))
                    },
                    getSize: function () {
                        return this._size && !this._sizeChanged || (this._size = new t(this._container.clientWidth || 0, this._container.clientHeight || 0), this._sizeChanged = !1), this._size.clone()
                    },
                    getPixelBounds: function (n, t) {
                        var i = this._getTopLeftPoint(n, t);
                        return new v(i, i.add(this.getSize()))
                    },
                    getPixelOrigin: function () {
                        return this._checkIfLoaded(), this._pixelOrigin
                    },
                    getPixelWorldBounds: function (n) {
                        return this.options.crs.getProjectedBounds(void 0 === n ? this.getZoom() : n)
                    },
                    getPane: function (n) {
                        return "string" == typeof n ? this._panes[n] : n
                    },
                    getPanes: function () {
                        return this._panes
                    },
                    getContainer: function () {
                        return this._container
                    },
                    getZoomScale: function (n, t) {
                        var i = this.options.crs;
                        return t = void 0 === t ? this._zoom : t, i.scale(n) / i.scale(t)
                    },
                    getScaleZoom: function (n, t) {
                        var r = this.options.crs, i;
                        return t = void 0 === t ? this._zoom : t, i = r.zoom(n * r.scale(t)), isNaN(i) ? 1 / 0 : i
                    },
                    project: function (n, t) {
                        return t = void 0 === t ? this._zoom : t, this.options.crs.latLngToPoint(y(n), t)
                    },
                    unproject: function (n, t) {
                        return t = void 0 === t ? this._zoom : t, this.options.crs.pointToLatLng(i(n), t)
                    },
                    layerPointToLatLng: function (n) {
                        var t = i(n).add(this.getPixelOrigin());
                        return this.unproject(t)
                    },
                    latLngToLayerPoint: function (n) {
                        return this.project(y(n))._round()._subtract(this.getPixelOrigin())
                    },
                    wrapLatLng: function (n) {
                        return this.options.crs.wrapLatLng(y(n))
                    },
                    wrapLatLngBounds: function (n) {
                        return this.options.crs.wrapLatLngBounds(k(n))
                    },
                    distance: function (n, t) {
                        return this.options.crs.distance(y(n), y(t))
                    },
                    containerPointToLayerPoint: function (n) {
                        return i(n).subtract(this._getMapPanePos())
                    },
                    layerPointToContainerPoint: function (n) {
                        return i(n).add(this._getMapPanePos())
                    },
                    containerPointToLatLng: function (n) {
                        var t = this.containerPointToLayerPoint(i(n));
                        return this.layerPointToLatLng(t)
                    },
                    latLngToContainerPoint: function (n) {
                        return this.layerPointToContainerPoint(this.latLngToLayerPoint(y(n)))
                    },
                    mouseEventToContainerPoint: function (n) {
                        return cs(n, this._container)
                    },
                    mouseEventToLayerPoint: function (n) {
                        return this.containerPointToLayerPoint(this.mouseEventToContainerPoint(n))
                    },
                    mouseEventToLatLng: function (n) {
                        return this.layerPointToLatLng(this.mouseEventToLayerPoint(n))
                    },
                    _initContainer: function (n) {
                        var t = this._container = ss(n);
                        if (!t) throw new Error("Map container not found.");
                        if (t._leaflet_id) throw new Error("Map container is already initialized.");
                        u(t, "scroll", this._onScroll, this);
                        this._containerId = o(t)
                    },
                    _initLayout: function () {
                        var t = this._container, n;
                        this._fadeAnimated = this.options.fadeAnimation && et;
                        r(t, "leaflet-container" + (wt ? " leaflet-touch" : "") + (ei ? " leaflet-retina" : "") + (di ? " leaflet-oldie" : "") + (fh ? " leaflet-safari" : "") + (this._fadeAnimated ? " leaflet-fade-anim" : ""));
                        n = lr(t, "position");
                        "absolute" !== n && "relative" !== n && "fixed" !== n && (t.style.position = "relative");
                        this._initPanes();
                        this._initControlPos && this._initControlPos()
                    },
                    _initPanes: function () {
                        var n = this._panes = {};
                        this._paneRenderers = {};
                        this._mapPane = this.createPane("mapPane", this._container);
                        b(this._mapPane, new t(0, 0));
                        this.createPane("tilePane");
                        this.createPane("shadowPane");
                        this.createPane("overlayPane");
                        this.createPane("markerPane");
                        this.createPane("tooltipPane");
                        this.createPane("popupPane");
                        this.options.markerZoomAnimation || (r(n.markerPane, "leaflet-zoom-hide"), r(n.shadowPane, "leaflet-zoom-hide"))
                    },
                    _resetView: function (n, i) {
                        var u, r;
                        b(this._mapPane, new t(0, 0));
                        u = !this._loaded;
                        this._loaded = !0;
                        i = this._limitZoom(i);
                        this.fire("viewprereset");
                        r = this._zoom !== i;
                        this._moveStart(r, !1)._move(n, i)._moveEnd(r);
                        this.fire("viewreset");
                        u && this.fire("load")
                    },
                    _moveStart: function (n, t) {
                        return n && this.fire("zoomstart"), t || this.fire("movestart"), this
                    },
                    _move: function (n, t, i) {
                        void 0 === t && (t = this._zoom);
                        var r = this._zoom !== t;
                        return this._zoom = t, this._lastCenter = n, this._pixelOrigin = this._getNewPixelOrigin(n), (r || i && i.pinch) && this.fire("zoom", i), this.fire("move", i)
                    },
                    _moveEnd: function (n) {
                        return n && this.fire("zoomend"), this.fire("moveend")
                    },
                    _stop: function () {
                        return nt(this._flyToFrame), this._panAnim && this._panAnim.stop(), this
                    },
                    _rawPanBy: function (n) {
                        b(this._mapPane, this._getMapPanePos().subtract(n))
                    },
                    _getZoomSpan: function () {
                        return this.getMaxZoom() - this.getMinZoom()
                    },
                    _panInsideMaxBounds: function () {
                        this._enforcingBounds || this.panInsideBounds(this.options.maxBounds)
                    },
                    _checkIfLoaded: function () {
                        if (!this._loaded) throw new Error("Set map center and zoom first.");
                    },
                    _initEvents: function (n) {
                        this._targets = {};
                        this._targets[o(this._container)] = this;
                        var t = n ? w : u;
                        t(this._container, "click dblclick mousedown mouseup mouseover mouseout mousemove contextmenu keypress", this._handleDOMEvent, this);
                        this.options.trackResize && t(window, "resize", this._onResize, this);
                        et && this.options.transform3DLimit && (n ? this.off : this.on).call(this, "moveend", this._onMoveEnd)
                    },
                    _onResize: function () {
                        nt(this._resizeRequest);
                        this._resizeRequest = d(function () {
                            this.invalidateSize({debounceMoveend: !0})
                        }, this)
                    },
                    _onScroll: function () {
                        this._container.scrollTop = 0;
                        this._container.scrollLeft = 0
                    },
                    _onMoveEnd: function () {
                        var n = this._getMapPanePos();
                        Math.max(Math.abs(n.x), Math.abs(n.y)) >= this.options.transform3DLimit && this._resetView(this.getCenter(), this.getZoom())
                    },
                    _findEventTargets: function (n, t) {
                        for (var r, u = [], f = "mouseout" === t || "mouseover" === t, i = n.target || n.srcElement, e = !1; i;) {
                            if ((r = this._targets[o(i)]) && ("click" === t || "preclick" === t) && !n._simulated && this._draggableMoved(r)) {
                                e = !0;
                                break
                            }
                            if (r && r.listens(t, !0)) {
                                if (f && !hu(i, n)) break;
                                if (u.push(r), f) break
                            }
                            if (i === this._container) break;
                            i = i.parentNode
                        }
                        return u.length || e || f || !hu(i, n) || (u = [this]), u
                    },
                    _handleDOMEvent: function (n) {
                        if (this._loaded && !df(n)) {
                            var t = n.type;
                            "mousedown" !== t && "keypress" !== t || yf(n.target || n.srcElement);
                            this._fireDOMEvent(n, t)
                        }
                    },
                    _mouseEvents: ["click", "dblclick", "mouseover", "mouseout", "contextmenu"],
                    _fireDOMEvent: function (n, t, i) {
                        var e, u, r, o, f;
                        if ("click" === n.type && (e = s({}, n), e.type = "preclick", this._fireDOMEvent(e, e.type, i)), !n._stopped && (i = (i || []).concat(this._findEventTargets(n, t)), i.length)) for (u = i[0], "contextmenu" === t && u.listens(t, !0) && ft(n), r = {originalEvent: n}, "keypress" !== n.type && (o = u.getLatLng && (!u._radius || u._radius <= 10), r.containerPoint = o ? this.latLngToContainerPoint(u.getLatLng()) : this.mouseEventToContainerPoint(n), r.layerPoint = this.containerPointToLayerPoint(r.containerPoint), r.latlng = o ? u.getLatLng() : this.layerPointToLatLng(r.layerPoint)), f = 0; f < i.length; f++) if (i[f].fire(t, r, !0), r.originalEvent._stopped || !1 === i[f].options.bubblingMouseEvents && -1 !== ts(this._mouseEvents, t)) return
                    },
                    _draggableMoved: function (n) {
                        return n = n.dragging && n.dragging.enabled() ? n : this, n.dragging && n.dragging.moved() || this.boxZoom && this.boxZoom.moved()
                    },
                    _clearHandlers: function () {
                        for (var n = 0, t = this._handlers.length; n < t; n++) this._handlers[n].disable()
                    },
                    whenReady: function (n, t) {
                        return this._loaded ? n.call(t || this, {target: this}) : this.on("load", n, t), this
                    },
                    _getMapPanePos: function () {
                        return ri(this._mapPane) || new t(0, 0)
                    },
                    _moved: function () {
                        var n = this._getMapPanePos();
                        return n && !n.equals([0, 0])
                    },
                    _getTopLeftPoint: function (n, t) {
                        return (n && void 0 !== t ? this._getNewPixelOrigin(n, t) : this.getPixelOrigin()).subtract(this._getMapPanePos())
                    },
                    _getNewPixelOrigin: function (n, t) {
                        var i = this.getSize()._divideBy(2);
                        return this.project(n, t)._subtract(i)._add(this._getMapPanePos())._round()
                    },
                    _latLngToNewLayerPoint: function (n, t, i) {
                        var r = this._getNewPixelOrigin(i, t);
                        return this.project(n, t)._subtract(r)
                    },
                    _latLngBoundsToNewLayerBounds: function (n, t, i) {
                        var r = this._getNewPixelOrigin(i, t);
                        return ot([this.project(n.getSouthWest(), t)._subtract(r), this.project(n.getNorthWest(), t)._subtract(r), this.project(n.getSouthEast(), t)._subtract(r), this.project(n.getNorthEast(), t)._subtract(r)])
                    },
                    _getCenterLayerPoint: function () {
                        return this.containerPointToLayerPoint(this.getSize()._divideBy(2))
                    },
                    _getCenterOffset: function (n) {
                        return this.latLngToLayerPoint(n).subtract(this._getCenterLayerPoint())
                    },
                    _limitCenter: function (n, t, i) {
                        if (!i) return n;
                        var r = this.project(n, t), u = this.getSize().divideBy(2), e = new v(r.subtract(u), r.add(u)),
                            f = this._getBoundsOffset(e, i, t);
                        return f.round().equals([0, 0]) ? n : this.unproject(r.add(f), t)
                    },
                    _limitOffset: function (n, t) {
                        if (!t) return n;
                        var i = this.getPixelBounds(), r = new v(i.min.add(n), i.max.add(n));
                        return n.add(this._getBoundsOffset(r, t))
                    },
                    _getBoundsOffset: function (n, i, r) {
                        var u = ot(this.project(i.getNorthEast(), r), this.project(i.getSouthWest(), r)),
                            f = u.min.subtract(n.min), e = u.max.subtract(n.max);
                        return new t(this._rebound(f.x, -e.x), this._rebound(f.y, -e.y))
                    },
                    _rebound: function (n, t) {
                        return n + t > 0 ? Math.round(n - t) / 2 : Math.max(0, Math.ceil(n)) - Math.max(0, Math.floor(t))
                    },
                    _limitZoom: function (n) {
                        var i = this.getMinZoom(), r = this.getMaxZoom(), t = et ? this.options.zoomSnap : 1;
                        return t && (n = Math.round(n / t) * t), Math.max(i, Math.min(r, n))
                    },
                    _onPanTransitionStep: function () {
                        this.fire("move")
                    },
                    _onPanTransitionEnd: function () {
                        p(this._mapPane, "leaflet-pan-anim");
                        this.fire("moveend")
                    },
                    _tryAnimatedPan: function (n, t) {
                        var i = this._getCenterOffset(n)._trunc();
                        return !(!0 !== (t && t.animate) && !this.getSize().contains(i)) && (this.panBy(i, t), !0)
                    },
                    _createAnimProxy: function () {
                        var n = this._proxy = e("div", "leaflet-proxy leaflet-zoom-animated");
                        this._panes.mapPane.appendChild(n);
                        this.on("zoomanim", function (n) {
                            var t = de, i = this._proxy.style[t];
                            si(this._proxy, this.project(n.center, n.zoom), this.getZoomScale(n.zoom, 1));
                            i === this._proxy.style[t] && this._animatingZoom && this._onZoomTransitionEnd()
                        }, this);
                        this.on("load moveend", function () {
                            var t = this.getCenter(), n = this.getZoom();
                            si(this._proxy, this.project(t, n), this.getZoomScale(n, 1))
                        }, this);
                        this._on("unload", this._destroyAnimProxy, this)
                    },
                    _destroyAnimProxy: function () {
                        a(this._proxy);
                        delete this._proxy
                    },
                    _catchTransitionEnd: function (n) {
                        this._animatingZoom && n.propertyName.indexOf("transform") >= 0 && this._onZoomTransitionEnd()
                    },
                    _nothingToAnimate: function () {
                        return !this._container.getElementsByClassName("leaflet-zoom-animated").length
                    },
                    _tryAnimatedZoom: function (n, t, i) {
                        if (this._animatingZoom) return !0;
                        if (i = i || {}, !this._zoomAnimated || !1 === i.animate || this._nothingToAnimate() || Math.abs(t - this._zoom) > this.options.zoomAnimationThreshold) return !1;
                        var r = this.getZoomScale(t), u = this._getCenterOffset(n)._divideBy(1 - 1 / r);
                        return !(!0 !== i.animate && !this.getSize().contains(u)) && (d(function () {
                            this._moveStart(!0, !1)._animateZoom(n, t, !0)
                        }, this), !0)
                    },
                    _animateZoom: function (n, t, i, u) {
                        this._mapPane && (i && (this._animatingZoom = !0, this._animateToCenter = n, this._animateToZoom = t, r(this._mapPane, "leaflet-zoom-anim")), this.fire("zoomanim", {
                            center: n,
                            zoom: t,
                            noUpdate: u
                        }), setTimeout(c(this._onZoomTransitionEnd, this), 250))
                    },
                    _onZoomTransitionEnd: function () {
                        this._animatingZoom && (this._mapPane && p(this._mapPane, "leaflet-zoom-anim"), this._animatingZoom = !1, this._move(this._animateToCenter, this._animateToZoom), d(function () {
                            this._moveEnd(!0)
                        }, this))
                    }
                }), ct = kt.extend({
                    options: {position: "topright"}, initialize: function (n) {
                        l(this, n)
                    }, getPosition: function () {
                        return this.options.position
                    }, setPosition: function (n) {
                        var t = this._map;
                        return t && t.removeControl(this), this.options.position = n, t && t.addControl(this), this
                    }, getContainer: function () {
                        return this._container
                    }, addTo: function (n) {
                        this.remove();
                        this._map = n;
                        var t = this._container = this.onAdd(n), u = this.getPosition(), i = n._controlCorners[u];
                        return r(t, "leaflet-control"), -1 !== u.indexOf("bottom") ? i.insertBefore(t, i.firstChild) : i.appendChild(t), this
                    }, remove: function () {
                        return this._map ? (a(this._container), this.onRemove && this.onRemove(this._map), this._map = null, this) : this
                    }, _refocusOnMap: function (n) {
                        this._map && n && n.screenX > 0 && n.screenY > 0 && this._map.getContainer().focus()
                    }
                }), gr = function (n) {
                    return new ct(n)
                };
            f.include({
                addControl: function (n) {
                    return n.addTo(this), this
                }, removeControl: function (n) {
                    return n.remove(), this
                }, _initControlPos: function () {
                    function n(n, u) {
                        var f = t + n + " " + t + u;
                        i[n + u] = e("div", f, r)
                    }

                    var i = this._controlCorners = {}, t = "leaflet-",
                        r = this._controlContainer = e("div", t + "control-container", this._container);
                    n("top", "left");
                    n("top", "right");
                    n("bottom", "left");
                    n("bottom", "right")
                }, _clearControlPos: function () {
                    for (var n in this._controlCorners) a(this._controlCorners[n]);
                    a(this._controlContainer);
                    delete this._controlCorners;
                    delete this._controlContainer
                }
            });
            var ph = ct.extend({
                options: {
                    collapsed: !0,
                    position: "topright",
                    autoZIndex: !0,
                    hideSingleBase: !1,
                    sortLayers: !1,
                    sortFunction: function (n, t, i, r) {
                        return i < r ? -1 : r < i ? 1 : 0
                    }
                }, initialize: function (n, t, i) {
                    l(this, i);
                    this._layerControlInputs = [];
                    this._layers = [];
                    this._lastZIndex = 0;
                    this._handlingClick = !1;
                    for (var r in n) this._addLayer(n[r], r);
                    for (r in t) this._addLayer(t[r], r, !0)
                }, onAdd: function (n) {
                    this._initLayout();
                    this._update();
                    this._map = n;
                    n.on("zoomend", this._checkDisabledLayers, this);
                    for (var t = 0; t < this._layers.length; t++) this._layers[t].layer.on("add remove", this._onLayerChange, this);
                    return this._container
                }, addTo: function (n) {
                    return ct.prototype.addTo.call(this, n), this._expandIfNotCollapsed()
                }, onRemove: function () {
                    this._map.off("zoomend", this._checkDisabledLayers, this);
                    for (var n = 0; n < this._layers.length; n++) this._layers[n].layer.off("add remove", this._onLayerChange, this)
                }, addBaseLayer: function (n, t) {
                    return this._addLayer(n, t), this._map ? this._update() : this
                }, addOverlay: function (n, t) {
                    return this._addLayer(n, t, !0), this._map ? this._update() : this
                }, removeLayer: function (n) {
                    n.off("add remove", this._onLayerChange, this);
                    var t = this._getLayer(o(n));
                    return t && this._layers.splice(this._layers.indexOf(t), 1), this._map ? this._update() : this
                }, expand: function () {
                    r(this._container, "leaflet-control-layers-expanded");
                    this._section.style.height = null;
                    var n = this._map.getSize().y - (this._container.offsetTop + 50);
                    return n < this._section.clientHeight ? (r(this._section, "leaflet-control-layers-scrollbar"), this._section.style.height = n + "px") : p(this._section, "leaflet-control-layers-scrollbar"), this._checkDisabledLayers(), this
                }, collapse: function () {
                    return p(this._container, "leaflet-control-layers-expanded"), this
                }, _initLayout: function () {
                    var n = "leaflet-control-layers", t = this._container = e("div", n), f = this.options.collapsed, r,
                        i;
                    t.setAttribute("aria-haspopup", !0);
                    ar(t);
                    bf(t);
                    r = this._section = e("section", n + "-list");
                    f && (this._map.on("click", this.collapse, this), gi || u(t, {
                        mouseenter: this.expand,
                        mouseleave: this.collapse
                    }, this));
                    i = this._layersLink = e("a", n + "-toggle", t);
                    i.href = "#";
                    i.title = "Layers";
                    wt ? (u(i, "click", yt), u(i, "click", this.expand, this)) : u(i, "focus", this.expand, this);
                    f || this.expand();
                    this._baseLayersList = e("div", n + "-base", r);
                    this._separator = e("div", n + "-separator", r);
                    this._overlaysList = e("div", n + "-overlays", r);
                    t.appendChild(r)
                }, _getLayer: function (n) {
                    for (var t = 0; t < this._layers.length; t++) if (this._layers[t] && o(this._layers[t].layer) === n) return this._layers[t]
                }, _addLayer: function (n, t, i) {
                    this._map && n.on("add remove", this._onLayerChange, this);
                    this._layers.push({layer: n, name: t, overlay: i});
                    this.options.sortLayers && this._layers.sort(c(function (n, t) {
                        return this.options.sortFunction(n.layer, t.layer, n.name, t.name)
                    }, this));
                    this.options.autoZIndex && n.setZIndex && (this._lastZIndex++, n.setZIndex(this._lastZIndex));
                    this._expandIfNotCollapsed()
                }, _update: function () {
                    if (!this._container) return this;
                    hf(this._baseLayersList);
                    hf(this._overlaysList);
                    this._layerControlInputs = [];
                    for (var n, r, t, u = 0, i = 0; i < this._layers.length; i++) t = this._layers[i], this._addItem(t), r = r || t.overlay, n = n || !t.overlay, u += t.overlay ? 0 : 1;
                    return this.options.hideSingleBase && (n = n && u > 1, this._baseLayersList.style.display = n ? "" : "none"), this._separator.style.display = r && n ? "" : "none", this
                }, _onLayerChange: function (n) {
                    this._handlingClick || this._update();
                    var t = this._getLayer(o(n.target)),
                        i = t.overlay ? "add" === n.type ? "overlayadd" : "overlayremove" : "add" === n.type ? "baselayerchange" : null;
                    i && this._map.fire(i, t)
                }, _createRadioElement: function (n, t) {
                    var r = '<input type="radio" class="leaflet-control-layers-selector" name="' + n + '"' + (t ? ' checked="checked"' : "") + "/>",
                        i = document.createElement("div");
                    return i.innerHTML = r, i.firstChild
                }, _addItem: function (n) {
                    var t, r = document.createElement("label"), e = this._map.hasLayer(n.layer), f, i;
                    return n.overlay ? (t = document.createElement("input"), t.type = "checkbox", t.className = "leaflet-control-layers-selector", t.defaultChecked = e) : t = this._createRadioElement("leaflet-base-layers", e), this._layerControlInputs.push(t), t.layerId = o(n.layer), u(t, "click", this._onInputClick, this), f = document.createElement("span"), f.innerHTML = " " + n.name, i = document.createElement("div"), r.appendChild(i), i.appendChild(t), i.appendChild(f), (n.overlay ? this._overlaysList : this._baseLayersList).appendChild(r), this._checkDisabledLayers(), r
                }, _onInputClick: function () {
                    var t, u, f = this._layerControlInputs, i = [], r = [], n;
                    for (this._handlingClick = !0, n = f.length - 1; n >= 0; n--) t = f[n], u = this._getLayer(t.layerId).layer, t.checked ? i.push(u) : t.checked || r.push(u);
                    for (n = 0; n < r.length; n++) this._map.hasLayer(r[n]) && this._map.removeLayer(r[n]);
                    for (n = 0; n < i.length; n++) this._map.hasLayer(i[n]) || this._map.addLayer(i[n]);
                    this._handlingClick = !1;
                    this._refocusOnMap()
                }, _checkDisabledLayers: function () {
                    for (var t, n, r = this._layerControlInputs, u = this._map.getZoom(), i = r.length - 1; i >= 0; i--) t = r[i], n = this._getLayer(t.layerId).layer, t.disabled = void 0 !== n.options.minZoom && u < n.options.minZoom || void 0 !== n.options.maxZoom && u > n.options.maxZoom
                }, _expandIfNotCollapsed: function () {
                    return this._map && !this.options.collapsed && this.expand(), this
                }, _expand: function () {
                    return this.expand()
                }, _collapse: function () {
                    return this.collapse()
                }
            }), ra = function (n, t, i) {
                return new ph(n, t, i)
            }, io = ct.extend({
                options: {
                    position: "topleft",
                    zoomInText: "+",
                    zoomInTitle: "Zoom in",
                    zoomOutText: "&#x2212;",
                    zoomOutTitle: "Zoom out"
                }, onAdd: function (n) {
                    var i = "leaflet-control-zoom", r = e("div", i + " leaflet-bar"), t = this.options;
                    return this._zoomInButton = this._createButton(t.zoomInText, t.zoomInTitle, i + "-in", r, this._zoomIn), this._zoomOutButton = this._createButton(t.zoomOutText, t.zoomOutTitle, i + "-out", r, this._zoomOut), this._updateDisabled(), n.on("zoomend zoomlevelschange", this._updateDisabled, this), r
                }, onRemove: function (n) {
                    n.off("zoomend zoomlevelschange", this._updateDisabled, this)
                }, disable: function () {
                    return this._disabled = !0, this._updateDisabled(), this
                }, enable: function () {
                    return this._disabled = !1, this._updateDisabled(), this
                }, _zoomIn: function (n) {
                    !this._disabled && this._map._zoom < this._map.getMaxZoom() && this._map.zoomIn(this._map.options.zoomDelta * (n.shiftKey ? 3 : 1))
                }, _zoomOut: function (n) {
                    !this._disabled && this._map._zoom > this._map.getMinZoom() && this._map.zoomOut(this._map.options.zoomDelta * (n.shiftKey ? 3 : 1))
                }, _createButton: function (n, t, i, r, f) {
                    var o = e("a", i, r);
                    return o.innerHTML = n, o.href = "#", o.title = t, o.setAttribute("role", "button"), o.setAttribute("aria-label", t), ar(o), u(o, "click", yt), u(o, "click", f, this), u(o, "click", this._refocusOnMap, this), o
                }, _updateDisabled: function () {
                    var n = this._map, t = "leaflet-disabled";
                    p(this._zoomInButton, t);
                    p(this._zoomOutButton, t);
                    (this._disabled || n._zoom === n.getMinZoom()) && r(this._zoomOutButton, t);
                    (this._disabled || n._zoom === n.getMaxZoom()) && r(this._zoomInButton, t)
                }
            });
            f.mergeOptions({zoomControl: !0});
            f.addInitHook(function () {
                this.options.zoomControl && (this.zoomControl = new io, this.addControl(this.zoomControl))
            });
            var ua = function (n) {
                return new io(n)
            }, wh = ct.extend({
                options: {position: "bottomleft", maxWidth: 100, metric: !0, imperial: !0}, onAdd: function (n) {
                    var t = e("div", "leaflet-control-scale"), i = this.options;
                    return this._addScales(i, "leaflet-control-scale-line", t), n.on(i.updateWhenIdle ? "moveend" : "move", this._update, this), n.whenReady(this._update, this), t
                }, onRemove: function (n) {
                    n.off(this.options.updateWhenIdle ? "moveend" : "move", this._update, this)
                }, _addScales: function (n, t, i) {
                    n.metric && (this._mScale = e("div", t, i));
                    n.imperial && (this._iScale = e("div", t, i))
                }, _update: function () {
                    var n = this._map, t = n.getSize().y / 2,
                        i = n.distance(n.containerPointToLatLng([0, t]), n.containerPointToLatLng([this.options.maxWidth, t]));
                    this._updateScales(i)
                }, _updateScales: function (n) {
                    this.options.metric && n && this._updateMetric(n);
                    this.options.imperial && n && this._updateImperial(n)
                }, _updateMetric: function (n) {
                    var t = this._getRoundNum(n), i = t < 1e3 ? t + " m" : t / 1e3 + " km";
                    this._updateScale(this._mScale, i, t / n)
                }, _updateImperial: function (n) {
                    var i, r, u, t = 3.2808399 * n;
                    t > 5280 ? (i = t / 5280, r = this._getRoundNum(i), this._updateScale(this._iScale, r + " mi", r / i)) : (u = this._getRoundNum(t), this._updateScale(this._iScale, u + " ft", u / t))
                }, _updateScale: function (n, t, i) {
                    n.style.width = Math.round(this.options.maxWidth * i) + "px";
                    n.innerHTML = t
                }, _getRoundNum: function (n) {
                    var i = Math.pow(10, (Math.floor(n) + "").length - 1), t = n / i;
                    return t = t >= 10 ? 10 : t >= 5 ? 5 : t >= 3 ? 3 : t >= 2 ? 2 : 1, i * t
                }
            }), fa = function (n) {
                return new wh(n)
            }, ro = ct.extend({
                options: {
                    position: "bottomright",
                    prefix: '<a href="http://leafletjs.com" title="A JS library for interactive maps">Leaflet<\/a>'
                }, initialize: function (n) {
                    l(this, n);
                    this._attributions = {}
                }, onAdd: function (n) {
                    n.attributionControl = this;
                    this._container = e("div", "leaflet-control-attribution");
                    ar(this._container);
                    for (var t in n._layers) n._layers[t].getAttribution && this.addAttribution(n._layers[t].getAttribution());
                    return this._update(), this._container
                }, setPrefix: function (n) {
                    return this.options.prefix = n, this._update(), this
                }, addAttribution: function (n) {
                    return n ? (this._attributions[n] || (this._attributions[n] = 0), this._attributions[n]++, this._update(), this) : this
                }, removeAttribution: function (n) {
                    return n ? (this._attributions[n] && (this._attributions[n]--, this._update()), this) : this
                }, _update: function () {
                    var n, i, t;
                    if (this._map) {
                        n = [];
                        for (i in this._attributions) this._attributions[i] && n.push(i);
                        t = [];
                        this.options.prefix && t.push(this.options.prefix);
                        n.length && t.push(n.join(", "));
                        this._container.innerHTML = t.join(" | ")
                    }
                }
            });
            f.mergeOptions({attributionControl: !0});
            f.addInitHook(function () {
                this.options.attributionControl && (new ro).addTo(this)
            });
            bh = function (n) {
                return new ro(n)
            };
            ct.Layers = ph;
            ct.Zoom = io;
            ct.Scale = wh;
            ct.Attribution = ro;
            gr.layers = ra;
            gr.zoom = ua;
            gr.scale = fa;
            gr.attribution = bh;
            lt = kt.extend({
                initialize: function (n) {
                    this._map = n
                }, enable: function () {
                    return this._enabled ? this : (this._enabled = !0, this.addHooks(), this)
                }, disable: function () {
                    return this._enabled ? (this._enabled = !1, this.removeHooks(), this) : this
                }, enabled: function () {
                    return !!this._enabled
                }
            });
            lt.addTo = function (n, t) {
                return n.addHandler(t, this), this
            };
            var kh, ea = {Events: it}, dh = wt ? "touchstart mousedown" : "mousedown",
                gh = {mousedown: "mouseup", touchstart: "touchend", pointerdown: "touchend", MSPointerDown: "touchend"},
                uo = {
                    mousedown: "mousemove",
                    touchstart: "touchmove",
                    pointerdown: "touchmove",
                    MSPointerDown: "touchmove"
                }, li = ki.extend({
                    options: {clickTolerance: 3}, initialize: function (n, t, i, r) {
                        l(this, r);
                        this._element = n;
                        this._dragStartTarget = t || n;
                        this._preventOutline = i
                    }, enable: function () {
                        this._enabled || (u(this._dragStartTarget, dh, this._onDown, this), this._enabled = !0)
                    }, disable: function () {
                        this._enabled && (li._dragging === this && this.finishDrag(), w(this._dragStartTarget, dh, this._onDown, this), this._enabled = !1, this._moved = !1)
                    }, _onDown: function (n) {
                        if (!n._simulated && this._enabled && (this._moved = !1, !cf(this._element, "leaflet-zoom-anim") && !(li._dragging || n.shiftKey || 1 !== n.which && 1 !== n.button && !n.touches || (li._dragging = this, this._preventOutline && yf(this._element), af(), yr(), this._moving)))) {
                            this.fire("down");
                            var i = n.touches ? n.touches[0] : n, r = hs(this._element);
                            this._startPoint = new t(i.clientX, i.clientY);
                            this._parentScale = pf(r);
                            u(document, uo[n.type], this._onMove, this);
                            u(document, gh[n.type], this._onUp, this)
                        }
                    }, _onMove: function (n) {
                        if (!n._simulated && this._enabled) {
                            if (n.touches && n.touches.length > 1) return void (this._moved = !0);
                            var u = n.touches && 1 === n.touches.length ? n.touches[0] : n,
                                i = new t(u.clientX, u.clientY)._subtract(this._startPoint);
                            (i.x || i.y) && (Math.abs(i.x) + Math.abs(i.y) < this.options.clickTolerance || (i.x /= this._parentScale.x, i.y /= this._parentScale.y, ft(n), this._moved || (this.fire("dragstart"), this._moved = !0, this._startPos = ri(this._element).subtract(i), r(document.body, "leaflet-dragging"), this._lastTarget = n.target || n.srcElement, window.SVGElementInstance && this._lastTarget instanceof SVGElementInstance && (this._lastTarget = this._lastTarget.correspondingUseElement), r(this._lastTarget, "leaflet-drag-target")), this._newPos = this._startPos.add(i), this._moving = !0, nt(this._animRequest), this._lastEvent = n, this._animRequest = d(this._updatePosition, this, !0)))
                        }
                    }, _updatePosition: function () {
                        var n = {originalEvent: this._lastEvent};
                        this.fire("predrag", n);
                        b(this._element, this._newPos);
                        this.fire("drag", n)
                    }, _onUp: function (n) {
                        !n._simulated && this._enabled && this.finishDrag()
                    }, finishDrag: function () {
                        p(document.body, "leaflet-dragging");
                        this._lastTarget && (p(this._lastTarget, "leaflet-drag-target"), this._lastTarget = null);
                        for (var n in uo) w(document, uo[n], this._onMove, this), w(document, gh[n], this._onUp, this);
                        vf();
                        pr();
                        this._moved && this._moving && (nt(this._animRequest), this.fire("dragend", {distance: this._newPos.distanceTo(this._startPos)}));
                        this._moving = !1;
                        li._dragging = !1
                    }
                }), oa = (Object.freeze || Object)({
                    simplify: as,
                    pointToSegmentDistance: vs,
                    closestPointOnSegment: kc,
                    clipSegment: ys,
                    _getEdgeIntersection: cu,
                    _getBitCode: ci,
                    _sqClosestPointOnSegment: vr,
                    isFlat: dt,
                    _flat: ps
                }), sa = (Object.freeze || Object)({clipPolygon: ws}), fo = {
                    project: function (n) {
                        return new t(n.lng, n.lat)
                    }, unproject: function (n) {
                        return new h(n.y, n.x)
                    }, bounds: new v([-180, -90], [180, 90])
                }, eo = {
                    R: 6378137,
                    R_MINOR: 6356752.3142451793,
                    bounds: new v([-20037508.34279, -15496570.73972], [20037508.34279, 18764656.23138]),
                    project: function (n) {
                        var u = Math.PI / 180, r = this.R, i = n.lat * u, f = this.R_MINOR / r, e = Math.sqrt(1 - f * f),
                            o = e * Math.sin(i), s = Math.tan(Math.PI / 4 - i / 2) / Math.pow((1 - o) / (1 + o), e / 2);
                        return i = -r * Math.log(Math.max(s, 1e-10)), new t(n.lng * u * r, i)
                    },
                    unproject: function (n) {
                        for (var t, f = 180 / Math.PI, r = this.R, e = this.R_MINOR / r, o = Math.sqrt(1 - e * e), s = Math.exp(-n.y / r), i = Math.PI / 2 - 2 * Math.atan(s), c = 0, u = .1; c < 15 && Math.abs(u) > 1e-7; c++) t = o * Math.sin(i), t = Math.pow((1 - t) / (1 + t), o / 2), u = Math.PI / 2 - 2 * Math.atan(s * t) - i, i += u;
                        return new h(i * f, n.x * f / r)
                    }
                }, ha = (Object.freeze || Object)({LonLat: fo, Mercator: eo, SphericalMercator: fe}), ca = s({}, ui, {
                    code: "EPSG:3395", projection: eo, transformation: function () {
                        var n = .5 / (Math.PI * eo.R);
                        return cr(n, .5, -n, .5)
                    }()
                }), nc = s({}, ui, {code: "EPSG:4326", projection: fo, transformation: cr(1 / 180, 1, -1 / 180, .5)}),
                la = s({}, gt, {
                    projection: fo, transformation: cr(1, 0, -1, 0), scale: function (n) {
                        return Math.pow(2, n)
                    }, zoom: function (n) {
                        return Math.log(n) / Math.LN2
                    }, distance: function (n, t) {
                        var i = t.lng - n.lng, r = t.lat - n.lat;
                        return Math.sqrt(i * i + r * r)
                    }, infinite: !0
                });
            gt.Earth = ui;
            gt.EPSG3395 = ca;
            gt.EPSG3857 = oe;
            gt.EPSG900913 = yl;
            gt.EPSG4326 = nc;
            gt.Simple = la;
            rt = ki.extend({
                options: {pane: "overlayPane", attribution: null, bubblingMouseEvents: !0},
                addTo: function (n) {
                    return n.addLayer(this), this
                },
                remove: function () {
                    return this.removeFrom(this._map || this._mapToAdd)
                },
                removeFrom: function (n) {
                    return n && n.removeLayer(this), this
                },
                getPane: function (n) {
                    return this._map.getPane(n ? this.options[n] || n : this.options.pane)
                },
                addInteractiveTarget: function (n) {
                    return this._map._targets[o(n)] = this, this
                },
                removeInteractiveTarget: function (n) {
                    return delete this._map._targets[o(n)], this
                },
                getAttribution: function () {
                    return this.options.attribution
                },
                _layerAdd: function (n) {
                    var t = n.target, i;
                    t.hasLayer(this) && ((this._map = t, this._zoomAnimated = t._zoomAnimated, this.getEvents) && (i = this.getEvents(), t.on(i, this), this.once("remove", function () {
                        t.off(i, this)
                    }, this)), this.onAdd(t), this.getAttribution && t.attributionControl && t.attributionControl.addAttribution(this.getAttribution()), this.fire("add"), t.fire("layeradd", {layer: this}))
                }
            });
            f.include({
                addLayer: function (n) {
                    if (!n._layerAdd) throw new Error("The provided object is not a Layer.");
                    var t = o(n);
                    return this._layers[t] ? this : (this._layers[t] = n, n._mapToAdd = this, n.beforeAdd && n.beforeAdd(this), this.whenReady(n._layerAdd, n), this)
                }, removeLayer: function (n) {
                    var t = o(n);
                    return this._layers[t] ? (this._loaded && n.onRemove(this), n.getAttribution && this.attributionControl && this.attributionControl.removeAttribution(n.getAttribution()), delete this._layers[t], this._loaded && (this.fire("layerremove", {layer: n}), n.fire("remove")), n._map = n._mapToAdd = null, this) : this
                }, hasLayer: function (n) {
                    return !!n && o(n) in this._layers
                }, eachLayer: function (n, t) {
                    for (var i in this._layers) n.call(t, this._layers[i]);
                    return this
                }, _addLayers: function (n) {
                    n = n ? pt(n) ? n : [n] : [];
                    for (var t = 0, i = n.length; t < i; t++) this.addLayer(n[t])
                }, _addZoomLimit: function (n) {
                    !isNaN(n.options.maxZoom) && isNaN(n.options.minZoom) || (this._zoomBoundLayers[o(n)] = n, this._updateZoomLevels())
                }, _removeZoomLimit: function (n) {
                    var t = o(n);
                    this._zoomBoundLayers[t] && (delete this._zoomBoundLayers[t], this._updateZoomLevels())
                }, _updateZoomLevels: function () {
                    var n = 1 / 0, t = -1 / 0, u = this._getZoomSpan(), r, i;
                    for (r in this._zoomBoundLayers) i = this._zoomBoundLayers[r].options, n = void 0 === i.minZoom ? n : Math.min(n, i.minZoom), t = void 0 === i.maxZoom ? t : Math.max(t, i.maxZoom);
                    this._layersMaxZoom = t === -1 / 0 ? void 0 : t;
                    this._layersMinZoom = n === 1 / 0 ? void 0 : n;
                    u !== this._getZoomSpan() && this.fire("zoomlevelschange");
                    void 0 === this.options.maxZoom && this._layersMaxZoom && this.getZoom() > this._layersMaxZoom && this.setZoom(this._layersMaxZoom);
                    void 0 === this.options.minZoom && this._layersMinZoom && this.getZoom() < this._layersMinZoom && this.setZoom(this._layersMinZoom)
                }
            });
            var ur = rt.extend({
                initialize: function (n, t) {
                    l(this, t);
                    this._layers = {};
                    var i, r;
                    if (n) for (i = 0, r = n.length; i < r; i++) this.addLayer(n[i])
                }, addLayer: function (n) {
                    var t = this.getLayerId(n);
                    return this._layers[t] = n, this._map && this._map.addLayer(n), this
                }, removeLayer: function (n) {
                    var t = n in this._layers ? n : this.getLayerId(n);
                    return this._map && this._layers[t] && this._map.removeLayer(this._layers[t]), delete this._layers[t], this
                }, hasLayer: function (n) {
                    return !!n && (n in this._layers || this.getLayerId(n) in this._layers)
                }, clearLayers: function () {
                    return this.eachLayer(this.removeLayer, this)
                }, invoke: function (n) {
                    var i, t, r = Array.prototype.slice.call(arguments, 1);
                    for (i in this._layers) t = this._layers[i], t[n] && t[n].apply(t, r);
                    return this
                }, onAdd: function (n) {
                    this.eachLayer(n.addLayer, n)
                }, onRemove: function (n) {
                    this.eachLayer(n.removeLayer, n)
                }, eachLayer: function (n, t) {
                    for (var i in this._layers) n.call(t, this._layers[i]);
                    return this
                }, getLayer: function (n) {
                    return this._layers[n]
                }, getLayers: function () {
                    var n = [];
                    return this.eachLayer(n.push, n), n
                }, setZIndex: function (n) {
                    return this.invoke("setZIndex", n)
                }, getLayerId: function (n) {
                    return o(n)
                }
            }), aa = function (n, t) {
                return new ur(n, t)
            }, ai = ur.extend({
                addLayer: function (n) {
                    return this.hasLayer(n) ? this : (n.addEventParent(this), ur.prototype.addLayer.call(this, n), this.fire("layeradd", {layer: n}))
                }, removeLayer: function (n) {
                    return this.hasLayer(n) ? (n in this._layers && (n = this._layers[n]), n.removeEventParent(this), ur.prototype.removeLayer.call(this, n), this.fire("layerremove", {layer: n})) : this
                }, setStyle: function (n) {
                    return this.invoke("setStyle", n)
                }, bringToFront: function () {
                    return this.invoke("bringToFront")
                }, bringToBack: function () {
                    return this.invoke("bringToBack")
                }, getBounds: function () {
                    var t = new tt, i, n;
                    for (i in this._layers) n = this._layers[i], t.extend(n.getBounds ? n.getBounds() : n.getLatLng());
                    return t
                }
            }), va = function (n) {
                return new ai(n)
            }, fr = kt.extend({
                options: {popupAnchor: [0, 0], tooltipAnchor: [0, 0]}, initialize: function (n) {
                    l(this, n)
                }, createIcon: function (n) {
                    return this._createIcon("icon", n)
                }, createShadow: function (n) {
                    return this._createIcon("shadow", n)
                }, _createIcon: function (n, t) {
                    var r = this._getIconUrl(n), i;
                    if (!r) {
                        if ("icon" === n) throw new Error("iconUrl not set in Icon options (see the docs).");
                        return null
                    }
                    return i = this._createImg(r, t && "IMG" === t.tagName ? t : null), this._setIconStyles(i, n), i
                }, _setIconStyles: function (n, t) {
                    var f = this.options, u = f[t + "Size"], r, e;
                    "number" == typeof u && (u = [u, u]);
                    r = i(u);
                    e = i("shadow" === t && f.shadowAnchor || f.iconAnchor || r && r.divideBy(2, !0));
                    n.className = "leaflet-marker-" + t + " " + (f.className || "");
                    e && (n.style.marginLeft = -e.x + "px", n.style.marginTop = -e.y + "px");
                    r && (n.style.width = r.x + "px", n.style.height = r.y + "px")
                }, _createImg: function (n, t) {
                    return t = t || document.createElement("img"), t.src = n, t
                }, _getIconUrl: function (n) {
                    return ei && this.options[n + "RetinaUrl"] || this.options[n + "Url"]
                }
            }), nu = fr.extend({
                options: {
                    iconUrl: "marker-icon.png",
                    iconRetinaUrl: "marker-icon-2x.png",
                    shadowUrl: "marker-shadow.png",
                    iconSize: [25, 41],
                    iconAnchor: [12, 41],
                    popupAnchor: [1, -34],
                    tooltipAnchor: [16, -28],
                    shadowSize: [41, 41]
                }, _getIconUrl: function (n) {
                    return nu.imagePath || (nu.imagePath = this._detectIconPath()), (this.options.imagePath || nu.imagePath) + fr.prototype._getIconUrl.call(this, n)
                }, _detectIconPath: function () {
                    var t = e("div", "leaflet-default-icon-path", document.body),
                        n = lr(t, "background-image") || lr(t, "backgroundImage");
                    return document.body.removeChild(t), n = null === n || 0 !== n.indexOf("url") ? "" : n.replace(/^url\(["']?/, "").replace(/marker-icon\.png["']?\)$/, "")
                }
            }), tc = lt.extend({
                initialize: function (n) {
                    this._marker = n
                }, addHooks: function () {
                    var n = this._marker._icon;
                    this._draggable || (this._draggable = new li(n, n, !0));
                    this._draggable.on({
                        dragstart: this._onDragStart,
                        predrag: this._onPreDrag,
                        drag: this._onDrag,
                        dragend: this._onDragEnd
                    }, this).enable();
                    r(n, "leaflet-marker-draggable")
                }, removeHooks: function () {
                    this._draggable.off({
                        dragstart: this._onDragStart,
                        predrag: this._onPreDrag,
                        drag: this._onDrag,
                        dragend: this._onDragEnd
                    }, this).disable();
                    this._marker._icon && p(this._marker._icon, "leaflet-marker-draggable")
                }, moved: function () {
                    return this._draggable && this._draggable._moved
                }, _adjustPan: function (n) {
                    var e = this._marker, o = e._map, c = this._marker.options.autoPanSpeed,
                        s = this._marker.options.autoPanPadding, u = ri(e._icon), r = o.getPixelBounds(),
                        h = o.getPixelOrigin(), t = ot(r.min._subtract(h).add(s), r.max._subtract(h).subtract(s)), f;
                    t.contains(u) || (f = i((Math.max(t.max.x, u.x) - t.max.x) / (r.max.x - t.max.x) - (Math.min(t.min.x, u.x) - t.min.x) / (r.min.x - t.min.x), (Math.max(t.max.y, u.y) - t.max.y) / (r.max.y - t.max.y) - (Math.min(t.min.y, u.y) - t.min.y) / (r.min.y - t.min.y)).multiplyBy(c), o.panBy(f, {animate: !1}), this._draggable._newPos._add(f), this._draggable._startPos._add(f), b(e._icon, this._draggable._newPos), this._onDrag(n), this._panRequest = d(this._adjustPan.bind(this, n)))
                }, _onDragStart: function () {
                    this._oldLatLng = this._marker.getLatLng();
                    this._marker.closePopup().fire("movestart").fire("dragstart")
                }, _onPreDrag: function (n) {
                    this._marker.options.autoPan && (nt(this._panRequest), this._panRequest = d(this._adjustPan.bind(this, n)))
                }, _onDrag: function (n) {
                    var t = this._marker, i = t._shadow, r = ri(t._icon), u = t._map.layerPointToLatLng(r);
                    i && b(i, r);
                    t._latlng = u;
                    n.latlng = u;
                    n.oldLatLng = this._oldLatLng;
                    t.fire("move", n).fire("drag", n)
                }, _onDragEnd: function (n) {
                    nt(this._panRequest);
                    delete this._oldLatLng;
                    this._marker.fire("moveend").fire("dragend", n)
                }
            }), tu = rt.extend({
                options: {
                    icon: new nu,
                    interactive: !0,
                    keyboard: !0,
                    title: "",
                    alt: "",
                    zIndexOffset: 0,
                    opacity: 1,
                    riseOnHover: !1,
                    riseOffset: 250,
                    pane: "markerPane",
                    bubblingMouseEvents: !1,
                    draggable: !1,
                    autoPan: !1,
                    autoPanPadding: [50, 50],
                    autoPanSpeed: 10
                }, initialize: function (n, t) {
                    l(this, t);
                    this._latlng = y(n)
                }, onAdd: function (n) {
                    this._zoomAnimated = this._zoomAnimated && n.options.markerZoomAnimation;
                    this._zoomAnimated && n.on("zoomanim", this._animateZoom, this);
                    this._initIcon();
                    this.update()
                }, onRemove: function (n) {
                    this.dragging && this.dragging.enabled() && (this.options.draggable = !0, this.dragging.removeHooks());
                    delete this.dragging;
                    this._zoomAnimated && n.off("zoomanim", this._animateZoom, this);
                    this._removeIcon();
                    this._removeShadow()
                }, getEvents: function () {
                    return {zoom: this.update, viewreset: this.update}
                }, getLatLng: function () {
                    return this._latlng
                }, setLatLng: function (n) {
                    var t = this._latlng;
                    return this._latlng = y(n), this.update(), this.fire("move", {oldLatLng: t, latlng: this._latlng})
                }, setZIndexOffset: function (n) {
                    return this.options.zIndexOffset = n, this.update()
                }, setIcon: function (n) {
                    return this.options.icon = n, this._map && (this._initIcon(), this.update()), this._popup && this.bindPopup(this._popup, this._popup.options), this
                }, getElement: function () {
                    return this._icon
                }, update: function () {
                    if (this._icon && this._map) {
                        var n = this._map.latLngToLayerPoint(this._latlng).round();
                        this._setPos(n)
                    }
                    return this
                }, _initIcon: function () {
                    var n = this.options, f = "leaflet-zoom-" + (this._zoomAnimated ? "animated" : "hide"),
                        t = n.icon.createIcon(this._icon), e = !1, i, u;
                    t !== this._icon && (this._icon && this._removeIcon(), e = !0, n.title && (t.title = n.title), "IMG" === t.tagName && (t.alt = n.alt || ""));
                    r(t, f);
                    n.keyboard && (t.tabIndex = "0");
                    this._icon = t;
                    n.riseOnHover && this.on({mouseover: this._bringToFront, mouseout: this._resetZIndex});
                    i = n.icon.createShadow(this._shadow);
                    u = !1;
                    i !== this._shadow && (this._removeShadow(), u = !0);
                    i && (r(i, f), i.alt = "");
                    this._shadow = i;
                    n.opacity < 1 && this._updateOpacity();
                    e && this.getPane().appendChild(this._icon);
                    this._initInteraction();
                    i && u && this.getPane("shadowPane").appendChild(this._shadow)
                }, _removeIcon: function () {
                    this.options.riseOnHover && this.off({mouseover: this._bringToFront, mouseout: this._resetZIndex});
                    a(this._icon);
                    this.removeInteractiveTarget(this._icon);
                    this._icon = null
                }, _removeShadow: function () {
                    this._shadow && a(this._shadow);
                    this._shadow = null
                }, _setPos: function (n) {
                    b(this._icon, n);
                    this._shadow && b(this._shadow, n);
                    this._zIndex = n.y + this.options.zIndexOffset;
                    this._resetZIndex()
                }, _updateZIndex: function (n) {
                    this._icon.style.zIndex = this._zIndex + n
                }, _animateZoom: function (n) {
                    var t = this._map._latLngToNewLayerPoint(this._latlng, n.zoom, n.center).round();
                    this._setPos(t)
                }, _initInteraction: function () {
                    if (this.options.interactive && (r(this._icon, "leaflet-interactive"), this.addInteractiveTarget(this._icon), tc)) {
                        var n = this.options.draggable;
                        this.dragging && (n = this.dragging.enabled(), this.dragging.disable());
                        this.dragging = new tc(this);
                        n && this.dragging.enable()
                    }
                }, setOpacity: function (n) {
                    return this.options.opacity = n, this._map && this._updateOpacity(), this
                }, _updateOpacity: function () {
                    var n = this.options.opacity;
                    ut(this._icon, n);
                    this._shadow && ut(this._shadow, n)
                }, _bringToFront: function () {
                    this._updateZIndex(this.options.riseOffset)
                }, _resetZIndex: function () {
                    this._updateZIndex(0)
                }, _getPopupAnchor: function () {
                    return this.options.icon.options.popupAnchor
                }, _getTooltipAnchor: function () {
                    return this.options.icon.options.tooltipAnchor
                }
            }), oi = rt.extend({
                options: {
                    stroke: !0,
                    color: "#3388ff",
                    weight: 3,
                    opacity: 1,
                    lineCap: "round",
                    lineJoin: "round",
                    dashArray: null,
                    dashOffset: null,
                    fill: !1,
                    fillColor: null,
                    fillOpacity: .2,
                    fillRule: "evenodd",
                    interactive: !0,
                    bubblingMouseEvents: !0
                }, beforeAdd: function (n) {
                    this._renderer = n.getRenderer(this)
                }, onAdd: function () {
                    this._renderer._initPath(this);
                    this._reset();
                    this._renderer._addPath(this)
                }, onRemove: function () {
                    this._renderer._removePath(this)
                }, redraw: function () {
                    return this._map && this._renderer._updatePath(this), this
                }, setStyle: function (n) {
                    return l(this, n), this._renderer && this._renderer._updateStyle(this), this
                }, bringToFront: function () {
                    return this._renderer && this._renderer._bringToFront(this), this
                }, bringToBack: function () {
                    return this._renderer && this._renderer._bringToBack(this), this
                }, getElement: function () {
                    return this._path
                }, _reset: function () {
                    this._project();
                    this._update()
                }, _clickTolerance: function () {
                    return (this.options.stroke ? this.options.weight / 2 : 0) + this._renderer.options.tolerance
                }
            }), tf = oi.extend({
                options: {fill: !0, radius: 10}, initialize: function (n, t) {
                    l(this, t);
                    this._latlng = y(n);
                    this._radius = this.options.radius
                }, setLatLng: function (n) {
                    return this._latlng = y(n), this.redraw(), this.fire("move", {latlng: this._latlng})
                }, getLatLng: function () {
                    return this._latlng
                }, setRadius: function (n) {
                    return this.options.radius = this._radius = n, this.redraw()
                }, getRadius: function () {
                    return this._radius
                }, setStyle: function (n) {
                    var t = n && n.radius || this._radius;
                    return oi.prototype.setStyle.call(this, n), this.setRadius(t), this
                }, _project: function () {
                    this._point = this._map.latLngToLayerPoint(this._latlng);
                    this._updateBounds()
                }, _updateBounds: function () {
                    var n = this._radius, r = this._radiusY || n, t = this._clickTolerance(), i = [n + t, r + t];
                    this._pxBounds = new v(this._point.subtract(i), this._point.add(i))
                }, _update: function () {
                    this._map && this._updatePath()
                }, _updatePath: function () {
                    this._renderer._updateCircle(this)
                }, _empty: function () {
                    return this._radius && !this._renderer._bounds.intersects(this._pxBounds)
                }, _containsPoint: function (n) {
                    return n.distanceTo(this._point) <= this._radius + this._clickTolerance()
                }
            }), oo = tf.extend({
                initialize: function (n, t, i) {
                    if ("number" == typeof t && (t = s({}, i, {radius: t})), l(this, t), this._latlng = y(n), isNaN(this.options.radius)) throw new Error("Circle radius cannot be NaN");
                    this._mRadius = this.options.radius
                }, setRadius: function (n) {
                    return this._mRadius = n, this.redraw()
                }, getRadius: function () {
                    return this._mRadius
                }, getBounds: function () {
                    var n = [this._radius, this._radiusY || this._radius];
                    return new tt(this._map.layerPointToLatLng(this._point.subtract(n)), this._map.layerPointToLatLng(this._point.add(n)))
                }, setStyle: oi.prototype.setStyle, _project: function () {
                    var e = this._latlng.lng, i = this._latlng.lat, n = this._map, o = n.options.crs, c;
                    if (o.distance === ui.distance) {
                        var t = Math.PI / 180, u = this._mRadius / ui.R / t, h = n.project([i + u, e]),
                            l = n.project([i - u, e]), f = h.add(l).divideBy(2), s = n.unproject(f).lat,
                            r = Math.acos((Math.cos(u * t) - Math.sin(i * t) * Math.sin(s * t)) / (Math.cos(i * t) * Math.cos(s * t))) / t;
                        (isNaN(r) || 0 === r) && (r = u / Math.cos(Math.PI / 180 * i));
                        this._point = f.subtract(n.getPixelOrigin());
                        this._radius = isNaN(r) ? 0 : f.x - n.project([s, e - r]).x;
                        this._radiusY = f.y - h.y
                    } else c = o.unproject(o.project(this._latlng).subtract([this._mRadius, 0])), this._point = n.latLngToLayerPoint(this._latlng), this._radius = this._point.x - n.latLngToLayerPoint(c).x;
                    this._updateBounds()
                }
            }), ni = oi.extend({
                options: {smoothFactor: 1, noClip: !1}, initialize: function (n, t) {
                    l(this, t);
                    this._setLatLngs(n)
                }, getLatLngs: function () {
                    return this._latlngs
                }, setLatLngs: function (n) {
                    return this._setLatLngs(n), this.redraw()
                }, isEmpty: function () {
                    return !this._latlngs.length
                }, closestLayerPoint: function (n) {
                    for (var s, r, u, f = 1 / 0, t = null, h = vr, e = 0, c = this._parts.length; e < c; e++) for (var o = this._parts[e], i = 1, l = o.length; i < l; i++) r = o[i - 1], u = o[i], s = h(n, r, u, !0), s < f && (f = s, t = h(n, r, u));
                    return t && (t.distance = Math.sqrt(f)), t
                }, getCenter: function () {
                    if (!this._map) throw new Error("Must add layer to map before using getCenter()");
                    var n, r, f, e, u, t, o, i = this._rings[0], s = i.length;
                    if (!s) return null;
                    for (n = 0, r = 0; n < s - 1; n++) r += i[n].distanceTo(i[n + 1]) / 2;
                    if (0 === r) return this._map.layerPointToLatLng(i[0]);
                    for (n = 0, e = 0; n < s - 1; n++) if (u = i[n], t = i[n + 1], f = u.distanceTo(t), (e += f) > r) return o = (e - r) / f, this._map.layerPointToLatLng([t.x - o * (t.x - u.x), t.y - o * (t.y - u.y)])
                }, getBounds: function () {
                    return this._bounds
                }, addLatLng: function (n, t) {
                    return t = t || this._defaultShape(), n = y(n), t.push(n), this._bounds.extend(n), this.redraw()
                }, _setLatLngs: function (n) {
                    this._bounds = new tt;
                    this._latlngs = this._convertLatLngs(n)
                }, _defaultShape: function () {
                    return dt(this._latlngs) ? this._latlngs : this._latlngs[0]
                }, _convertLatLngs: function (n) {
                    for (var i = [], r = dt(n), t = 0, u = n.length; t < u; t++) r ? (i[t] = y(n[t]), this._bounds.extend(i[t])) : i[t] = this._convertLatLngs(n[t]);
                    return i
                }, _project: function () {
                    var n = new v, i, r;
                    this._rings = [];
                    this._projectLatlngs(this._latlngs, this._rings, n);
                    i = this._clickTolerance();
                    r = new t(i, i);
                    this._bounds.isValid() && n.isValid() && (n.min._subtract(r), n.max._add(r), this._pxBounds = n)
                }, _projectLatlngs: function (n, t, i) {
                    var r, u, e = n[0] instanceof h, f = n.length;
                    if (e) {
                        for (u = [], r = 0; r < f; r++) u[r] = this._map.latLngToLayerPoint(n[r]), i.extend(u[r]);
                        t.push(u)
                    } else for (r = 0; r < f; r++) this._projectLatlngs(n[r], t, i)
                }, _clipPoints: function () {
                    var o = this._renderer._bounds, u, n, t, s, e, f, i, r;
                    if (this._parts = [], this._pxBounds && this._pxBounds.intersects(o)) {
                        if (this.options.noClip) return void (this._parts = this._rings);
                        for (r = this._parts, u = 0, t = 0, s = this._rings.length; u < s; u++) for (i = this._rings[u], n = 0, e = i.length; n < e - 1; n++) (f = ys(i[n], i[n + 1], o, n, !0)) && (r[t] = r[t] || [], r[t].push(f[0]), f[1] === i[n + 1] && n !== e - 2 || (r[t].push(f[1]), t++))
                    }
                }, _simplifyPoints: function () {
                    for (var t = this._parts, i = this.options.smoothFactor, n = 0, r = t.length; n < r; n++) t[n] = as(t[n], i)
                }, _update: function () {
                    this._map && (this._clipPoints(), this._simplifyPoints(), this._updatePath())
                }, _updatePath: function () {
                    this._renderer._updatePoly(this)
                }, _containsPoint: function (n, t) {
                    var r, i, f, o, e, u, s = this._clickTolerance();
                    if (!this._pxBounds || !this._pxBounds.contains(n)) return !1;
                    for (r = 0, o = this._parts.length; r < o; r++) for (u = this._parts[r], i = 0, e = u.length, f = e - 1; i < e; f = i++) if ((t || 0 !== i) && vs(n, u[f], u[i]) <= s) return !0;
                    return !1
                }
            });
            ni._flat = ps;
            var er = ni.extend({
                options: {fill: !0}, isEmpty: function () {
                    return !this._latlngs.length || !this._latlngs[0].length
                }, getCenter: function () {
                    if (!this._map) throw new Error("Must add layer to map before using getCenter()");
                    var r, e, n, t, u, i, o, s, c, f = this._rings[0], h = f.length;
                    if (!h) return null;
                    for (i = o = s = 0, r = 0, e = h - 1; r < h; e = r++) n = f[r], t = f[e], u = n.y * t.x - t.y * n.x, o += (n.x + t.x) * u, s += (n.y + t.y) * u, i += 3 * u;
                    return c = 0 === i ? f[0] : [o / i, s / i], this._map.layerPointToLatLng(c)
                }, _convertLatLngs: function (n) {
                    var t = ni.prototype._convertLatLngs.call(this, n), i = t.length;
                    return i >= 2 && t[0] instanceof h && t[0].equals(t[i - 1]) && t.pop(), t
                }, _setLatLngs: function (n) {
                    ni.prototype._setLatLngs.call(this, n);
                    dt(this._latlngs) && (this._latlngs = [this._latlngs])
                }, _defaultShape: function () {
                    return dt(this._latlngs[0]) ? this._latlngs[0] : this._latlngs[0][0]
                }, _clipPoints: function () {
                    var n = this._renderer._bounds, u = this.options.weight, f = new t(u, u), r, i, e;
                    if (n = new v(n.min.subtract(f), n.max.add(f)), this._parts = [], this._pxBounds && this._pxBounds.intersects(n)) {
                        if (this.options.noClip) return void (this._parts = this._rings);
                        for (i = 0, e = this._rings.length; i < e; i++) r = ws(this._rings[i], n, !0), r.length && this._parts.push(r)
                    }
                }, _updatePath: function () {
                    this._renderer._updatePoly(this, !0)
                }, _containsPoint: function (n) {
                    var i, t, r, u, f, e, h, o, s = !1;
                    if (!this._pxBounds || !this._pxBounds.contains(n)) return !1;
                    for (u = 0, h = this._parts.length; u < h; u++) for (i = this._parts[u], f = 0, o = i.length, e = o - 1; f < o; e = f++) t = i[f], r = i[e], t.y > n.y != r.y > n.y && n.x < (r.x - t.x) * (n.y - t.y) / (r.y - t.y) + t.x && (s = !s);
                    return s || ni.prototype._containsPoint.call(this, n, !0)
                }
            }), ti = ai.extend({
                initialize: function (n, t) {
                    l(this, t);
                    this._layers = {};
                    n && this.addData(n)
                }, addData: function (n) {
                    var u, e, i, f = pt(n) ? n : n.features, r, t;
                    if (f) {
                        for (u = 0, e = f.length; u < e; u++) i = f[u], (i.geometries || i.geometry || i.features || i.coordinates) && this.addData(i);
                        return this
                    }
                    return (r = this.options, r.filter && !r.filter(n)) ? this : (t = ne(n, r), t ? (t.feature = vu(n), t.defaultOptions = t.options, this.resetStyle(t), r.onEachFeature && r.onEachFeature(n, t), this.addLayer(t)) : this)
                }, resetStyle: function (n) {
                    return n.options = s({}, n.defaultOptions), this._setLayerStyle(n, this.options.style), this
                }, setStyle: function (n) {
                    return this.eachLayer(function (t) {
                        this._setLayerStyle(t, n)
                    }, this)
                }, _setLayerStyle: function (n, t) {
                    "function" == typeof t && (t = t(n.feature));
                    n.setStyle && n.setStyle(t)
                }
            }), so = {
                toGeoJSON: function (n) {
                    return bi(this, {type: "Point", coordinates: ie(this.getLatLng(), n)})
                }
            };
            tu.include(so);
            oo.include(so);
            tf.include(so);
            ni.include({
                toGeoJSON: function (n) {
                    var t = !dt(this._latlngs), i = au(this._latlngs, t ? 1 : 0, !1, n);
                    return bi(this, {type: (t ? "Multi" : "") + "LineString", coordinates: i})
                }
            });
            er.include({
                toGeoJSON: function (n) {
                    var t = !dt(this._latlngs), r = t && !dt(this._latlngs[0]),
                        i = au(this._latlngs, r ? 2 : t ? 1 : 0, !0, n);
                    return t || (i = [i]), bi(this, {type: (r ? "Multi" : "") + "Polygon", coordinates: i})
                }
            });
            ur.include({
                toMultiPoint: function (n) {
                    var t = [];
                    return this.eachLayer(function (i) {
                        t.push(i.toGeoJSON(n).geometry.coordinates)
                    }), bi(this, {type: "MultiPoint", coordinates: t})
                }, toGeoJSON: function (n) {
                    var r = this.feature && this.feature.geometry && this.feature.geometry.type, i, t;
                    return "MultiPoint" === r ? this.toMultiPoint(n) : (i = "GeometryCollection" === r, t = [], this.eachLayer(function (r) {
                        var f, u;
                        r.toGeoJSON && (f = r.toGeoJSON(n), i ? t.push(f.geometry) : (u = vu(f), "FeatureCollection" === u.type ? t.push.apply(t, u.features) : t.push(u)))
                    }), i ? bi(this, {geometries: t, type: "GeometryCollection"}) : {
                        type: "FeatureCollection",
                        features: t
                    })
                }
            });
            var ya = bs, ho = rt.extend({
                options: {
                    opacity: 1,
                    alt: "",
                    interactive: !1,
                    crossOrigin: !1,
                    errorOverlayUrl: "",
                    zIndex: 1,
                    className: ""
                }, initialize: function (n, t, i) {
                    this._url = n;
                    this._bounds = k(t);
                    l(this, i)
                }, onAdd: function () {
                    this._image || (this._initImage(), this.options.opacity < 1 && this._updateOpacity());
                    this.options.interactive && (r(this._image, "leaflet-interactive"), this.addInteractiveTarget(this._image));
                    this.getPane().appendChild(this._image);
                    this._reset()
                }, onRemove: function () {
                    a(this._image);
                    this.options.interactive && this.removeInteractiveTarget(this._image)
                }, setOpacity: function (n) {
                    return this.options.opacity = n, this._image && this._updateOpacity(), this
                }, setStyle: function (n) {
                    return n.opacity && this.setOpacity(n.opacity), this
                }, bringToFront: function () {
                    return this._map && pi(this._image), this
                }, bringToBack: function () {
                    return this._map && wi(this._image), this
                }, setUrl: function (n) {
                    return this._url = n, this._image && (this._image.src = n), this
                }, setBounds: function (n) {
                    return this._bounds = k(n), this._map && this._reset(), this
                }, getEvents: function () {
                    var n = {zoom: this._reset, viewreset: this._reset};
                    return this._zoomAnimated && (n.zoomanim = this._animateZoom), n
                }, setZIndex: function (n) {
                    return this.options.zIndex = n, this._updateZIndex(), this
                }, getBounds: function () {
                    return this._bounds
                }, getElement: function () {
                    return this._image
                }, _initImage: function () {
                    var t = "IMG" === this._url.tagName, n = this._image = t ? this._url : e("img");
                    if (r(n, "leaflet-image-layer"), this._zoomAnimated && r(n, "leaflet-zoom-animated"), this.options.className && r(n, this.options.className), n.onselectstart = g, n.onmousemove = g, n.onload = c(this.fire, this, "load"), n.onerror = c(this._overlayOnError, this, "error"), (this.options.crossOrigin || "" === this.options.crossOrigin) && (n.crossOrigin = !0 === this.options.crossOrigin ? "" : this.options.crossOrigin), this.options.zIndex && this._updateZIndex(), t) return void (this._url = n.src);
                    n.src = this._url;
                    n.alt = this.options.alt
                }, _animateZoom: function (n) {
                    var t = this._map.getZoomScale(n.zoom),
                        i = this._map._latLngBoundsToNewLayerBounds(this._bounds, n.zoom, n.center).min;
                    si(this._image, i, t)
                }, _reset: function () {
                    var n = this._image,
                        t = new v(this._map.latLngToLayerPoint(this._bounds.getNorthWest()), this._map.latLngToLayerPoint(this._bounds.getSouthEast())),
                        i = t.getSize();
                    b(n, t.min);
                    n.style.width = i.x + "px";
                    n.style.height = i.y + "px"
                }, _updateOpacity: function () {
                    ut(this._image, this.options.opacity)
                }, _updateZIndex: function () {
                    this._image && void 0 !== this.options.zIndex && null !== this.options.zIndex && (this._image.style.zIndex = this.options.zIndex)
                }, _overlayOnError: function () {
                    this.fire("error");
                    var n = this.options.errorOverlayUrl;
                    n && this._url !== n && (this._url = n, this._image.src = n)
                }
            }), pa = function (n, t, i) {
                return new ho(n, t, i)
            }, ic = ho.extend({
                options: {autoplay: !0, loop: !0}, _initImage: function () {
                    var o = "VIDEO" === this._url.tagName, n = this._image = o ? this._url : e("video"), t, f;
                    if (r(n, "leaflet-image-layer"), this._zoomAnimated && r(n, "leaflet-zoom-animated"), n.onselectstart = g, n.onmousemove = g, n.onloadeddata = c(this.fire, this, "load"), o) {
                        for (var i = n.getElementsByTagName("source"), s = [], u = 0; u < i.length; u++) s.push(i[u].src);
                        return void (this._url = i.length > 0 ? s : [n.src])
                    }
                    for (pt(this._url) || (this._url = [this._url]), n.autoplay = !!this.options.autoplay, n.loop = !!this.options.loop, t = 0; t < this._url.length; t++) f = e("source"), f.src = this._url[t], n.appendChild(f)
                }
            }), ii = rt.extend({
                options: {offset: [0, 7], className: "", pane: "popupPane"}, initialize: function (n, t) {
                    l(this, n);
                    this._source = t
                }, onAdd: function (n) {
                    this._zoomAnimated = n._zoomAnimated;
                    this._container || this._initLayout();
                    n._fadeAnimated && ut(this._container, 0);
                    clearTimeout(this._removeTimeout);
                    this.getPane().appendChild(this._container);
                    this.update();
                    n._fadeAnimated && ut(this._container, 1);
                    this.bringToFront()
                }, onRemove: function (n) {
                    n._fadeAnimated ? (ut(this._container, 0), this._removeTimeout = setTimeout(c(a, void 0, this._container), 200)) : a(this._container)
                }, getLatLng: function () {
                    return this._latlng
                }, setLatLng: function (n) {
                    return this._latlng = y(n), this._map && (this._updatePosition(), this._adjustPan()), this
                }, getContent: function () {
                    return this._content
                }, setContent: function (n) {
                    return this._content = n, this.update(), this
                }, getElement: function () {
                    return this._container
                }, update: function () {
                    this._map && (this._container.style.visibility = "hidden", this._updateContent(), this._updateLayout(), this._updatePosition(), this._container.style.visibility = "", this._adjustPan())
                }, getEvents: function () {
                    var n = {zoom: this._updatePosition, viewreset: this._updatePosition};
                    return this._zoomAnimated && (n.zoomanim = this._animateZoom), n
                }, isOpen: function () {
                    return !!this._map && this._map.hasLayer(this)
                }, bringToFront: function () {
                    return this._map && pi(this._container), this
                }, bringToBack: function () {
                    return this._map && wi(this._container), this
                }, _updateContent: function () {
                    if (this._content) {
                        var n = this._contentNode,
                            t = "function" == typeof this._content ? this._content(this._source || this) : this._content;
                        if ("string" == typeof t) n.innerHTML = t; else {
                            for (; n.hasChildNodes();) n.removeChild(n.firstChild);
                            n.appendChild(t)
                        }
                        this.fire("contentupdate")
                    }
                }, _updatePosition: function () {
                    var u, f;
                    if (this._map) {
                        var t = this._map.latLngToLayerPoint(this._latlng), n = i(this.options.offset),
                            r = this._getAnchor();
                        this._zoomAnimated ? b(this._container, t.add(r)) : n = n.add(t).add(r);
                        u = this._containerBottom = -n.y;
                        f = this._containerLeft = -Math.round(this._containerWidth / 2) + n.x;
                        this._container.style.bottom = u + "px";
                        this._container.style.left = f + "px"
                    }
                }, _getAnchor: function () {
                    return [0, 0]
                }
            }), or = ii.extend({
                options: {
                    maxWidth: 300,
                    minWidth: 50,
                    maxHeight: null,
                    autoPan: !0,
                    autoPanPaddingTopLeft: null,
                    autoPanPaddingBottomRight: null,
                    autoPanPadding: [5, 5],
                    keepInView: !1,
                    closeButton: !0,
                    autoClose: !0,
                    closeOnEscapeKey: !0,
                    className: ""
                }, openOn: function (n) {
                    return n.openPopup(this), this
                }, onAdd: function (n) {
                    ii.prototype.onAdd.call(this, n);
                    n.fire("popupopen", {popup: this});
                    this._source && (this._source.fire("popupopen", {popup: this}, !0), this._source instanceof oi || this._source.on("preclick", hi))
                }, onRemove: function (n) {
                    ii.prototype.onRemove.call(this, n);
                    n.fire("popupclose", {popup: this});
                    this._source && (this._source.fire("popupclose", {popup: this}, !0), this._source instanceof oi || this._source.off("preclick", hi))
                }, getEvents: function () {
                    var n = ii.prototype.getEvents.call(this);
                    return (void 0 !== this.options.closeOnClick ? this.options.closeOnClick : this._map.options.closePopupOnClick) && (n.preclick = this._close), this.options.keepInView && (n.moveend = this._adjustPan), n
                }, _close: function () {
                    this._map && this._map.closePopup(this)
                }, _initLayout: function () {
                    var n = "leaflet-popup",
                        i = this._container = e("div", n + " " + (this.options.className || "") + " leaflet-zoom-animated"),
                        r = this._wrapper = e("div", n + "-content-wrapper", i), t;
                    (this._contentNode = e("div", n + "-content", r), ar(r), bf(this._contentNode), u(r, "contextmenu", hi), this._tipContainer = e("div", n + "-tip-container", i), this._tip = e("div", n + "-tip", this._tipContainer), this.options.closeButton) && (t = this._closeButton = e("a", n + "-close-button", i), t.href = "#close", t.innerHTML = "&#215;", u(t, "click", this._onCloseButtonClick, this))
                }, _updateLayout: function () {
                    var i = this._contentNode, n = i.style, t, f, u;
                    n.width = "";
                    n.whiteSpace = "nowrap";
                    t = i.offsetWidth;
                    t = Math.min(t, this.options.maxWidth);
                    t = Math.max(t, this.options.minWidth);
                    n.width = t + 1 + "px";
                    n.whiteSpace = "";
                    n.height = "";
                    f = i.offsetHeight;
                    u = this.options.maxHeight;
                    u && f > u ? (n.height = u + "px", r(i, "leaflet-popup-scrolled")) : p(i, "leaflet-popup-scrolled");
                    this._containerWidth = this._container.offsetWidth
                }, _animateZoom: function (n) {
                    var t = this._map._latLngToNewLayerPoint(this._latlng, n.zoom, n.center), i = this._getAnchor();
                    b(this._container, t.add(i))
                }, _adjustPan: function () {
                    if (this.options.autoPan) {
                        this._map._panAnim && this._map._panAnim.stop();
                        var s = this._map, v = parseInt(lr(this._container, "marginBottom"), 10) || 0,
                            h = this._container.offsetHeight + v, c = this._containerWidth,
                            l = new t(this._containerLeft, -h - this._containerBottom);
                        l._add(ri(this._container));
                        var n = s.layerPointToContainerPoint(l), a = i(this.options.autoPanPadding),
                            f = i(this.options.autoPanPaddingTopLeft || a),
                            e = i(this.options.autoPanPaddingBottomRight || a), o = s.getSize(), r = 0, u = 0;
                        n.x + c + e.x > o.x && (r = n.x + c - o.x + e.x);
                        n.x - r - f.x < 0 && (r = n.x - f.x);
                        n.y + h + e.y > o.y && (u = n.y + h - o.y + e.y);
                        n.y - u - f.y < 0 && (u = n.y - f.y);
                        (r || u) && s.fire("autopanstart").panBy([r, u])
                    }
                }, _onCloseButtonClick: function (n) {
                    this._close();
                    yt(n)
                }, _getAnchor: function () {
                    return i(this._source && this._source._getPopupAnchor ? this._source._getPopupAnchor() : [0, 0])
                }
            }), wa = function (n, t) {
                return new or(n, t)
            };
            f.mergeOptions({closePopupOnClick: !0});
            f.include({
                openPopup: function (n, t, i) {
                    return n instanceof or || (n = new or(i).setContent(n)), t && n.setLatLng(t), this.hasLayer(n) ? this : (this._popup && this._popup.options.autoClose && this.closePopup(), this._popup = n, this.addLayer(n))
                }, closePopup: function (n) {
                    return n && n !== this._popup || (n = this._popup, this._popup = null), n && this.removeLayer(n), this
                }
            });
            rt.include({
                bindPopup: function (n, t) {
                    return n instanceof or ? (l(n, t), this._popup = n, n._source = this) : (this._popup && !t || (this._popup = new or(t, this)), this._popup.setContent(n)), this._popupHandlersAdded || (this.on({
                        click: this._openPopup,
                        keypress: this._onKeyPress,
                        remove: this.closePopup,
                        move: this._movePopup
                    }), this._popupHandlersAdded = !0), this
                }, unbindPopup: function () {
                    return this._popup && (this.off({
                        click: this._openPopup,
                        keypress: this._onKeyPress,
                        remove: this.closePopup,
                        move: this._movePopup
                    }), this._popupHandlersAdded = !1, this._popup = null), this
                }, openPopup: function (n, t) {
                    if (n instanceof rt || (t = n, n = this), n instanceof ai) for (var i in this._layers) {
                        n = this._layers[i];
                        break
                    }
                    return t || (t = n.getCenter ? n.getCenter() : n.getLatLng()), this._popup && this._map && (this._popup._source = n, this._popup.update(), this._map.openPopup(this._popup, t)), this
                }, closePopup: function () {
                    return this._popup && this._popup._close(), this
                }, togglePopup: function (n) {
                    return this._popup && (this._popup._map ? this.closePopup() : this.openPopup(n)), this
                }, isPopupOpen: function () {
                    return !!this._popup && this._popup.isOpen()
                }, setPopupContent: function (n) {
                    return this._popup && this._popup.setContent(n), this
                }, getPopup: function () {
                    return this._popup
                }, _openPopup: function (n) {
                    var t = n.layer || n.target;
                    if (this._popup && this._map) {
                        if (yt(n), t instanceof oi) return void this.openPopup(n.layer || n.target, n.latlng);
                        this._map.hasLayer(this._popup) && this._popup._source === t ? this.closePopup() : this.openPopup(t, n.latlng)
                    }
                }, _movePopup: function (n) {
                    this._popup.setLatLng(n.latlng)
                }, _onKeyPress: function (n) {
                    13 === n.originalEvent.keyCode && this._openPopup(n)
                }
            });
            vi = ii.extend({
                options: {
                    pane: "tooltipPane",
                    offset: [0, 0],
                    direction: "auto",
                    permanent: !1,
                    sticky: !1,
                    interactive: !1,
                    opacity: .9
                }, onAdd: function (n) {
                    ii.prototype.onAdd.call(this, n);
                    this.setOpacity(this.options.opacity);
                    n.fire("tooltipopen", {tooltip: this});
                    this._source && this._source.fire("tooltipopen", {tooltip: this}, !0)
                }, onRemove: function (n) {
                    ii.prototype.onRemove.call(this, n);
                    n.fire("tooltipclose", {tooltip: this});
                    this._source && this._source.fire("tooltipclose", {tooltip: this}, !0)
                }, getEvents: function () {
                    var n = ii.prototype.getEvents.call(this);
                    return wt && !this.options.permanent && (n.preclick = this._close), n
                }, _close: function () {
                    this._map && this._map.closeTooltip(this)
                }, _initLayout: function () {
                    var n = "leaflet-tooltip " + (this.options.className || "") + " leaflet-zoom-" + (this._zoomAnimated ? "animated" : "hide");
                    this._contentNode = this._container = e("div", n)
                }, _updateLayout: function () {
                }, _adjustPan: function () {
                }, _setPosition: function (n) {
                    var h = this._map, u = this._container, c = h.latLngToContainerPoint(h.getCenter()),
                        l = h.layerPointToContainerPoint(n), f = this.options.direction, o = u.offsetWidth,
                        s = u.offsetHeight, t = i(this.options.offset), e = this._getAnchor();
                    "top" === f ? n = n.add(i(-o / 2 + t.x, -s + t.y + e.y, !0)) : "bottom" === f ? n = n.subtract(i(o / 2 - t.x, -t.y, !0)) : "center" === f ? n = n.subtract(i(o / 2 + t.x, s / 2 - e.y + t.y, !0)) : "right" === f || "auto" === f && l.x < c.x ? (f = "right", n = n.add(i(t.x + e.x, e.y - s / 2 + t.y, !0))) : (f = "left", n = n.subtract(i(o + e.x - t.x, s / 2 - e.y - t.y, !0)));
                    p(u, "leaflet-tooltip-right");
                    p(u, "leaflet-tooltip-left");
                    p(u, "leaflet-tooltip-top");
                    p(u, "leaflet-tooltip-bottom");
                    r(u, "leaflet-tooltip-" + f);
                    b(u, n)
                }, _updatePosition: function () {
                    var n = this._map.latLngToLayerPoint(this._latlng);
                    this._setPosition(n)
                }, setOpacity: function (n) {
                    this.options.opacity = n;
                    this._container && ut(this._container, n)
                }, _animateZoom: function (n) {
                    var t = this._map._latLngToNewLayerPoint(this._latlng, n.zoom, n.center);
                    this._setPosition(t)
                }, _getAnchor: function () {
                    return i(this._source && this._source._getTooltipAnchor && !this.options.sticky ? this._source._getTooltipAnchor() : [0, 0])
                }
            });
            rc = function (n, t) {
                return new vi(n, t)
            };
            f.include({
                openTooltip: function (n, t, i) {
                    return n instanceof vi || (n = new vi(i).setContent(n)), t && n.setLatLng(t), this.hasLayer(n) ? this : this.addLayer(n)
                }, closeTooltip: function (n) {
                    return n && this.removeLayer(n), this
                }
            });
            rt.include({
                bindTooltip: function (n, t) {
                    return n instanceof vi ? (l(n, t), this._tooltip = n, n._source = this) : (this._tooltip && !t || (this._tooltip = new vi(t, this)), this._tooltip.setContent(n)), this._initTooltipInteractions(), this._tooltip.options.permanent && this._map && this._map.hasLayer(this) && this.openTooltip(), this
                }, unbindTooltip: function () {
                    return this._tooltip && (this._initTooltipInteractions(!0), this.closeTooltip(), this._tooltip = null), this
                }, _initTooltipInteractions: function (n) {
                    if (n || !this._tooltipHandlersAdded) {
                        var i = n ? "off" : "on", t = {remove: this.closeTooltip, move: this._moveTooltip};
                        this._tooltip.options.permanent ? t.add = this._openTooltip : (t.mouseover = this._openTooltip, t.mouseout = this.closeTooltip, this._tooltip.options.sticky && (t.mousemove = this._moveTooltip), wt && (t.click = this._openTooltip));
                        this[i](t);
                        this._tooltipHandlersAdded = !n
                    }
                }, openTooltip: function (n, t) {
                    if (n instanceof rt || (t = n, n = this), n instanceof ai) for (var i in this._layers) {
                        n = this._layers[i];
                        break
                    }
                    return t || (t = n.getCenter ? n.getCenter() : n.getLatLng()), this._tooltip && this._map && (this._tooltip._source = n, this._tooltip.update(), this._map.openTooltip(this._tooltip, t), this._tooltip.options.interactive && this._tooltip._container && (r(this._tooltip._container, "leaflet-clickable"), this.addInteractiveTarget(this._tooltip._container))), this
                }, closeTooltip: function () {
                    return this._tooltip && (this._tooltip._close(), this._tooltip.options.interactive && this._tooltip._container && (p(this._tooltip._container, "leaflet-clickable"), this.removeInteractiveTarget(this._tooltip._container))), this
                }, toggleTooltip: function (n) {
                    return this._tooltip && (this._tooltip._map ? this.closeTooltip() : this.openTooltip(n)), this
                }, isTooltipOpen: function () {
                    return this._tooltip.isOpen()
                }, setTooltipContent: function (n) {
                    return this._tooltip && this._tooltip.setContent(n), this
                }, getTooltip: function () {
                    return this._tooltip
                }, _openTooltip: function (n) {
                    var t = n.layer || n.target;
                    this._tooltip && this._map && this.openTooltip(t, this._tooltip.options.sticky ? n.latlng : void 0)
                }, _moveTooltip: function (n) {
                    var t, i, r = n.latlng;
                    this._tooltip.options.sticky && n.originalEvent && (t = this._map.mouseEventToContainerPoint(n.originalEvent), i = this._map.containerPointToLayerPoint(t), r = this._map.layerPointToLatLng(i));
                    this._tooltip.setLatLng(r)
                }
            });
            co = fr.extend({
                options: {iconSize: [12, 12], html: !1, bgPos: null, className: "leaflet-div-icon"},
                createIcon: function (n) {
                    var t = n && "DIV" === n.tagName ? n : document.createElement("div"), r = this.options, u;
                    return (t.innerHTML = !1 !== r.html ? r.html : "", r.bgPos) && (u = i(r.bgPos), t.style.backgroundPosition = -u.x + "px " + -u.y + "px"), this._setIconStyles(t, "icon"), t
                },
                createShadow: function () {
                    return null
                }
            });
            fr.Default = nu;
            var iu = rt.extend({
                options: {
                    tileSize: 256,
                    opacity: 1,
                    updateWhenIdle: nr,
                    updateWhenZooming: !0,
                    updateInterval: 200,
                    zIndex: 1,
                    bounds: null,
                    minZoom: 0,
                    maxZoom: void 0,
                    maxNativeZoom: void 0,
                    minNativeZoom: void 0,
                    noWrap: !1,
                    pane: "tilePane",
                    className: "",
                    keepBuffer: 2
                }, initialize: function (n) {
                    l(this, n)
                }, onAdd: function () {
                    this._initContainer();
                    this._levels = {};
                    this._tiles = {};
                    this._resetView();
                    this._update()
                }, beforeAdd: function (n) {
                    n._addZoomLimit(this)
                }, onRemove: function (n) {
                    this._removeAllTiles();
                    a(this._container);
                    n._removeZoomLimit(this);
                    this._container = null;
                    this._tileZoom = void 0
                }, bringToFront: function () {
                    return this._map && (pi(this._container), this._setAutoZIndex(Math.max)), this
                }, bringToBack: function () {
                    return this._map && (wi(this._container), this._setAutoZIndex(Math.min)), this
                }, getContainer: function () {
                    return this._container
                }, setOpacity: function (n) {
                    return this.options.opacity = n, this._updateOpacity(), this
                }, setZIndex: function (n) {
                    return this.options.zIndex = n, this._updateZIndex(), this
                }, isLoading: function () {
                    return this._loading
                }, redraw: function () {
                    return this._map && (this._removeAllTiles(), this._update()), this
                }, getEvents: function () {
                    var n = {
                        viewprereset: this._invalidateAll,
                        viewreset: this._resetView,
                        zoom: this._resetView,
                        moveend: this._onMoveEnd
                    };
                    return this.options.updateWhenIdle || (this._onMove || (this._onMove = uf(this._onMoveEnd, this.options.updateInterval, this)), n.move = this._onMove), this._zoomAnimated && (n.zoomanim = this._animateZoom), n
                }, createTile: function () {
                    return document.createElement("div")
                }, getTileSize: function () {
                    var n = this.options.tileSize;
                    return n instanceof t ? n : new t(n, n)
                }, _updateZIndex: function () {
                    this._container && void 0 !== this.options.zIndex && null !== this.options.zIndex && (this._container.style.zIndex = this.options.zIndex)
                }, _setAutoZIndex: function (n) {
                    for (var r, u = this.getPane().children, t = -n(-1 / 0, 1 / 0), i = 0, f = u.length; i < f; i++) r = u[i].style.zIndex, u[i] !== this._container && r && (t = n(t, +r));
                    isFinite(t) && (this.options.zIndex = t + n(-1, 1), this._updateZIndex())
                }, _updateOpacity: function () {
                    var u, n, t;
                    if (this._map && !di) {
                        ut(this._container, this.options.opacity);
                        var f = +new Date, i = !1, r = !1;
                        for (u in this._tiles) n = this._tiles[u], n.current && n.loaded && (t = Math.min(1, (f - n.loaded) / 200), ut(n.el, t), t < 1 ? i = !0 : (n.active ? r = !0 : this._onOpaqueTile(n), n.active = !0));
                        r && !this._noPrune && this._pruneTiles();
                        i && (nt(this._fadeFrame), this._fadeFrame = d(this._updateOpacity, this))
                    }
                }, _onOpaqueTile: g, _initContainer: function () {
                    this._container || (this._container = e("div", "leaflet-layer " + (this.options.className || "")), this._updateZIndex(), this.options.opacity < 1 && this._updateOpacity(), this.getPane().appendChild(this._container))
                }, _updateLevels: function () {
                    var i = this._tileZoom, u = this.options.maxZoom, t, n, r;
                    if (void 0 !== i) {
                        for (t in this._levels) this._levels[t].el.children.length || t === i ? (this._levels[t].el.style.zIndex = u - Math.abs(i - t), this._onUpdateLevel(t)) : (a(this._levels[t].el), this._removeTilesAtZoom(t), this._onRemoveLevel(t), delete this._levels[t]);
                        return n = this._levels[i], r = this._map, n || (n = this._levels[i] = {}, n.el = e("div", "leaflet-tile-container leaflet-zoom-animated", this._container), n.el.style.zIndex = u, n.origin = r.project(r.unproject(r.getPixelOrigin()), i).round(), n.zoom = i, this._setZoomTransform(n, r.getCenter(), r.getZoom()), n.el.offsetWidth, this._onCreateLevel(n)), this._level = n, n
                    }
                }, _onUpdateLevel: g, _onRemoveLevel: g, _onCreateLevel: g, _pruneTiles: function () {
                    var t, i, r, n;
                    if (this._map) {
                        if (r = this._map.getZoom(), r > this.options.maxZoom || r < this.options.minZoom) return void this._removeAllTiles();
                        for (t in this._tiles) i = this._tiles[t], i.retain = i.current;
                        for (t in this._tiles) (i = this._tiles[t], i.current && !i.active) && (n = i.coords, this._retainParent(n.x, n.y, n.z, n.z - 5) || this._retainChildren(n.x, n.y, n.z, n.z + 2));
                        for (t in this._tiles) this._tiles[t].retain || this._removeTile(t)
                    }
                }, _removeTilesAtZoom: function (n) {
                    for (var t in this._tiles) this._tiles[t].coords.z === n && this._removeTile(t)
                }, _removeAllTiles: function () {
                    for (var n in this._tiles) this._removeTile(n)
                }, _invalidateAll: function () {
                    for (var n in this._levels) a(this._levels[n].el), this._onRemoveLevel(n), delete this._levels[n];
                    this._removeAllTiles();
                    this._tileZoom = void 0
                }, _retainParent: function (n, i, r, u) {
                    var o = Math.floor(n / 2), s = Math.floor(i / 2), e = r - 1, h = new t(+o, +s), c, f;
                    return h.z = +e, c = this._tileCoordsToKey(h), f = this._tiles[c], f && f.active ? (f.retain = !0, !0) : (f && f.loaded && (f.retain = !0), e > u && this._retainParent(o, s, e, u))
                }, _retainChildren: function (n, i, r, u) {
                    for (var o, s, h, f, e = 2 * n; e < 2 * n + 2; e++) for (o = 2 * i; o < 2 * i + 2; o++) s = new t(e, o), s.z = r + 1, h = this._tileCoordsToKey(s), f = this._tiles[h], f && f.active ? f.retain = !0 : (f && f.loaded && (f.retain = !0), r + 1 < u && this._retainChildren(e, o, r + 1, u))
                }, _resetView: function (n) {
                    var t = n && (n.pinch || n.flyTo);
                    this._setView(this._map.getCenter(), this._map.getZoom(), t, t)
                }, _animateZoom: function (n) {
                    this._setView(n.center, n.zoom, !0, n.noUpdate)
                }, _clampZoom: function (n) {
                    var t = this.options;
                    return void 0 !== t.minNativeZoom && n < t.minNativeZoom ? t.minNativeZoom : void 0 !== t.maxNativeZoom && t.maxNativeZoom < n ? t.maxNativeZoom : n
                }, _setView: function (n, t, i, r) {
                    var u = this._clampZoom(Math.round(t)), f;
                    (void 0 !== this.options.maxZoom && u > this.options.maxZoom || void 0 !== this.options.minZoom && u < this.options.minZoom) && (u = void 0);
                    f = this.options.updateWhenZooming && u !== this._tileZoom;
                    r && !f || (this._tileZoom = u, this._abortLoading && this._abortLoading(), this._updateLevels(), this._resetGrid(), void 0 !== u && this._update(n), i || this._pruneTiles(), this._noPrune = !!i);
                    this._setZoomTransforms(n, t)
                }, _setZoomTransforms: function (n, t) {
                    for (var i in this._levels) this._setZoomTransform(this._levels[i], n, t)
                }, _setZoomTransform: function (n, t, i) {
                    var r = this._map.getZoomScale(i, n.zoom),
                        u = n.origin.multiplyBy(r).subtract(this._map._getNewPixelOrigin(t, i)).round();
                    et ? si(n.el, u, r) : b(n.el, u)
                }, _resetGrid: function () {
                    var t = this._map, n = t.options.crs, i = this._tileSize = this.getTileSize(), r = this._tileZoom,
                        u = this._map.getPixelWorldBounds(this._tileZoom);
                    u && (this._globalTileRange = this._pxBoundsToTileRange(u));
                    this._wrapX = n.wrapLng && !this.options.noWrap && [Math.floor(t.project([0, n.wrapLng[0]], r).x / i.x), Math.ceil(t.project([0, n.wrapLng[1]], r).x / i.y)];
                    this._wrapY = n.wrapLat && !this.options.noWrap && [Math.floor(t.project([n.wrapLat[0], 0], r).y / i.x), Math.ceil(t.project([n.wrapLat[1], 0], r).y / i.y)]
                }, _onMoveEnd: function () {
                    this._map && !this._map._animatingZoom && this._update()
                }, _getTiledPixelBounds: function (n) {
                    var t = this._map, u = t._animatingZoom ? Math.max(t._animateToZoom, t.getZoom()) : t.getZoom(),
                        f = t.getZoomScale(u, this._tileZoom), i = t.project(n, this._tileZoom).floor(),
                        r = t.getSize().divideBy(2 * f);
                    return new v(i.subtract(r), i.add(r))
                }, _update: function (n) {
                    var h = this._map, c, l, o, s, r, f, a, y;
                    if (h && (c = this._clampZoom(h.getZoom()), void 0 === n && (n = h.getCenter()), void 0 !== this._tileZoom)) {
                        var w = this._getTiledPixelBounds(n), i = this._pxBoundsToTileRange(w), p = i.getCenter(),
                            u = [], e = this.options.keepBuffer,
                            b = new v(i.getBottomLeft().subtract([e, -e]), i.getTopRight().add([e, -e]));
                        if (!(isFinite(i.min.x) && isFinite(i.min.y) && isFinite(i.max.x) && isFinite(i.max.y))) throw new Error("Attempted to load an infinite number of tiles");
                        for (l in this._tiles) o = this._tiles[l].coords, o.z === this._tileZoom && b.contains(new t(o.x, o.y)) || (this._tiles[l].current = !1);
                        if (Math.abs(c - this._tileZoom) > 1) return void this._setView(n, c);
                        for (s = i.min.y; s <= i.max.y; s++) for (r = i.min.x; r <= i.max.x; r++) f = new t(r, s), (f.z = this._tileZoom, this._isValidTile(f)) && (a = this._tiles[this._tileCoordsToKey(f)], a ? a.current = !0 : u.push(f));
                        if (u.sort(function (n, t) {
                            return n.distanceTo(p) - t.distanceTo(p)
                        }), 0 !== u.length) {
                            for (this._loading || (this._loading = !0, this.fire("loading")), y = document.createDocumentFragment(), r = 0; r < u.length; r++) this._addTile(u[r], y);
                            this._level.el.appendChild(y)
                        }
                    }
                }, _isValidTile: function (n) {
                    var i = this._map.options.crs, t, r;
                    return !i.infinite && (t = this._globalTileRange, !i.wrapLng && (n.x < t.min.x || n.x > t.max.x) || !i.wrapLat && (n.y < t.min.y || n.y > t.max.y)) ? !1 : this.options.bounds ? (r = this._tileCoordsToBounds(n), k(this.options.bounds).overlaps(r)) : !0
                }, _keyToBounds: function (n) {
                    return this._tileCoordsToBounds(this._keyToTileCoords(n))
                }, _tileCoordsToNwSe: function (n) {
                    var t = this._map, i = this.getTileSize(), r = n.scaleBy(i), u = r.add(i);
                    return [t.unproject(r, n.z), t.unproject(u, n.z)]
                }, _tileCoordsToBounds: function (n) {
                    var i = this._tileCoordsToNwSe(n), t = new tt(i[0], i[1]);
                    return this.options.noWrap || (t = this._map.wrapLatLngBounds(t)), t
                }, _tileCoordsToKey: function (n) {
                    return n.x + ":" + n.y + ":" + n.z
                }, _keyToTileCoords: function (n) {
                    var i = n.split(":"), r = new t(+i[0], +i[1]);
                    return r.z = +i[2], r
                }, _removeTile: function (n) {
                    var t = this._tiles[n];
                    t && (a(t.el), delete this._tiles[n], this.fire("tileunload", {
                        tile: t.el,
                        coords: this._keyToTileCoords(n)
                    }))
                }, _initTile: function (n) {
                    r(n, "leaflet-tile");
                    var t = this.getTileSize();
                    n.style.width = t.x + "px";
                    n.style.height = t.y + "px";
                    n.onselectstart = g;
                    n.onmousemove = g;
                    di && this.options.opacity < 1 && ut(n, this.options.opacity);
                    gi && !br && (n.style.WebkitBackfaceVisibility = "hidden")
                }, _addTile: function (n, t) {
                    var r = this._getTilePos(n), u = this._tileCoordsToKey(n),
                        i = this.createTile(this._wrapCoords(n), c(this._tileReady, this, n));
                    this._initTile(i);
                    this.createTile.length < 2 && d(c(this._tileReady, this, n, null, i));
                    b(i, r);
                    this._tiles[u] = {el: i, coords: n, current: !0};
                    t.appendChild(i);
                    this.fire("tileloadstart", {tile: i, coords: n})
                }, _tileReady: function (n, t, i) {
                    t && this.fire("tileerror", {error: t, tile: i, coords: n});
                    var u = this._tileCoordsToKey(n);
                    (i = this._tiles[u]) && (i.loaded = +new Date, this._map._fadeAnimated ? (ut(i.el, 0), nt(this._fadeFrame), this._fadeFrame = d(this._updateOpacity, this)) : (i.active = !0, this._pruneTiles()), t || (r(i.el, "leaflet-tile-loaded"), this.fire("tileload", {
                        tile: i.el,
                        coords: n
                    })), this._noTilesToLoad() && (this._loading = !1, this.fire("load"), di || !this._map._fadeAnimated ? d(this._pruneTiles, this) : setTimeout(c(this._pruneTiles, this), 250)))
                }, _getTilePos: function (n) {
                    return n.scaleBy(this.getTileSize()).subtract(this._level.origin)
                }, _wrapCoords: function (n) {
                    var i = new t(this._wrapX ? hr(n.x, this._wrapX) : n.x, this._wrapY ? hr(n.y, this._wrapY) : n.y);
                    return i.z = n.z, i
                }, _pxBoundsToTileRange: function (n) {
                    var t = this.getTileSize();
                    return new v(n.min.unscaleBy(t).floor(), n.max.unscaleBy(t).ceil().subtract([1, 1]))
                }, _noTilesToLoad: function () {
                    for (var n in this._tiles) if (!this._tiles[n].loaded) return !1;
                    return !0
                }
            }), sr = iu.extend({
                options: {
                    minZoom: 0,
                    maxZoom: 18,
                    subdomains: "abc",
                    errorTileUrl: "",
                    zoomOffset: 0,
                    tms: !1,
                    zoomReverse: !1,
                    detectRetina: !1,
                    crossOrigin: !1
                }, initialize: function (n, t) {
                    this._url = n;
                    t = l(this, t);
                    t.detectRetina && ei && t.maxZoom > 0 && (t.tileSize = Math.floor(t.tileSize / 2), t.zoomReverse ? (t.zoomOffset--, t.minZoom++) : (t.zoomOffset++, t.maxZoom--), t.minZoom = Math.max(0, t.minZoom));
                    "string" == typeof t.subdomains && (t.subdomains = t.subdomains.split(""));
                    gi || this.on("tileunload", this._onTileRemove)
                }, setUrl: function (n, t) {
                    return this._url === n && void 0 === t && (t = !0), this._url = n, t || this.redraw(), this
                }, createTile: function (n, t) {
                    var i = document.createElement("img");
                    return u(i, "load", c(this._tileOnLoad, this, t, i)), u(i, "error", c(this._tileOnError, this, t, i)), (this.options.crossOrigin || "" === this.options.crossOrigin) && (i.crossOrigin = !0 === this.options.crossOrigin ? "" : this.options.crossOrigin), i.alt = "", i.setAttribute("role", "presentation"), i.src = this.getTileUrl(n), i
                }, getTileUrl: function (n) {
                    var t = {r: ei ? "@2x" : "", s: this._getSubdomain(n), x: n.x, y: n.y, z: this._getZoomForUrl()}, i;
                    return this._map && !this._map.options.crs.infinite && (i = this._globalTileRange.max.y - n.y, this.options.tms && (t.y = i), t["-y"] = i), ns(this._url, s(t, this.options))
                }, _tileOnLoad: function (n, t) {
                    di ? setTimeout(c(n, this, null, t), 0) : n(null, t)
                }, _tileOnError: function (n, t, i) {
                    var r = this.options.errorTileUrl;
                    r && t.getAttribute("src") !== r && (t.src = r);
                    n(i, t)
                }, _onTileRemove: function (n) {
                    n.tile.onload = null
                }, _getZoomForUrl: function () {
                    var n = this._tileZoom, t = this.options.maxZoom, i = this.options.zoomReverse,
                        r = this.options.zoomOffset;
                    return i && (n = t - n), n + r
                }, _getSubdomain: function (n) {
                    var t = Math.abs(n.x + n.y) % this.options.subdomains.length;
                    return this.options.subdomains[t]
                }, _abortLoading: function () {
                    var t, n;
                    for (t in this._tiles) this._tiles[t].coords.z !== this._tileZoom && (n = this._tiles[t].el, n.onload = g, n.onerror = g, n.complete || (n.src = pu, a(n), delete this._tiles[t]))
                }, _removeTile: function (n) {
                    var t = this._tiles[n];
                    if (t) return uh || t.el.setAttribute("src", pu), iu.prototype._removeTile.call(this, n)
                }, _tileReady: function (n, t, i) {
                    if (this._map && (!i || i.getAttribute("src") !== pu)) return iu.prototype._tileReady.call(this, n, t, i)
                }
            }), uc = sr.extend({
                defaultWmsParams: {
                    service: "WMS",
                    request: "GetMap",
                    layers: "",
                    styles: "",
                    format: "image/jpeg",
                    transparent: !1,
                    version: "1.1.1"
                }, options: {crs: null, uppercase: !1}, initialize: function (n, t) {
                    var i, r, u, f;
                    this._url = n;
                    i = s({}, this.defaultWmsParams);
                    for (r in t) r in this.options || (i[r] = t[r]);
                    t = l(this, t);
                    u = t.detectRetina && ei ? 2 : 1;
                    f = this.getTileSize();
                    i.width = f.x * u;
                    i.height = f.y * u;
                    this.wmsParams = i
                }, onAdd: function (n) {
                    this._crs = this.options.crs || n.options.crs;
                    this._wmsVersion = parseFloat(this.wmsParams.version);
                    var t = this._wmsVersion >= 1.3 ? "crs" : "srs";
                    this.wmsParams[t] = this._crs.code;
                    sr.prototype.onAdd.call(this, n)
                }, getTileUrl: function (n) {
                    var r = this._tileCoordsToNwSe(n), u = this._crs, f = ot(u.project(r[0]), u.project(r[1])),
                        t = f.min, i = f.max,
                        o = (this._wmsVersion >= 1.3 && this._crs === nc ? [t.y, t.x, i.y, i.x] : [t.x, t.y, i.x, i.y]).join(","),
                        e = sr.prototype.getTileUrl.call(this, n);
                    return e + go(this.wmsParams, e, this.options.uppercase) + (this.options.uppercase ? "&BBOX=" : "&bbox=") + o
                }, setParams: function (n, t) {
                    return s(this.wmsParams, n), t || this.redraw(), this
                }
            });
            sr.WMS = uc;
            ks.wms = cl;
            var bt = rt.extend({
                options: {padding: .1, tolerance: 0}, initialize: function (n) {
                    l(this, n);
                    o(this);
                    this._layers = this._layers || {}
                }, onAdd: function () {
                    this._container || (this._initContainer(), this._zoomAnimated && r(this._container, "leaflet-zoom-animated"));
                    this.getPane().appendChild(this._container);
                    this._update();
                    this.on("update", this._updatePaths, this)
                }, onRemove: function () {
                    this.off("update", this._updatePaths, this);
                    this._destroyContainer()
                }, getEvents: function () {
                    var n = {
                        viewreset: this._reset,
                        zoom: this._onZoom,
                        moveend: this._update,
                        zoomend: this._onZoomEnd
                    };
                    return this._zoomAnimated && (n.zoomanim = this._onAnimZoom), n
                }, _onAnimZoom: function (n) {
                    this._updateTransform(n.center, n.zoom)
                }, _onZoom: function () {
                    this._updateTransform(this._map.getCenter(), this._map.getZoom())
                }, _updateTransform: function (n, t) {
                    var i = this._map.getZoomScale(t, this._zoom), f = ri(this._container),
                        r = this._map.getSize().multiplyBy(.5 + this.options.padding),
                        e = this._map.project(this._center, t), o = this._map.project(n, t), s = o.subtract(e),
                        u = r.multiplyBy(-i).add(f).add(r).subtract(s);
                    et ? si(this._container, u, i) : b(this._container, u)
                }, _reset: function () {
                    this._update();
                    this._updateTransform(this._center, this._zoom);
                    for (var n in this._layers) this._layers[n]._reset()
                }, _onZoomEnd: function () {
                    for (var n in this._layers) this._layers[n]._project()
                }, _updatePaths: function () {
                    for (var n in this._layers) this._layers[n]._update()
                }, _update: function () {
                    var n = this.options.padding, t = this._map.getSize(),
                        i = this._map.containerPointToLayerPoint(t.multiplyBy(-n)).round();
                    this._bounds = new v(i, i.add(t.multiplyBy(1 + 2 * n)).round());
                    this._center = this._map.getCenter();
                    this._zoom = this._map.getZoom()
                }
            }), fc = bt.extend({
                getEvents: function () {
                    var n = bt.prototype.getEvents.call(this);
                    return n.viewprereset = this._onViewPreReset, n
                }, _onViewPreReset: function () {
                    this._postponeUpdatePaths = !0
                }, onAdd: function () {
                    bt.prototype.onAdd.call(this);
                    this._draw()
                }, _initContainer: function () {
                    var n = this._container = document.createElement("canvas");
                    u(n, "mousemove", uf(this._onMouseMove, 32, this), this);
                    u(n, "click dblclick mousedown mouseup contextmenu", this._onClick, this);
                    u(n, "mouseout", this._handleMouseOut, this);
                    this._ctx = n.getContext("2d")
                }, _destroyContainer: function () {
                    nt(this._redrawRequest);
                    delete this._ctx;
                    a(this._container);
                    w(this._container);
                    delete this._container
                }, _updatePaths: function () {
                    var n, t;
                    if (!this._postponeUpdatePaths) {
                        this._redrawBounds = null;
                        for (t in this._layers) n = this._layers[t], n._update();
                        this._redraw()
                    }
                }, _update: function () {
                    if (!this._map._animatingZoom || !this._bounds) {
                        bt.prototype._update.call(this);
                        var t = this._bounds, n = this._container, i = t.getSize(), r = ei ? 2 : 1;
                        b(n, t.min);
                        n.width = r * i.x;
                        n.height = r * i.y;
                        n.style.width = i.x + "px";
                        n.style.height = i.y + "px";
                        ei && this._ctx.scale(2, 2);
                        this._ctx.translate(-t.min.x, -t.min.y);
                        this.fire("update")
                    }
                }, _reset: function () {
                    bt.prototype._reset.call(this);
                    this._postponeUpdatePaths && (this._postponeUpdatePaths = !1, this._updatePaths())
                }, _initPath: function (n) {
                    this._updateDashArray(n);
                    this._layers[o(n)] = n;
                    var t = n._order = {layer: n, prev: this._drawLast, next: null};
                    this._drawLast && (this._drawLast.next = t);
                    this._drawLast = t;
                    this._drawFirst = this._drawFirst || this._drawLast
                }, _addPath: function (n) {
                    this._requestRedraw(n)
                }, _removePath: function (n) {
                    var r = n._order, t = r.next, i = r.prev;
                    t ? t.prev = i : this._drawLast = i;
                    i ? i.next = t : this._drawFirst = t;
                    delete n._order;
                    delete this._layers[o(n)];
                    this._requestRedraw(n)
                }, _updatePath: function (n) {
                    this._extendRedrawBounds(n);
                    n._project();
                    n._update();
                    this._requestRedraw(n)
                }, _updateStyle: function (n) {
                    this._updateDashArray(n);
                    this._requestRedraw(n)
                }, _updateDashArray: function (n) {
                    if ("string" == typeof n.options.dashArray) {
                        for (var i, r = n.options.dashArray.split(/[, ]+/), u = [], t = 0; t < r.length; t++) {
                            if (i = Number(r[t]), isNaN(i)) return;
                            u.push(i)
                        }
                        n.options._dashArray = u
                    } else n.options._dashArray = n.options.dashArray
                }, _requestRedraw: function (n) {
                    this._map && (this._extendRedrawBounds(n), this._redrawRequest = this._redrawRequest || d(this._redraw, this))
                }, _extendRedrawBounds: function (n) {
                    if (n._pxBounds) {
                        var t = (n.options.weight || 0) + 1;
                        this._redrawBounds = this._redrawBounds || new v;
                        this._redrawBounds.extend(n._pxBounds.min.subtract([t, t]));
                        this._redrawBounds.extend(n._pxBounds.max.add([t, t]))
                    }
                }, _redraw: function () {
                    this._redrawRequest = null;
                    this._redrawBounds && (this._redrawBounds.min._floor(), this._redrawBounds.max._ceil());
                    this._clear();
                    this._draw();
                    this._redrawBounds = null
                }, _clear: function () {
                    var n = this._redrawBounds, t;
                    n ? (t = n.getSize(), this._ctx.clearRect(n.min.x, n.min.y, t.x, t.y)) : this._ctx.clearRect(0, 0, this._container.width, this._container.height)
                }, _draw: function () {
                    var i, n = this._redrawBounds, r, t;
                    for ((this._ctx.save(), n) && (r = n.getSize(), this._ctx.beginPath(), this._ctx.rect(n.min.x, n.min.y, r.x, r.y), this._ctx.clip()), this._drawing = !0, t = this._drawFirst; t; t = t.next) i = t.layer, (!n || i._pxBounds && i._pxBounds.intersects(n)) && i._updatePath();
                    this._drawing = !1;
                    this._ctx.restore()
                }, _updatePoly: function (n, t) {
                    if (this._drawing) {
                        var i, r, o, f, e = n._parts, s = e.length, u = this._ctx;
                        if (s) {
                            for (u.beginPath(), i = 0; i < s; i++) {
                                for (r = 0, o = e[i].length; r < o; r++) f = e[i][r], u[r ? "lineTo" : "moveTo"](f.x, f.y);
                                t && u.closePath()
                            }
                            this._fillStroke(u, n)
                        }
                    }
                }, _updateCircle: function (n) {
                    if (this._drawing && !n._empty()) {
                        var u = n._point, t = this._ctx, r = Math.max(Math.round(n._radius), 1),
                            i = (Math.max(Math.round(n._radiusY), 1) || r) / r;
                        1 !== i && (t.save(), t.scale(1, i));
                        t.beginPath();
                        t.arc(u.x, u.y / i, r, 0, 2 * Math.PI, !1);
                        1 !== i && t.restore();
                        this._fillStroke(t, n)
                    }
                }, _fillStroke: function (n, t) {
                    var i = t.options;
                    i.fill && (n.globalAlpha = i.fillOpacity, n.fillStyle = i.fillColor || i.color, n.fill(i.fillRule || "evenodd"));
                    i.stroke && 0 !== i.weight && (n.setLineDash && n.setLineDash(t.options && t.options._dashArray || []), n.globalAlpha = i.opacity, n.lineWidth = i.weight, n.strokeStyle = i.color, n.lineCap = i.lineCap, n.lineJoin = i.lineJoin, n.stroke())
                }, _onClick: function (n) {
                    for (var t, r, u = this._map.mouseEventToLayerPoint(n), i = this._drawFirst; i; i = i.next) t = i.layer, t.options.interactive && t._containsPoint(u) && !this._map._draggableMoved(t) && (r = t);
                    r && (kf(n), this._fireEvent([r], n))
                }, _onMouseMove: function (n) {
                    if (this._map && !this._map.dragging.moving() && !this._map._animatingZoom) {
                        var t = this._map.mouseEventToLayerPoint(n);
                        this._handleMouseHover(n, t)
                    }
                }, _handleMouseOut: function (n) {
                    var t = this._hoveredLayer;
                    t && (p(this._container, "leaflet-interactive"), this._fireEvent([t], n, "mouseout"), this._hoveredLayer = null)
                }, _handleMouseHover: function (n, t) {
                    for (var u, i, f = this._drawFirst; f; f = f.next) u = f.layer, u.options.interactive && u._containsPoint(t) && (i = u);
                    i !== this._hoveredLayer && (this._handleMouseOut(n), i && (r(this._container, "leaflet-interactive"), this._fireEvent([i], n, "mouseover"), this._hoveredLayer = i));
                    this._hoveredLayer && this._fireEvent([this._hoveredLayer], n)
                }, _fireEvent: function (n, t, i) {
                    this._map._fireDOMEvent(t, i || t.type, n)
                }, _bringToFront: function (n) {
                    var t = n._order, i, r;
                    t && (i = t.next, r = t.prev, i && (i.prev = r, r ? r.next = i : i && (this._drawFirst = i), t.prev = this._drawLast, this._drawLast.next = t, t.next = null, this._drawLast = t, this._requestRedraw(n)))
                }, _bringToBack: function (n) {
                    var t = n._order, r, i;
                    t && (r = t.next, i = t.prev, i && (i.next = r, r ? r.prev = i : i && (this._drawLast = i), t.prev = null, t.next = this._drawFirst, this._drawFirst.prev = t, this._drawFirst = t, this._requestRedraw(n)))
                }
            }), ru = function () {
                try {
                    return document.namespaces.add("lvml", "urn:schemas-microsoft-com:vml"), function (n) {
                        return document.createElement("<lvml:" + n + ' class="lvml">')
                    }
                } catch (n) {
                    return function (n) {
                        return document.createElement("<" + n + ' xmlns="urn:schemas-microsoft.com:vml" class="lvml">')
                    }
                }
            }(), ba = {
                _initContainer: function () {
                    this._container = e("div", "leaflet-vml-container")
                }, _update: function () {
                    this._map._animatingZoom || (bt.prototype._update.call(this), this.fire("update"))
                }, _initPath: function (n) {
                    var t = n._container = ru("shape");
                    r(t, "leaflet-vml-shape " + (this.options.className || ""));
                    t.coordsize = "1 1";
                    n._path = ru("path");
                    t.appendChild(n._path);
                    this._updateStyle(n);
                    this._layers[o(n)] = n
                }, _addPath: function (n) {
                    var t = n._container;
                    this._container.appendChild(t);
                    n.options.interactive && n.addInteractiveTarget(t)
                }, _removePath: function (n) {
                    var t = n._container;
                    a(t);
                    n.removeInteractiveTarget(t);
                    delete this._layers[o(n)]
                }, _updateStyle: function (n) {
                    var i = n._stroke, r = n._fill, t = n.options, u = n._container;
                    u.stroked = !!t.stroke;
                    u.filled = !!t.fill;
                    t.stroke ? (i || (i = n._stroke = ru("stroke")), u.appendChild(i), i.weight = t.weight + "px", i.color = t.color, i.opacity = t.opacity, i.dashStyle = t.dashArray ? pt(t.dashArray) ? t.dashArray.join(" ") : t.dashArray.replace(/( *, *)/g, " ") : "", i.endcap = t.lineCap.replace("butt", "flat"), i.joinstyle = t.lineJoin) : i && (u.removeChild(i), n._stroke = null);
                    t.fill ? (r || (r = n._fill = ru("fill")), u.appendChild(r), r.color = t.fillColor || t.color, r.opacity = t.fillOpacity) : r && (u.removeChild(r), n._fill = null)
                }, _updateCircle: function (n) {
                    var t = n._point.round(), i = Math.round(n._radius), r = Math.round(n._radiusY || i);
                    this._setPath(n, n._empty() ? "M0 0" : "AL " + t.x + "," + t.y + " " + i + "," + r + " 0,23592600")
                }, _setPath: function (n, t) {
                    n._path.v = t
                }, _bringToFront: function (n) {
                    pi(n._container)
                }, _bringToBack: function (n) {
                    wi(n._container)
                }
            }, rf = ku ? ru : rs, uu = bt.extend({
                getEvents: function () {
                    var n = bt.prototype.getEvents.call(this);
                    return n.zoomstart = this._onZoomStart, n
                }, _initContainer: function () {
                    this._container = rf("svg");
                    this._container.setAttribute("pointer-events", "none");
                    this._rootGroup = rf("g");
                    this._container.appendChild(this._rootGroup)
                }, _destroyContainer: function () {
                    a(this._container);
                    w(this._container);
                    delete this._container;
                    delete this._rootGroup;
                    delete this._svgSize
                }, _onZoomStart: function () {
                    this._update()
                }, _update: function () {
                    if (!this._map._animatingZoom || !this._bounds) {
                        bt.prototype._update.call(this);
                        var t = this._bounds, n = t.getSize(), i = this._container;
                        this._svgSize && this._svgSize.equals(n) || (this._svgSize = n, i.setAttribute("width", n.x), i.setAttribute("height", n.y));
                        b(i, t.min);
                        i.setAttribute("viewBox", [t.min.x, t.min.y, n.x, n.y].join(" "));
                        this.fire("update")
                    }
                }, _initPath: function (n) {
                    var t = n._path = rf("path");
                    n.options.className && r(t, n.options.className);
                    n.options.interactive && r(t, "leaflet-interactive");
                    this._updateStyle(n);
                    this._layers[o(n)] = n
                }, _addPath: function (n) {
                    this._rootGroup || this._initContainer();
                    this._rootGroup.appendChild(n._path);
                    n.addInteractiveTarget(n._path)
                }, _removePath: function (n) {
                    a(n._path);
                    n.removeInteractiveTarget(n._path);
                    delete this._layers[o(n)]
                }, _updatePath: function (n) {
                    n._project();
                    n._update()
                }, _updateStyle: function (n) {
                    var t = n._path, i = n.options;
                    t && (i.stroke ? (t.setAttribute("stroke", i.color), t.setAttribute("stroke-opacity", i.opacity), t.setAttribute("stroke-width", i.weight), t.setAttribute("stroke-linecap", i.lineCap), t.setAttribute("stroke-linejoin", i.lineJoin), i.dashArray ? t.setAttribute("stroke-dasharray", i.dashArray) : t.removeAttribute("stroke-dasharray"), i.dashOffset ? t.setAttribute("stroke-dashoffset", i.dashOffset) : t.removeAttribute("stroke-dashoffset")) : t.setAttribute("stroke", "none"), i.fill ? (t.setAttribute("fill", i.fillColor || i.color), t.setAttribute("fill-opacity", i.fillOpacity), t.setAttribute("fill-rule", i.fillRule || "evenodd")) : t.setAttribute("fill", "none"))
                }, _updatePoly: function (n, t) {
                    this._setPath(n, us(n._parts, t))
                }, _updateCircle: function (n) {
                    var i = n._point, t = Math.max(Math.round(n._radius), 1),
                        u = Math.max(Math.round(n._radiusY), 1) || t, r = "a" + t + "," + u + " 0 1,0 ",
                        f = n._empty() ? "M0 0" : "M" + (i.x - t) + "," + i.y + r + 2 * t + ",0 " + r + 2 * -t + ",0 ";
                    this._setPath(n, f)
                }, _setPath: function (n, t) {
                    n._path.setAttribute("d", t)
                }, _bringToFront: function (n) {
                    pi(n._path)
                }, _bringToBack: function (n) {
                    wi(n._path)
                }
            });
            ku && uu.include(ba);
            f.include({
                getRenderer: function (n) {
                    var t = n.options.renderer || this._getPaneRenderer(n.options.pane) || this.options.renderer || this._renderer;
                    return t || (t = this._renderer = this._createRenderer()), this.hasLayer(t) || this.addLayer(t), t
                }, _getPaneRenderer: function (n) {
                    if ("overlayPane" === n || void 0 === n) return !1;
                    var t = this._paneRenderers[n];
                    return void 0 === t && (t = this._createRenderer({pane: n}), this._paneRenderers[n] = t), t
                }, _createRenderer: function (n) {
                    return this.options.preferCanvas && ds(n) || gs(n)
                }
            });
            lo = er.extend({
                initialize: function (n, t) {
                    er.prototype.initialize.call(this, this._boundsToLatLngs(n), t)
                }, setBounds: function (n) {
                    return this.setLatLngs(this._boundsToLatLngs(n))
                }, _boundsToLatLngs: function (n) {
                    return n = k(n), [n.getSouthWest(), n.getNorthWest(), n.getNorthEast(), n.getSouthEast()]
                }
            });
            uu.create = rf;
            uu.pointsToPath = us;
            ti.geometryToLayer = ne;
            ti.coordsToLatLng = te;
            ti.coordsToLatLngs = lu;
            ti.latLngToCoords = ie;
            ti.latLngsToCoords = au;
            ti.getFeature = bi;
            ti.asFeature = vu;
            f.mergeOptions({boxZoom: !0});
            ao = lt.extend({
                initialize: function (n) {
                    this._map = n;
                    this._container = n._container;
                    this._pane = n._panes.overlayPane;
                    this._resetStateTimeout = 0;
                    n.on("unload", this._destroy, this)
                }, addHooks: function () {
                    u(this._container, "mousedown", this._onMouseDown, this)
                }, removeHooks: function () {
                    w(this._container, "mousedown", this._onMouseDown, this)
                }, moved: function () {
                    return this._moved
                }, _destroy: function () {
                    a(this._pane);
                    delete this._pane
                }, _resetState: function () {
                    this._resetStateTimeout = 0;
                    this._moved = !1
                }, _clearDeferredResetState: function () {
                    0 !== this._resetStateTimeout && (clearTimeout(this._resetStateTimeout), this._resetStateTimeout = 0)
                }, _onMouseDown: function (n) {
                    if (!n.shiftKey || 1 !== n.which && 1 !== n.button) return !1;
                    this._clearDeferredResetState();
                    this._resetState();
                    yr();
                    af();
                    this._startPoint = this._map.mouseEventToContainerPoint(n);
                    u(document, {
                        contextmenu: yt,
                        mousemove: this._onMouseMove,
                        mouseup: this._onMouseUp,
                        keydown: this._onKeyDown
                    }, this)
                }, _onMouseMove: function (n) {
                    this._moved || (this._moved = !0, this._box = e("div", "leaflet-zoom-box", this._container), r(this._container, "leaflet-crosshair"), this._map.fire("boxzoomstart"));
                    this._point = this._map.mouseEventToContainerPoint(n);
                    var t = new v(this._point, this._startPoint), i = t.getSize();
                    b(this._box, t.min);
                    this._box.style.width = i.x + "px";
                    this._box.style.height = i.y + "px"
                }, _finish: function () {
                    this._moved && (a(this._box), p(this._container, "leaflet-crosshair"));
                    pr();
                    vf();
                    w(document, {
                        contextmenu: yt,
                        mousemove: this._onMouseMove,
                        mouseup: this._onMouseUp,
                        keydown: this._onKeyDown
                    }, this)
                }, _onMouseUp: function (n) {
                    if ((1 === n.which || 1 === n.button) && (this._finish(), this._moved)) {
                        this._clearDeferredResetState();
                        this._resetStateTimeout = setTimeout(c(this._resetState, this), 0);
                        var t = new tt(this._map.containerPointToLatLng(this._startPoint), this._map.containerPointToLatLng(this._point));
                        this._map.fitBounds(t).fire("boxzoomend", {boxZoomBounds: t})
                    }
                }, _onKeyDown: function (n) {
                    27 === n.keyCode && this._finish()
                }
            });
            f.addInitHook("addHandler", "boxZoom", ao);
            f.mergeOptions({doubleClickZoom: !0});
            vo = lt.extend({
                addHooks: function () {
                    this._map.on("dblclick", this._onDoubleClick, this)
                }, removeHooks: function () {
                    this._map.off("dblclick", this._onDoubleClick, this)
                }, _onDoubleClick: function (n) {
                    var t = this._map, i = t.getZoom(), r = t.options.zoomDelta,
                        u = n.originalEvent.shiftKey ? i - r : i + r;
                    "center" === t.options.doubleClickZoom ? t.setZoom(u) : t.setZoomAround(n.containerPoint, u)
                }
            });
            f.addInitHook("addHandler", "doubleClickZoom", vo);
            f.mergeOptions({
                dragging: !0,
                inertia: !br,
                inertiaDeceleration: 3400,
                inertiaMaxSpeed: 1 / 0,
                easeLinearity: .2,
                worldCopyJump: !1,
                maxBoundsViscosity: 0
            });
            yo = lt.extend({
                addHooks: function () {
                    if (!this._draggable) {
                        var n = this._map;
                        this._draggable = new li(n._mapPane, n._container);
                        this._draggable.on({
                            dragstart: this._onDragStart,
                            drag: this._onDrag,
                            dragend: this._onDragEnd
                        }, this);
                        this._draggable.on("predrag", this._onPreDragLimit, this);
                        n.options.worldCopyJump && (this._draggable.on("predrag", this._onPreDragWrap, this), n.on("zoomend", this._onZoomEnd, this), n.whenReady(this._onZoomEnd, this))
                    }
                    r(this._map._container, "leaflet-grab leaflet-touch-drag");
                    this._draggable.enable();
                    this._positions = [];
                    this._times = []
                }, removeHooks: function () {
                    p(this._map._container, "leaflet-grab");
                    p(this._map._container, "leaflet-touch-drag");
                    this._draggable.disable()
                }, moved: function () {
                    return this._draggable && this._draggable._moved
                }, moving: function () {
                    return this._draggable && this._draggable._moving
                }, _onDragStart: function () {
                    var n = this._map, t;
                    (n._stop(), this._map.options.maxBounds && this._map.options.maxBoundsViscosity) ? (t = k(this._map.options.maxBounds), this._offsetLimit = ot(this._map.latLngToContainerPoint(t.getNorthWest()).multiplyBy(-1), this._map.latLngToContainerPoint(t.getSouthEast()).multiplyBy(-1).add(this._map.getSize())), this._viscosity = Math.min(1, Math.max(0, this._map.options.maxBoundsViscosity))) : this._offsetLimit = null;
                    n.fire("movestart").fire("dragstart");
                    n.options.inertia && (this._positions = [], this._times = [])
                }, _onDrag: function (n) {
                    if (this._map.options.inertia) {
                        var t = this._lastTime = +new Date,
                            i = this._lastPos = this._draggable._absPos || this._draggable._newPos;
                        this._positions.push(i);
                        this._times.push(t);
                        this._prunePositions(t)
                    }
                    this._map.fire("move", n).fire("drag", n)
                }, _prunePositions: function (n) {
                    for (; this._positions.length > 1 && n - this._times[0] > 50;) this._positions.shift(), this._times.shift()
                }, _onZoomEnd: function () {
                    var n = this._map.getSize().divideBy(2), t = this._map.latLngToLayerPoint([0, 0]);
                    this._initialWorldOffset = t.subtract(n).x;
                    this._worldWidth = this._map.getPixelWorldBounds().getSize().x
                }, _viscousLimit: function (n, t) {
                    return n - (n - t) * this._viscosity
                }, _onPreDragLimit: function () {
                    if (this._viscosity && this._offsetLimit) {
                        var n = this._draggable._newPos.subtract(this._draggable._startPos), t = this._offsetLimit;
                        n.x < t.min.x && (n.x = this._viscousLimit(n.x, t.min.x));
                        n.y < t.min.y && (n.y = this._viscousLimit(n.y, t.min.y));
                        n.x > t.max.x && (n.x = this._viscousLimit(n.x, t.max.x));
                        n.y > t.max.y && (n.y = this._viscousLimit(n.y, t.max.y));
                        this._draggable._newPos = this._draggable._startPos.add(n)
                    }
                }, _onPreDragWrap: function () {
                    var i = this._worldWidth, t = Math.round(i / 2), n = this._initialWorldOffset,
                        r = this._draggable._newPos.x, u = (r - t + n) % i + t - n, f = (r + t + n) % i - t - n,
                        e = Math.abs(u + n) < Math.abs(f + n) ? u : f;
                    this._draggable._absPos = this._draggable._newPos.clone();
                    this._draggable._newPos.x = e
                }, _onDragEnd: function (n) {
                    var t = this._map, r = t.options, h = !r.inertia || this._times.length < 2;
                    if (t.fire("dragend", n), h) t.fire("moveend"); else {
                        this._prunePositions(+new Date);
                        var c = this._lastPos.subtract(this._positions[0]), l = (this._lastTime - this._times[0]) / 1e3,
                            u = r.easeLinearity, f = c.multiplyBy(u / l), e = f.distanceTo([0, 0]),
                            o = Math.min(r.inertiaMaxSpeed, e), a = f.multiplyBy(o / e),
                            s = o / (r.inertiaDeceleration * u), i = a.multiplyBy(-s / 2).round();
                        i.x || i.y ? (i = t._limitOffset(i, t.options.maxBounds), d(function () {
                            t.panBy(i, {duration: s, easeLinearity: u, noMoveStart: !0, animate: !0})
                        })) : t.fire("moveend")
                    }
                }
            });
            f.addInitHook("addHandler", "dragging", yo);
            f.mergeOptions({keyboard: !0, keyboardPanDelta: 80});
            po = lt.extend({
                keyCodes: {
                    left: [37],
                    right: [39],
                    down: [40],
                    up: [38],
                    zoomIn: [187, 107, 61, 171],
                    zoomOut: [189, 109, 54, 173]
                }, initialize: function (n) {
                    this._map = n;
                    this._setPanDelta(n.options.keyboardPanDelta);
                    this._setZoomDelta(n.options.zoomDelta)
                }, addHooks: function () {
                    var n = this._map._container;
                    n.tabIndex <= 0 && (n.tabIndex = "0");
                    u(n, {focus: this._onFocus, blur: this._onBlur, mousedown: this._onMouseDown}, this);
                    this._map.on({focus: this._addHooks, blur: this._removeHooks}, this)
                }, removeHooks: function () {
                    this._removeHooks();
                    w(this._map._container, {
                        focus: this._onFocus,
                        blur: this._onBlur,
                        mousedown: this._onMouseDown
                    }, this);
                    this._map.off({focus: this._addHooks, blur: this._removeHooks}, this)
                }, _onMouseDown: function () {
                    if (!this._focused) {
                        var n = document.body, t = document.documentElement, i = n.scrollTop || t.scrollTop,
                            r = n.scrollLeft || t.scrollLeft;
                        this._map._container.focus();
                        window.scrollTo(r, i)
                    }
                }, _onFocus: function () {
                    this._focused = !0;
                    this._map.fire("focus")
                }, _onBlur: function () {
                    this._focused = !1;
                    this._map.fire("blur")
                }, _setPanDelta: function (n) {
                    for (var u = this._panKeys = {}, r = this.keyCodes, t = 0, i = r.left.length; t < i; t++) u[r.left[t]] = [-1 * n, 0];
                    for (t = 0, i = r.right.length; t < i; t++) u[r.right[t]] = [n, 0];
                    for (t = 0, i = r.down.length; t < i; t++) u[r.down[t]] = [0, n];
                    for (t = 0, i = r.up.length; t < i; t++) u[r.up[t]] = [0, -1 * n]
                }, _setZoomDelta: function (n) {
                    for (var u = this._zoomKeys = {}, r = this.keyCodes, t = 0, i = r.zoomIn.length; t < i; t++) u[r.zoomIn[t]] = n;
                    for (t = 0, i = r.zoomOut.length; t < i; t++) u[r.zoomOut[t]] = -n
                }, _addHooks: function () {
                    u(document, "keydown", this._onKeyDown, this)
                }, _removeHooks: function () {
                    w(document, "keydown", this._onKeyDown, this)
                }, _onKeyDown: function (n) {
                    if (!(n.altKey || n.ctrlKey || n.metaKey)) {
                        var u, r = n.keyCode, t = this._map;
                        if (r in this._panKeys) t._panAnim && t._panAnim._inProgress || (u = this._panKeys[r], n.shiftKey && (u = i(u).multiplyBy(3)), t.panBy(u), t.options.maxBounds && t.panInsideBounds(t.options.maxBounds)); else if (r in this._zoomKeys) t.setZoom(t.getZoom() + (n.shiftKey ? 3 : 1) * this._zoomKeys[r]); else {
                            if (27 !== r || !t._popup || !t._popup.options.closeOnEscapeKey) return;
                            t.closePopup()
                        }
                        yt(n)
                    }
                }
            });
            f.addInitHook("addHandler", "keyboard", po);
            f.mergeOptions({scrollWheelZoom: !0, wheelDebounceTime: 40, wheelPxPerZoomLevel: 60});
            wo = lt.extend({
                addHooks: function () {
                    u(this._map._container, "mousewheel", this._onWheelScroll, this);
                    this._delta = 0
                }, removeHooks: function () {
                    w(this._map._container, "mousewheel", this._onWheelScroll, this)
                }, _onWheelScroll: function (n) {
                    var i = ls(n), r = this._map.options.wheelDebounceTime, t;
                    this._delta += i;
                    this._lastMousePos = this._map.mouseEventToContainerPoint(n);
                    this._startTime || (this._startTime = +new Date);
                    t = Math.max(r - (+new Date - this._startTime), 0);
                    clearTimeout(this._timer);
                    this._timer = setTimeout(c(this._performZoom, this), t);
                    yt(n)
                }, _performZoom: function () {
                    var n = this._map, t = n.getZoom(), i = this._map.options.zoomSnap || 0;
                    n._stop();
                    var e = this._delta / (4 * this._map.options.wheelPxPerZoomLevel),
                        u = 4 * Math.log(2 / (1 + Math.exp(-Math.abs(e)))) / Math.LN2, f = i ? Math.ceil(u / i) * i : u,
                        r = n._limitZoom(t + (this._delta > 0 ? f : -f)) - t;
                    this._delta = 0;
                    this._startTime = null;
                    r && ("center" === n.options.scrollWheelZoom ? n.setZoom(t + r) : n.setZoomAround(this._lastMousePos, t + r))
                }
            });
            f.addInitHook("addHandler", "scrollWheelZoom", wo);
            f.mergeOptions({tap: !0, tapTolerance: 15});
            bo = lt.extend({
                addHooks: function () {
                    u(this._map._container, "touchstart", this._onDown, this)
                }, removeHooks: function () {
                    w(this._map._container, "touchstart", this._onDown, this)
                }, _onDown: function (n) {
                    if (n.touches) {
                        if (ft(n), this._fireClick = !0, n.touches.length > 1) return this._fireClick = !1, void clearTimeout(this._holdTimeout);
                        var i = n.touches[0], f = i.target;
                        this._startPos = this._newPos = new t(i.clientX, i.clientY);
                        f.tagName && "a" === f.tagName.toLowerCase() && r(f, "leaflet-active");
                        this._holdTimeout = setTimeout(c(function () {
                            this._isTapValid() && (this._fireClick = !1, this._onUp(), this._simulateEvent("contextmenu", i))
                        }, this), 1e3);
                        this._simulateEvent("mousedown", i);
                        u(document, {touchmove: this._onMove, touchend: this._onUp}, this)
                    }
                }, _onUp: function (n) {
                    if (clearTimeout(this._holdTimeout), w(document, {
                        touchmove: this._onMove,
                        touchend: this._onUp
                    }, this), this._fireClick && n && n.changedTouches) {
                        var i = n.changedTouches[0], t = i.target;
                        t && t.tagName && "a" === t.tagName.toLowerCase() && p(t, "leaflet-active");
                        this._simulateEvent("mouseup", i);
                        this._isTapValid() && this._simulateEvent("click", i)
                    }
                }, _isTapValid: function () {
                    return this._newPos.distanceTo(this._startPos) <= this._map.options.tapTolerance
                }, _onMove: function (n) {
                    var i = n.touches[0];
                    this._newPos = new t(i.clientX, i.clientY);
                    this._simulateEvent("mousemove", i)
                }, _simulateEvent: function (n, t) {
                    var i = document.createEvent("MouseEvents");
                    i._simulated = !0;
                    t.target._simulatedClick = !0;
                    i.initMouseEvent(n, !0, !0, window, 1, t.screenX, t.screenY, t.clientX, t.clientY, !1, !1, !1, !1, 0, null);
                    t.target.dispatchEvent(i)
                }
            });
            wt && !st && f.addInitHook("addHandler", "tap", bo);
            f.mergeOptions({touchZoom: wt && !br, bounceAtZoomLimits: !0});
            ko = lt.extend({
                addHooks: function () {
                    r(this._map._container, "leaflet-touch-zoom");
                    u(this._map._container, "touchstart", this._onTouchStart, this)
                }, removeHooks: function () {
                    p(this._map._container, "leaflet-touch-zoom");
                    w(this._map._container, "touchstart", this._onTouchStart, this)
                }, _onTouchStart: function (n) {
                    var t = this._map, i, r;
                    !n.touches || 2 !== n.touches.length || t._animatingZoom || this._zooming || (i = t.mouseEventToContainerPoint(n.touches[0]), r = t.mouseEventToContainerPoint(n.touches[1]), this._centerPoint = t.getSize()._divideBy(2), this._startLatLng = t.containerPointToLatLng(this._centerPoint), "center" !== t.options.touchZoom && (this._pinchStartLatLng = t.containerPointToLatLng(i.add(r)._divideBy(2))), this._startDist = i.distanceTo(r), this._startZoom = t.getZoom(), this._moved = !1, this._zooming = !0, t._stop(), u(document, "touchmove", this._onTouchMove, this), u(document, "touchend", this._onTouchEnd, this), ft(n))
                }, _onTouchMove: function (n) {
                    var r, e;
                    if (n.touches && 2 === n.touches.length && this._zooming) {
                        var t = this._map, u = t.mouseEventToContainerPoint(n.touches[0]),
                            f = t.mouseEventToContainerPoint(n.touches[1]), i = u.distanceTo(f) / this._startDist;
                        if (this._zoom = t.getScaleZoom(i, this._startZoom), !t.options.bounceAtZoomLimits && (this._zoom < t.getMinZoom() && i < 1 || this._zoom > t.getMaxZoom() && i > 1) && (this._zoom = t._limitZoom(this._zoom)), "center" === t.options.touchZoom) {
                            if (this._center = this._startLatLng, 1 === i) return
                        } else {
                            if (r = u._add(f)._divideBy(2)._subtract(this._centerPoint), 1 === i && 0 === r.x && 0 === r.y) return;
                            this._center = t.unproject(t.project(this._pinchStartLatLng, this._zoom).subtract(r), this._zoom)
                        }
                        this._moved || (t._moveStart(!0, !1), this._moved = !0);
                        nt(this._animRequest);
                        e = c(t._move, t, this._center, this._zoom, {pinch: !0, round: !1});
                        this._animRequest = d(e, this, !0);
                        ft(n)
                    }
                }, _onTouchEnd: function () {
                    if (!this._moved || !this._zooming) return void (this._zooming = !1);
                    this._zooming = !1;
                    nt(this._animRequest);
                    w(document, "touchmove", this._onTouchMove);
                    w(document, "touchend", this._onTouchEnd);
                    this._map.options.zoomAnimation ? this._map._animateZoom(this._center, this._map._limitZoom(this._zoom), !0, this._map.options.zoomSnap) : this._map._resetView(this._center, this._map._limitZoom(this._zoom))
                }
            });
            f.addInitHook("addHandler", "touchZoom", ko);
            f.BoxZoom = ao;
            f.DoubleClickZoom = vo;
            f.Drag = yo;
            f.Keyboard = po;
            f.ScrollWheelZoom = wo;
            f.Tap = bo;
            f.TouchZoom = ko;
            Object.freeze = nh;
            n.version = "1.4.0";
            n.Control = ct;
            n.control = gr;
            n.Browser = dl;
            n.Evented = ki;
            n.Mixin = ea;
            n.Util = vl;
            n.Class = kt;
            n.Handler = lt;
            n.extend = s;
            n.bind = c;
            n.stamp = o;
            n.setOptions = l;
            n.DomEvent = ia;
            n.DomUtil = na;
            n.PosAnimation = yh;
            n.Draggable = li;
            n.LineUtil = oa;
            n.PolyUtil = sa;
            n.Point = t;
            n.point = i;
            n.Bounds = v;
            n.bounds = ot;
            n.Transformation = of;
            n.transformation = cr;
            n.Projection = ha;
            n.LatLng = h;
            n.latLng = y;
            n.LatLngBounds = tt;
            n.latLngBounds = k;
            n.CRS = gt;
            n.GeoJSON = ti;
            n.geoJSON = bs;
            n.geoJson = ya;
            n.Layer = rt;
            n.LayerGroup = ur;
            n.layerGroup = aa;
            n.FeatureGroup = ai;
            n.featureGroup = va;
            n.ImageOverlay = ho;
            n.imageOverlay = pa;
            n.VideoOverlay = ic;
            n.videoOverlay = ol;
            n.DivOverlay = ii;
            n.Popup = or;
            n.popup = wa;
            n.Tooltip = vi;
            n.tooltip = rc;
            n.Icon = fr;
            n.icon = tl;
            n.DivIcon = co;
            n.divIcon = sl;
            n.Marker = tu;
            n.marker = il;
            n.TileLayer = sr;
            n.tileLayer = ks;
            n.GridLayer = iu;
            n.gridLayer = hl;
            n.SVG = uu;
            n.svg = gs;
            n.Renderer = bt;
            n.Canvas = fc;
            n.canvas = ds;
            n.Path = oi;
            n.CircleMarker = tf;
            n.circleMarker = rl;
            n.Circle = oo;
            n.circle = ul;
            n.Polyline = ni;
            n.polyline = fl;
            n.Polygon = er;
            n.polygon = el;
            n.Rectangle = lo;
            n.rectangle = ll;
            n.Map = f;
            n.map = bc;
            ec = window.L;
            n.noConflict = function () {
                return window.L = ec, this
            };
            window.L = n
        })
    }, {}], 5: [function (n, t, i) {
        !function (n, t) {
            "object" == typeof i && i && "string" != typeof i.nodeName ? t(i) : "function" == typeof define && define.amd ? define(["exports"], t) : (n.Mustache = {}, t(n.Mustache))
        }(this, function (n) {
            function u(n) {
                return "function" == typeof n
            }

            function c(n) {
                return f(n) ? "array" : typeof n
            }

            function o(n) {
                return n.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
            }

            function s(n, t) {
                return null != n && "object" == typeof n && t in n
            }

            function l(n, t) {
                return null != n && "object" != typeof n && n.hasOwnProperty && n.hasOwnProperty(t)
            }

            function a(n, t) {
                return d.call(n, t)
            }

            function v(n) {
                return !a(g, n)
            }

            function y(n) {
                return String(n).replace(/[&<>"'`=\/]/g, function (n) {
                    return nt[n]
                })
            }

            function p(t, i) {
                function st(n) {
                    if ("string" == typeof n && (n = n.split(it, 2)), !f(n) || 2 !== n.length) throw new Error("Invalid tags: " + n);
                    nt = new RegExp(o(n[0]) + "\\s*");
                    l = new RegExp("\\s*" + o(n[1]));
                    ht = new RegExp("\\s*" + o("}" + n[1]))
                }

                var c, e, s, d, ot, y, u, g, ct;
                if (!t) return [];
                var nt, l, ht, ft = [], a = [], p = [], et = !1, k = !1;
                for (st(i || n.tags), u = new r(t); !u.eos();) {
                    if (c = u.pos, s = u.scanUntil(nt)) for (g = 0, ct = s.length; g < ct; ++g) d = s.charAt(g), v(d) ? p.push(a.length) : k = !0, a.push(["text", d, c, c + 1]), c += 1, "\n" === d && function () {
                        if (et && !k) for (; p.length;) delete a[p.pop()]; else p = [];
                        et = !1;
                        k = !1
                    }();
                    if (!u.scan(nt)) break;
                    if (et = !0, e = u.scan(ut) || "name", u.scan(tt), "=" === e ? (s = u.scanUntil(h), u.scan(h), u.scanUntil(l)) : "{" === e ? (s = u.scanUntil(ht), u.scan(rt), u.scanUntil(l), e = "&") : s = u.scanUntil(l), !u.scan(l)) throw new Error("Unclosed tag at " + u.pos);
                    if (ot = [e, s, c, u.pos], a.push(ot), "#" === e || "^" === e) ft.push(ot); else if ("/" === e) {
                        if (!(y = ft.pop())) throw new Error('Unopened section "' + s + '" at ' + c);
                        if (y[1] !== s) throw new Error('Unclosed section "' + y[1] + '" at ' + c);
                    } else "name" === e || "{" === e || "&" === e ? k = !0 : "=" === e && st(s)
                }
                if (y = ft.pop()) throw new Error('Unclosed section "' + y[1] + '" at ' + u.pos);
                return b(w(a))
            }

            function w(n) {
                for (var t, i, u = [], r = 0, f = n.length; r < f; ++r) (t = n[r]) && ("text" === t[0] && i && "text" === i[0] ? (i[1] += t[1], i[3] = t[3]) : (u.push(t), i = t));
                return u
            }

            function b(n) {
                for (var t, e, u = [], r = u, i = [], f = 0, o = n.length; f < o; ++f) switch (t = n[f], t[0]) {
                    case"#":
                    case"^":
                        r.push(t);
                        i.push(t);
                        r = t[4] = [];
                        break;
                    case"/":
                        e = i.pop();
                        e[5] = t[2];
                        r = i.length > 0 ? i[i.length - 1][4] : u;
                        break;
                    default:
                        r.push(t)
                }
                return u
            }

            function r(n) {
                this.string = n;
                this.tail = n;
                this.pos = 0
            }

            function i(n, t) {
                this.view = n;
                this.cache = {".": this.view};
                this.parent = t
            }

            function t() {
                this.cache = {}
            }

            var k = Object.prototype.toString, f = Array.isArray || function (n) {
                return "[object Array]" === k.call(n)
            }, d = RegExp.prototype.test, g = /\S/, nt = {
                "&": "&amp;",
                "<": "&lt;",
                ">": "&gt;",
                '"': "&quot;",
                "'": "&#39;",
                "/": "&#x2F;",
                "`": "&#x60;",
                "=": "&#x3D;"
            }, tt = /\s*/, it = /\s+/, h = /\s*=/, rt = /\s*\}/, ut = /#|\^|\/|>|\{|&|=|!/, e;
            return r.prototype.eos = function () {
                return "" === this.tail
            }, r.prototype.scan = function (n) {
                var i = this.tail.match(n), t;
                return !i || 0 !== i.index ? "" : (t = i[0], this.tail = this.tail.substring(t.length), this.pos += t.length, t)
            }, r.prototype.scanUntil = function (n) {
                var t, i = this.tail.search(n);
                switch (i) {
                    case-1:
                        t = this.tail;
                        this.tail = "";
                        break;
                    case 0:
                        t = "";
                        break;
                    default:
                        t = this.tail.substring(0, i);
                        this.tail = this.tail.substring(i)
                }
                return this.pos += t.length, t
            }, i.prototype.push = function (n) {
                return new i(n, this)
            }, i.prototype.lookup = function (n) {
                var i, h = this.cache, t, f, e, r, o;
                if (h.hasOwnProperty(n)) i = h[n]; else {
                    for (r = this, o = !1; r;) {
                        if (n.indexOf(".") > 0) for (t = r.view, f = n.split("."), e = 0; null != t && e < f.length;) e === f.length - 1 && (o = s(t, f[e]) || l(t, f[e])), t = t[f[e++]]; else t = r.view[n], o = s(r.view, n);
                        if (o) {
                            i = t;
                            break
                        }
                        r = r.parent
                    }
                    h[n] = i
                }
                return u(i) && (i = i.call(this.view)), i
            }, t.prototype.clearCache = function () {
                this.cache = {}
            }, t.prototype.parse = function (t, i) {
                var u = this.cache, f = t + ":" + (i || n.tags).join(":"), r = u[f];
                return null == r && (r = u[f] = p(t, i)), r
            }, t.prototype.render = function (n, t, r, u) {
                var f = this.parse(n, u), e = t instanceof i ? t : new i(t);
                return this.renderTokens(f, e, r, n, u)
            }, t.prototype.renderTokens = function (n, t, i, r, u) {
                for (var e, o, f, h = "", s = 0, c = n.length; s < c; ++s) f = void 0, e = n[s], o = e[0], "#" === o ? f = this.renderSection(e, t, i, r) : "^" === o ? f = this.renderInverted(e, t, i, r) : ">" === o ? f = this.renderPartial(e, t, i, u) : "&" === o ? f = this.unescapedValue(e, t) : "name" === o ? f = this.escapedValue(e, t) : "text" === o && (f = this.rawValue(e)), void 0 !== f && (h += f);
                return h
            }, t.prototype.renderSection = function (n, t, i, r) {
                function c(n) {
                    return l.render(n, t, i)
                }

                var l = this, o = "", e = t.lookup(n[1]), s, h;
                if (e) {
                    if (f(e)) for (s = 0, h = e.length; s < h; ++s) o += this.renderTokens(n[4], t.push(e[s]), i, r); else if ("object" == typeof e || "string" == typeof e || "number" == typeof e) o += this.renderTokens(n[4], t.push(e), i, r); else if (u(e)) {
                        if ("string" != typeof r) throw new Error("Cannot use higher-order sections without the original template");
                        e = e.call(t.view, r.slice(n[3], n[5]), c);
                        null != e && (o += e)
                    } else o += this.renderTokens(n[4], t, i, r);
                    return o
                }
            }, t.prototype.renderInverted = function (n, t, i, r) {
                var u = t.lookup(n[1]);
                if (!u || f(u) && 0 === u.length) return this.renderTokens(n[4], t, i, r)
            }, t.prototype.renderPartial = function (n, t, i, r) {
                if (i) {
                    var f = u(i) ? i(n[1]) : i[n[1]];
                    return null != f ? this.renderTokens(this.parse(f, r), t, i, f) : void 0
                }
            }, t.prototype.unescapedValue = function (n, t) {
                var i = t.lookup(n[1]);
                if (null != i) return i
            }, t.prototype.escapedValue = function (t, i) {
                var r = i.lookup(t[1]);
                if (null != r) return n.escape(r)
            }, t.prototype.rawValue = function (n) {
                return n[1]
            }, n.name = "mustache.js", n.version = "3.0.1", n.tags = ["{{", "}}"], e = new t, n.clearCache = function () {
                return e.clearCache()
            }, n.parse = function (n, t) {
                return e.parse(n, t)
            }, n.render = function (n, t, i, r) {
                if ("string" != typeof n) throw new TypeError('Invalid template! Template should be a "string" but "' + c(n) + '" was given as the first argument for mustache#render(template, view, partials)');
                return e.render(n, t, i, r)
            }, n.to_html = function (t, i, r, f) {
                var e = n.render(t, i, r);
                if (!u(f)) return e;
                f(e)
            }, n.escape = y, n.Scanner = r, n.Context = i, n.Writer = t, n
        })
    }, {}], 6: [function (n, t) {
        t.exports = {
            author: "Mapbox",
            name: "mapbox.js",
            description: "Mapbox plugin for Leaflet",
            version: "3.3.0",
            homepage: "http://mapbox.com/",
            engines: {node: ">=10"},
            repository: {type: "git", url: "git://github.com/mapbox/mapbox.js.git"},
            main: "src/index.js",
            files: ["dist", "src", "*.md"],
            dependencies: {
                "@mapbox/corslite": "0.0.7",
                "@mapbox/sanitize-caja": "^0.1.4",
                leaflet: "1.4.0",
                mustache: "3.0.1"
            },
            scripts: {
                test: "eslint src && phantomjs node_modules/mocha-phantomjs-core/mocha-phantomjs-core.js test/index.html && mocha test/docs.js",
                prepublishOnly: "npm run build",
                build: "make"
            },
            license: "BSD-3-Clause",
            devDependencies: {
                browserify: "^16.5.0",
                "clean-css-cli": "^4.3.0",
                eslint: "^6.8.0",
                "expect.js": "0.3.1",
                happen: "0.3.2",
                marked: "^0.8.0",
                minifyify: "^7.3.5",
                minimist: "1.2.0",
                mocha: "^7.0.1",
                "mocha-phantomjs-core": "2.1.2",
                "phantomjs-prebuilt": "2.1.16",
                sinon: "7.2.4"
            },
            optionalDependencies: {}
        }
    }, {}], 7: [function (n, t) {
        "use strict";
        t.exports = {
            HTTP_URL: "http://api.mapbox.com",
            HTTPS_URL: "https://api.mapbox.com",
            FORCE_HTTPS: !0,
            REQUIRE_ACCESS_TOKEN: !0,
            TEMPLATE_STYLES: {
                "mapbox.dark": "mapbox/dark-v10",
                "mapbox.light": "mapbox/light-v10",
                "mapbox.osm-bright": "mapbox/bright-v9",
                "mapbox.outdoors": "mapbox/outdoors-v11",
                "mapbox.satellite": "mapbox/satellite-v9",
                "mapbox.streets": "mapbox/streets-v11",
                "mapbox.streets-basic": "mapbox/basic-v9",
                "mapbox.streets-satellite": "mapbox/satellite-streets-v11"
            }
        }
    }, {}], 8: [function (n, t) {
        "use strict";
        var i = n("./util"), e = n("./format_url"), o = n("./request"), r = n("./marker"), u = n("./simplestyle"),
            f = L.FeatureGroup.extend({
                options: {
                    filter: function () {
                        return !0
                    }, sanitizer: n("@mapbox/sanitize-caja"), style: u.style, popupOptions: {closeButton: !1}
                }, initialize: function (n, t) {
                    L.setOptions(this, t);
                    this._layers = {};
                    "string" == typeof n ? i.idUrl(n, this) : n && "object" == typeof n && this.setGeoJSON(n)
                }, setGeoJSON: function (n) {
                    return this._geojson = n, this.clearLayers(), this._initialize(n), this
                }, getGeoJSON: function () {
                    return this._geojson
                }, loadURL: function (n) {
                    return this._request && "abort" in this._request && this._request.abort(), this._request = o(n, L.bind(function (t, r) {
                        this._request = null;
                        t && "abort" !== t.type ? (i.log("could not load features at " + n), this.fire("error", {error: t})) : r && (this.setGeoJSON(r), this.fire("ready"))
                    }, this)), this
                }, loadID: function (n) {
                    return this.loadURL(e("/v4/" + n + "/features.json", this.options.accessToken))
                }, setFilter: function (n) {
                    return this.options.filter = n, this._geojson && (this.clearLayers(), this._initialize(this._geojson)), this
                }, getFilter: function () {
                    return this.options.filter
                }, _initialize: function (n) {
                    var t, o, f = L.Util.isArray(n) ? n : n.features;
                    if (f) for (t = 0, o = f.length; t < o; t++) (f[t].geometries || f[t].geometry || f[t].features) && this._initialize(f[t]); else if (this.options.filter(n)) {
                        var h = {accessToken: this.options.accessToken},
                            c = this.options.pointToLayer || function (n, t) {
                                return r.style(n, t, h)
                            }, i = L.GeoJSON.geometryToLayer(n, {pointToLayer: c}),
                            s = r.createPopup(n, this.options.sanitizer), e = this.options.style, l = e === u.style;
                        e && "setStyle" in i && (!l || !(i instanceof L.Circle || i instanceof L.CircleMarker)) && ("function" == typeof e && (e = e(n)), i.setStyle(e));
                        i.feature = n;
                        s && i.bindPopup(s, this.options.popupOptions);
                        this.addLayer(i)
                    }
                }
            });
        t.exports.FeatureLayer = f;
        t.exports.featureLayer = function (n, t) {
            return new f(n, t)
        }
    }, {
        "./format_url": 10,
        "./marker": 23,
        "./request": 24,
        "./simplestyle": 26,
        "./util": 29,
        "@mapbox/sanitize-caja": 2
    }], 9: [function (n, t) {
        "use strict";
        var i = L.Class.extend({
            includes: L.Evented.prototype || L.Mixin.Events, data: {}, record: function (n) {
                L.extend(this.data, n);
                this.fire("change")
            }
        });
        t.exports = new i
    }, {}], 10: [function (n, t) {
        "use strict";
        var i = n("./config"), u = n("./util").warn, r = n("../package.json").version;
        t.exports = function (n, t) {
            if (!(t = t || L.mapbox.accessToken) && i.REQUIRE_ACCESS_TOKEN) throw new Error("An API access token is required to use Mapbox.js. See https://www.mapbox.com/mapbox.js/api/v" + r + "/api-access-tokens/");
            var u = "https:" === document.location.protocol || i.FORCE_HTTPS ? i.HTTPS_URL : i.HTTP_URL;
            if (u = u.replace(/\/v4$/, ""), u += n, i.REQUIRE_ACCESS_TOKEN) {
                if ("s" === t[0]) throw new Error("Use a public access token (pk.*) with Mapbox.js, not a secret access token (sk.*). See https://www.mapbox.com/mapbox.js/api/v" + r + "/api-access-tokens/");
                u += -1 !== u.indexOf("?") ? "&access_token=" : "?access_token=";
                u += t
            }
            return u
        };
        t.exports.tileJSON = function (n, r) {
            if (0 === n.indexOf("mapbox://styles")) throw new Error("Styles created with Mapbox Studio need to be used with L.mapbox.styleLayer, not L.mapbox.tileLayer");
            if (-1 !== n.indexOf("/")) return n;
            var f;
            return n in i.TEMPLATE_STYLES ? f = t.exports("/styles/v1/" + i.TEMPLATE_STYLES[n], r) : (u("Warning: this implementation is loading a Mapbox Studio Classic style (" + n + "). Studio Classic styles are scheduled for deprecation: https://blog.mapbox.com/deprecating-studio-classic-styles-c65a744140a6"), f = t.exports("/v4/" + n + ".json", r)), 0 === f.indexOf("https") && (f += "&secure"), f
        };
        t.exports.style = function (n, i) {
            if (-1 === n.indexOf("mapbox://styles/")) throw new Error("Incorrectly formatted Mapbox style at " + n);
            var r = n.split("mapbox://styles/")[1];
            return t.exports("/styles/v1/" + r, i)
        }
    }, {"../package.json": 6, "./config": 7, "./util": 29}], 11: [function (n, t) {
        "use strict";
        var i = n("./util"), u = n("./format_url"), f = n("./feedback"), r = n("./request");
        t.exports = function (n, t) {
            function o(n, t) {
                var i = Math.pow(10, t);
                return n.lat = Math.round(n.lat * i) / i, n.lng = Math.round(n.lng * i) / i, n
            }

            t || (t = {});
            var e = {};
            return i.strict(n, "string"), -1 === n.indexOf("/") && (n = u("/geocoding/v5/" + n + "/{query}.json", t.accessToken, 5)), e.getURL = function () {
                return n
            }, e.queryURL = function (n) {
                var r = L.Util.isArray, c = !(r(n) || "string" == typeof n), t = c ? n.query : n, s, u, i, h;
                if (r(t)) {
                    for (s = [], u = 0; u < t.length; u++) s[u] = encodeURIComponent(t[u]);
                    t = s.join(";")
                } else t = encodeURIComponent(t);
                return f.record({geocoding: t}), i = L.Util.template(e.getURL(), {query: t}), c && ((n.types && (i += r(n.types) ? "&types=" + n.types.join() : "&types=" + n.types), n.country && (i += r(n.country) ? "&country=" + n.country.join() : "&country=" + n.country), n.bbox && (i += r(n.bbox) ? "&bbox=" + n.bbox.join() : "&bbox=" + n.bbox), n.proximity) && (h = o(L.latLng(n.proximity), 3), i += "&proximity=" + h.lng + "," + h.lat), "boolean" == typeof n.autocomplete && (i += "&autocomplete=" + n.autocomplete)), i
            }, e.query = function (n, t) {
                return i.strict(t, "function"), r(e.queryURL(n), function (n, r) {
                    if (r && (r.length || r.features)) {
                        var u = {results: r};
                        r.features && r.features.length && (u.latlng = [r.features[0].center[1], r.features[0].center[0]], r.features[0].bbox && (u.bounds = r.features[0].bbox, u.lbounds = i.lbounds(u.bounds)));
                        t(null, u)
                    } else t(n || !0)
                }), e
            }, e.reverseQuery = function (n, t) {
                function s(n) {
                    var t;
                    return t = void 0 !== n.lat && void 0 !== n.lng ? L.latLng(n.lat, n.lng) : void 0 !== n.lat && void 0 !== n.lon ? L.latLng(n.lat, n.lon) : L.latLng(n[1], n[0]), t = o(t, 5), t.lng + "," + t.lat
                }

                var u = "", i, f;
                if (n.length && n[0].length) {
                    for (i = 0, f = []; i < n.length; i++) f.push(s(n[i]));
                    u = f.join(";")
                } else u = s(n);
                return r(e.queryURL(u), function (n, i) {
                    t(n, i)
                }), e
            }, e
        }
    }, {"./feedback": 9, "./format_url": 10, "./request": 24, "./util": 29}], 12: [function (n, t) {
        "use strict";
        var r = n("./geocoder"), u = n("./util"), i = L.Control.extend({
            includes: L.Evented.prototype || L.Mixin.Events,
            options: {
                proximity: !0,
                position: "topleft",
                pointZoom: 16,
                keepOpen: !1,
                autocomplete: !1,
                queryOptions: {}
            },
            initialize: function (n, t) {
                L.Util.setOptions(this, t);
                this.setURL(n);
                this._updateSubmit = L.bind(this._updateSubmit, this);
                this._updateAutocomplete = L.bind(this._updateAutocomplete, this);
                this._chooseResult = L.bind(this._chooseResult, this)
            },
            setURL: function (n) {
                return this.geocoder = r(n, {accessToken: this.options.accessToken}), this
            },
            getURL: function () {
                return this.geocoder.getURL()
            },
            setID: function (n) {
                return this.setURL(n)
            },
            setTileJSON: function (n) {
                return this.setURL(n.geocoder)
            },
            _toggle: function (n) {
                n && L.DomEvent.stop(n);
                L.DomUtil.hasClass(this._container, "active") ? (L.DomUtil.removeClass(this._container, "active"), this._results.innerHTML = "", this._input.blur()) : (L.DomUtil.addClass(this._container, "active"), this._input.focus(), this._input.select())
            },
            _closeIfOpen: function () {
                L.DomUtil.hasClass(this._container, "active") && !this.options.keepOpen && (L.DomUtil.removeClass(this._container, "active"), this._results.innerHTML = "", this._input.blur())
            },
            onAdd: function (n) {
                var t = L.DomUtil.create("div", "leaflet-control-mapbox-geocoder leaflet-bar leaflet-control"),
                    r = L.DomUtil.create("a", "leaflet-control-mapbox-geocoder-toggle mapbox-icon mapbox-icon-geocoder", t),
                    f = L.DomUtil.create("div", "leaflet-control-mapbox-geocoder-results", t),
                    e = L.DomUtil.create("div", "leaflet-control-mapbox-geocoder-wrap", t),
                    u = L.DomUtil.create("form", "leaflet-control-mapbox-geocoder-form", e),
                    i = L.DomUtil.create("input", "", u);
                return r.href = "#", r.innerHTML = "&nbsp;", i.type = "text", i.setAttribute("placeholder", "Search"), L.DomEvent.addListener(u, "submit", this._geocode, this), L.DomEvent.addListener(i, "keyup", this._autocomplete, this), L.DomEvent.disableClickPropagation(t), this._map = n, this._results = f, this._input = i, this._form = u, this.options.keepOpen ? L.DomUtil.addClass(t, "active") : (this._map.on("click", this._closeIfOpen, this), L.DomEvent.addListener(r, "click", this._toggle, this)), t
            },
            _updateSubmit: function (n, t) {
                if (L.DomUtil.removeClass(this._container, "searching"), this._results.innerHTML = "", n || !t) this.fire("error", {error: n}); else {
                    var i = [];
                    t.results && t.results.features && (i = t.results.features);
                    1 === i.length ? (this.fire("autoselect", {feature: i[0]}), this.fire("found", {results: t.results}), this._chooseResult(i[0]), this._closeIfOpen()) : i.length > 1 ? (this.fire("found", {results: t.results}), this._displayResults(i)) : (this.fire("notfound"), this._displayResults(i))
                }
            },
            _updateAutocomplete: function (n, t) {
                if (this._results.innerHTML = "", n || !t) this.fire("error", {error: n}); else {
                    var i = [];
                    t.results && t.results.features && (i = t.results.features);
                    i.length ? this.fire("found", {results: t.results}) : this.fire("notfound");
                    this._displayResults(i)
                }
            },
            _displayResults: function (n) {
                for (var u, r, t, i = 0, f = Math.min(n.length, 5); i < f; i++) u = n[i], r = u.place_name, r.length && (t = L.DomUtil.create("a", "", this._results), t["innerText" in t ? "innerText" : "textContent"] = r, t.setAttribute("title", r), t.href = "#", L.bind(function (n) {
                    L.DomEvent.addListener(t, "click", function (t) {
                        this._chooseResult(n);
                        L.DomEvent.stop(t);
                        this.fire("select", {feature: n})
                    }, this)
                }, this)(u));
                n.length > 5 && (L.DomUtil.create("span", "", this._results).innerHTML = "Top 5 of " + n.length + "  results")
            },
            _chooseResult: function (n) {
                n.bbox ? this._map.fitBounds(u.lbounds(n.bbox)) : n.center && this._map.setView([n.center[1], n.center[0]], void 0 === this._map.getZoom() ? this.options.pointZoom : Math.max(this._map.getZoom(), this.options.pointZoom))
            },
            _geocode: function (n) {
                if (L.DomEvent.preventDefault(n), "" === this._input.value) return this._updateSubmit();
                L.DomUtil.addClass(this._container, "searching");
                this.geocoder.query(L.Util.extend({
                    query: this._input.value,
                    proximity: !!this.options.proximity && this._map.getCenter()
                }, this.options.queryOptions), this._updateSubmit)
            },
            _autocomplete: function () {
                if (this.options.autocomplete) return "" === this._input.value ? this._updateAutocomplete() : void this.geocoder.query(L.Util.extend({
                    query: this._input.value,
                    proximity: !!this.options.proximity && this._map.getCenter()
                }, this.options.queryOptions), this._updateAutocomplete)
            }
        });
        t.exports.GeocoderControl = i;
        t.exports.geocoderControl = function (n, t) {
            return new i(n, t)
        }
    }, {"./geocoder": 11, "./util": 29}], 13: [function (n, t) {
        "use strict";

        function i(n) {
            return n >= 93 && n--, n >= 35 && n--, n - 32
        }

        t.exports = function (n) {
            return function (t, r) {
                if (n) {
                    var u = i(n.grid[r].charCodeAt(t)), f = n.keys[u];
                    return n.data[f]
                }
            }
        }
    }, {}], 14: [function (n, t) {
        "use strict";
        var i = n("./util"), u = n("mustache"), r = L.Control.extend({
            options: {pinnable: !0, follow: !1, sanitizer: n("@mapbox/sanitize-caja"), touchTeaser: !0, location: !0},
            _currentContent: "",
            _pinned: !1,
            initialize: function (n, t) {
                L.Util.setOptions(this, t);
                i.strict_instance(n, L.Class, "L.mapbox.gridLayer");
                this._layer = n
            },
            setTemplate: function (n) {
                return i.strict(n, "string"), this.options.template = n, this
            },
            _template: function (n, t) {
                var i, r;
                if (t && (i = this.options.template || this._layer.getTileJSON().template, i)) return r = {}, r["__" + n + "__"] = !0, this.options.sanitizer(u.to_html(i, L.extend(r, t)))
            },
            _show: function (n, t) {
                n !== this._currentContent && (this._currentContent = n, this.options.follow ? (this._popup.setContent(n).setLatLng(t.latLng), this._map._popup !== this._popup && this._popup.openOn(this._map)) : (this._container.style.display = "block", this._contentWrapper.innerHTML = n))
            },
            hide: function () {
                return this._pinned = !1, this._currentContent = "", this._map.closePopup(), this._container.style.display = "none", this._contentWrapper.innerHTML = "", L.DomUtil.removeClass(this._container, "closable"), this
            },
            _mouseover: function (n) {
                if (n.data ? L.DomUtil.addClass(this._map._container, "map-clickable") : L.DomUtil.removeClass(this._map._container, "map-clickable"), !this._pinned) {
                    var t = this._template("teaser", n.data);
                    t ? this._show(t, n) : this.hide()
                }
            },
            _mousemove: function (n) {
                this._pinned || this.options.follow && this._popup.setLatLng(n.latLng)
            },
            _navigateTo: function (n) {
                window.top.location.href = n
            },
            _click: function (n) {
                var i = this._template("location", n.data), t;
                if (this.options.location && i && 0 === i.search(/^https?:/)) return this._navigateTo(this._template("location", n.data));
                this.options.pinnable && (t = this._template("full", n.data), !t && this.options.touchTeaser && L.Browser.touch && (t = this._template("teaser", n.data)), t ? (L.DomUtil.addClass(this._container, "closable"), this._pinned = !0, this._show(t, n)) : this._pinned && (L.DomUtil.removeClass(this._container, "closable"), this._pinned = !1, this.hide()))
            },
            _onPopupClose: function () {
                this._currentContent = null;
                this._pinned = !1
            },
            _createClosebutton: function (n, t) {
                var i = L.DomUtil.create("a", "close", n);
                return i.innerHTML = "close", i.href = "#", i.title = "close", L.DomEvent.on(i, "click", L.DomEvent.stopPropagation).on(i, "mousedown", L.DomEvent.stopPropagation).on(i, "dblclick", L.DomEvent.stopPropagation).on(i, "click", L.DomEvent.preventDefault).on(i, "click", t, this), i
            },
            onAdd: function (n) {
                this._map = n;
                var t = L.DomUtil.create("div", "leaflet-control-grid map-tooltip"),
                    i = L.DomUtil.create("div", "map-tooltip-content");
                return t.style.display = "none", this._createClosebutton(t, this.hide), t.appendChild(i), this._contentWrapper = i, this._popup = new L.Popup({
                    autoPan: !1,
                    closeOnClick: !1
                }), n.on("popupclose", this._onPopupClose, this), L.DomEvent.disableClickPropagation(t).addListener(t, "mousewheel", L.DomEvent.stopPropagation), this._layer.on("mouseover", this._mouseover, this).on("mousemove", this._mousemove, this).on("click", this._click, this), t
            },
            onRemove: function (n) {
                n.off("popupclose", this._onPopupClose, this);
                this._layer.off("mouseover", this._mouseover, this).off("mousemove", this._mousemove, this).off("click", this._click, this)
            }
        });
        t.exports.GridControl = r;
        t.exports.gridControl = function (n, t) {
            return new r(n, t)
        }
    }, {"./util": 29, "@mapbox/sanitize-caja": 2, mustache: 5}], 15: [function (n, t) {
        "use strict";
        var i = n("./util"), u = n("./request"), f = n("./grid"), r = L.Layer.extend({
            includes: [n("./load_tilejson")], options: {
                template: function () {
                    return ""
                }
            }, _mouseOn: null, _tilejson: {}, _cache: {}, initialize: function (n, t) {
                L.Util.setOptions(this, t);
                this._loadTileJSON(n)
            }, _setTileJSON: function (n) {
                return i.strict(n, "object"), L.extend(this.options, {
                    grids: n.grids,
                    minZoom: n.minzoom,
                    maxZoom: n.maxzoom,
                    bounds: n.bounds && i.lbounds(n.bounds)
                }), this._tilejson = n, this._cache = {}, this._update(), this
            }, getTileJSON: function () {
                return this._tilejson
            }, active: function () {
                return !!(this._map && this.options.grids && this.options.grids.length)
            }, onAdd: function (n) {
                this._map = n;
                this._update();
                this._map.on("click", this._click, this).on("mousemove", this._move, this).on("moveend", this._update, this)
            }, onRemove: function () {
                this._map.off("click", this._click, this).off("mousemove", this._move, this).off("moveend", this._update, this)
            }, getData: function (n, t) {
                if (this.active()) {
                    var i = this._map, r = i.project(n.wrap()), u = Math.floor(r.x / 256), f = Math.floor(r.y / 256),
                        e = i.options.crs.scale(i.getZoom()) / 256;
                    return u = (u + e) % e, f = (f + e) % e, this._getTile(i.getZoom(), u, f, function (n) {
                        var i = Math.floor((r.x - 256 * u) / 4), e = Math.floor((r.y - 256 * f) / 4);
                        t(n(i, e))
                    }), this
                }
            }, _click: function (n) {
                this.getData(n.latlng, L.bind(function (t) {
                    this.fire("click", {latLng: n.latlng, data: t})
                }, this))
            }, _move: function (n) {
                this.getData(n.latlng, L.bind(function (t) {
                    t !== this._mouseOn ? (this._mouseOn && this.fire("mouseout", {
                        latLng: n.latlng,
                        data: this._mouseOn
                    }), this.fire("mouseover", {
                        latLng: n.latlng,
                        data: t
                    }), this._mouseOn = t) : this.fire("mousemove", {latLng: n.latlng, data: t})
                }, this))
            }, _getTileURL: function (n) {
                var t = this.options.grids, i = (n.x + n.y) % t.length, r = t[i];
                return L.Util.template(r, n)
            }, _update: function () {
                var u, t, r;
                if (this.active() && (u = this._map.getPixelBounds(), t = this._map.getZoom(), !(t > this.options.maxZoom || t < this.options.minZoom))) for (var i = L.bounds(u.min.divideBy(256)._floor(), u.max.divideBy(256)._floor()), n = this._map.options.crs.scale(t) / 256, f = i.min.x; f <= i.max.x; f++) for (r = i.min.y; r <= i.max.y; r++) this._getTile(t, (f % n + n) % n, (r % n + n) % n)
            }, _getTile: function (n, t, i, r) {
                var e = n + "_" + t + "_" + i, o = L.point(t, i);
                if (o.z = n, this._tileShouldBeLoaded(o)) {
                    if (e in this._cache) {
                        r;
                        return
                    }
                    this._cache[e] = [];
                    r && this._cache[e].push(r);
                    u(this._getTileURL(o), L.bind(function (n, t) {
                        var r = this._cache[e], i;
                        for (this._cache[e] = f(t), i = 0; i < r.length; ++i) r[i](this._cache[e])
                    }, this))
                }
            }, _tileShouldBeLoaded: function (n) {
                if (n.z > this.options.maxZoom || n.z < this.options.minZoom) return !1;
                if (this.options.bounds) {
                    var t = n.multiplyBy(256), i = t.add(new L.Point(256, 256)), r = this._map.unproject(t),
                        u = this._map.unproject(i), f = new L.LatLngBounds([r, u]);
                    if (!this.options.bounds.intersects(f)) return !1
                }
                return !0
            }
        });
        t.exports.GridLayer = r;
        t.exports.gridLayer = function (n, t) {
            return new r(n, t)
        }
    }, {"./grid": 13, "./load_tilejson": 19, "./request": 24, "./util": 29}], 16: [function (n, t) {
        "use strict";
        var i = n("./leaflet");
        n("./mapbox");
        t.exports = i
    }, {"./leaflet": 17, "./mapbox": 21}], 17: [function (n, t) {
        t.exports = window.L = n("leaflet/dist/leaflet-src")
    }, {"leaflet/dist/leaflet-src": 4}], 18: [function (n, t) {
        "use strict";
        var i = L.Control.extend({
            options: {position: "bottomright", sanitizer: n("@mapbox/sanitize-caja")},
            initialize: function (n) {
                L.setOptions(this, n);
                this._legends = {}
            },
            onAdd: function () {
                return this._container = L.DomUtil.create("div", "map-legends wax-legends"), L.DomEvent.disableClickPropagation(this._container), this._update(), this._container
            },
            addLegend: function (n) {
                return n ? (this._legends[n] || (this._legends[n] = 0), this._legends[n]++, this._update()) : this
            },
            removeLegend: function (n) {
                return n ? (this._legends[n] && this._legends[n]--, this._update()) : this
            },
            _update: function () {
                var t, n, i;
                if (!this._map) return this;
                this._container.innerHTML = "";
                t = "none";
                for (n in this._legends) this._legends.hasOwnProperty(n) && this._legends[n] && (i = L.DomUtil.create("div", "map-legend wax-legend", this._container), i.innerHTML = this.options.sanitizer(n), t = "block");
                return this._container.style.display = t, this
            }
        });
        t.exports.LegendControl = i;
        t.exports.legendControl = function (n) {
            return new i(n)
        }
    }, {"@mapbox/sanitize-caja": 2}], 19: [function (n, t) {
        "use strict";
        var r = n("./request"), i = n("./format_url"), u = n("./util");
        t.exports = {
            _loadTileJSON: function (n) {
                if ("string" == typeof n) {
                    n = i.tileJSON(n, this.options && this.options.accessToken);
                    var t = -1 !== n.indexOf("/styles/v1/");
                    r(n, L.bind(function (r, f) {
                        r ? (u.log("could not load TileJSON at " + n), this.fire("error", {error: r})) : f && t ? (f.tiles = [i("/styles/v1/" + f.owner + "/" + f.id + "/tiles/256/{z}/{x}/{y}", this.options.accessToken)], this._setTileJSON(f), this.fire("ready")) : f && (this._setTileJSON(f), this.fire("ready"))
                    }, this))
                } else n && "object" == typeof n && this._setTileJSON(n)
            }
        }
    }, {"./format_url": 10, "./request": 24, "./util": 29}], 20: [function (n, t) {
        "use strict";

        function i(n, t) {
            return !t || n.accessToken ? n : L.extend({accessToken: t}, n)
        }

        var f = n("./tile_layer").tileLayer, e = n("./feature_layer").featureLayer, o = n("./grid_layer").gridLayer,
            s = n("./grid_control").gridControl, h = n("./share_control").shareControl,
            c = n("./legend_control").legendControl, l = n("./mapbox_logo").mapboxLogoControl, r = n("./feedback"),
            u = L.Map.extend({
                includes: [n("./load_tilejson")],
                options: {
                    tileLayer: {},
                    featureLayer: {},
                    gridLayer: {},
                    legendControl: {},
                    gridControl: {},
                    shareControl: !1,
                    sanitizer: n("@mapbox/sanitize-caja")
                },
                _tilejson: {},
                initialize: function (n, t, u) {
                    if (L.Map.prototype.initialize.call(this, n, L.extend({}, L.Map.prototype.options, u)), this.attributionControl) {
                        this.attributionControl.setPrefix("");
                        var a = this.options.attributionControl.compact;
                        (a || !1 !== a && this._container.offsetWidth <= 640) && L.DomUtil.addClass(this.attributionControl._container, "leaflet-compact-attribution");
                        void 0 === a && this.on("resize", function () {
                            this._container.offsetWidth > 640 ? L.DomUtil.removeClass(this.attributionControl._container, "leaflet-compact-attribution") : L.DomUtil.addClass(this.attributionControl._container, "leaflet-compact-attribution")
                        })
                    }
                    this.options.tileLayer && (this.tileLayer = f(void 0, i(this.options.tileLayer, this.options.accessToken)), this.addLayer(this.tileLayer));
                    this.options.featureLayer && (this.featureLayer = e(void 0, i(this.options.featureLayer, this.options.accessToken)), this.addLayer(this.featureLayer));
                    this.options.gridLayer && (this.gridLayer = o(void 0, i(this.options.gridLayer, this.options.accessToken)), this.addLayer(this.gridLayer));
                    this.options.gridLayer && this.options.gridControl && (this.gridControl = s(this.gridLayer, this.options.gridControl), this.addControl(this.gridControl));
                    this.options.legendControl && (this.legendControl = c(this.options.legendControl), this.addControl(this.legendControl));
                    this.options.shareControl && (this.shareControl = h(void 0, i(this.options.shareControl, this.options.accessToken)), this.addControl(this.shareControl));
                    this._mapboxLogoControl = l(this.options.mapboxLogoControl);
                    this.addControl(this._mapboxLogoControl);
                    this._loadTileJSON(t);
                    this.on("layeradd", this._onLayerAdd, this).on("layerremove", this._onLayerRemove, this).on("moveend", this._updateMapFeedbackLink, this);
                    this.whenReady(function () {
                        r.on("change", this._updateMapFeedbackLink, this)
                    });
                    this.on("unload", function () {
                        r.off("change", this._updateMapFeedbackLink, this)
                    })
                },
                _setTileJSON: function (n) {
                    return this._tilejson = n, this._initialize(n), this
                },
                getTileJSON: function () {
                    return this._tilejson
                },
                _initialize: function (n) {
                    if (this.tileLayer && (this.tileLayer._setTileJSON(n), this._updateLayer(this.tileLayer)), this.featureLayer && !this.featureLayer.getGeoJSON() && n.data && n.data[0] && this.featureLayer.loadURL(n.data[0]), this.gridLayer && (this.gridLayer._setTileJSON(n), this._updateLayer(this.gridLayer)), this.legendControl && n.legend && this.legendControl.addLegend(n.legend), this.shareControl && this.shareControl._setTileJSON(n), this._mapboxLogoControl._setTileJSON(n), !this._loaded && n.center) {
                        var t = n.zoom ? n.zoom : n.center[2], i = void 0 !== this.getZoom() ? this.getZoom() : t,
                            r = L.latLng(n.center[1], n.center[0]);
                        this.setView(r, i)
                    }
                },
                _updateMapFeedbackLink: function () {
                    var n, i, t;
                    if (this._controlContainer && this._controlContainer.getElementsByClassName && (n = this._controlContainer.getElementsByClassName("mapbox-improve-map"), n.length && this._loaded)) {
                        var u = this.getCenter().wrap(), e = this._tilejson || {}, o = e.id || "",
                            f = "#" + o + "/" + u.lng.toFixed(3) + "/" + u.lat.toFixed(3) + "/" + this.getZoom();
                        for (i in r.data) f += "/" + i + "=" + r.data[i];
                        for (t = 0; t < n.length; t++) n[t].hash = f
                    }
                },
                _onLayerAdd: function (n) {
                    "on" in n.layer && n.layer.on("ready", this._onLayerReady, this);
                    window.setTimeout(L.bind(this._updateMapFeedbackLink, this), 0)
                },
                _onLayerRemove: function (n) {
                    "on" in n.layer && n.layer.off("ready", this._onLayerReady, this);
                    window.setTimeout(L.bind(this._updateMapFeedbackLink, this), 0)
                },
                _onLayerReady: function (n) {
                    this._updateLayer(n.target)
                },
                _updateLayer: function (n) {
                    var t, i;
                    n.options && (this.attributionControl && this._loaded && n.getAttribution && this.attributionControl.addAttribution(n.getAttribution()), L.stamp(n) in this._zoomBoundLayers || !n.options.maxZoom && !n.options.minZoom || (this._zoomBoundLayers[L.stamp(n)] = n), t = this._mapboxLogoControl.getContainer(), L.DomUtil.hasClass(t, "mapbox-logo-true") || (i = n.getTileJSON(), this._mapboxLogoControl._setTileJSON(i)), this._updateMapFeedbackLink(), this._updateZoomLevels())
                }
            });
        t.exports.Map = u;
        t.exports.map = function (n, t, i) {
            return new u(n, t, i)
        }
    }, {
        "./feature_layer": 8,
        "./feedback": 9,
        "./grid_control": 14,
        "./grid_layer": 15,
        "./legend_control": 18,
        "./load_tilejson": 19,
        "./mapbox_logo": 22,
        "./share_control": 25,
        "./tile_layer": 28,
        "@mapbox/sanitize-caja": 2
    }], 21: [function (n, t) {
        "use strict";
        var i = n("./geocoder_control"), r = n("./grid_control"), u = n("./feature_layer"), f = n("./legend_control"),
            e = n("./share_control"), o = n("./tile_layer"), s = n("./map"), h = n("./grid_layer"),
            c = n("./style_layer");
        L.mapbox = t.exports = {
            VERSION: n("../package.json").version,
            geocoder: n("./geocoder"),
            marker: n("./marker"),
            simplestyle: n("./simplestyle"),
            tileLayer: o.tileLayer,
            TileLayer: o.TileLayer,
            styleLayer: c.styleLayer,
            StyleLayer: c.StyleLayer,
            shareControl: e.shareControl,
            ShareControl: e.ShareControl,
            legendControl: f.legendControl,
            LegendControl: f.LegendControl,
            geocoderControl: i.geocoderControl,
            GeocoderControl: i.GeocoderControl,
            gridControl: r.gridControl,
            GridControl: r.GridControl,
            gridLayer: h.gridLayer,
            GridLayer: h.GridLayer,
            featureLayer: u.featureLayer,
            FeatureLayer: u.FeatureLayer,
            map: s.map,
            Map: s.Map,
            config: n("./config"),
            sanitize: n("@mapbox/sanitize-caja"),
            template: n("mustache").to_html,
            feedback: n("./feedback")
        };
        window.L.Icon.Default.imagePath = ("https:" === document.location.protocol || "http:" === document.location.protocol ? "" : "https:") + "//api.tiles.mapbox.com/mapbox.js/v" + n("../package.json").version + "/images/"
    }, {
        "../package.json": 6,
        "./config": 7,
        "./feature_layer": 8,
        "./feedback": 9,
        "./geocoder": 11,
        "./geocoder_control": 12,
        "./grid_control": 14,
        "./grid_layer": 15,
        "./legend_control": 18,
        "./map": 20,
        "./marker": 23,
        "./share_control": 25,
        "./simplestyle": 26,
        "./style_layer": 27,
        "./tile_layer": 28,
        "@mapbox/sanitize-caja": 2,
        mustache: 5
    }], 22: [function (n, t) {
        "use strict";
        var i = L.Control.extend({
            options: {position: "bottomleft"}, initialize: function (n) {
                L.setOptions(this, n)
            }, onAdd: function () {
                return this._container = L.DomUtil.create("div", "mapbox-logo"), this._container
            }, _setTileJSON: function (n) {
                n.mapbox_logo && L.DomUtil.addClass(this._container, "mapbox-logo-true");
                n.tilejson || "mapbox" !== n.owner || L.DomUtil.addClass(this._container, "mapbox-logo-true")
            }
        });
        t.exports.MapboxLogoControl = i;
        t.exports.mapboxLogoControl = function (n) {
            return new i(n)
        }
    }, {}], 23: [function (n, t) {
        "use strict";

        function i(n, t) {
            n = n || {};
            var r = {small: [20, 50], medium: [30, 70], large: [35, 90]}, i = n["marker-size"] || "medium",
                u = "marker-symbol" in n && "" !== n["marker-symbol"] ? "-" + n["marker-symbol"] : "",
                f = (n["marker-color"] || "7e7e7e").replace("#", "");
            return L.icon({
                iconUrl: e("/v4/marker/pin-" + i.charAt(0) + u + "+" + f + (L.Browser.retina ? "@2x" : "") + ".png", t && t.accessToken),
                iconSize: r[i],
                iconAnchor: [r[i][0] / 2, r[i][1] / 2],
                popupAnchor: [0, -r[i][1] / 2]
            })
        }

        function u(n, t, u) {
            return L.marker(t, {
                icon: i(n.properties, u),
                title: o.strip_tags(r(n.properties && n.properties.title || ""))
            })
        }

        function f(n, t) {
            if (!n || !n.properties) return "";
            var i = "";
            return n.properties.title && (i += '<div class="marker-title">' + n.properties.title + "<\/div>"), n.properties.description && (i += '<div class="marker-description">' + n.properties.description + "<\/div>"), (t || r)(i)
        }

        var e = n("./format_url"), o = n("./util"), r = n("@mapbox/sanitize-caja");
        t.exports = {icon: i, style: u, createPopup: f}
    }, {"./format_url": 10, "./util": 29, "@mapbox/sanitize-caja": 2}], 24: [function (n, t) {
        "use strict";
        var r = n("@mapbox/corslite"), i = n("./util").strict, u = n("./config"),
            f = /^(https?:)?(?=\/\/(.|api)\.tiles\.mapbox\.com\/)/;
        t.exports = function (n, t) {
            function e(n, i) {
                !n && i && (i = JSON.parse(i.responseText));
                t(n, i)
            }

            return i(n, "string"), i(t, "function"), n = n.replace(f, function (n, t) {
                return "withCredentials" in new window.XMLHttpRequest ? "https:" === t || "https:" === document.location.protocol || u.FORCE_HTTPS ? "https:" : "http:" : document.location.protocol
            }), r(n, e)
        }
    }, {"./config": 7, "./util": 29, "@mapbox/corslite": 1}], 25: [function (n, t) {
        "use strict";
        var i = n("./format_url"), r = L.Control.extend({
            includes: [n("./load_tilejson")], options: {position: "topleft", url: ""}, initialize: function (n, t) {
                L.setOptions(this, t);
                this._loadTileJSON(n)
            }, _setTileJSON: function (n) {
                this._tilejson = n
            }, onAdd: function (n) {
                this._map = n;
                var t = L.DomUtil.create("div", "leaflet-control-mapbox-share leaflet-bar"),
                    i = L.DomUtil.create("a", "mapbox-share mapbox-icon mapbox-icon-share", t);
                return i.href = "#", this._modal = L.DomUtil.create("div", "mapbox-modal", this._map._container), this._mask = L.DomUtil.create("div", "mapbox-modal-mask", this._modal), this._content = L.DomUtil.create("div", "mapbox-modal-content", this._modal), L.DomEvent.addListener(i, "click", this._shareClick, this), L.DomEvent.disableClickPropagation(t), this._map.on("mousedown", this._clickOut, this), t
            }, _clickOut: function (n) {
                if (this._sharing) return L.DomEvent.preventDefault(n), L.DomUtil.removeClass(this._modal, "active"), this._content.innerHTML = "", void (this._sharing = null)
            }, _shareClick: function (n) {
                function f(n, t, i) {
                    var r = document.createElement("a");
                    return r.setAttribute("class", n), r.setAttribute("href", t), r.setAttribute("target", "_blank"), i = document.createTextNode(i), r.appendChild(i), r
                }

                var t, u, s;
                if (L.DomEvent.stop(n), this._sharing) return this._clickOut(n);
                var r = this._tilejson || this._map._tilejson || {},
                    e = encodeURIComponent(this.options.url || r.webpage || window.location),
                    o = encodeURIComponent(r.name),
                    c = i("/v4/" + r.id + "/" + this._map.getCenter().lng + "," + this._map.getCenter().lat + "," + this._map.getZoom() + "/600x600.png", this.options.accessToken),
                    l = i("/v4/" + r.id + ".html", this.options.accessToken),
                    a = "//twitter.com/intent/tweet?status=" + o + " " + e,
                    v = "//www.facebook.com/sharer.php?u=" + e + "&t=" + o,
                    y = "//www.pinterest.com/pin/create/button/?url=" + e + "&media=" + c + "&description=" + o,
                    p = '<iframe width="100%" height="500px" frameBorder="0" src="' + l + '"><\/iframe>';
                L.DomUtil.addClass(this._modal, "active");
                this._sharing = L.DomUtil.create("div", "mapbox-modal-body", this._content);
                var w = f("mapbox-button mapbox-button-icon mapbox-icon-twitter", a, "Twitter"),
                    b = f("mapbox-button mapbox-button-icon mapbox-icon-facebook", v, "Facebook"),
                    k = f("mapbox-button mapbox-button-icon mapbox-icon-pinterest", y, "Pinterest"),
                    h = document.createElement("h3"), d = document.createTextNode("Share this map");
                h.appendChild(d);
                t = document.createElement("div");
                t.setAttribute("class", "mapbox-share-buttons");
                t.appendChild(b);
                t.appendChild(w);
                t.appendChild(k);
                this._sharing.appendChild(h);
                this._sharing.appendChild(t);
                u = L.DomUtil.create("input", "mapbox-embed", this._sharing);
                u.type = "text";
                u.value = p;
                L.DomUtil.create("label", "mapbox-embed-description", this._sharing).innerHTML = "Copy and paste this <strong>HTML code<\/strong> into documents to embed this map on web pages.";
                s = L.DomUtil.create("a", "leaflet-popup-close-button", this._sharing);
                s.href = "#";
                L.DomEvent.disableClickPropagation(this._sharing);
                L.DomEvent.addListener(s, "click", this._clickOut, this);
                L.DomEvent.addListener(u, "click", function (n) {
                    n.target.focus();
                    n.target.select()
                })
            }
        });
        t.exports.ShareControl = r;
        t.exports.shareControl = function (n, t) {
            return new r(n, t)
        }
    }, {"./format_url": 10, "./load_tilejson": 19}], 26: [function (n, t) {
        "use strict";

        function u(n, t) {
            var r = {};
            for (var i in t) r[i] = void 0 === n[i] ? t[i] : n[i];
            return r
        }

        function f(n) {
            for (var r = {}, t = 0; t < i.length; t++) r[i[t][1]] = n[i[t][0]];
            return r
        }

        function e(n) {
            return f(u(n.properties || {}, r))
        }

        var r = {stroke: "#555555", "stroke-width": 2, "stroke-opacity": 1, fill: "#555555", "fill-opacity": .5},
            i = [["stroke", "color"], ["stroke-width", "weight"], ["stroke-opacity", "opacity"], ["fill", "fillColor"], ["fill-opacity", "fillOpacity"]];
        t.exports = {style: e, defaults: r}
    }, {}], 27: [function (n, t) {
        "use strict";
        var i = n("./util"), r = n("./format_url"), u = n("./request"), f = L.TileLayer.extend({
            options: {sanitizer: n("@mapbox/sanitize-caja")}, initialize: function (n, t) {
                L.TileLayer.prototype.initialize.call(this, void 0, L.extend({}, t, {
                    tileSize: 512,
                    zoomOffset: -1,
                    minNativeZoom: 0,
                    tms: !1
                }));
                this._url = this._formatTileURL(n);
                this._getAttribution(n)
            }, _getAttribution: function (n) {
                var t = r.style(n, this.options && this.options.accessToken);
                u(t, L.bind(function (f, e) {
                    var o, s, h;
                    f && (i.log("could not load Mapbox style at " + t), this.fire("error", {error: f}));
                    o = [];
                    for (s in e.sources) h = e.sources[s].url.split("mapbox://")[1], o.push(h);
                    u(r.tileJSON(o.join(), this.options.accessToken), L.bind(function (t, r) {
                        t ? (i.log("could not load TileJSON at " + n), this.fire("error", {error: t})) : r && (i.strict(r, "object"), this.options.attribution = this.options.sanitizer(r.attribution), this._tilejson = r, this.fire("ready"))
                    }, this))
                }, this))
            }, setUrl: null, _formatTileURL: function (n) {
                if ("string" == typeof n) {
                    -1 === n.indexOf("mapbox://styles/") && (i.log("Incorrectly formatted Mapbox style at " + n), this.fire("error"));
                    var t = n.split("mapbox://styles/")[1];
                    return r("/styles/v1/" + t + "/tiles/{z}/{x}/{y}{r}", this.options.accessToken)
                }
                if ("object" == typeof n) return r("/styles/v1/" + n.owner + "/" + n.id + "/tiles/{z}/{x}/{y}{r}", this.options.accessToken)
            }
        });
        t.exports.StyleLayer = f;
        t.exports.styleLayer = function (n, t) {
            return new f(n, t)
        }
    }, {"./format_url": 10, "./request": 24, "./util": 29, "@mapbox/sanitize-caja": 2}], 28: [function (n, t) {
        "use strict";
        var i = n("./util"), r = /\.((?:png|jpg)\d*)(?=$|\?)/, u = L.TileLayer.extend({
            includes: [n("./load_tilejson")],
            options: {sanitizer: n("@mapbox/sanitize-caja")},
            formats: ["png", "jpg", "png32", "png64", "png128", "png256", "jpg70", "jpg80", "jpg90"],
            scalePrefix: "@2x.",
            initialize: function (n, t) {
                L.TileLayer.prototype.initialize.call(this, void 0, t);
                this._tilejson = {};
                t && t.format && i.strict_oneof(t.format, this.formats);
                this._loadTileJSON(n)
            },
            setFormat: function (n) {
                return i.strict(n, "string"), this.options.format = n, this.redraw(), this
            },
            setUrl: null,
            _setTileJSON: function (n) {
                if (i.strict(n, "object"), !this.options.format) {
                    var t = n.tiles[0].match(r);
                    t && (this.options.format = t[1])
                }
                return L.extend(this.options, {
                    tiles: n.tiles,
                    attribution: this.options.sanitizer(n.attribution),
                    minZoom: n.minzoom || 0,
                    maxZoom: n.maxzoom || 18,
                    tms: "tms" === n.scheme,
                    bounds: n.bounds && i.lbounds(n.bounds)
                }), this._tilejson = n, this.redraw(), this
            },
            getTileJSON: function () {
                return this._tilejson
            },
            getTileUrl: function (n) {
                var i = this.options.tiles, f = Math.floor(Math.abs(n.x + n.y) % i.length), u = i[f],
                    t = L.Util.template(u, n);
                return t && this.options.format ? t.replace(r, (L.Browser.retina ? this.scalePrefix : ".") + this.options.format) : L.Browser.retina && -1 !== u.indexOf("/styles/v1") ? t.replace("?", "@2x?") : t
            },
            _update: function () {
                this.options.tiles && L.TileLayer.prototype._update.call(this)
            }
        });
        t.exports.TileLayer = u;
        t.exports.tileLayer = function (n, t) {
            return new u(n, t)
        }
    }, {"./load_tilejson": 19, "./util": 29, "@mapbox/sanitize-caja": 2}], 29: [function (n, t) {
        "use strict";

        function r(n, t) {
            if (!t || !t.length) return !1;
            for (var i = 0; i < t.length; i++) if (t[i] === n) return !0;
            return !1
        }

        var i = {};
        t.exports = {
            idUrl: function (n, t) {
                -1 === n.indexOf("/") ? t.loadID(n) : t.loadURL(n)
            }, log: function (n) {
                "object" == typeof console && "function" == typeof console.error && console.error(n)
            }, warn: function (n) {
                i[n] || "object" == typeof console && "function" == typeof console.warn && (i[n] = !0, console.warn(n))
            }, strict: function (n, t) {
                if (typeof n !== t) throw new Error("Invalid argument: " + t + " expected");
            }, strict_instance: function (n, t, i) {
                if (!(n instanceof t)) throw new Error("Invalid argument: " + i + " expected");
            }, strict_oneof: function (n, t) {
                if (!r(n, t)) throw new Error("Invalid argument: " + n + " given, valid values are " + t.join(", "));
            }, strip_tags: function (n) {
                return n.replace(/<[^<]+>/g, "")
            }, lbounds: function (n) {
                return new L.LatLngBounds([[n[1], n[0]], [n[3], n[2]]])
            }
        }
    }, {}]
}, {}, [16]);
/*!
    JDate
*/
isDate = function (n) {
    return /Date/.test(Object.prototype.toString.call(n)) && !isNaN(n.getTime())
};
Data = window.Date;
proto = JDate.prototype;
proto._persianDate = function () {
    return this._cached_date_ts != +this._d && (this._cached_date_ts = +this._d, this._cached_date = jd_to_persian(gregorian_to_jd(this._d.getFullYear(), this._d.getMonth() + 1, this._d.getDate()))), this._cached_date
};
proto._persianUTCDate = function () {
    return this._cached_utc_date_ts != +this._d && (this._cached_utc_date_ts = +this._d, this._cached_utc_date = jd_to_persian(gregorian_to_jd(this._d.getUTCFullYear(), this._d.getUTCMonth() + 1, this._d.getUTCDate()))), this._cached_utc_date
};
proto._setPersianDate = function (n, t, i) {
    var r = this._persianDate(), u;
    r[n] = t;
    i !== undefined && (r[2] = i);
    u = jd_to_gregorian(persian_to_jd_fixed(r[0], r[1], r[2]));
    this._d.setFullYear(u[0]);
    this._d.setMonth(u[1] - 1, u[2])
};
proto._setUTCPersianDate = function (n, t, i) {
    var r = this._persianUTCDate(), u;
    i !== undefined && (r[2] = i);
    r[n] = t;
    u = jd_to_gregorian(persian_to_jd_fixed(r[0], r[1], r[2]));
    this._d.setUTCFullYear(u[0]);
    this._d.setUTCMonth(u[1] - 1, u[2])
};
proto.getDate = function () {
    return this._persianDate()[2]
};
proto.getMonth = function () {
    return this._persianDate()[1] - 1
};
proto.getFullYear = function () {
    return this._persianDate()[0]
};
proto.getUTCDate = function () {
    return this._persianUTCDate()[2]
};
proto.getUTCMonth = function () {
    return this._persianUTCDate()[1] - 1
};
proto.getUTCFullYear = function () {
    return this._persianUTCDate()[0]
};
proto.setDate = function (n) {
    this._setPersianDate(2, n)
};
proto.setFullYear = function (n) {
    this._setPersianDate(0, n)
};
proto.setMonth = function (n, t) {
    this._setPersianDate(1, n + 1, t)
};
proto.setUTCDate = function (n) {
    this._setUTCPersianDate(2, n)
};
proto.setUTCFullYear = function (n) {
    this._setUTCPersianDate(0, n)
};
proto.setUTCMonth = function (n, t) {
    this._setUTCPersianDate(1, n + 1, t)
};
proto.toLocaleString = function () {
    return `${this.getFullYear()}/${pad2(this.getMonth() + 1)}/${pad2(this.getDate())} ${pad2(this.getHours())}:${pad2(this.getMinutes())}:${pad2(this.getSeconds())}`
};
JDate.now = function () {
    return Date.now()
};
JDate.parse = function (n) {
    return new JDate(n).getTime()
};
proto.toDateString = function () {
    return JDate.i18n.weekdays[this.getDay()] + " " + pad2(this.getDate()) + " " + JDate.i18n.months[this.getMonth()] + " " + this.getFullYear()
};
JDate.i18n = {
    months: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"],
    weekdays: ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"],
    weekdaysShort: ["یک", "دو", "سه", "چهار", "پنج", "جمعه", "شنبه"]
};
JDate.UTC = function (n, t, i, r, u, f, e) {
    var o = jd_to_gregorian(persian_to_jd_fixed(n, t + 1, i || 1));
    return Date.UTC(o[0], o[1] - 1, o[2], r || 0, u || 0, f || 0, e || 0)
};
["getHours", "getMilliseconds", "getMinutes", "getSeconds", "getTime", "getUTCDay", "getUTCHours", "getTimezoneOffset", "getUTCMilliseconds", "getUTCMinutes", "getUTCSeconds", "setHours", "setMilliseconds", "setMinutes", "setSeconds", "setTime", "setUTCHours", "setUTCMilliseconds", "setUTCMinutes", "setUTCSeconds", "toISOString", "toJSON", "toString", "toLocaleDateString", "toLocaleTimeString", "toTimeString", "toUTCString", "valueOf", "getDay"].forEach(n => {
    proto[n] = function () {
        return this._d[n].apply(this._d, arguments)
    }
});
/*!
 * Pikaday
 *
 * Copyright © 2014 David Bushell | BSD & MIT license | https://github.com/dbushell/Pikaday
 */
(function (n, t) {
    "use strict";
    var i;
    if (typeof exports == "object") {
        try {
            i = require("moment")
        } catch (r) {
        }
        module.exports = t(i)
    } else typeof define == "function" && define.amd ? define(function (n) {
        try {
            i = n("moment")
        } catch (r) {
        }
        return t(i)
    }) : n.Pikaday = t(n.moment)
})(this, function (n) {
    return function (t, i) {
        "use strict";
        var h = typeof n == "function", v = !!window.addEventListener, r = window.document, y = window.setTimeout,
            e = function (n, t, i, r) {
                v ? n.addEventListener(t, i, !!r) : n.attachEvent("on" + t, i)
            }, s = function (n, t, i, r) {
                v ? n.removeEventListener(t, i, !!r) : n.detachEvent("on" + t, i)
            }, w = function (n, t, i) {
                var u;
                r.createEvent ? (u = r.createEvent("HTMLEvents"), u.initEvent(t, !0, !1), u = c(u, i), n.dispatchEvent(u)) : r.createEventObject && (u = r.createEventObject(), u = c(u, i), n.fireEvent("on" + t, u))
            }, nt = function (n) {
                return n.trim ? n.trim() : n.replace(/^\s+|\s+$/g, "")
            }, u = function (n, t) {
                return (" " + n.className + " ").indexOf(" " + t + " ") !== -1
            }, tt = function (n, t) {
                u(n, t) || (n.className = n.className === "" ? t : n.className + " " + t)
            }, it = function (n, t) {
                n.className = nt((" " + n.className + " ").replace(" " + t + " ", " "))
            }, p = function (n) {
                return /Array/.test(Object.prototype.toString.call(n))
            }, f = function (n) {
                return /Object/.test(Object.prototype.toString.call(n)) && typeof n.getTime == "function" && !isNaN(n.getTime()) || /Date/.test(Object.prototype.toString.call(n)) && !isNaN(n.getTime())
            }, at = function (n) {
                return /Date/.test(Object.prototype.toString.call(n)) && !isNaN(n.getTime())
            }, rt = function (n) {
                var t = n.getDay();
                return t === 4 || t === 5
            }, ut = function (n) {
                return jalCal(n).leap === 0
            }, b = function (n, t) {
                return [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, ut(n, !0) ? 30 : 29][t]
            }, l = function (n) {
                f(n) && n.setHours(0, 0, 0, 0)
            }, a = function (n, t) {
                return n.getTime() === t.getTime()
            }, c = function (n, i, r) {
                var u, e;
                for (u in i) e = n[u] !== undefined, e && typeof i[u] == "object" && i[u] !== null && i[u].nodeName === undefined ? f(i[u]) ? r && (n[u] = new t(i[u].getTime())) : p(i[u]) ? r && (n[u] = i[u].slice(0)) : n[u] = c({}, i[u], r) : (r || !e) && (n[u] = i[u]);
                return n
            }, k = function (n) {
                return n.month < 0 && (n.year -= Math.ceil(Math.abs(n.month) / 12), n.month += 12), n.month > 11 && (n.year += Math.floor(Math.abs(n.month) / 12), n.month -= 12), n
            }, o = {
                field: null,
                bound: undefined,
                position: "bottom left",
                reposition: !0,
                format: "YYYY-MM-DD",
                defaultDate: null,
                setDefaultDate: !1,
                firstDay: 0,
                formatStrict: !1,
                minDate: null,
                maxDate: null,
                yearRange: 10,
                showWeekNumber: !1,
                minYear: 0,
                maxYear: 9999,
                minMonth: undefined,
                maxMonth: undefined,
                startRange: null,
                endRange: null,
                isRTL: !0,
                persianNumbers: !0,
                yearSuffix: "",
                showMonthAfterYear: !1,
                showDaysInNextAndPreviousMonths: !1,
                numberOfMonths: 1,
                mainCalendar: "left",
                container: undefined,
                i18n: {
                    previousMonth: "ماه قبل",
                    nextMonth: "ماه بعد",
                    months: ["فروردین", "اردیبهشت", "خرداد", "تیر", "مرداد", "شهریور", "مهر", "آبان", "آذر", "دی", "بهمن", "اسفند"],
                    weekdays: ["یکشنبه", "دوشنبه", "سه شنبه", "چهارشنبه", "پنجشنبه", "جمعه", "شنبه"],
                    weekdaysShort: ["یک", "دو", "سه", "چهار", "پنج", "جمعه", "شنبه"]
                },
                theme: null,
                onSelect: null,
                onOpen: null,
                onClose: null,
                onDraw: null
            }, d = function (n, t, i) {
                for (t += n.firstDay; t >= 7;) t -= 7;
                return i ? n.i18n.weekdaysShort[t] : n.i18n.weekdays[t]
            }, ft = function (n) {
                var t = [], i = "false";
                if (n.isEmpty) if (n.showDaysInNextAndPreviousMonths) t.push("is-outside-current-month"); else return '<td class="is-empty"><\/td>';
                return n.isDisabled && t.push("is-disabled"), n.isToday && t.push("is-today"), n.isSelected && (t.push("is-selected"), i = "true"), n.isInRange && t.push("is-inrange"), n.isStartRange && t.push("is-startrange"), n.isEndRange && t.push("is-endrange"), '<td data-day="' + n.day + '" class="' + t.join(" ") + '" aria-selected="' + i + '"><button class="pika-button pika-day" type="button" data-pika-year="' + n.year + '" data-pika-month="' + n.month + '" data-pika-day="' + n.day + '">' + (n.persianNumbers ? digits_en2fa(n.day) : n.day) + "<\/button><\/td>"
            }, et = function (n, i, r, u) {
                var f = new t(r, 0, 1), e = Math.ceil(((new t(r, i, n) - f) / 864e5 + f.getDay() + 1) / 7);
                return '<td class="pika-week">' + (u ? digits_en2fa(e) : e) + "<\/td>"
            }, ot = function (n, t) {
                return "<tr>" + (t ? n.reverse() : n).join("") + "<\/tr>"
            }, st = function (n) {
                return "<tbody>" + n.join("") + "<\/tbody>"
            }, ht = function (n) {
                var t, i = [];
                for (n.showWeekNumber && i.push("<th><\/th>"), t = 0; t < 7; t++) i.push('<th scope="col"><abbr title="' + d(n, t) + '">' + d(n, t, !0) + "<\/abbr><\/th>");
                return "<thead><tr>" + (n.isRTL ? i.reverse() : i).join("") + "<\/tr><\/thead>"
            }, ct = function (n, t, i, r, u, f) {
                for (var c, e = n._o, v = i === e.minYear, y = i === e.maxYear, h = '<div id="' + f + '" class="pika-title" role="heading" aria-live="assertive">', l, a, w = !0, b = !0, s = [], o = 0; o < 12; o++) s.push('<option value="' + (i === u ? o - t : 12 + o - t) + '"' + (o === r ? ' selected="selected"' : "") + (v && o < e.minMonth || y && o > e.maxMonth ? 'disabled="disabled"' : "") + ">" + e.i18n.months[o] + "<\/option>");
                for (l = '<div class="pika-label">' + e.i18n.months[r] + '<select class="pika-select pika-select-month" tabindex="-1">' + s.join("") + "<\/select><\/div>", p(e.yearRange) ? (o = e.yearRange[0], c = e.yearRange[1] + 1) : (o = i - e.yearRange, c = 1 + i + e.yearRange), s = []; o < c && o <= e.maxYear; o++) o >= e.minYear && s.push('<option value="' + o + '"' + (o === i ? ' selected="selected"' : "") + ">" + (e.persianNumbers ? digits_en2fa(o) : o) + "<\/option>");
                return a = '<div class="pika-label">' + (e.persianNumbers ? digits_en2fa(i) : i) + e.yearSuffix + '<select class="pika-select pika-select-year" tabindex="-1">' + s.join("") + "<\/select><\/div>", h += e.showMonthAfterYear ? a + l : l + a, v && (r === 0 || e.minMonth >= r) && (w = !1), y && (r === 11 || e.maxMonth <= r) && (b = !1), t === 0 && (h += '<button class="pika-prev' + (w ? "" : " is-disabled") + '" type="button">' + e.i18n.previousMonth + "<\/button>"), t === n._o.numberOfMonths - 1 && (h += '<button class="pika-next' + (b ? "" : " is-disabled") + '" type="button">' + e.i18n.nextMonth + "<\/button>"), h + "<\/div>"
            }, lt = function (n, t, i) {
                return '<table cellpadding="0" cellspacing="0" class="pika-table" role="grid" aria-labelledby="' + i + '">' + ht(n) + st(t) + "<\/table>"
            }, g = function (i) {
                var o = this, s = o.config(i), c;
                o._onMouseDown = function (n) {
                    if (o._v) {
                        n = n || window.event;
                        var i = n.target || n.srcElement;
                        if (i) if (u(i, "is-disabled") || (!u(i, "pika-button") || u(i, "is-empty") || u(i.parentNode, "is-disabled") ? u(i, "pika-prev") ? o.prevMonth() : u(i, "pika-next") && o.nextMonth() : (o.setDate(new t(i.getAttribute("data-pika-year"), i.getAttribute("data-pika-month"), i.getAttribute("data-pika-day"))), s.bound && y(function () {
                            o.hide();
                            s.field && s.field.blur()
                        }, 100))), u(i, "pika-select")) o._c = !0; else if (n.preventDefault) n.preventDefault(); else return n.returnValue = !1, !1
                    }
                };
                o._onChange = function (n) {

                    n = n || window.event;
                    var t = n.target || n.srcElement;
                    t && (u(t, "pika-select-month") ? o.gotoMonth(t.value) : u(t, "pika-select-year") && o.gotoYear(t.value))
                };
                o._onKeyChange = function (n) {
                    if (n = n || window.event, o.isVisible()) switch (n.keyCode) {
                        case 13:
                        case 27:
                            s.field.blur();
                            break;
                        case 37:
                            n.preventDefault();
                            o.adjustDate("subtract", 1);
                            break;
                        case 38:
                            o.adjustDate("subtract", 7);
                            break;
                        case 39:
                            o.adjustDate("add", 1);
                            break;
                        case 40:
                            o.adjustDate("add", 7)
                    }
                };
                o._onInputChange = function (i) {
                    var r;
                    i.firedBy !== o && (h ? (r = n(s.field.value, s.format, s.formatStrict), r = r && r.isValid() ? r.toDate() : null, r = new t(newDay)) : r = new t(t.parse(s.field.value)), f(r) && o.setDate(r), o._v || o.show())
                };
                o._onInputFocus = function () {
                    o.show()
                };
                o._onInputClick = function () {
                    o.show()
                };
                o._onInputBlur = function () {
                    var n = r.activeElement;
                    do if (u(n, "pika-single")) return; while (n = n.parentNode);
                    o._c || (o._b = y(function () {
                        o.hide()
                    }, 50));
                    o._c = !1
                };
                o._onClick = function (n) {
                    n = n || window.event;
                    var t = n.target || n.srcElement, i = t;
                    if (t) {
                        !v && u(t, "pika-select") && (t.onchange || (t.setAttribute("onchange", "return;"), e(t, "change", o._onChange)));
                        do if (u(i, "pika-single") || i === s.trigger) return; while (i = i.parentNode);
                        o._v && t !== s.trigger && i !== s.trigger && o.hide()
                    }
                };
                o.el = r.createElement("div");
                o.el.className = "pika-single" + (s.isRTL ? " is-rtl" : "") + (s.theme ? " " + s.theme : "");
                e(o.el, "mousedown", o._onMouseDown, !0);
                e(o.el, "touchend", o._onMouseDown, !0);
                e(o.el, "change", o._onChange);
                e(r, "keydown", o._onKeyChange);
                s.field && (s.container ? s.container.appendChild(o.el) : s.bound ? r.body.appendChild(o.el) : s.field.parentNode.insertBefore(o.el, s.field.nextSibling), e(s.field, "change", o._onInputChange), s.defaultDate || (h && s.field.value ? (s.defaultDate = n(s.field.value, s.format).toDate(), s.defaultDate = new t(s.defaultDate)) : s.defaultDate = new t(t.parse(s.field.value)), s.setDefaultDate = !0));
                c = s.defaultDate;
                f(c) ? s.setDefaultDate ? o.setDate(c, !0) : o.gotoDate(c) : o.gotoDate(new t);
                s.bound ? (this.hide(), o.el.className += " is-bound", e(s.trigger, "click", o._onInputClick), e(s.trigger, "focus", o._onInputFocus), e(s.trigger, "blur", o._onInputBlur)) : this.show()
            };
        return g.prototype = {
            config: function (n) {
                var i, r, u;
                return this._o || (this._o = c({}, o, !0)), i = c(this._o, n, !0), i.isRTL = !!i.isRTL, i.persianNumbers = !!i.persianNumbers, i.field = i.field && i.field.nodeName ? i.field : null, i.theme = typeof i.theme == "string" && i.theme ? i.theme : null, i.bound = !!(i.bound !== undefined ? i.field && i.bound : i.field), i.trigger = i.trigger && i.trigger.nodeName ? i.trigger : i.field, i.disableWeekends = !!i.disableWeekends, i.disableDayFn = typeof i.disableDayFn == "function" ? i.disableDayFn : null, r = parseInt(i.numberOfMonths, 10) || 1, i.numberOfMonths = r > 4 ? 4 : r, f(i.minDate) || (i.minDate = !1), f(i.maxDate) || (i.maxDate = !1), i.minDate && i.maxDate && i.maxDate < i.minDate && (i.maxDate = i.minDate = !1), i.minDate && this.setMinDate(i.minDate), i.maxDate && this.setMaxDate(i.maxDate), p(i.yearRange) ? (i.yearRange[0] > 1500 && (i.yearRange[0] = toJalaali(i.yearRange[0], 4, 29).jy), i.yearRange[1] > 1500 && (i.yearRange[1] = toJalaali(i.yearRange[1], 4, 29).jy), u = (new t).getFullYear() - 10, i.yearRange[0] = parseInt(i.yearRange[0], 10) || u, i.yearRange[1] = parseInt(i.yearRange[1], 10) || u) : (i.yearRange = Math.abs(parseInt(i.yearRange, 10)) || o.yearRange, i.yearRange > 100 && (i.yearRange = 100)), i
            }, toString: function (t) {
                var i = f(this._d) ? h ? n(this._d._d).format(t || this._o.format) : this._d.toDateString() : "";
                return this._o.persianNumbers && (i = digits_en2fa(i)), i
            }, getMoment: function () {
                return h ? n(this._d._d) : null
            }, setMoment: function (i, r) {
                h && n.isMoment(i) && this.setDate(new t(i.toDate()), r)
            }, getDate: function () {
                return f(this._d) ? new t(this._d.getTime()) : new t
            }, setDate: function (n, i) {
                if (!n) return this._d = null, this._o.field && (this._o.field.value = "", w(this._o.field, "change", {firedBy: this})), this.draw();
                if (typeof n == "string" && (n = new t(t.parse(n))), f(n)) {
                    var r = this._o.minDate, u = this._o.maxDate;
                    f(r) && n < r ? n = r : f(u) && n > u && (n = u);
                    this._d = new t(n.getTime());
                    l(this._d);
                    this.gotoDate(this._d);
                    this._o.field && (this._o.field.value = this.toString(), w(this._o.field, "change", {firedBy: this}));
                    i || typeof this._o.onSelect != "function" || this._o.onSelect.call(this, this.getDate())
                }
            }, gotoDate: function (n) {
                var r = !0;
                if (f(n)) {
                    if (this.calendars) {
                        var e = new t(this.calendars[0].year, this.calendars[0].month, 1),
                            i = new t(this.calendars[this.calendars.length - 1].year, this.calendars[this.calendars.length - 1].month, 1),
                            u = n.getTime();
                        i.setMonth(i.getMonth() + 1);
                        i.setDate(i.getDate() - 1);
                        r = u < e.getTime() || i.getTime() < u
                    }
                    r && (this.calendars = [{
                        month: n.getMonth(),
                        year: n.getFullYear()
                    }], this._o.mainCalendar === "right" && (this.calendars[0].month += 1 - this._o.numberOfMonths));
                    this.adjustCalendars()
                }
            }, adjustDate: function (i, r) {
                var f = this.getDate(), e = parseInt(r) * 864e5, u;
                i === "add" ? u = new t(f.valueOf() + e) : i === "subtract" && (u = new t(f.valueOf() - e));
                h && (i === "add" ? u = n(f).add(r, "days").toDate() : i === "subtract" && (u = n(f).subtract(r, "days").toDate()), u = new t(u));
                this.setDate(u)
            }, adjustCalendars: function () {
                this.calendars[0] = k(this.calendars[0]);
                for (var n = 1; n < this._o.numberOfMonths; n++) this.calendars[n] = k({
                    month: this.calendars[0].month + n,
                    year: this.calendars[0].year
                });
                this.draw()
            }, gotoToday: function () {
                this.gotoDate(new t)
            }, gotoMonth: function (n) {
                isNaN(n) || (this.calendars[0].month = parseInt(n, 10), this.adjustCalendars())
            }, nextMonth: function () {
                this.calendars[0].month++;
                this.adjustCalendars()
            }, prevMonth: function () {
                this.calendars[0].month--;
                this.adjustCalendars()
            }, gotoYear: function (n) {
                isNaN(n) || (this.calendars[0].year = parseInt(n, 10), this.adjustCalendars())
            }, setMinDate: function (n) {
                n instanceof i && (n = t(n));
                n instanceof t ? (l(n), this._o.minDate = n, this._o.minYear = n.getFullYear(), this._o.minMonth = n.getMonth()) : (this._o.minDate = o.minDate, this._o.minYear = o.minYear, this._o.minMonth = o.minMonth, this._o.startRange = o.startRange);
                this.draw()
            }, setMaxDate: function (n) {
                n instanceof i ? (l(n), this._o.maxDate = n, this._o.maxYear = n.getFullYear(), this._o.maxMonth = n.getMonth()) : (this._o.maxDate = o.maxDate, this._o.maxYear = o.maxYear, this._o.maxMonth = o.maxMonth, this._o.endRange = o.endRange);
                this.draw()
            }, setStartRange: function (n) {
                this._o.startRange = n
            }, setEndRange: function (n) {
                this._o.endRange = n
            }, draw: function (n) {
                var i;
                if (this._v || n) {
                    var t = this._o, e = t.minYear, o = t.maxYear, r = t.minMonth, u = t.maxMonth, s = "", f;
                    for (this._y <= e && (this._y = e, !isNaN(r) && this._m < r && (this._m = r)), this._y >= o && (this._y = o, !isNaN(u) && this._m > u && (this._m = u)), f = "pika-title-" + Math.random().toString(36).replace(/[^a-z]+/g, "").substr(0, 2), i = 0; i < t.numberOfMonths; i++) s += '<div class="pika-lendar">' + ct(this, i, this.calendars[i].year, this.calendars[i].month, this.calendars[0].year, f) + this.render(this.calendars[i].year, this.calendars[i].month, f) + "<\/div>";
                    if (this.el.innerHTML = s, t.bound && t.field.type !== "hidden" && y(function () {
                        t.trigger.focus()
                    }, 1), typeof this._o.onDraw == "function") this._o.onDraw(this);
                    t.bound && t.field.setAttribute("aria-label", "Use the arrow keys to pick a date")
                }
            }, adjustPosition: function () {
                var n, t, f, e, s, h, c, i, u, o;
                if (!this._o.container) {
                    if (this.el.style.position = "absolute", n = this._o.trigger, t = n, f = this.el.offsetWidth, e = this.el.offsetHeight, s = window.innerWidth || r.documentElement.clientWidth, h = window.innerHeight || r.documentElement.clientHeight, c = window.pageYOffset || r.body.scrollTop || r.documentElement.scrollTop, typeof n.getBoundingClientRect == "function") o = n.getBoundingClientRect(), i = o.left + window.pageXOffset, u = o.bottom + window.pageYOffset; else for (i = t.offsetLeft, u = t.offsetTop + t.offsetHeight; t = t.offsetParent;) i += t.offsetLeft, u += t.offsetTop;
                    (this._o.reposition && i + f > s || this._o.position.indexOf("right") > -1 && i - f + n.offsetWidth > 0) && (i = i - f + n.offsetWidth);
                    (this._o.reposition && u + e > h + c || this._o.position.indexOf("top") > -1 && u - e - n.offsetHeight > 0) && (u = u - e - n.offsetHeight);
                    this.el.style.left = i + "px";
                    this.el.style.top = u + "px"
                }
            }, render: function (n, i, r) {
                var u = this._o, g = new t, v = b(n, i), e = new t(n, i, 1).getDay(), nt = [], c = [], s, w, st;
                l(g);
                u.firstDay > 0 && (e -= u.firstDay, e < 0 && (e += 7));
                for (var tt = i === 0 ? 11 : i - 1, ht = i === 11 ? 0 : i + 1, it = i === 0 ? n - 1 : n, ct = i === 11 ? n + 1 : n, at = b(it, tt), y = v + e, p = y; p > 7;) p -= 7;
                for (y += 7 - p, s = 0, w = 0; s < y; s++) {
                    var o = new t(n, i, 1 + (s - e)), vt = f(this._d) ? a(o, this._d) : !1, yt = a(o, g),
                        ut = s < e || s >= v + e, h = 1 + (s - e), k = i, d = n,
                        pt = u.startRange && a(u.startRange, o), wt = u.endRange && a(u.endRange, o),
                        bt = u.startRange && u.endRange && u.startRange < o && o < u.endRange,
                        kt = u.minDate && o < u.minDate || u.maxDate && o > u.maxDate || u.disableWeekends && rt(o) || u.disableDayFn && u.disableDayFn(o);
                    ut && (s < e ? (h = at + h, k = tt, d = it) : (h = h - v, k = ht, d = ct));
                    st = {
                        day: h,
                        month: k,
                        year: d,
                        isSelected: vt,
                        isToday: yt,
                        isDisabled: kt,
                        isEmpty: ut,
                        isStartRange: pt,
                        isEndRange: wt,
                        isInRange: bt,
                        persianNumbers: u.persianNumbers,
                        showDaysInNextAndPreviousMonths: u.showDaysInNextAndPreviousMonths
                    };
                    c.push(ft(st));
                    ++w == 7 && (u.showWeekNumber && c.unshift(et(s - e, i, n, u.persianNumbers)), nt.push(ot(c, u.isRTL)), c = [], w = 0)
                }
                return lt(u, nt, r)
            }, isVisible: function () {
                return this._v
            }, show: function () {
                this.isVisible() || (it(this.el, "is-hidden"), this._v = !0, this.draw(), this._o.bound && (e(r, "click", this._onClick), this.adjustPosition()), typeof this._o.onOpen == "function" && this._o.onOpen.call(this))
            }, hide: function () {
                var n = this._v;
                n !== !1 && (this._o.bound && s(r, "click", this._onClick), this.el.style.position = "static", this.el.style.left = "auto", this.el.style.top = "auto", tt(this.el, "is-hidden"), this._v = !1, n !== undefined && typeof this._o.onClose == "function" && this._o.onClose.call(this))
            }, destroy: function () {
                this.hide();
                s(this.el, "mousedown", this._onMouseDown, !0);
                s(this.el, "touchend", this._onMouseDown, !0);
                s(this.el, "change", this._onChange);
                this._o.field && (s(this._o.field, "change", this._onInputChange), this._o.bound && (s(this._o.trigger, "click", this._onInputClick), s(this._o.trigger, "focus", this._onInputFocus), s(this._o.trigger, "blur", this._onInputBlur)));
                this.el.parentNode && this.el.parentNode.removeChild(this.el)
            }
        }, g
    }(JDate, Date)
});
var prefex = "mfl-", pages = [], currPage = 1, parentId, formId = 0, marker = null, versionValue = "ver 1.0.0",
    versionInfo = "<p>melrio.com : " + versionValue + " &copy; <p>", persianRegex = /^[\u0600-\u06FF\s]+$/,
    selfportraitAsBase64 = "iVBORw0KGgoAAAANSUhEUgAAAnYAAAJ2CAMAAADlij/LAAADAFBMVEVHcExeyPJfx/JeyPJeyPIw//9dyPNeyPNdyPJdyPJdyPJdx/FdyPJmzPxdyPJfyvNdyPNdyfNdyPJeyPSA//9VqutmzP9dyPJAv/NbyPJn1P9eyPNdyfJV1P9dyPNdyfJdyPJdyPNdyPJeyPJeyfNdyPJdyPJdyPNVxuNdyPNdyPNcx/Rcx/hdyfJdyfNdyfJdyPJdx/JdyPJfx/Bex/JgwvNfyfJYvepdyPFdyPNs0P1dyPJeyPJdyfNbyPNd0ftdyPNdyPKAgP9eyfJdxvNbyPFdx/IAANVaxfBcyPNeyPNbyPJdyPJeyPNeyfJV//9dyfJgyvNdyPJdx/KAv/9dyfNixO9dyPJdyPJgy/JcyPJdyPJdyPNdyPNdyPNdyfNfyfRdyPJdyPJeyPJdyPJdyPNdx/FezPJdyPJeyfNdyfJdyPNeyfJVx/9eyPJdyfJeyPJcyPJeyPJcyPJgzv9dyPNdyPNdyPNdyPJdyPJdyPNeyPJeyfNdyfNcyfJdyvFcxvFeyPNdyPJeyfNdyPJPt9ldyPNdyPJdyPJcyPJdyPJcyfJcyPJdyPJdyfJdyPNex/Jdx/JdyPJdx/JdyPNdyfJgv99dyfRdx/JdyPJdyPNdyPJeyfJdyPFfyfRcyPNdyPJdyPJex/FfyPJdy/NdyPJdyfNdyfJdyPNjx/NdyPJcyPJeyfNdyPJeyPJdyPNdyfJdyPJdyfJdx/NdyfNdyfJdyPJdyPJeyPJdyPNdyPNdyfNeyfNmzMxdx/FdyPNcyfNdx/Jdx/NdyfNdyPJeyfJdyPJdyPFdx/JdyPFdxPBeyPNdyPJdyfJdyPNcyPFcyPJeyPJeyPJeyfVdyfNdyfNcyfJdyPJdyPJdyfJhzvNdyPNeyfNeyfNcxvFi2P9dyPKA//9eyPNdyPFcwutdyfJcyfJcyfJdyPNdxvZdyfNdyPFeyfJdyfNdyfJdyfNVxvFdyfNdx/KAgIBdyPNdyPJdyPNc0f9eyPNdyfNZv/JdyvNeyO5fy/VdyPJeyPIwG6+UAAAA/nRSTlMAT054wQF+mLWM/ZL+CtQ++f78bwIDBd4EDgyaYwb2/vr27EL586H7CT+oLSTZ5v3ya9gPNhUwC/T3B8y5Mh8L/OACdRYc+AERF9IZz+VRA90Y3/wEpg35yhpb7oaxU3NZacnw7UiAFMcT67pMCYvcPDSf+wi91ud6hF8gz1cvLSQ5s31VBq12ifDpRT3zbNFajZ7hvvQITNOk6JW89R0qr0MrKCJGql2QEoJnjzol5O9uxSmAS2RltErNYSYFvPon6nyHxJaXqJxxIuOyt8I3/t5iN+F7UNu36hVxZ5NRDaME6akZWaCKvxr54pzTja8S/sACu3eRENLAFG48Taztn3gAAGlNSURBVHja7Z0HVBTX/vhvEkcNLvhCAEFDOSAqMQiIgoCgorSjBguKLRBEFLBERFHEgo3Yji12IgIq9t5rVIg9amyoSXz+XqrG+HKSGE3eP7v/mQXLvXNnd3Z37t0ZmO85v995kdmZO/d+5pZvBUAVVVRRRRVVVFFFFVVUUUUVVVRRRRVVVFFFueLcwiUjrn/EhR0n95ZObVklpWvXHVufF/Vd28AOrozaRapII6E9124fMWDBvk1jiselH7Gr/KT7nV3ebk2ba7RV0tEjZNkcz/EXe+Qs7Hf5UJ8z5cnXkryCIlxUBFUxVZyCY/Pmf5i8qexypWfqsqYdtSaJxsPNf+DOsEOPhh5Mm9q2b2ik2qGqGBQm9tjojUN/GJezy10rjSR2Si94HJA25cEstXdV4cvylkkTVqU3GmivJSBvrem+8tCoA/Pvqv2sSpXEb85/fcLZlW72Gi1pscnu3n7x0fDvWqi9XqvFJXzAqBOVTbVURTN38A+7vaIc1O6vhbs416j3By3duewtrXXEw3PlkpRwW0d1JGoPcl2uLOjj66e1vgz5NHf/eld1RGq+9PWZUL+Xh1Y24u45uHByhKrmq7niEHv9jT+GyAi55+K3xq4wqX+oOkI1T0JbppTdcNfKV7wvl5e0VcepJonthsJn/lrZS/aNS1sj1NGqGdPcya+nt9MqRjTPyhv+GqMOm6Il4fqMCm+twsTGd0nS3+rYKVVPMnLaj3ZuWkWK/eqCAXdVpZ7yxGX+Nl+NVskyp3iAqlhRlDhm/Wj3UKt40awunhyojqYyZNbe0zmfaWuI+A28nxarjqncJabo4E8k93M2brtOdb/Rw7d1a7tnz1bmtG5d2Wvnak9vkjpoTeWb05zUkZWzsmRDn7mSbug0Nr2z/SvT6zfbNmP3xFtebUovRER1jWubERsY6NK3r0tgYODmYV169i/KOxY0+vD3W5+UNyn4beXcxOyO0lp83fr9r6e6zZOpsqR0XyPJ1jb/nTm/rSrfnbR9hxlhEa5R4WnXZtQ71K2R578kI+/sYXWbJz8JTqoviR0iu1NFny0TfUq7SmAidfg1v829r858YSdJy+ztTuer4ywncV6/pVdvC0f1oX+vQ48PjO7aV/LAw3jbzaVJp+9PGuJm4QbA3b94Q7A62nKZ6NIKllkYZ1OncKtPFOkQr75BM/cV2+2y0Hb2doRqOpODXjggzJJz5Jylo76f2iWeXnPXz+v8bQ8bC8Abf2aqOupW1guvW5xp7spl79nt3LVSa4SxMvFx+7cM3xlttkf8if3qWms9cfBZZe5mffWnWw6vt67R89crKX1yss1UH/ZLUZXIVlLSjTgRYtYs5//p6YZ3ZaGAZVzWHf3h54fmeJ9qGs2IU1V51CV24iTzJopdh2a8LStJbtLavH3CrsWlKnhUJTDAzkarysBzLVXwaElMxsef+KnMVRkvvg1PUImgIT0ndFJxeynRZdvVBAPk9V5PVOgQaVcWpHJBVO5+NVfFDKNPeTdc9YAnJo0X+LqrjGHFe8xalQ8iMnaAnXqQMKBOKSxST7XSK4f/SVfRMiye+/qrnEgrWV8kqlwZlV4pI1VUpLMhHWsWrTIlyiv6l3tqBh+pjq9DPVWgRB9qD21Xt3gSSMKASo1KkymH2nrqFs9ScWh4WYXOVMlMdlHJsUSKzriZtrfJrLSrQVI510yHh8GHVYOZ2TIypZUJKa17dyq8lRcYbFuDJDgwYkWDj8xxBU08u17d4pl3fvWpMGF9tU+f2aFm9kOL1/8yB7zMj/uqDJkuGZtMSYboebAGRxeM3NjKDO406fPVCc9Ecf3+Z5N6OL9md0fUuKtmgOdRr6dKkimyrsyk2l/Ta3wxr9h3zTrQVw5wVmESPdUFnDKpcytqQXaQ4C9QoqaLMRd6FOSp0dziJHycaV92ZV5t6JW4lfBbD9mx/ZIY7dJ/UparSIk4uCUPNG0haZpUOzpmAwyZ+wHg1LC+iBLLfh+cVKkypjUJumyqhrSgljjWMkuQ92Y1wk4lv4lIXJqZovoHGBTbZJMz02SH15bOyYfdcDrpd7TOSenGnV/9Lq1T2RKW9SeMH2BDvoX/e2mtOasxxTBLe6sPG0dFBDW12vi7ihdeQg8an+rc+hw7DP/LgdrTQUnwm09+/u8dOnc3PuGVqWWlsNK/zKgNyGZcSSTYDf2Tfy0KXcmD/Q5nvPKXTcZrDfUYEalChorD/o+M2iJ6fDiWvXIU9I85tcjHZ2wY9OqPXu2+KfWNZvlz+1P1h0IkcJTRXjtVtyq31g/Qv46rTb1UAL16MfQ3xxFhRtWd/bJUK+2rMtWoM2f2u3uxfd+nNnVTocEvLrazUef/zAXqQvtCnDYaO0vYLCx5oZ2bDv3lXG3qqMXQq9fhHXWjVhmzW9j06aryViV/NzGm8jz18Su7kvrQn+rVpp560zB27Eo7r5+xCc/ORyWOk9IwIxkm7D9Y96o5W8VOGDt2kzxhjhHu/BccV6FzvmbMBNv6FhwXoGJnCDsALhQbOZ1pymq9G17fBkb6KGQT2kcqdoaxA64bjZktumXVbuqO1TF8gvXrNppn6FexM4Idm3lyjJEsCp7XanFiMibNSIzAnKH/5v9Kxc4odsAhzc5wz15dXGtVx87JRqpK9JuG026q2BnHjvUKLTS8e/EbHlU7qft1leEF1n+CLfZ3KnZisAOMl53h/u3UsDaaLNZVvGU4GGyawA9V7ERhB0CXb5oa5G7gwVqXOoDxuWE4qr1cMCxHxU4kdsB5heGoz4c/jq1d1CVsNeyo88xLON6JEnaueVNbmipTT7rICTsAvrtv0ACk+aBWmcqOjzI4/dssiTPwYyrYMfsvZ0Y3NVn8czoHywk74HQt0+D3feRY7aGubZnBGPdWAwwWlqGBnfMMc9OHXh2+WU7YgZi9Sw2eLC7Oqy0Hi/f2GJz4x+0w/HMa2L3+0PzscudkhR3rHJprMJmM/9Ha4QsV7mswjD3X2PaIBnaWZIX33iEv7IBjmsGTRfTNWuAawLxm0Bex0Qajd6CA3XdcodplBc1Mlfu9yAYVmYcdqzv+oLeh9WVJja/FHfmOoRlfM309kAN2LTkNf4EZi88Arkk3ZYcdGNl5jaGPvbhLzabO9bShgtchW1oAWWAXxGF334wf3kIiuuSCHWBKGhn63PfU6DQyIwsNRbB3el3UoYoCdlM47PqYkSopSa7YsWmgpxua73yn1lzqXAxaYdNFqpAoYBfB5fA6ta+zqfJEfxQJkCV2ILjcUGayXqNrKnV3hxugzuOMWAU/BeyYHAtOsiHh8sQORCYZqoo6cH/NVOBFVBjyrAuIB/LBDhywoJbj9EiZYsfuWScZGoIBNdEzYH2YoW2dl/hPjQZ2Y38wuwhL6/VAttiB7woMvFe7r2vefFfaS/h93SuKTLgTFZus6/le5mTl13iOeQBkjB1wzDVg9fP7qqYpjrMMZDixH5MB5IYdazgefet9k2VePtEJw3LsQPxGA5F6iUNrVta2aTsNvOuTWUCG2MlRJMAOMFd6GKhkVF6T5jsfA0eoO9+bOEGo2FmEHRtIW0c4Ht5mcc3x/PQyECD2s8nlT1XsLMQObC4THo/mhTVlnS1JNRAxYbpVRsXOUuzArHJhN1v3ei1qRF9tMFDbpDgOqNjRxw44pwjbxjt+XhO4KxGmzm+JLVCxswZ2gJk5RJi7espfZ72EV9joGU5Axc462AGwXViR6n5G6fOdj/BpIjrFvDwcKnaSYAdKhQ3PzTc5KbqfpglrToZMNlOzqmInDXaGjOQ2o5Ssv8sS1hKPLzH3puKwc83Lmt9QiTI6vMiRDnZgWH1hy9EW5c53pcIWsYvTAEns1r25MjOkt40SJdttdfrN/lSwA7HfCiqOPZ4o1S9gvfCmNceCIibGsYu476FVtrjlZtDADiw/I+jhpVmgTD+oCGFPp5WWOAcZxc6nUqt4eSv9JA3sgOufglXe3I4qcb67K7xhHWxR+Stj2M3L1NYE+egkDezArFzBLCne95RHnctwwQ69bFnGZiPYra+iLqTb/W8aKFG+KcupmoDCutDADjATBDMgzPFSGnUjhaN1frMwT7hh7FyrAqTG+dgCpUrgvar67G8yNLADzm/8S/Dgp7DU2q6FgtTdzgAksXudO03YN1B2uejN+sqx0eupYAccdgs6BvTaoaRuizwteEAa/hSQxC7hEvePZUo37gR2416jnA52AOwWdAw40l85nca8I/gaw+8Cotj15GrQ+Ocr3h6Rxm307VwpYef4P6EQ2rfaByqmz14TzHNS/ykgi90G7t++VTx1wJbTPu1aRwk7dr4TDN3+1lUhXRYumNPpt2GAMHYpZMPy6Uk99j2yfahhF5ksuL8rj1dEh73nS0hzIga7Cdy/JdUA7GZwL7KfGnbA8YmQZSdxtxLyLrYVzNWZLk2FNYPY1eX+7bUagN0b3Iu8Tg87wAwVyoHndkv+3XVcMDzkWQRQsZMvdmDWKBuBkZsbJPfeShgllA07R6okDSp2ZLADTtuElK2tZZ7+jtkqtDO9uBeo2MkbO+B6X2ilWtpX1p3lI1TlZPx1oGInd+xAYLFg+nk5R/WsE6roNKQEqNjJHzsQ+IdQdMXH8vW++1XI1ylkJlCxUwJ2oKtQBrx2I+TaU86r3hKKEWNU7JSBHdghpHW9s06mx4lkgYOQ32lJK4Sr2JHEDoSvFnLOlWcVgTShWtiPpA1CUrEjih1IEzgWaprJMZjsmEAgtqZYYodLFTuy2IEBbgLzhwyDyfrWETKJST03q9gRxs5hglBwxQa59ZJzA4GN3Q3JFdwqdoSxA8ebCAxmjwsy66VrAv4Lp7YDFTulYQf6fiBkrRgpq04qFcjD3HQyo2KnPOzAd78IcJcrp+3d3wKR2PYfE2ilih157ECpwAHRLSlGNl3k1MRdoFDpLKBip0jswH4BddjFY7Lpoo0CgeWXNwMVO4ViF7lbIPhvqVxiK6buEgiyjAIqdkrFjo3mwB9nNYMcZdFBgZfx1KUSSmWgYkcHu9ilAk4BaXLoH4dR+K/CI4VRsVMyduCCQLmeHl1l0D/7BTR2haTmYhU7StiB+QLW2TLrb+/6C2TsrPg3ULFTOHbMV/jkd9lbrd07oQKBYp3I5WxRsaOFHXB6hN9AZVo7lOwgvupqyAqgYqd87ECsgB1gj3Uzaq3H6040WxgVu5qAHZgikBr1tDWNZLYn8I36gGR1SBU7itiBjficPP4Nrdc1TLK9FfxjVOxoYpdQiJ9ZwqyXgCwIv8Q+LAEqdjUFu6qUZxgZZa1ltsVlgY0dULGrOdiBKfgCXm4+VuqZZHyqlhN9VexqEnbMVry2IizWKh0Tjnft7J4PVOxqEnYgspmMllnXcXjPzg+Bil3Nwg7EtcYvs1es0C8BWAW2+yNnFbuahh0owRtnB9PPA7UOX4t9IfnQcRU76tgxW7BzzNUZv9NeYvG22KYUygWp2FHHDgQPxifymkq5V77HKoo1uUDFriZiB6YMwXI3LoFqp2T8jLcQB6rY1UzsmPPYhNqaazT7hNmEN9RdByp2NRM7MBJvfu9VRLENPvgyOxOAil1NxQ68h1fTnqNXLGUkPmtn+kgVu5qLHViANUol0rORpWCP07vaABW7GozdyPrYyaYfrbQoRdhEBX4zGBW7mowdWIutJWdzns7THc5gUxRPoqWyVrGzEnagMz49BZ1aKQ2xyR7dtgMVuxqOXYvfsNwtobHMJeC97BY7qNjVdOzAFaxfrwcNvdkA7HkiJw6o2NV47JhyfDYe8nFkdyux7k7/ABW7mo8dCMa6QDWfSJz3odjJriBSxa42YAdex6YeaZ1B+LHHsIfo1TRz7anYWRG743hP47qEH4t9qk0yULGrHdiB/PE4AgZGEH1oVjRWZfdvFbvagh34GDvdfU5SiRL6BTYBkBdQsas12C0/gnU+IhlX8Q82M8EqRxW72oMdSMOueMXkYmjGpuMeOPckULGrRdg598FR0JucJ8oArIPpaaBiV5uwAzuwnnd1SKX4bGyHe1xloIpd7cIObMGayN4n9LQFuEoFNjOBil0tw25za4pRs3ex5bxPLFexq23YgYm4rCh+A4g86ytchSc3L6BiV+uws92DNZEdJ/AoF2y+qfuMil3tww74dMTB8A6BJz3BOnfmAxW7Wogdgy04u1L602XPTrgHvQlU7GojdiAcV4DRRvJ6FTHY+vE7i1Tsaid2DmdwPNhJPd1l4CY7zQxGxa52YgeOYXXGUvt7Yv0OemwGKna1FDswG3uYbSHpMwI/wT0kAKjY1Vrsuq7GENFR2ukuAGeg8HVRsau92DF1saaKYAkfEYuzxtq8A1Tsai92oGsv3HSXJOETJuKyriwMVLGrzdiBCbhorgrpEi2GTsJNdhuBil2txu7pTpx2Qzpr6QisjmaWil3txg4k47g4JFV6CAdsIsejQMWulmMXh9NvLMuS6O4+IbidnYuKXW3HDryB2901k2a6c1xFxfymYqc87PpfxMXM5kty73U4q2/rQBU7FTuwD7f9kqZKxGLcrXcDFTsVO9AWl3nslBT7Lxdctfi5gSp2KnasY9I2nA7loAR3xpa0mwFU7FTsWDmJq84TZmvxfYNxNbvH59VA7OKXt2jRwtVBxc4UcXiEweNhmsX3TcMlNPuGqTHYxTg+3X4wd9WJI749enXq1OnLj3L2HKo3YXLpWAcVOzGShctkXWZpjRTnAsxdvdeCmoHd2GPXHuXY63Cyps6otP6OKnZG5RAuxGaHhTddvwyXuxPUBOwcWtbd468zIJrulyb2VLEzIhs0BHQouLwDHUcrHzsm9ujlNTBkNr2bN29u7wf9o5/n/fmhKnaGpAWu1OwnlmUICMb5VNWZpXjsIvZ98gIt+/Fhxds6H036J23Dhg2Hb01OmXHuj5zUl3+u+DBYxc6AHMV4xdkvsuiWSZgqolcnAoVjl7Fl53OoPAv+dz1qLG8uDMzfX74n+zl4gyczKnaCMqwRZmq6bUmGgARcdTPfzcrGLvjg+CqcbL4847VcGCiHLtfq76pegH8b7Sg/7FqH95UFd7iN2JogC25YijPHDgWKxu76X1Xbt6b1Jxs3tay7mVMF3rJtw2SHndbj2ajtTtbHLg+nQym34IY4S69blJKxGzmhatfm1idLXBrApxuf6THV2HkxcsOOG42wrzcz1uauDFcAz/xMYKG4VfssUDB2F5ZWzXQFJqwBsybmaLgfJda1lR92nH180Horg+fTHNOse+arZHCOdiXKxS5mXg89dc/+MW1p6lJXr2vpWNBTjtix1WhGxVl1RDrgDKjF5t4tBpcbuV8HxWLneNCbo8d7tsk7cSboJxvup7+skyV2Wm2rt12sOST/w6iMh5hrqSjCZLTT/A8oFTvXGc05dHJKzFmSbPfpJ7xODeWJHTsdeFnRiByFy31obmmmgxiGPYuUil38bA8OnPpmnogiV7Tifr7aR6bYab0fW3Ed+gHToPSRZt1q1k+47J1Aodg5vclR81m5+RaWvDDuDqleMsWOjaq6brWjxTTMDNXbvBCyvTh1jJdCsXOuy62wIcmWrERdv+C4635drthpPb+2FneuCzHNGWXWrU7jYrJDFYrdwUTOmemAZbaGzf/luOsVIVfstNnbrLXQdsb5A5hTIMUxB3OnN4AysSuJ5uY6iyPKg6dz3KUPkyt2Ws0lK6lS8jF1hu3NcTLO+ox/o9S1ysQu/0uWlt7Jlpf4bsupmzWPZskVOzb1zQOrDIpjMaYt58y40Y84v4J4RWIX/CnnOlcuRQ3AB79wtwqQL3baSeutMipJ9vym9Opi8m1cMCnt/CYCRWJXl1sai6XZlgZlsvfa1VK+2GntIqwxKn1bYSxat0y+zfyHmJPSZkVil8UZJ1r3l+bpzEbucLK0gwyw8/fHczf4O2sMCy51ezNTtzUMLu72B6BE7PpWSKpucyjktnfJjPWxqxN3+iKWu58yrKG6wzgZ33lq4k1G4urapSkSuxTOnJorHSZPfdn7ZUbIADvWfjloDo67s870h6UDTnX3uqnsYtTOPZ4qEbsunFHLbqSEDTjMLbOPGDlgB35f9wXG6+jqUCsYaLdIYNT6UW5B2WZjV87510k6Tyf8wLkbt5EFdqyB4GuMHd5tMv1xyVqD8YwxLVdOAuYcm+gFFIhdBBc4USatom2dJ3vPVY7ywI49XGNSS7d6j/q4zDqCMZscNukW1zH22NbBCsSOyWUJ8Q+SuAmDOLf4cLlgB2Lv88uGVNhSHxhcWbp6MabcYQbmDtuAArGL68QScl/qJnw3hL1rPdlgB1xH8cpZ25ymviXagTkPtI414QahFRjsRisRuwMsH9lTpG4C85i9bbtfZYMdcJrAU7PuukJ7YGIwZ1n77Sbc4KQ3/wY3IhWIHdOP5eMD6dUJe7kA2gXywQ4wybz5Lp26txDOZ2mQCb//GvP7BkCB2E1lvexs3pe+DY6cC9Se4/LBDjhu6Wj1SoThiZiK2ib8fjrGHluiROy2cGaxWAKNuMWmJ/OfIiPswKzP0a3VTtrGWVwKTo34DJy27TC64mEKxC70CIsdkaNQMKeX+VhO2IHg9jxFK+0MGoMwq2SK6F/j4mPvMwrErpTNX9ecTNDNGBa79k5ywg5EoNH03lMpD40PZpW9JPrXhZgTSRJQIHYpLBvjybBxj/Mv6Ckr7EAJ6pJSRnmuaIHJS9eqq1j1yTOMz1NPBWLH3GfZIJQ8oyuXS2WevLBjULNoYhblscEk0O4t1jDZEuPH1R4oEDtbOxaNFPOfxBiojRrKuVMNlRd2YGw3ZNjepewS8A9me/amyN+mYH67QInY9WcDsv3MdwTOaDAuQFhHsonF7pDMsAOjEYWrfzjdsYnFVOXpJ877xwGTN8rmmBKx82LJaNXWbN3cKtZ1RXgZncjFLsbIDDvHb5CBK6S7u4u/zWcnRJwKJfYG/6fdQpWI3QJOp2tuyx2Se7M/F1aSX2/KZvGJkxl2vJqHuyjvyXdjVkpxZdavu8vXDYB1YvWxE43dYktOFK/riwesFMwOlXeHnQzD5YYdOC9RAhwzJQjjdNdM/AsjquYR8mDOcWq5L6IaMoRdGcuNuclMwzOr0mVPE7qgMRt723GF7LALrETiyFypjlDwSj49nUSFuf7B/2H3KDlA1/NoOt+vzBB2l1luzDRMhl+sTtP+ueAl3di/HpUddiDlKnTRvzbQHaTPMZs7MUbEvpiaecNlUAAuv7wTrt6LIew4/Yl53t3hlfoEjOz/3RF0y+ZivpPlh90wZLpbQvdQMZM/M2jEVFr3wdS2e2L11XXvmDn4oFBD2HEzljlRFMzo7hx1l/9xY0MTBadLbgnfJz/swBvw3vwi3ajZnhi/9D4ifodxTc6eZmXqpoxZIxQDbwi7uazazgzHmchFnPOwbk9PJ25C6yY03a2yYOdIErs4RHc2k+5YYWpBtTYeyMNgKqB4LrcqdF3/HCicesEAds6ZZjkCOA110+d1YvOWzmRTtHcUOv83Yy8aJUPswBLEUkF3tMoxcWzGXRK6YKy5/7UmdIHnhxjK+GEAOyc2wCvbZOzyb3PlADSfcl56CVwxFN9AhWF3HXY0XkY39ArnvmT85HUFs7XbbUXqNgzWaCli5zxTf4S1WVWV7mWiRtitTrbY2faDu2g/3bUJM0ucM3qsWYDR2oVbDbqMBm5G8htJi13RWS7iX5e9pbr6nQsXi3Fqh7KwA0/gLvqc6pC51uEP0jNjzmcMphZFL2sleoqc52s0rZaU2MWe99Tr6i7OfPF1rmBNYLrptsrCDikV15puZlnM5q65MSOiK2agp1updvnILe20FLFzmrywqgje0r0v/zGhgDNVfKUs7Bz3wJs7uqvVCEzqJ2PrfJSfbLR2EUv9tPSws53Zr7ceuiFPoLkt7xT7b9E+isIOXWXphpDleZpexP19/k88fKwBHeNVKSZ1pUTYdUkJq7KFNW+PHvYncru9G0GKwm4trDEuo2pkcuzGH6VPjfwGE/uzuqs1sNu4S0sNu9LHnTRV1PVaxPNJnPVIXzJqh5KwS+gBXVhJtyBZPf4o7TRS5m0ppoiPFaBzvdlRiLMh/pJid/y9gPTnFdw7PcGdHYLr6IEMVxB2yMjbnKQ6eAMwCmPDQR0uO/k/2WQF6h5/JgBdoxkn60uHXXzL8+1Tn0P3yQyBaT3vmb76ThKjHOwmw702gOrovYcZN8PJ1sOX8X9BP0Ofa6ENljkbuwMZJngXG8QuJrTna4V23s+Za54T0FZQp7lXX7k9evFmxWB3DF4SCqkOn9N4U1OZDHiL9wN36vnmx+Kp00y6prcNW46da9SVrU3CmupeyJCCEQZ93/O66S/zPdpXIdgFd7PiPok5xB+8vwwqjEdhwmsTaGNX3htH3fi3q22jlmAXfNJr4qCCbt09XiKnsx98/pixo17UdP2Zw69bcoQisAP34f1wC6oDiMlifMNQJFX8Cf4PDlGOLHc4j1PXrdn0QtFtDnaR759ZVfzTL6fcEnu7v0KcTjPk8sfHxCRuCr3pX/UDt/QZsQrALgC6MnoK1SE8jNkfGXJC2YzRlW2hS11MEmZ7qUl/ZdIyB7s0ex1fQiY9GpAn9qNipp3wq/5dnxj5YzcfCjnR0N2f52Pi+z80dH1T/vWv0cWuJSbv+MPcV32PzMFuNgycxt57z7bvj5nmETRycphN1ZE3WP7Y9Uy14twRiCnOacgh9nX+5e3oBmb/iklE6lsCRUGbih0TuD6rrJo3v2WrK9NXzZi51qyMiPFefbj8YkMmr42Llzl2AB75YrpzxwemZTPBOLT7/kqzvc5n+N51fyBHadOwmzm5T06q/jww6cnBpIZTIvpasldl9usPI9GfnEjeK2/s4NQOz+jmbcecKbobuPwsJmiMasrimbzMz73r/RtYgJ1ujc3zlXWMFA0cnfj8dsuWyRo7ONf+XLpasCS+BizbgO/TSvPz9UgiXXh5MLLr8hQ+pmH3UiTGTi/yxe4efJCkG4S1lh9x5SdcTGc535XXnWoJWV4+x+wJfF8/M7DzHigldh7jNfLHbgfckdeoYtfiDn/+Eva+asmvgBxCs7rBfPQj8eiM8TAVix2zSD8zNb28e2roYymxuxHbM6lZVa6K9v+WK3aB461pWE/nYydsoEvi7+d3taXX1rFfoFPtbJxfs1jsfLgpThe2n7OyLJYSu4sca/n1ojlNzJ8OMsXOFa5o056uh3gTPnbjTDrIUrRRHEZLVH6LTcgnErtAruyr5lGV+V567IBzEme3aG6s6IXtinp7fMMKFnRl6GKHRMt+QjdT3P8wJAlqSVfxLy6g11QkAkCrDcPXJBCJ3ShOTfd59UdOADsA5nHJA3wNVqp2fD2neheYOohVeQd27V8tPTkMAr2e/Lltd9ZYItjBGcc0dPPceWFi/NcLKaXSTXeCl7KpyKnbWyDyRBx2eWwmCt2J5261RLBjUtgaP37n0UscRufW33Pim1vsVO2YzCYY8D3boMll1t2lfduDORefy42lpQkbn+lNdiG/NSSB3Tw48xPdqqxFNuIzmsQ24mNHz0HQoQA5cQtlUxKH3WmuoOeLvMVEsAMJ7dnbPoM9xmOm/lHlUWWfs9/x/aa6nFu27PLqtKNPc/d+/vapz8VbV6deb13zTN8eAzW6ZQfZHaJjcLCTlNidHGjFEHuXi+JROoZJNUIvw3wpktKpTrAl2Nn2g0Ajgx2Yz3oHIDUEfdga8Tvbr/pva3ud2/nWupznmQGdZrNalz078vPz155k/998Lkxt8PubARMV0EvnXfL3weJuv3y6b6902PWF3ToeUcXOFVOiUyg71mi+/iSxC7WWIucZN8FVQRR2eSHssM4njd3Yhex9Z0CfeSedW/IDdvKKvdVLx1576+XZopdOd5stbV7m+6z8Kej5mU437u+qv+z9Wbcwp0oVOGSBg1TYIekx6Xp6xtznY7dKIL/4Rv6lldTSttgiPgB9YizC7nUuuN/FFOwSroz41VTs9PeFqhs90SU+Tyyb9wl7kHgludx9DrvX3XTN/XRH+rPYub1QiX7NqRcHN3iTjevoncxIhR18catAqtzd5LN0WSCd7VCMssWJVjunwJvQh8KOiaKw282O5E9OpmD3dlNdnVhTseOqBEClK0/o/njx1JvsceHCyz8dYrELrtQV+EzuoZvd9TPd+BeZeRvqdJkz2UH5PeuIzrNIKuyOwgc0up6ekzHFEhvjL/2Bf+kSau1ESuAWO1qGXV2WhwLGFOxYTYfGy1TsDnOT6qt/P6J72WWTuXSfL9pwkg0Xut0wsQdrEk/SPcv7TBf94mmddbrqHf9UjW6iVNhlQR+yhm7RuGm8qsraaLw+zGkcH7ub1NoJK+2aG8j9Sgg7duulW2EqdmkodgW6Sc/DfBwKOe3IxmrPvr3cGed2Wu9urF4lS/flBXZv16/KLYTZzua7qD6XOLjpOkuFXYdo6OqhVLHLx2SkwNdDCs6xov7kLnyKzgm0EDtukV3qRBw7bpH95dW/L9JpyqtPBf9464p76EIKDm8GoVOGdtd5/KS7necfMhmMvK+79IDFTlcZsN7BdeogtsCF7jHzHONbUmGHeHqWxdDEDqeMw1eZwNSLQpQDBCUNNowZ8uwQhd1+LqA6kDh2XOGxYsgs8anO5osNcU+HhReG6C4+mMIede1DUtewzqHtdk9kjxR/app3+1mT6sUeKeq66TRN261htXx39mgSF0e0CB129D+65x+cBNjB3pPdqKYbY/qJrVuXx6++s+skrWbCOYo+87IUuzw3UxUo5mAXysXPnoYuiPqUVYV4Vu7k4n2+/OKLwS9c83beXshi1/gb1pLbYybDYtc+8/mfPhrMXp2afplNJDrXSzIFCnKaHNKf6ipbwMfuR+yF8/kX/kxNbQd/mUM6WIrdSG68l5DGbjTrumyDrAguH3fXOzS780Svt4tfv9+H9ephseP9lZMjXaXDDo6M0bShit1jPk34Olwf8i+0o6U/CQ0TX71WnHHsY3bSaRdEFjvXExwovGRGyw/Y2w8/uIgnhRx21cJih/x16+ePkut+qWs/UjLsdsB1sg5Qxe5tPk0V2AuTTfGRkliQ/PCDLMeuyhWgL1Hsvs5mlS4YY+cj3VlcyMwAGDv8AVCTJhl2GfC2/k+q2GF8N3thL9zEx24MrUbu8Baf7Uek49M+zt2u3iyC2KXt4hLf8VXMHewSsQl/RGAHPtcNkgy7BDhr3E9UsWvD90EZiE1yV2bFjABBGvEOCCKxc1nJ7ZUeZZDCLn4ml6LMA5OVt22vdkHmYjf0eQVbCbADcE3jTKoxgFF8fbE31uPuMh+7FFqNLIGDFPtLgB0YzTm1a8I2mIDdwnHjxk1P+l0MdlFN9LV6FjuIx+6epvvnhdVyX+NPHrutsAqeatDirER+XA72UMPPf6K5RauRM+GD7DApsANJ+oyJNum7p3T5Rhx2ejFYm1iPXdH6pGbt9EktxuD2cELYZQx+JdTxs1zy2CGJUOiWReEn6LQ/jDtM8s0Z9KIrYeeXn59Kgh24d6pqiG12njKOXesXRBjaWTZksftXpVvVhR3rYR10XmJX9AaEcM+UGc/l5muu5LGL8rRixv0wcSk9kWQt+sVuPa02vgM996NAabBjgi77vZxf7htuwtDnSaE+MVSzueSzlze8cxCPzkvstunmHnY2qSMkxc4BNo+toordJT52OH/xtfwEXx7UMmccMCGzuHjsWPeGlF9eJKMwEo2UkLZbLweLDF01ouPz22U2EbrwJXblOl3vy7Mn7HuyMbyDFbBDTomTqCbIbCLOTLGdn2RsF7U2ToSe2ylQKuzY6KzXmrWuqnPymxQNHVDF3Kn2u/MFIxBfYjdR5129HjcNeyeePnZ14ew3D2hiN0icG90Ifkp+X2pthL0C72RIhx271LYYFsQp8STJevQGF/E9LSLY0NL5ErsFup/yDm4qLBwz7pTG/lwL6ti9Bkco0MzwAL7CJIbFfcfWrEgBFyH913dSYqdXXnIOKVJ87OfYGzUxcs0L7BwW6p5XK+ty2t/mDaZ6NT/wQuYxRLHLMyVLv8SC4akCo23CVPS8RE+nDT94qtTYubAWWg8JKr4lVLDYnTd0xdSUUU1Sq7ELt7/z4ngSGWDTvSpE+lj0y1NJdBei2I09Bf1gMU3sMOWMJ2GyPAy1pkv7Mfg8kyQ1duBLdowPWt7O/t3Z+5QIHx03VFX0qcYuWVfw8vt2tKvWzHRYXPBCfkwgil08bB4b7kwRu3BMPgpMrArGJEvv6+i/U7wvvVnY9WFhaGZ5Phcf9lzsIRjow3BJALzt+rWrwi62ju7VF/lT13455b0dkrmtV1+K2O24yuPpImbzNIaP3QxqbbTtJj7zilnYBXBlEsda3M5c7kQhGF00ualu/NYHjYs6VWE3QZM675W/JrXTBNDG7iBsJqBpHovgFwPOxDy/WKwXMhGBkyzvPC41dms5RcZoS1vpbMfeRTAtTFylznfdyyOFc3sdbNN+Avu/08Duir1J1YSllO/4Kffb8ctUAkzc2EZ6jcyFY9vCpcbOlqsXa3HprXD2ZNK7odBfr2kSb71yknVaqoNt2ht19fVL8d4SrLRJkB67iNXQLyZQxC6DH8RjX8r/kDHpnl6n18j3bUSbD83CjouV1nW31Ee/HuelImRCcSjWTXJ+BbvIs7qCV1eVHe11+sZeSLXHisdM6bFbDm9evqWInYsdHyi+4rAFxnRLsWz2BdjPs2KW1Nit9cd7Apu0XVnNYifogzirh24TpLcrSdW1fyUqYJIuVT+Jby7uNgkndcKlxw68C8eBUsTOFgOUlyg4KQZ9RMJW6zXhUmPHTGeRsXtqUSP3sWusv2B5GNsQXQCEXeQI+9T3Xvy5ZdPUan91pw5YWU5gb4ckNNpFL5MSNunTP/ylGFNubC3Fj2Mb/OhvGImxY7M26l7kfDBP9OEZ9wX/HKvTfQ9hx+6XdWOeH3uXF+i+FaE2kxq7/dCZInE+vQF1+JQP1CL+MewT/g5wPUXsEKV25l2psXNdykKz2pLPvQmXXEJ4Gg7U6QYg2Hm5Nb9ZpatrMSo7VcziITV2+W5Wix6bzseO//j+rfjFNGmqeQIRb9S6UmMHNnAp7x7Fm91CLy5L5xjhlEAtUnVPEOwc3mhq348LDPGZ5OeRUj2BM4HDsLLZgQB28XCdgG8ojuhZMRq5CH4Fi11RFBvJIP5Z4/Okxs6Zs1SEmF3isucz9ufjDRT+S0ivTnX3ilO749cD9Qv7Pl2ric+3DUU7dVhZk0YAOzAc+slvFOsE/CDGz/MCPwPKna4UsQMlHkj5DEeJsQP5XEzPxffM1BSPYeP2NQb9wkfpBr6HYMeUfKTXGd/Udbv+PPXNd60TsZLpQwI7+Cc9htEb0HpizF5IpKo+AWQcTexGIgfu6HlSYwcOcn7r6ZvNOml35rK8XjZYDzQoVVfQ+FXsxh6uY69rxanms+bq7AvaVNv9452wkkBib4f4H4WU0hvQb8Tk/T/Jrzd2oy1N7MACJPVP61+lxm45V1ZWU2aGQZwZwBnXVht0yAKOmzSaQ21GOq+txm5vfRvdrk0nq9Qnzbx1bs2MH2gkx66lVrxvj7Qyio/dbN5Fe/k+7T02U8XO5RTy/PuuEmMHHnA2Vb9Vplek0cc+hgww4sLS9wuNzm3nz52q/O3a2ulSN71wfY+fcrapbpzewjFrLHuuOT52LDt1xo9l9cmOY8eyLYqcxZDALuOOtcpo5/Kxe8y7qLS5KPcokoLWnrLf5ygxdqClPoTxWxO/J8cBnGemzWmjF9rmdn/pb3dalwNpoJjRFzWcy19ctyFL4oM/HTKkfYeEH4aEdYkp9xzy5TqQ8iWRvd1y2Oj5Ab1iXphaxg14F03lp0ppTTe5N3iAVpNtGiA1diCNm7Y0n5qkGnKa4PZqPhWDi3HR5LpDd6/WY/cHz6/0tK4PO+pF/2FtZlxFtBwX1z90q6OYZu6cOvANt8MksEMc2hrFUxvO03zs6hnbA1gDO3DeD82y/MYsibFjXtenCujlJV6T0LOMs2/YNBEdAVR9pBisQ6PgJ+vas4YKJjaKvZVLURG74tpGxcaA0KiitgxIiHUggh1cde7qMNljZ0cbO5cjPEPJub7SYscmV9evs26zRS60ju/r0wXYNxC/IazGbo9u3NcN9/Z/auuU4GobG7XWZ8Fl3SFjtEuPnVdv6EclVtszicXuWV/K2IENfrxGfPGdxNgx27/U77+evSZmwruwSh9wk3jahGNINXYBbAKB5rtadfrI19f3o07jU9loXTejHozSY3cBzkjxFbXB7KwY7CI/x8R9ZEmLHbtqtq/KFLAnzUiNIcd1hVXR1d1Ncst9rrdbN+NEZauB7aI9PDyi2w3s7lv/pnEjt/TYuVorI4VysAM+fO2hdvXrjLTYgQ43/fU09e73dp5w2vzGI8r0+0CdX/11wBzsWGnR/2TQle3bt18POtlV1HwpPXZI8uqFDrTGcoJS9nYg31eLEe/zx6XFDkQG/VaVaEez69Ovwvs6oVoFR9uimWM+ydZf4t7qgImhPxlfLjM7JHeL9NjNgD9iagnbFXOSjVipxYqmsK+02LFLz6Jnmuc2eM+K+4Pefmdjderqoymnz9T/6Hn6J92QN00ep9BuNuaWkYmpX+3FIiF2I+CMFNRKjcwQg10pPwVK66d0qWvcXisgfod6Sowd65W5RSdC+pljxMzVtTavsJzzwebRU6TGrgjuy2tWVBfzY6jWevCretI1jjGPNVpBySmVGjvgpV9lDSDH/a2POW/y6zNdSHp702W4r719uaPU2MVkGhl6etht4l20LoSf0P1vqtilZQtCp7lRnMSQwC51wLlJ3jjk7L+c/uRbc7EDF4pDdGbJ3AnP95HSYYfUM15Kq9ZIOX8c3+RddIyfVfHnuzSpa2sngFxiem7Dtg6AyGz3H/ZYm1fyv03fLk2f9KxK+l2+1KRu0tQMBzDIbOyA696kDz9cxBVVLFj0oWFpz6rztlb9z+83PHhxsJYQO/hXnWitYYv5Y1nOu2j9HN5F43vSVNmV45hbllNvZhep9XYwdsJiAXbV6iAPo85SADRg51yMQ5SE2C2CDT/rKI3oGf5w7uNdlMfPmO1Jszjaer53c2bxgiBM7lXlYBfTjHMrzbA2dlNg89hMSiOKcWp/g3dR1HjeRf5FFLHjuUC7bYvDW6+Ugx3oz9lzbw+zMnZxF8VX1pJQMCE8/MyA313kXdQ0jx51vFCO1beEPMMkwi5mJnnsQMMh3HwXZF3sZsGB0rcpDSkml9PX/B19D/5mfgc97PYhHu3+whVlpcEufsFqMdhV4q2xfdcHzS/ZMM8n61isYb/JyYmcynlflDWxQ5Kmt6Lk6XlbTC6nQEzJ9inUqIttjTjaGdiASIFdTMsTeo2dUex09s1edQlN6DJlY3nxwl3Zve1t/Pz8bDr2zva3+2D2gNLNAmPJzPTUJ2rvtyTAwWrYpcCLGJ2oe+ff+EDxIzk6LORfRc2OAvbDCZ8038QTxW7kV55VejLj2LEuoR9WJ5EYtmHLuIt+WC1Pdo+CgKn4Nm9/VnWbbk5Ww87nX1BtJTqJvFoM5ndTGl/T1E/MVYSEeQQ/+BNDu3CLsYuZOu65yVUMdrroba7AIe5a8WphfbZ+XxA2IQ93CPp7X6bGutj1Hwj97jSVMf33SjHzGIMpsDiZFnahSNCYweTWlmLXN1lvl3AfKA67xD0bG7edXOymFSGJ01/DVdv59eBOq2LHwJr4/1IZ080f8Q8LmF3bIX4vHqSF3RXEGBxLELugv/ROntmFA8RgZ397Q4u9oyq1YsWm30SMm5RjHatiB1bBDm1UanrFded1TjRGU91HXGkyIoL4yAwFxLBzmaD38HS3S3MUobfz+MPHdvu3a7SmSMeV95bLDbtk2AyQT2NMo/j6/12Yw8wZMbkDCMkX8CZpCinsYq7v0S+cbg0yqtTFcw09qHzSTNfRxdlaU8X+dpDMsPOCo8eohPHk8esYt8LUQcLYRD+nRF3fTtBj+zkRwm7s0F166nI2cFv/cDZO4iMDzynaGJvXZ5nWHBmCBFpaG7su2fTDeKbyuwWX+CeZf1kZJexa+psyyZqLXej/66aHzntb1d4mi8Uue0Ck4HOCn97015ore07KCbux8JliFQ2FsRe/U1ZignOu8S/7gxJ2I2Bl2AYS2MVO/KkqCmySV7XeNovziUv9XnAIrqRrLZCLSYx8sIuEC8uGhVIY1Jn8PknHFLRN4vv2LqSEHTzRamIlxy4+/8cbVUGK7Wa/uLvrEk6d5vYhnrtZT1K1Fonbx86ywQ4Jp3Gjkdwmhd8lOGuwF9+rvRUl7OBMaL2AxNj1XfHC2TfsVYWl0w8cim7v4HS8/Qu0lkrHBlU6vIy2bePYk8zK/m2x8ojFLo80dvthk/c0+toJfQovnD6LrxCNppNylIF9FdpLi11c50nNXziNw7l8OjTx45K3YtSTpWFay8WmKlNaWK9enVjsEzv1wgobi+s3mzR262An3hQKo9qA3yHbMJdF8PfP2XTSeSKOOWckxG75tEep7lXELVvD/j9feFfj+g0332UHINZUpuFqI0Rpoud4zm21+tTAZb0NXOW+ipvv/mM8hKIJaexcYFeLcxRG9Vt+f+BqT7kM5CugplLBLnihSSZD8dhlHP3p+US3s0HWKC4YDHG9cd3GXRCdDE3rzC1PQ8SNv/wo+bXte6PaBm6OWx++4eC2pd2FQt78zoTKAzuk08IojCrG2LoVt9Tt5PfaPCrYxX5kxBfQLOyY/PJOflWjajMpII4BG7iZrR9iMw39kfvXxM6vniv2Cx4mNG5/TdjeE00ymhDnM6rSBq853hcjD+zg1JqZFNKN8Q2KV+/hrgsT45ZHQtq2MskBQRR2LiOKn090nmd99CqTuBucjRUtBMPkenDZUOq+0O/GpAlp64YcOio4XJE+97FTZO+JDIddyC8LBaTSjwp2SZCOKnE08UFl+OVk8U/F+AI8oYJdF9j/ZIXl2GUkL3zu2lQ5dMfv1R1xifvvdNRS77SP484j97kX5pVWWObean3zpMGsNczJb3C8Zl7hsFvYQehnbdyoYLcukbKTRwbfqNjuJO7CR2JyzZKQngNN8vITgd3+6mB/t6VJr5ThDHiL293xlHrOT7h50X5Q1XwX1Qvrxtnv+8ZG38NhXbEHRjWfKgfsnD2NJCORWkrt+Z4A2AjdofwuK6aD3S5TjBRisFtRdYz4Jhwyfe1N1Ge146ltmc6cWs/+Mec4EvgT7mTQ75arqDeJH3CD/+tdfjLADiyFflrhSnpQ0/hu2N2xivmD/B7rRyXaowucw/6WNNgt3PoAab1DJYedx2H+VLDbQ58n1hHEP8Y4rWemuIh+lwt1+Oq7VDlgB8fo7yQeeX+A349H8HzyL/yISqqxjItGj9kmYjdiyH0fTMGcBvpJcBzfD5M5yM13mnIwk6+Gyy42KVy4w+e8TU10UxlgB6cGsAknPaiYqhSXsBdiPFXmUomU7fuLkUQZJmPXH1+RboQeu39hVnGHg8vY42xSET9YeNn55SYqv7fwdjWpMsCu1IZuurH7YtKM6fUYfL88t3Aa2LnCmsVDElopYOlaFS/WHnMiddjYVFcwi99Xq9NM3mcc34dOmTbe1scOWVKIV/jkbza0H+MnnU78K/fTwA5x9fd0JoVdwhdVeeuw+qOPPY+9z/vwfIPMeJ3jb6JWi3ZhVsfOaRzsg0R4SFu05sOEj30OxfiXLaCCHeKs0IYUduDjKr3KJf50x9y6mBv8C/r6jfaa9T5jP0CtG3usjh3YBO8dEsgO6QOM9hM/rpEF4nwGpJcV8PFxNjHsrlR5errNR//gsi26U8/z6Nt3MjctQk80V9+uk1bHbiPcIsIVPjH+TPbf4S99jFHcUUknvwPWF/sGksLOtrJquitDjrlTK3S6J3HoJmOXj9lv5IMaLAocrI1dFgzCRLJDOoLPUiuBYQ3AqFpa0MDOGXZBsZ9MCjswptp6AR2VmI2tWMvtrx8jOzKNBRYkBtUfeKywNnYZ3WmuY+f5LIUJsJT2Ft/6TSdZ+4+IlroDKexmVltqXw1OatuE+5fcu0jKK009Z0u21Ogya9fWytgB2KuxgqwloFD8yjmFH4esuUAFu/nIUweQwq5t0+rp7qUnYZsw7h927UVDmJ5Z5uO6Pxpx+dzHWBm7P+EQowySAxqPyTL2ptDpg58+QHuYCnazGiEnyAxC2DlUVE93Y6ohmLVVHzr72aYEJPVQtoW+hsdRHaD3WitjN5GiSjbYTpyTp34IVlotHwW6FSp0JoMdqFuN3a6qQ2r/s/qAslPXfm9jYzzcxCTJQx3j67taF7tSir6UbfkHWT/BRJkYj7tzdLCbipz83N4nhN3o5354n3POKfPt9P87PZxB09kPsTyfObqr9vjQutiFwn38J8nxzOKTlLpW6OJR/Isr6GDn+C7y3FPryGCXUa1C0S27AJZPaKf/n0fY+p6BSJG9Ny3fcdui6vfKnlbFzgE+U/w0i+B4HuWT1Elw44RxVpm7nA53/w8tAnTkARHsmLPP4xca5P23+n+l3uVKSMCTXb4E7+S1TITynR52SDjyeJJlUWZj9CeCKuD5/DiUdpSKZzig9iT3E7+SwA5ce15lbFknvX2W+8+veJvLZjFSTOHnUA+oIKtiB09BmpYEh3O6uNjsKrnL93+3X0EHOzCFl1vpr7sksMuHSowt21LM/v+VfUP3wENyXZqNNWqY/MnWmtiFZ9PyfXJoJNb/RC/dTbpa2pbyo8j3HCOA3e+TXqGu1wimhA2kyB6xGfart5PI5/sgYvhoftCa2PX8WYT7myRSxA9m0vwjfDmmoGsfStiB/vzAyp2vOUqOHZj9AjqbS2zuf0cu+9jkaWTySdqeQPNA9bcids4VtHyf5vGXzd4GyhIstpZVlpPX+W19ODtQcuz0ZWS5xBSpE/TOP1+z/xW0G1YVSxZGegX97Jsw1sMOOVPsakxsKL/C6E8cTTr3rqZWeOx3TMSk3+ULUmMXO5cd51Ebtnyzvepo9V133c9d4M1/ry5SvROzCfXXbmhF7N6Bo6XbEBvKMZjcdoY2nfxkHlfp1USJxeUxvLMoQVrsHAvYcS5++d8x23RhDvCT60tnJo9Fw273uFgPuzYhdGK0QzE1TpoYuP67udZyMK46zWKONNrEwkBJsQMB7Di3eiVYNty7LL4Vka0dJwOQrcPVt62HnYunSam1zBaca7Gh3Ga4gj1L6GHnWIbNd5M+VVLsprAqlOwrrzy1/eOeULIdzSIJ32k5anEcEmU17AA8qe8h5dg+DZOHyGAmR0zp2W6OtKhzSBZIxr9rIiMhdmMXsgP9ajaz15KnQpbrh5LuK0rR/FGrnK2GHez7tJNUmerdmBD3/ib+4F8utLDb4C2QJS79NSmxA4XsQP/0yseUd200FDLmJm0JQtS7Jvqw1bCDfZ+aXyEzjjHNMPklgg1qFzC5Y3woUYcppMxJ0zppoZIusmAFaxA79UrgeUlSGhTZ6i9tRulY1Pes21NrYbdWY1ImQXP3SjmmmMY4ifK0mp3CqQ8OuoeHNjhLe5Jld9ZsBgDNK6VNAw7fgkLXUp2lfbN7iKOx9qa1sAscD93gUQyRkczAJNY1XIHFAePpWUAHu3tNMUmnL/nMklpvx8o4dqTrvVy3/9wAF0eYI/GbHUcjQXflWwk7JEY77DiRkZzHH0gPL8M/WcL/SSMqBfme8mdm/zKs47Xl2CVzybNHvvjP4Sh2Uke3FKGnimJX62CH3CCaTGalLRjtq5EMU5giFv4taWDHt6f81QbvoWU5duHZ7Cr7Ij7Z1XfFa9Ai6y+5j+ETVBeZZCXsPoTbQSaX7KcYZzsjX3I4Jn0qjQzGsWhsdO9CoRO0SOzWXvZNEXjZwBx2qM8//6+IU5Nh07V3W8mnclRt7/vUOtgFwcGB54ls7XaKTfb0UmyHmGbXkEzVg8ZGlwvu68VhF7OUNfELFTg4x+V9eqHcjN56Bdr0R++V/PXmoSEt5dbBLhZ2hDtLYihHP+QjZGzmcsTNkOQ3d43RNPGrhKuxicMuoQebn1MoYdVkNq/rnedz2ve6CSehtOLZXtJrwlFV1q4gq2AXA3ti5cQTGMu3MQqJk6YqN1lZE0EcuxIkFWGYgcIJ4rD7vy9Zh7oRArfIG8ImUXzuN71PVxgLe3kekP4Fo9DwxS9GWgM7MAi6w0AC0feRGAtnd6O7ZaQoml6SSFPHIAdoN0N7XQmwY7go7U3VE8BZ3SEG9hNZTOAVAzoi0QIbrYIdfGa3JxB9jxS30ct/jWZwWo9RGBOvE7D8lBFnSImxY2c4ttpi1d5hbLrumRO8tfiJQJ6rxhWo63SgNbDLay6ouJZIgmzMCfJ3xTih2JFO2F4CP8/T4KouBXZtuDwoVfHCf+/UdY9C8oN0JfCOV9BTRWGkFbBDwmWKIylowrTZIgqJYnL1hJDe3JXjBoQkdiO7s4NdZZK80FzXtA3sd9vRi8RLnkH1odutgd0l+EwhvcL4EiYyW0QWo8n8ygyGMzBJsLWDZ9g1hsMFpcCO6fPCxXgD+7+uBcE72kEk3vIB6mj8l60VsIMT9obkS75fwmzS2ouYUyP46ca0Y8hi9wA+5g12Io4dmMjF8OhNVLvZ/1XoAvvDPiOSqOFoNsbjljZ2SPWRe5Jr7TA+k6fFTD2YHFF2jYliNx9WMO4D5LFbx1UC06cw5rzvZsfDYew2U0i85nI0fLHVA/rY5XmTndcxtdrF6UExm7tosvUp4J2V9goF7Gals6OtNxVw7igT0fRMj4m851T0VLHEmTp2jbtB9zgh8Ss6V/Dp8QwWpdrBaJnPE8UO7k03VwrYgXJ2tAdzBridbGTFdpC1RmR6Iov2sGhGGrd51LFj4NxaQyS2U1zI5MNTX9Qv+2N8yw/Fk8QO1mv3AzSwm8cmoRh4EoBhbLWxO+vByGfwKksmQUhcDi+NNG3sEAWH/Vpp33CmxlQXz+cSiqnyPodkVipXeGL+gQp2jTP1iyuYxjpBtbblpcYK60vkVZPQoIHO1LGDU6q5S1sogDmH8dUVuVEux6yyIwhi13ilKScKibADH7DD3ccBDGADK4az/xkOG69sZhJ5VWc0A9fqAbSx659JMAHPSH4iG22lyBiwEswZ+AxB7H5tZFJgiUTYpbDDPXcY2FJtnWUWIof3UCLvuncX0rG/hFDGjoGdffpJGo+ahVljV4m0cbXFlICuDCWH3V1YbTeTDnYnudLFG0DZc3MFEqupCSCjGZ+B5h7zpowdqAcn9ZQ0x80+THS9aC9hjH0j5Ao57OJgxfZ+Oth14DKMjXJmq1JU5RCPuINYdMjkHNqMBkkt+4wydnCq4Oz5Un5UmOQn3LlNnGzFbO72kcPuOzjCZR4d7MAmFju7rj/rdM318SXMN6hSjczpfQO6hUmljF2pqd4hJmwh/DGpnkS780RgVugKcrmz42BH+jRK2P3DlqRwu8XGzN6p2nwEIYqjpiVkltlv0VBgN7rYIXELZyV0L0rRWDRfPcO4GO8lhl2XuSY5lUqFXdR4Nn6M29pVDx+Dxod3I6NE2YE6GvvbUMUuHg6WtZMuZCEeU9hEa8LurJyqoSIDPsIcpYRdTHsWOe5c8fwWLZEAcc1QMn6GXyGOkJpUqtghKkoP6faweRjvk9UmnJQbYjxEfyOGnQvsfPAGJez0Udq6V0IXHdB8V55Tyegp0Y232zdUsUOsn/9I9mIbMbNVM1OOW7783/fuSgq7semGQCKHXWk1dhte6FQQ33rtbWciLzwftVWsiqSJ3Tp/QkkkMdUo3G+Z8PsYTE4Kg/kYLdtkXzJpWpUMO+ZnPXWJ+S9eex+6JZ5M5IUdUBNS6nWa2LnkkHFCCXTjQ9PKpBx6Sfb8O5wgFlEBu1oNdKSEHXikx67Ty+BIl9Zo9H4XIi+ch+ZUGxdPETsApwJaPVKit/oeM1cVmxQN9fdqjN6PWB2oBfCDwmlhl6TH7vIrjlZpaET7NjK5TA8i4Yv8kjgksUuGLQESOVMyH2BMFBMt+SCqZDcp7ObBc+sEWthxUdo63aubXn7x4SwibxyKekM2ekARO9jorjkqzTvl/QfjuZRn2j0GYLAjVgoSOXiHuVqKXV5AwP88Wa1cvYCX8vXa6k1CZFBK9T+dv8FhV/+Vi7avR/PGLCVzqkDDF90LHelhN8zfpDOc2BkcoyteauLG7O4cjGv7SULYOcKO1tEllmK3QoeRgdXRmoeX6QTly54HUW8xMvmuHNCCR2idFJLYgUlwyJQkbh5O7SU4hjoWY24ygdQqi5S5K3YigZ1u0KsHCbxotiG1FtlTRRyRV37QA7VdOtHDDnZCuSNJGHTeMsxEZbIqegBmyrRzJoQdEkVnc4sEds2ro33P2xjgbk3D66g5e3EkkXeejHSw32562MEZ2406X5hxTqnOL2QyMBGYs2ziNELYBSOBH70iLMPu+qd8af/V2OqHzTjxqbAMcliMJB9aRiR6ETig4YsXL1DDrtTP5DhWo69zBIPdVtOPw7hVdhMphTGqnr78nUXYMc58eWXLHu9sQEBcIzq2inB0+7zEkRZ2gXDu1OkSaGSzMBnPPXeYsQjwk1JoewwjxB1cj4SLy78SaZkCxZLlD3nzbDK5OJgtyDJrP48Wdo6wGeuUk+VvswkzS902I3NW4EBMEND7hLAbWQd9VOrZd/Y6Wge7WcPRPS2Zr20zahMJa0wJO9T53HIVRQYmdkfzjjl3uq81Jz+emZLEDxvSRDdqdvRYMEMdO9AGXf62kHnp1xATpN8EhhJ2aTYm+ZoZl/cxa2OqWTHuaZizbAiposZOmCQGer8Xux+uTbWljB3zGD1V5BN56QTUFnRnLyXs1mdKmxTdAXcS6GPWrWIx8ya5SlA+Hloh8W/doAVV7MAw1FbxLRnTbCnqF1lACbsEODxzkqWLWFE0ZtxWmHevNzG3smtBCDuHTRpB7lptpzvbsQ6LiKm+6Wtk3voJ+tKv0cEONIGPnHkWvgcmz5O28ql595qG8Z/6Vxqp6a7LJCHqek0DtLEb+Qe62/83kZfugEatrMyggx2cZMvewlG1xdRU1P5pplrGqR9uwSZW1ThoF566HlMBdezANMRWoSEUSrIB8bTSbGGoYLcWfmxdy95iBGaH5GZ2Zan/YSAgUcrg+cHOH0ddt2PACtihZmKJDJf8TdYYdCM7hQp2DrB67JBFk4kzzk1usNnKwJ6YVdZohI35ErkR87ywC8Aq2MXNRRpSj4zuKH88XsdKGDtwG3b3s6hy845USb0zmbO4nZYrMe6Yx7wIkIq7wDrYgRQkem7ZfDIvHYC8cvONVLA7DdsBLIqRw1Ru0razQMV+GBNScfV9YtgFomp7zW2M1xEl7PpW0PFyDUULgVR2pYEdUgrEknpXtt1xZwAL7LyBdjhvFmKZPa+h4bkf4OZ+StiBeYidWPM9mbf2QrcWbzIUsCuCFcZLLLgVzkfOxqKz8WlMBbJlpJI/dUBOzpoybEwTLex41sFKMqZZh0KkkxOzKGCHuLNWmj85zcIZmCa5WNK4fNzh8vPfyWBX0hvxQsH79VLDLn+IgHJDYumPhi8uXU4eOyRKNCTK7Bs1xBwE3S08eOJsbUP6k8EOSX2TLRBSQQ07ZgKyegwhlH7o2lXEEL2VAnaLoJfzM3vH7rgKg8guC233G3Amq5tEOj8DSfT1LrAyduBXJLGstozMdBeKOhr/HEEeu1K4u0eZe59jmKpN2vsW9kgwJueYthGRojwbYcK9p1gdO5CEOnx6kZnuUJsI+1bEsZsFl0C7bK5achQGEI3Fyqa3cceUdwh0PYMkR/vBwfrYMV+gqncyVbBifkSek92QOHYATu/Y3czNXdwdDHbpFufgjBiPM3wES9/1D+Bqc26CHwxF7EAWMg3ZE8o/FItWX0wfRBw7OEe4vY95d0nGTEuary3/EM/gnC9XSN/zr8ML2uAEOWDn8Cfy5p+QycUDvkc8rWwaEccuXAqj52acZveTnpZ3yFSc/+Vv0vuhIAMsnJ+fJnagP1osYTEZ7EJxKgOy2DWGrc7TzRrSA7hml0vRIycwN7bZLnW/L4dtRA+j5IEdz2SaSibDJ5iyizZ28bA3QPcOZtzDCTfZNc2XokP2YzLKai9J7Y8RBysdf3KWCXZjw1C/c0IJiPbRxq6qr16KOVHok3GT3beS9EcwzttzmdQJAjaITrdCFzuQFoKYrgiVX3PJoY1diY3IbY3wEpWOQcNDIlcdXLUB7SqJHQKewAqE7bLBzqkMNTcSmu7ueVDGDsk3ssr0O4xIxJDx11iJTveNMDcPkbigNjy4neJkgx04hkbN/o8Mds59KGPnWGGhI2WLOhgwspOk6hBcXJA5H4ch+QXO5esgH+zAIFQ/EEWGu3xPutgB2Ku2XUtTf49zx9Q+k0ynG4c7ZblJeqRDUl8YshBSx64LkorO/TGZ1GPgY9TN7ABZ7GDbn6l5hoEDNqReOoU6sxg73UmpuytdI7oHqGMHrjVHEhoHkcGuL3p4y7lLFLsHIRa5eo7ATXafSFgxq3QgLlhfymzScN3Bh9vNxU5vykySGIdg9LP+IIYMd2mo61ouUexiYK3bJNM0d8uXaglHeOFKwLNuQBIeZuH8ku2+Mxc7fU1SyWNa5yMOqNmElCiRaJ6/NVNIYgc+h3vdtHR0mGxJWu2NB1L2R0tc6KKHhN7tE6A7Zzqai52PPupTch7QELp+LmS4+y4TDV9sQRI7pICJSTWHWuDUue65knYHU4ab7qZL5/UIW2QXAnOx68I5oi+T3IC1Fjlk2pCqhBWA6EizN5LE7gKcMafQJGRx2tyBEjueh+NS+jSVLiEK7Bld32zsHPXKrxOBUuOAViNr1ZMMdsHohuliHEHsOsB5Z3JMOf7g0p5oZ0vcHQnY6e5TqcpVIUHqzczGDpRwpzPNWam5G/YR8uqPCU13o1Ef8QYEsUNKGtqY4NWVgrPUe0qeqWR7O8xj7KU6MzKXxafmNoxdvH4b9tbCpKcJkh43EZd7bSqZ/O2AQZOveG8nhx1aKUD8cHbBJT/UjpI81sQBO93ZhUpzd6fB4pO2GsbuRaL17sPL3pVQCj5DPVEIJeXo0gm1cQaTw26qv5mbu6E4Glavl747gnCzqhmVB7Ay9ohY/xOj2IEr3bU0JJFUpr+jiBbWZis57FrA9aq7iS2F0BOXbIeMC+y72JSH0tTU7gCHBp63BDtQGvYWDe4WEpruQm+jcck9iWEHmsFPEqm5i2yC65A5d0l0R7g35lFv5UqynAdLiR3YnDuXBndbCU13Qeg2+pwDMeyOwrE3M8X9KgubiTCXSG84jsE9a+AxAtglW4YdYIrOnxgS0jRRWkFLHPfoSog7NHxxTQkx7Na7m7G5c/0vjoSLhLL4r8VmeT0rxXQ3spv4nKYisNN/JT0vvCethKOntx8JeaJk5AgYRaTHLh7e3B2xFfOj13Db/KvE8m0W4rCL9pFiJk0X/9GJxI6AHEBMs7sI5UQBk9GJ9StS2MXAS1iImJTt2EwR2l6kCoOBIk/c8yqkiJSHUwLclid2LVBPlDJC051zMV4PKz12YCP8oEUiSO18leZOl90y4aKbXj3gmy9wYd2LjrLEDvgguo1EQjlRwA50z97HiRB26+Dzyznjv3gPq6B61oFct/fvhXviTgmmVziCx22vPLFzQFOS/2RL5kExddGtzH5C2CGHOV+jO/XIejgG7D8k2e8pfrhnnrH8VAHvUt0XyBM7nieK31FCDwpEw3Nbu5DBDqnHE1Jq7PqGOJ9i7R/LSXb7yF9wz/S2PEfASXiuX+okT+yYcsQ02yOQ0JP2o7GAdQlh9yEcUGFsx9QBFxqr9fAh2+/3sCaypRYv7M7w8u12RZ7YgS5oTpQZpNZzNHxxyFQy2OX5i0pm+Xz1/woLAKF8ky8k9BDuqZoAi2/8A5LK00Ge2IGDSHRX5g5CD8pDLS0fhBLBDsAq08rNhs86q7E2g72ku307zkSmPWWxCz3iX+2xXabY8VJznCNUhI35CtFTZM8EcATfT9I8CGb5YRuDF/fBmglnR5LudgZ7kNGuSrB09UJMIJNi5Ymd4wAkcsVtO6EnNd6D9HGnp99A/z1cmuekwU8xWDH4Nex54qM48v3ePxP35JB7lg7nt8i6Xc9Vhtjd3Vg8Ho2hXkqq5uR81Fbx4znxrv/iZRj8wbc38DpxWE927VEaXZ+MfXROrIW3fR2Jgc6ewMgNu7b7fsZsqG2SCD2OQcMXd8GjXizNY5zgBIb+wkFxDo+xpabTXWl0vstgLHd/WniY6YBqqjQ3sa7LzHArYTcyYDXeAWrlr4Se+KCTQcer+xI9Bg4W1R4WVtk1xTVjzXw6/X84Eft0S1U3W9FPKfsRZgYNPe9vHezWX/ITGH6/ZFLP/Lq3IewakFnMtwlODP2wzTjnTGcAHM5iH9/PwmV2rC8v2PfZPERt7DjlU2RTSwu77dUzj2bgHzc3IUvteEIpoMDynwxhV1eip8TCqYWeCWDE7MN+d+OLqH342FOFdpCFy+xk/rf92aWSV6yeTtOW8Eq+0MGOGVFVf8ym2wLWt7PvJKQRhaSeG+5mADvJqoPA04i/gH1sNNal2D2FFnWA+Ri7tQxpY+F0Vx8XAr7wx7QHLM9MW6+66ZhBoIOdT9WHlrm1yhZ2D81CNIVUT28ygN0GqZ5yDTqcu+PtY8vDsI2oCKaGHeiLNcxpj1i4zK7D79mvZre7+LN/Nn5rRQW7C3qLgc305+uJM1qdp5jUYS6utSB1NnlSPaQITl9bjFOhMFuwjfAfTVN/Nd8DayObbaEK66ifyXE0NLDrq88Q4f7nS8szaqtJPEzq2YuaC735jVjJHgI7P+3EJQfYPgevv4ikiZ2ArcLNQrfHhCYaGWLHvMEdIa42eGVGc0Qzr4URyqMNnG8LvfnwBMkeAscMaebxr3iKP8Xe2AyoSk9sMgJtawub4VL/Lflht4NLKun+LZTvJQI9VR0k9fTSXQJvPlS6Z3hpjOQCYf7ENsEjCVCWAdhl1v1zC1Mtxg2WH3b6qmudkOhE1P23klAxMhC5xR2flUBCF7cucP1ufgX3tGhsG751pY2dcwG+MyZbeN/+FRqZYRfhj7OAbUa0jJpcUi5nm7vhrZESHiEjYYO4PZofMMoX24RWFwB1yfsPtik7LT1fxX0gM+yecLPNOJ7T9kTEE8XzJKkGtPEmqSyuehn4W0dCXo+vwna9+wH61IEYfFyFdrqThTfuUGgjJ+xCOWWRPT9Lgy0avfgDsSYcwGxo2kma1/E9eANZB9ZITLTHn2lmWQE7sBw/Ldm8YemZOnLyRRlht5dbYysxIROjEaPKZ6Qc78DvyTwruGaCtId1+BsaCM3cQQOxPb96L7CKnMTbyFIt90h471sP2WCnLyLYBPcXNOPf8LGk2uDYGXW9+03i0KEJwufy2Ar89LIbWEkElllfy/WYThv69ZYJdrMFg+VbIrNA9mRijXDcCEdK5ki9mZ8Cb+4OvXQHYDbhz3h/uFoLu9BLeBRWSbDoOyV9Gi0H7H7Xn9jXYlem2ciA2LmQa0fpiZffoc0Jyb0+XOGzqv/LjOv38HPLf04Cq8m6VngtYooUygTXadswbo4XCxdSxc5Zn1UZnxq8K7oHfZtgQ2y/31NNeWVKX8nvHoOkTH5Rl2DvDewQ9w5grIcd2Ij/FPyl2V0zY6+cbn9xTvU+77M5O4fvGz2WoevU7sQ5OTUV+CNa0b1VFMmmHC8N+Lys2QyfFiRu7gVvaqZX/3MgfmNHLHOzyCWoDN+qHOliiWxP+iQdXLD74EyvdVWmeMrYccradkK6HlST+zgSKFQyYGvnrqoP6Pdv8OO7Osq6rY3ogdckvksuJwZd7BI4tZ2fkMkvCcnfvqulUrEDj2AFTdVZdhE2fkGb/b21WzsC7/969TRTM7Bz0B+bIoT2n2iShGJGqdgdhj2s/uC0/ll4FZmmnrO1WxuJ903QuqXVDOyqUpgKutO1QY7bHX2Uil0w7GObug6Av1fix9aui/Wb64L3xNJeXFczsNtqMAl5DBrLWjFSqdwhb3IThAps3InlQTBJBGZibUVGjcBOX+QvTPDgduEU/NbNJyoVu3mwg5Xv2KEdseNqP0EWGwlmgYDp/gemJmAXyIXiu10XfHu0DFLrpwrFLhA5Hb4pELV2u4M82huJTwOkvfrEoQZgBx5zj2kmGCXyN+qLdlqh2MUgUWruApunKLk0uKsdvoXt7tUE7LK4VXZZuODfkSqF2jtFCuVufogIY6Tb6/JpsI+/gFZxSg3ALlKfdGWcoBuhK3qmKlSoEmXsJOPUXR3kKKMWLxDwRM+JUj52YD6XccZvhmB/p/0LMQ22USZ2oLNx7G7byqnBx8uEmumifOwS9GUIlwm6Nb2DOsN94KhM7Pob9XLsnievFvfEx5loNUucFI8diNBXRm6XhDW4Og7gOWj5pSkTO1BshLp2h+XW4ixPgabedFA8diBNb5p0S8YYmkeexmzEBwcrE7s0e4PU2U+Qn6fDtasCR5+NyscOPNGrJrMv8daY/Nu4ker9jjKxc1loELviUPk12fGxH52Z2RrYxQ+t2vZ4b1v3ihXceW+DqsRn2WF+9JNIEzkaXjVA3cIMWX4qwwWauzpI8diBhCfVLkCp0wPCn8aw//I0POVQtd7IY2jPX8hliqApXXYKUzdXpgf0KKFUu613KB474DDzhQO/W2aPI0c+ynxhO5o7IB58j0x3cxSqM94iSF325BiZtrnhQIEmd5OyvDkcN/SI2ttdKH6Ie7fP6nMflQPq/H3OQZHY9fcXStCcK9s2MwcfCiXFkjCsE06+0ofe6826hckmGTazKjrWC8li7n9dkdilCKnuCmR8OHf4UcBa4V4gndoYLjO+lKYlymne2dWvnFw7nio4/NwjKgHNE1LfVXnQMYuEciWHtZVzu8cK5s0ZI5naeBRsfguk+4ZxK3IPHenRvXujSbfLb72MJgXrUezs9ysPuxVCufRa7ZV3w7seEWi4ZpRUySd3Q/f1Xkv9HSODM+LiMjq8unmzTeanXQ5brjTqrgtR539Y7k0/JpQ2xz5XovluA3zfFOu/MzMaZxq0SVEYdVfGCwzdw4Pyb/w8obOQxxvSbMOK4BCnpVaPYgp9kop94Upl6Yyn9BLaIP2ogFM5c1Qod0nieUk8M4Lhgm8ds6z8whnFQpbMfUqi7mQPoe1RH0VEJUXeFEoDG7JAivmOgY+y2ksJVn3dtWGCGtZUBemM9+YIvcVPm5XxBseXCL1BiCQm8kWwl39ikjVfNrzSykUzpJEiO6F3+OiBUt4huFhwvjsgwTYhqjt805+PWXEbPteQy4b3NIWMWP4vQq/ws4LSa3RJF3qLZRLMdwwaqZb+q7Ve9PoNw36RH8xSBnXdhF5gYImS9qd5vkLvEX3N8v1dCRqY295K2RF2NDJMnY2bIkatSHCuazpTWTqgqYKn8ZAUi9dZhpf/YlKQNV6yp+AkoY1uZTf8869ea6kEN+O9gvu66BSlBcGNHijI3QKLb57Ey0Mwdzf9U35wATaC2Wbhkq9WBBXZKmWkTgqeYf3qJgClyf45gjN3Z0vfxvZT/mCvXLSZ7qfJ5GLcqd1vzF67XFFTxJQegluEPxUYAccMaCf0Pg/3WViZDMzHFab5ZMn34Q8CG9OS/Z9hPHD3DVPYME0T3A35jWkBFCgOXwtWh+09yML57vcG+BuvGd+pES3BWMRu71XaXuj6eOGAHRegSGG+ShT8kupZuNcODtPKTrzfiFfaCAl6OrHaAVugUHEaKljTRNOnsWX3zporN+qG3FNcvuxFwtQtjQOKFedywap1mul3LftQk0LkRd1qL6WNjmOKm+DbpPcECpbji5sLv5mF+TQWNJUVdYpLWuy676Hg2/TrDxQtYwvdBd/tmWWOwfGdE+VDXaLiXNiXNxDOPBG2HihcnOt1FHy7i5bNEJHvuMmFus8WKO0I+3eZcJXyX4qA4qXF58LczUmybLRevygP6jTnlBYmFlUh/Dbd8kENkBb1hNdZ792W6RzW/aWRA3Y5GQobk6krhV/mlxpBHbvOnhE+V2gaWKYemvXxautTF9JQYSNy2ID2KawI1BBpsclGOBlumYWBv+vuW12T8tssRQ2H4wF/4Xfptx7UGHEaJXxq0oRZmJgnPryZv3WX2vbOShqMsbMNaADS+4MaJMe3GEiL+0mJpcfA9U/SrXmoXeqkoKHoUmboTXqCGiVOTwxMSAO3Wr6OT/26T85nVsKujoLOsScHGxiI9nGghgmzwMB8ZLNYgjLkjGOHtfMmH32HhkyHV6ZQpYyCQ9op4VHwK7YFNU4cjnoLv/HV+so6P82AWn9EKY5pTskGxsBmjAuogcLcm2NgoWp9RUl6/mSo7XYKmSUCf8g2MNf92QLUTPEyZFRI3aqg82AAHMHcWBGNbjnYQPdHD3UENVWyehl48exC5UzyR6GW71RCw2NujTfkypCSAGqu7DjylgHL5p61SnmPJHiijpV/ixsPMqRVHziZATVZ+rc36LSWpJCZvgTejN+VfYMvGOz3n0tADZfAbw29v9ubykiBmQUrv/LlvsCm9TDU6z1aghovruUGvTPTS2MU8BKlsBJS5pU8++b2NuS19VMUqAUSv9ugIav7RAVsMy7Ahf3k7Vq8d7ifoQ5/dzOoFRJ56z+GusGjnvy917rABYcOyPkrn2wwxu7hKFtQWySotUEb50rZh8N0gN0k68q3pT3PZRvqav+DDqD2SN5Sg9zNmTBW3u1n9kDt/Vy2DZ3/zGBHt9oPapX0PdfcUHdcvbxW3js8uALVIdmeJdYYpC5sL6hl4vxxO4M9cipA1m4dj2GjrDyn5KwKg13sV3AX1DpxGHHHYKdkH5KzUwpcCshTjttypzeGGO7g3A6gNsq6wYZ90TO3yneHdwvSSXjvkF0DmfA6hn1TT00GtVS6NDPcM/bFO+Ta9CxI5+0hu/wnI41MddqFbWJqK3asq7u34c6Zu0CmxrJhcMO3yqt1TJsKPyMf9DBQi4XZ0MNYVFa4PI+0sAvXn7JqW8YWIwFN7SaEgtotF5Ya4W7gPll6vQ6HGjlcTt/GhklG4je7H44EtV1G5hr5NN2fpcnQ73gT7JL/VDYNyxvT23B/Xr0dAVRhEyQaS6DTdIn8OuoAnMvlmEya1SLFWGe6lduqzHESc2ypsaj+nbvlForaBm7gCnl8wdfrGJnqtDf+cVSJqxbXQe2MhUCnl8grH/VdOAxOFqVjI77JNtKL9rejVNpeimOasROt1q1ZnpxabAunip9u/TPF2AU3jPbhG8Eqa5B0LTP2pWozb8pn4w5ifoDadsfaSZ+c0sJsjCV/tNvOqKChC+3WTKPJRuwGyMdclgI3bap1N3VBZcY2ddrsM21VyjAStMd4juDftsultdsT5WOnGNZgl9GuG/+9s4oYVkJP+xvtvd4FLeWh6+yyE2rXWStC94an0W5rXvBA5UtwrWgooqZTagNZJJxkYPNKD2slpOhwIMd4n81NcVXpMiCBo0TkR/TcEieDvfFQOH/xNOssEPcW2hvtr47tj6lnCSOTiI+YInatJnSxekt94CY9sUITZr2+56rxzhq/wEnlyqjEipnwtJWd+1q5nbZwUpHb/0e7AZEbTngY7yj39utUpkRNeNcHi/iGNTd2WzkU4A+oPbso6/+Xz1vaXMTnufOouqsTK31nDBGTM7jRV1bNtQvnVtQOoLq8Hl6aLaKHPM6uV2kSL79PHScm57+m02krZhAIhwf+XXpPTrhXIarMX6976lRnYs9e6yUqS/qQ2fnWOqUF2sH+qLSSK8ZO7Caqa/wXx6ocmSxF58QV7sxsEm6lFtaDs9wlUXno3a/sxJXfq5imak3MkXiffjaietit2MsqkT4j4FW2jPwwO743aLW4ikPdD6renObKyPMiK3dqKjZaYZMX2wrWj5H2gXad38xbXH9411NtYZZI3hIPcR1t89EM+h6McGZSzVGiD2sx+bLYClcV21UXYsuEuS5KPcXJmrM+lBeWW3AD/iJnDXDO33JRbD0/342zVG4sltCJrcUW/8pOD6BaITAQLmabXUpqs3G4bI7YPvDMVd3qpJGMugPFdrpm/JI29CyQv4+Bnz6bxEMcom7aeYh9/8T7x35XgZFKIj73F9vxWr9u59fTSlGZBm8AfpZeUZZxr1h8zdLsL1S3dWm3eFeKe4vufe2dglt0YlWewuu/jcSHCoesxb5+ot/a/UiSapSQfFPtU8dDPHj2F7+5QoO8XOQIOVK6WzvlJYclin9jP99rqqaOiNrq3mDxnz5LXreh4cSPdGubwrEeUiX7ZiLeab/MlFLKvb5SLWGkpO+A1qYMhbZdv4/zCS/+l+EnFkhzQh4wfa7GlBcdMqOrSgdBOf7OShuTyLsa9sYUkovPTBiPqxYHLjrnbZzuZtIranfm/qqSQVpXttXOtEHRRvfbMp9YPrfYSvhhYyybPNcG3PY08fXG56oZnKiAN7F1R1PJ61FvRCwZg9EWZLkzuzgpY5u1r5+/xrQX06w+3V/VmVCSFhMHmwoe6w83PYDEcnsMcYQ+Y5bO0Hn9zDGN3E1+p8onw1QaKEpwUoXG5EHSLltZmCR1yBlzH3GuND1iZqTPvopMM17Hd3cXlQTKkuB1aJnpI6X1c0sfVNJTSvtZEBKo2syURc9hc3jyoYHNzXiRxPRFI1UKrCAOWc0Gas2SzKW5+yOkMqBFfoE4nYp2dh7WMLm4k59ZbxBSP011M7EaePm5pzTmkdcxM6xw0frjUmzHN3yG6O6OG1+Z47vsH7S0e6J5bde2W5KlmsGsKi4Hwx5qzZZWl4am5VsaehM6HFn+Rhg+DxWNPr/K18bsNtv4TlB1w9YX27QyN60F4m9XPPT9tZYkzhuBGIvDhG7mnJeWfD/M3Am6apZeOkC1gslD4nfkfmJvCXlam2VzK+ot8Ir6t1kbJme0kuEC9IrjwXHTDjxuv9M/26Jmak6dUVdXOUnfRbfXaC0W+xt1mk2YfP3CUxO3fPMQp6zuL9Ms943IupXc5I/KRMtb57EnQFWYyE2OB5Vf1EoiHpk9BpctXnCvzYMEkc92vY3cokm883fhr6eUn91TOTdamlYNbNJQVZjIUpbfKx6ilUw0NtkhNwZ/UC9394cbwvOLema4jJwllEi0IbK7/GxnSHZHjXRt8R838d+qDUy+siM5vbeWgNj4j29k16/ixKWzP9TbNjt36OmbnVm5OWNL+eMzzcqmL3XTkhPNs9wgB3Vo5S0js0ZZeL6QldisrufTVx1VJYhr2rleNjWBOU2rb5PUAiYKki63mt1ROnRzyr4vilGHUlkS+fT1+62ylYqcfWbB5L/VDZ0yJfBwvdYK3OfZVP6QFKeOnoIlJnb7oGcaJTHn7vvYJ0NVltQAyUu51Kq3EpDrOP7222pe9RokXdPe7Bctb+YSu32zQs1KV/MUenkbm3UKkeWC6x7d6f7E9aqqpKZK/JStfVq7yYu5NTlnA4Li1bGp6cfbqUfPPWsuD+TsnzU5WuqijkntEMYpbn/upzutOe25r7nRfsvhnk7qmbW2Sd+siQ3+umEFK9pnPT7dtjG8gzoCtVac2k79sLx992w/OsDZePSqn5u0LkMtZK0KK3FeAYXjfD0J2tJCxud88fjotKdqX6sCS/B70wbsW3W5h8T6vdScE0063wovGqv2sCpCEuPaeFjLFQt+PFvRa6B3iLkGXb+Hy+bM/aX9ubrXSt6LDT6udqsq4o8c69scnpj845JDFZN8L2a2M4Zg73aeF327VRy6v23CgVsN13ZRVXGqWCQOI2O/i9hResXrn0UHFiTPyJ39uEE9vRQ22PZm+b6Pdx8Y8H5Jm9IdEV1jbVVnJVVUUUUVVVRRRRVVVFFFFVVUUUUVVVRRRZny/wGGZUNertQEUQAAAABJRU5ErkJggg==",
    theme = {template: "paginated"}, apiUrl = "http://api.geocoding.ir/api/address_validation?",
    apiToken = "09eb07718a37c49a0f761a890ee08f335bbc363f", _token = "455a4c910a3a606dcabcc174f0262e94573e6428",
    form_token = "AfePNJyeFrAJFFgzL6aex39hthdUhQvB", selectedfields = [{"id":"IdentityType","Required":false,"separator":"1","separatorName":"اطلاعات سجلی"},{"id":"FirstName","Required":false,"min":"3","max":"50","separator":"1","separatorName":"اطلاعات سجلی"},{"id":"LastName","Required":false,"min":"3","max":"50","separator":"1","separatorName":"اطلاعات سجلی"},{"id":"NationalID","Required":false,"length":"10","separator":"1","separatorName":"اطلاعات سجلی"},{"id":"CellNumber","Required":false,"min":"11","max":"11","separator":"1","separatorName":"اطلاعات سجلی"},{"id":"AddressStatus","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"PostCode","Required":false,"min":"10","max":"10","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"Province","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"City","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"RegionType","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"RegionName","Required":false,"min":"2","max":"50","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"MainRoadType","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"MainRoadName","Required":false,"min":"3","max":"80","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"PrimaryRoadType","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"PrimaryRoadName","Required":false,"min":"3","max":"80","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"SecondaryRoadType","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"SecondaryRoadName","Required":false,"min":"3","max":"80","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"HouseNumber","Required":false,"min":"1","max":"6","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"Floor","Required":false,"separator":"2","separatorName":"اطلاعات نشانی"},{"id":"Unit","Required":false,"min":"1","max":"10","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"BuildingName","Required":false,"min":"1","max":"50","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"DoorColor","Required":false,"min":"2","max":"20","separator":"2","separatorName":"اطلاعات نشانی"},{"id":"Location","Required":false,"separator":"4","separatorName":"موقعیت مکانی روی نقشه"},{"id":"PresentDays","Required":false,"separator":"3","separatorName":"اطلاعات تحویل"},{"id":"PresentHours","Required":false,"separator":"3","separatorName":"اطلاعات تحویل"},{"id":"DeliveryToSecuirty","Required":false,"separator":"3","separatorName":"اطلاعات تحویل"},{"id":"ExtraDeliveryNotes","Required":false,"separator":"3","separatorName":"اطلاعات تحویل"}]
;var controlsType = [], authenticatedCode = "",
    styleDatkccs = "@font-face{font-family:IRANSans;src:url(static/IRANSans.ttf)}.mfl-form-group{margin-bottom:5px;width:calc(50% - 10px);display:inline-block;padding:0 5px;vertical-align:top;position:relative;transition:all .3s}.mfl-form-group:after,.mfl-form-group:after{content:\"\";position:absolute;width:0;height:0;left:13px;top:20px;border-bottom:3px solid #39da8a;border-right:3px solid #39da8a;transform:rotate(45deg);border-radius:3px;opacity:0;transition:opacity .2s,width .3s,height .5s,top .2s}.mfl-form-group.validated:after{opacity:1;top:7px;width:5px;height:10px}.mfl-form-group.mfl-form-checkbox.validated:after,.mfl-form-group.mfl-form-radio.validated:after{opacity:0}.mfl-form-group.select:after{left:27px}.mfl-form-group.textarea.validated:after{top:4px}.mfl-form-group.validated .mfl-input,.mfl-form-group.validated .mfl-checkboxes-field,.mfl-form-group.validated .mfl-radios-field{border-color:#39da8a}.mfl-input{display:block;width:100%;height:32px;padding:5px 10 5px 22px;font-family:inherit;font-size:inherit;font-weight:400;line-height:20px;color:#fff;background-color:rgba(31,31,31,.1);background-clip:padding-box;border:1px solid rgba(255,255,255,.22);border-radius:3px;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out}.mfl-input:required{background-color:#696969}.mfl-inputTooth{display:block;width:100%;height:12px;padding:5px 10 5px 22px;font-family:inherit;font-size:inherit;font-weight:400;line-height:20px;color:#fff;background-color:rgba(31,31,31,.1);background-clip:padding-box;border:1px rgba(255,255,255,.22);border-style:none solid solid solid;border-radius:1px;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out}.mfl-inputTooth:required{background-color:#696969}.mfl-masktext{font-weight:300;color:#c7c7c7;text-align:right !important;direction:rtl !important;display:block;width:100%;height:32px;padding:5px 10 5px 22px;font-family:inherit;font-size:inherit;line-height:20px;background-color:rgba(31,31,31,.1);background-clip:padding-box;border:1px solid rgba(255,255,255,.22);border-radius:3px;transition:border-color .15s ease-in-out,box-shadow .15s ease-in-out}.mfl-form-validation{display:block;margin-top:3px;color:#ff5b5c;font-size:10px;font-weight:bold}.mfl-form{font-family:IRANSans;font-size:12.5px;line-height:13px;display:flex;width:400%}select.mfl-input{padding-top:4px;color:#c7c7c7}.mfl-form input[type=\"submit\"],.mfl-page button{color:#fff;background-color:#5a8dee;border-color:#007bff;display:inline-block;font-weight:400;text-align:center;vertical-align:middle;min-width:60px;cursor:pointer;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;border:1px solid transparent;padding:5px 10px;font-size:13px;line-height:1.5;border-radius:3px;transition:color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;font-family:inherit;min-width:90px}.mfl-input:focus{color:#fff;background-color:rgba(255,255,255,.12);border-color:#80bdff;outline:0;box-shadow:0 0 0 1px rgba(0,123,255,.25)}.mfl-input[type='checkbox']:focus{box-shadow:none}select.mfl-input[multiple]{height:auto;overflow:hidden;background-color:#41475e}.mfl-form-description{display:block;margin-top:3px;color:#a9a9a9;font-size:10px}.mfl-input::placeholder{font-weight:300;color:#c7c7c7;text-align:right !important;direction:rtl !important}.mfl-input.error,.mfl-radios-field.error,.mfl-checkboxes-field.error,input.error~.mfl-image{border-color:#ff5b5c !important}.mfl-checkbox>label{color:#696969}#mfl-deliveryDay-wrapper>label{color:#696969;line-height:2}.mfl-page{width:25%;box-shadow:-4px 4px 8px 0 rgba(11,26,51,.63);background-color:#272e48;border-radius:3px;padding:15px 8px;margin:0;height:fit-content;transition:all .3s;opacity:0}.mfl-page:first-child{opacity:1}.mfl-form-wrapper{width:100%;overflow:hidden;padding-top:10px;scroll-behavior:smooth;transition:all .2s}.mfl-form-bottom{min-height:12px}.mfl-form-pagination{text-align:center;margin-top:20px}.mfl-prev-button{margin:0 10px}.mfl-next-button{margin:0 10px}.mfl-page button:hover,.mfl-page input[type=\"submit\"]:hover{background-color:#719df0!important}#mfl-HouseNumber-wrapper .mfl-group-append{margin-right:-1px;display:inline-block;width:calc(40% - 25px) !important;vertical-align:top;overflow:hidden;padding:7px 5px 8px 19px;border-radius:3px;border-top-right-radius:0;border-bottom-right-radius:0;background-color:rgba(37,37,37,.37);border:1px solid rgba(255,255,255,.22)}#mfl-HouseNumber-wrapper .mfl-group-text{align-items:center;margin-bottom:0;font-size:10px;font-weight:400;line-height:1.5;color:#fff;text-align:center;white-space:nowrap}.mfl-input.mfl-input-prepend{border-top-left-radius:0;border-bottom-left-radius:0;display:inline-block;width:60%}.mfl-group-text [type='checkbox']{vertical-align:sub !important}.mfl-group-prepend{display:inline-block;width:60%;border-top-left-radius:0;border-bottom-left-radius:0}.mfl-input[disabled]{background-color:#808080}#mfl-recipient-wrapper,#mfl-deliveryTime-wrapper{width:98%}#mfl-map{width:100%;height:400px}.mfl-form-group option{background-color:#41475e}.mfl-page-title{color:#c7c7c7;padding:0 7px 10px;border-bottom:1px solid rgba(199,199,199,.3);margin-bottom:12px;font-weight:bold;font-size:16px}.mfl-submit-simple{display:none;text-align:center}.mfl-form-radio,.mfl-form-group.map{width:calc(100% - 10px)}.mfl-form-radio .mfl-radios-field,.mfl-form-checkbox .mfl-checkboxes-field,.mfl-form-group.cardnumber .mfl-card-field,.mfl-form-group.phone .mfl-phone-field{color:#c7c7c7;background-color:rgba(31,31,31,.1);padding:9px 10px;border:1px solid rgba(255,255,255,.22);border-radius:3px}.mfl-form-radio .mfl-radios-group,.mfl-form-checkbox .mfl-checkboxes-group{float:left;margin-top:-1px}.mfl-radios-field:required{border-bottom-color:#daa520;border-bottom-width:3px}.mfl-form-radio input[type='radio'],.mfl-form-checkbox input[type='checkbox']{position:relative;margin:2px 7px;cursor:pointer;width:10px;height:10px;vertical-align:text-top}.mfl-form-checkbox input[type='checkbox']{margin:2px 6px}.mfl-form-radio input[type='radio']:after{content:' ';position:absolute;right:-1px;left:-1px;top:-1px;bottom:-1px;border-radius:50%;background-color:#272e48;border:5px solid #272e48;box-shadow:0 0 1px 1px lightgray;transition:all .2s}.mfl-form-radio input[type='radio']:checked:after{background-color:#bdd1f8;border-width:3px}.mfl-form-checkbox input[type='checkbox']:after{content:' ';position:absolute;right:0;left:0;top:0;bottom:0;border-radius:2px;background-color:#272e48;box-shadow:0 0 1px 1px lightgray;transition:all .2s}.mfl-form-checkbox input[type='checkbox']~span.check{position:absolute;width:0;height:0;top:10px;right:7px;border-bottom:2px solid #bdd1f8;border-right:2px solid #bdd1f8;transform:rotate(45deg) scale(.9);opacity:0;pointer-events:none;transition:opacity .2s,width .2s,height .4s,top .2s}.mfl-form-checkbox input[type='checkbox']:checked~span.check{width:3px;height:9px;top:0;opacity:1}.mfl-checkboxes-group>span{position:relative}.mfl-form-radio label,.mfl-form-checkbox label{vertical-align:text-top;margin-left:5px;cursor:pointer;position:relative}.mfl-form-help{position:absolute;background:rgba(0,0,0,.9);color:#c7c7c7;z-index:999;bottom:calc(100% + 30px);font-size:11px;line-height:17px;border-radius:3px;opacity:0;padding:0;height:0;box-shadow:0 -1px 5px 1px rgba(158,158,158,.35);transition:all .3s}.mfl-form-help.show{opacity:1;padding:7px;height:auto;bottom:calc(100% + 10px)}.mfl-form-help:after{content:\"\";position:absolute;width:0;height:0;bottom:-10px;right:0;left:0;margin:auto;border-top:10px solid rgba(0,0,0,.9);border-right:10px solid transparent;border-left:10px solid transparent}.mfl-form-help .strike-word{text-decoration:line-through #ff5b5c;padding:0 2px;color:#ff5b5c}.mfl-form-group .multiline{float:none;margin-top:15px}.mfl-form-group .multiline>*{display:inline-block;margin:7px 0;color:#fff;width:100%}.mfl-form-group.textarea{width:calc(100% - 10px)}textarea.mfl-input{height:90px;resize:none;padding-top:2px;padding-bottom:2px}.mfl-form-group.image,.mfl-form-group.cardnumber,.mfl-form-group.phone{width:calc(100% - 10px)}.mfl-form-group.image>input[type='file']{display:none}.mfl-file-name{font-size:10px}.mfl-form-group.image>.mfl-image{color:#c7c7c7;background-color:rgba(31,31,31,.1);padding:9px 10px;border:1px solid rgba(255,255,255,.22);border-radius:3px;top:0;text-align:left;transition:all .2s}.mfl-form-group.image>.mfl-image>label{float:right;line-height:26px}.mfl-form-group.image>.mfl-image>button{font-size:10px;background-color:#fdac41 !important;border-color:#fdac41 !important;margin-right:5px}.mfl-card-group,.mfl-phone-group{float:left;margin-top:-4px;direction:ltr;width:60%}.mfl-card-group input{max-width:calc(25% - 3px);width:48px;background:rgba(255,255,255,.07);border:1px solid rgba(255,255,255,.22);border-radius:3px;color:#fff;text-align:center;margin-right:3px;font-size:14px;line-height:16px}.mfl-phone-group>*{background:rgba(255,255,255,.07);border:1px solid rgba(255,255,255,.22);border-radius:3px;color:#fff;text-align:center;margin-right:3px;font-size:12px;font-family:inherit;line-height:16px}.mfl-phone-group>*:nth-child(1){width:15%;height:20px;vertical-align:top;direction:rtl}.mfl-phone-group>*:nth-child(2){width:20%}.mfl-phone-group>*:nth-child(3){width:40%}.mfl-phone-group>*:nth-child(4){width:10%}.mfl-form-group.ltr input{direction:ltr}.mfl-form-group.ltr.text input{padding:5px 10px}.mfl-form-group.ltr:after{right:13px;left:unset}@media only screen and (min-width:768px){#mfl-Gender-wrapper{width:calc(50% - 10px)}}#mfl-HouseNumber{width:60%;display:inline-block;border-top-left-radius:0;border-bottom-left-radius:0}#mfl-HouseNumber-wrapper:after{left:calc(40% + 12px)}#mfl-PresentDays .multiline>*{width:50%}.mfl-page.address-page .mfl-page-body .mfl-form-group:nth-child(odd){width:calc(40% - 10px)}.mfl-page.address-page .mfl-page-body .mfl-form-group:nth-child(even){width:calc(60% - 10px)}.mfl-form-group.w50{width:calc(50% - 10px)}.mfl-form-group.w100{width:calc(100% - 10px)}.mfl-selfportrait-desc{text-align:center;margin:20px 0 5px}.mfl-selfportrait-desc>div:first-child{width:calc(59% - 20px);display:inline-block;padding:10px;vertical-align:middle}.mfl-selfportrait-desc>div:nth-child(2){width:calc(39% - 20px);display:inline-block;padding:10px 15px 10px 0;vertical-align:middle}.mfl-selfportrait-desc .code{font-size:36px;font-weight:bold;color:#ff5b5c;border:3px solid;line-height:60px;padding-top:5px}.mfl-selfportrait-desc .help{line-height:19px;display:block;text-align:justify;margin-top:10px}.mfl-selfportrait-desc .image{height:170px;background-size:contain;background-repeat:no-repeat;background-position:center}@media only screen and (max-width:767px){#mfl-HouseNumber-wrapper{width:calc(100% - 10px)}#mfl-PresentDays-wrapper,#mfl-PresentHours-wrapper{width:calc(100% - 10px)}#mfl-PresentHours .multile>*{width:50%}.mfl-form-group,.mfl-form-group.w50{width:calc(100% - 10px)}.mfl-card-group,.mfl-phone-group,.mfl-form-radio .mfl-radios-group{float:none;margin-top:10px;width:100%;text-align:center}}.autocomplete{position:relative;display:inline-block}input[type=submit]{background-color:#1e90ff;color:#fff}.autocomplete-items{position:absolute;border:1px solid #d4d4d4;border-bottom:none;border-top:none;z-index:99;top:100%;left:0;right:0}.autocomplete-items div{padding:10px;cursor:pointer;background-color:#1a233a;border-bottom:1px solid #1a233a}.autocomplete-items div:hover{background-color:#e9e9e9}.autocomplete-active{background-color:#1e90ff !important;color:#fff}",
    styleDatkSimpleccs = ".mfl-form-wrapper.simple{width:auto !important;box-shadow:-4px 4px 8px 0 rgba(11,26,51,.63);background-color:#272e48;border-radius:3px;padding:15px 8px;margin:0;height:fit-content}.mfl-form-wrapper.simple .mfl-page{width:auto !important;box-shadow:none;opacity:1 !important;transform:scale(1) !important;display:block !important;margin:0}.mfl-form-wrapper.simple .mfl-form{display:block;width:auto !important}.mfl-form-wrapper.simple .mfl-form-pagination{display:none}.mfl-form-wrapper.simple .mfl-submit-simple{display:block}",
    datePickercss = "@charset \"UTF-8\";.pika-single{z-index:9999;display:block;position:relative;color:#333;background:#fff;border:1px solid #ccc;border-bottom-color:#bbb;font-family:IRANSans}.pika-single:before,.pika-single:after{content:\" \";display:table}.pika-single:after{clear:both}.pika-single{*zoom:1}.pika-single.is-hidden{display:none}.pika-single.is-bound{position:absolute;box-shadow:0 5px 15px -5px rgba(0,0,0,.5)}.pika-lendar{float:left;width:240px;margin:8px}.pika-title{position:relative;text-align:center}.pika-label{display:inline-block;*display:inline;position:relative;z-index:9999;overflow:hidden;margin:0;padding:5px 3px;font-size:14px;line-height:20px;font-weight:bold;background-color:#fff}.pika-title select{cursor:pointer;position:absolute;z-index:9998;margin:0;left:0;top:5px;filter:alpha(opacity=0);opacity:0}.pika-prev,.pika-next{display:block;cursor:pointer;position:relative;outline:none;border:0;padding:0;width:20px;height:30px;text-indent:20px;white-space:nowrap;overflow:hidden;background-color:transparent;background-position:center center;background-repeat:no-repeat;background-size:75% 75%;opacity:.5;*position:absolute;*top:0}.pika-prev:hover,.pika-next:hover{opacity:1}.pika-prev,.is-rtl .pika-next{float:left;background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAeCAYAAAAsEj5rAAAAUklEQVR42u3VMQoAIBADQf8Pgj+OD9hG2CtONJB2ymQkKe0HbwAP0xucDiQWARITIDEBEnMgMQ8S8+AqBIl6kKgHiXqQqAeJepBo/z38J/U0uAHlaBkBl9I4GwAAAABJRU5ErkJggg==');*left:0}.pika-next,.is-rtl .pika-prev{float:right;background-image:url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAeCAYAAAAsEj5rAAAAU0lEQVR42u3VOwoAMAgE0dwfAnNjU26bYkBCFGwfiL9VVWoO+BJ4Gf3gtsEKKoFBNTCoCAYVwaAiGNQGMUHMkjGbgjk2mIONuXo0nC8XnCf1JXgArVIZAQh5TKYAAAAASUVORK5CYII=');*right:0}.pika-prev.is-disabled,.pika-next.is-disabled{cursor:default;opacity:.2}.pika-select{display:inline-block;*display:inline}.pika-table{width:100%;border-collapse:collapse;border-spacing:0;border:0}.pika-table th,.pika-table td{width:14.285714285714286%;padding:0}.pika-table th{color:#999;font-size:12px;line-height:25px;font-weight:bold;text-align:center}.pika-button{cursor:pointer;display:block;box-sizing:border-box;-moz-box-sizing:border-box;outline:none;border:0;margin:0;width:100%;padding:5px;color:#666;font-family:IRANSans;font-size:12px;line-height:15px;text-align:center;background:#f5f5f5}.pika-week{font-size:11px;color:#999}.is-today .pika-button{color:#3af;font-weight:bold}.is-selected .pika-button{color:#fff;font-weight:bold;background:#3af;box-shadow:inset 0 1px 3px #178fe5;border-radius:3px}.is-inrange .pika-button{background:#d5e9f7}.is-startrange .pika-button{color:#fff;background:#6cb31d;box-shadow:none;border-radius:3px}.is-endrange .pika-button{color:#fff;background:#3af;box-shadow:none;border-radius:3px}.is-disabled .pika-button,.is-outside-current-month .pika-button{pointer-events:none;cursor:default;color:#999;opacity:.3}.pika-button:hover{color:#fff;background:#ff8000;box-shadow:none;border-radius:3px}.pika-table abbr{border-bottom:none;cursor:help}",
    mapcss = ".leaflet-container{background:#fff;font:12px/20px 'Helvetica Neue',Arial,Helvetica,sans-serif;color:#404040;color:rgba(0,0,0,.75);outline:0;overflow:hidden;-ms-touch-action:none}.leaflet-container *,.leaflet-container :after,.leaflet-container :before{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}.leaflet-container h1,.leaflet-container h2,.leaflet-container h3,.leaflet-container h4,.leaflet-container h5,.leaflet-container h6,.leaflet-container p{font-size:15px;line-height:20px;margin:0 0 10px}.leaflet-container .marker-description img{margin-bottom:10px}.leaflet-container a{color:#3887be;font-weight:400;text-decoration:none}.leaflet-container a:hover{color:#63b6e5}.leaflet-container.dark a{color:#63b6e5}.leaflet-container.dark a:hover{color:#8fcaec}.leaflet-container .mapbox-button,.leaflet-container.dark .mapbox-button{background-color:#3887be;display:inline-block;height:40px;line-height:40px;text-decoration:none;color:#fff;font-size:12px;white-space:nowrap;text-overflow:ellipsis}.leaflet-container .mapbox-button:hover,.leaflet-container.dark .mapbox-button:hover{color:#fff;background-color:#3bb2d0}.leaflet-image-layer,.leaflet-layer,.leaflet-map-pane canvas,.leaflet-map-pane svg,.leaflet-marker-icon,.leaflet-marker-shadow,.leaflet-pane,.leaflet-tile,.leaflet-tile-container,.leaflet-zoom-box{position:absolute;left:0;top:0}.leaflet-container{overflow:hidden}.leaflet-marker-icon,.leaflet-marker-shadow,.leaflet-tile{-webkit-user-select:none;-moz-user-select:none;user-select:none;-webkit-user-drag:none}.leaflet-safari .leaflet-tile{image-rendering:-webkit-optimize-contrast}.leaflet-safari .leaflet-tile-container{width:1600px;height:1600px;-webkit-transform-origin:0 0}.leaflet-marker-icon,.leaflet-marker-shadow{display:block}.leaflet-container .leaflet-marker-pane img,.leaflet-container .leaflet-overlay-pane svg,.leaflet-container .leaflet-tile-pane img,.leaflet-container img.leaflet-image-layer{max-width:none!important}.leaflet-container.leaflet-touch-zoom{-ms-touch-action:pan-x pan-y;touch-action:pan-x pan-y}.leaflet-container.leaflet-touch-drag{-ms-touch-action:pinch-zoom}.leaflet-container.leaflet-touch-drag.leaflet-touch-drag{-ms-touch-action:none;touch-action:none}.leaflet-tile{filter:inherit;visibility:hidden}.leaflet-tile-loaded{visibility:inherit}.leaflet-zoom-box{width:0;height:0;z-index:800}.leaflet-overlay-pane svg{-moz-user-select:none}.leaflet-map-pane canvas{z-index:1}.leaflet-map-pane svg{z-index:2}.leaflet-tile-pane{z-index:2}.leaflet-overlay-pane{z-index:4}.leaflet-shadow-pane{z-index:5}.leaflet-marker-pane{z-index:6}.leaflet-tooltip-pane{z-index:7}.leaflet-popup-pane{z-index:8}.leaflet-vml-shape{width:1px;height:1px}.lvml{behavior:url(#default#VML);display:inline-block;position:absolute}.leaflet-control{position:relative;z-index:800;pointer-events:visiblePainted;pointer-events:auto}.leaflet-bottom,.leaflet-top{position:absolute;z-index:1000;pointer-events:none}.leaflet-top{top:0}.leaflet-right{right:0}.leaflet-bottom{bottom:0}.leaflet-left{left:0}.leaflet-control{float:left;clear:both}.leaflet-right .leaflet-control{float:right}.leaflet-top .leaflet-control{margin-top:10px}.leaflet-bottom .leaflet-control{margin-bottom:10px}.leaflet-left .leaflet-control{margin-left:10px}.leaflet-right .leaflet-control{margin-right:10px}.leaflet-fade-anim .leaflet-tile{will-change:opacity}.leaflet-fade-anim .leaflet-popup{opacity:0;-webkit-transition:opacity .2s linear;-moz-transition:opacity .2s linear;-o-transition:opacity .2s linear;transition:opacity .2s linear}.leaflet-fade-anim .leaflet-map-pane .leaflet-popup{opacity:1}.leaflet-zoom-animated{-webkit-transform-origin:0 0;-ms-transform-origin:0 0;transform-origin:0 0}.leaflet-zoom-anim .leaflet-zoom-animated{will-change:transform}.leaflet-zoom-anim .leaflet-zoom-animated{-webkit-transition:-webkit-transform .25s cubic-bezier(0,0,.25,1);-moz-transition:-moz-transform .25s cubic-bezier(0,0,.25,1);-o-transition:-o-transform .25s cubic-bezier(0,0,.25,1);transition:transform .25s cubic-bezier(0,0,.25,1)}.leaflet-pan-anim .leaflet-tile,.leaflet-zoom-anim .leaflet-tile{-webkit-transition:none;-moz-transition:none;-o-transition:none;transition:none}.leaflet-zoom-anim .leaflet-zoom-hide{visibility:hidden}.leaflet-interactive{cursor:pointer}.leaflet-grab{cursor:-webkit-grab;cursor:-moz-grab}.leaflet-crosshair,.leaflet-crosshair .leaflet-interactive{cursor:crosshair}.leaflet-control,.leaflet-popup-pane{cursor:auto}.leaflet-dragging .leaflet-grab,.leaflet-dragging .leaflet-grab .leaflet-interactive,.leaflet-dragging .leaflet-marker-draggable{cursor:move;cursor:-webkit-grabbing;cursor:-moz-grabbing}.leaflet-image-layer,.leaflet-marker-icon,.leaflet-marker-shadow,.leaflet-pane>svg path,.leaflet-tile-container{pointer-events:none}.leaflet-image-layer.leaflet-interactive,.leaflet-marker-icon.leaflet-interactive,.leaflet-pane>svg path.leaflet-interactive{pointer-events:visiblePainted;pointer-events:auto}.leaflet-container{outline:0}.leaflet-zoom-box{background:#fff;border:2px dotted #202020;opacity:.5}.leaflet-bar,.leaflet-control-layers{background-color:#fff;border:1px solid #999;border-color:rgba(0,0,0,.4);border-radius:3px;box-shadow:none}.leaflet-bar a,.leaflet-bar a:hover{color:#404040;color:rgba(0,0,0,.75);border-bottom:1px solid #ddd;border-bottom-color:rgba(0,0,0,.1)}.leaflet-bar a:last-child{border-bottom:none}.leaflet-bar a:active,.leaflet-bar a:hover{background-color:#f8f8f8;cursor:pointer}.leaflet-bar a:hover:first-child{border-radius:3px 3px 0 0}.leaflet-bar a:hover:last-child{border-radius:0 0 3px 3px}.leaflet-bar a:hover:only-of-type{border-radius:3px}.leaflet-bar .leaflet-disabled{cursor:default;opacity:.75}.leaflet-control-zoom-in,.leaflet-control-zoom-out{display:block;content:'';text-indent:-999em}.leaflet-control-layers .leaflet-control-layers-list,.leaflet-control-layers-expanded .leaflet-control-layers-toggle{display:none}.leaflet-control-layers-expanded .leaflet-control-layers-list{display:block;position:relative}.leaflet-control-layers-expanded{background:#fff;padding:6px 10px 6px 6px;color:#404040;color:rgba(0,0,0,.75)}.leaflet-control-layers-selector{margin-top:2px;position:relative;top:1px}.leaflet-control-layers label{display:block}.leaflet-control-layers-separator{height:0;border-top:1px solid #ddd;border-top-color:rgba(0,0,0,.1);margin:5px -10px 5px -6px}.leaflet-default-icon-path{background-image:url(images/marker-icon.png)}.leaflet-container .leaflet-control-attribution{background-color:rgba(255,255,255,.5);margin:0;box-shadow:none}.leaflet-container .leaflet-control-attribution a,.leaflet-container .map-info-container a{color:#404040}.leaflet-control-attribution a:hover,.map-info-container a:hover{color:inherit;text-decoration:underline}.leaflet-control-attribution,.leaflet-control-scale-line{padding:0 5px}.leaflet-left .leaflet-control-scale{margin-left:5px}.leaflet-bottom .leaflet-control-scale{margin-bottom:5px}.leaflet-container .mapbox-improve-map{font-weight:700}.leaflet-control-scale-line{background-color:rgba(255,255,255,.5);border:1px solid #999;border-color:rgba(0,0,0,.4);border-top:none;padding:2px 5px 1px;white-space:nowrap;overflow:hidden}.leaflet-control-scale-line:last-child{border-top:2px solid #ddd;border-top-color:rgba(0,0,0,.1);border-bottom:none;margin-top:-2px}.leaflet-container .leaflet-control-attribution.leaflet-compact-attribution{margin:10px}.leaflet-container .leaflet-control-attribution.leaflet-compact-attribution{background:#fff;border-radius:3px 13px 13px 3px;padding:3px 31px 3px 3px;visibility:hidden}.leaflet-control-attribution.leaflet-compact-attribution:hover{visibility:visible}.leaflet-control-attribution.leaflet-compact-attribution:after{content:'';background-color:#fff;background-color:rgba(255,255,255,.5);background-position:0 -78px;border-radius:50%;position:absolute;display:inline-block;width:26px;height:26px;vertical-align:middle;bottom:0;z-index:1;visibility:visible;cursor:pointer}.leaflet-control-attribution.leaflet-compact-attribution:hover:after{background-color:#fff}.leaflet-right .leaflet-control-attribution.leaflet-compact-attribution:after{right:0}.leaflet-left .leaflet-control-attribution.leaflet-compact-attribution:after{left:0}.leaflet-touch .leaflet-bar,.leaflet-touch .leaflet-control-layers{border:2px solid rgba(0,0,0,.2);background-clip:padding-box}.leaflet-popup{position:absolute;text-align:center;pointer-events:none}.leaflet-popup-content-wrapper{padding:1px;text-align:left;pointer-events:all}.leaflet-popup-content{padding:10px 10px 15px;margin:0;line-height:inherit}.leaflet-popup-close-button+.leaflet-popup-content-wrapper .leaflet-popup-content{padding-top:15px}.leaflet-popup-tip-container{width:20px;height:20px;margin:0 auto;position:relative}.leaflet-popup-tip{width:0;height:0;margin:0;border-left:10px solid transparent;border-right:10px solid transparent;border-top:10px solid #fff;box-shadow:none}.leaflet-popup-close-button{text-indent:-999em;position:absolute;top:0;right:0;pointer-events:all}.leaflet-popup-close-button:hover{background-color:#f8f8f8}.leaflet-popup-scrolled{overflow:auto;border-bottom:1px solid #ddd;border-top:1px solid #ddd}.leaflet-div-icon{background:#fff;border:1px solid #999;border-color:rgba(0,0,0,.4)}.leaflet-editing-icon{border-radius:3px}.leaflet-tooltip{position:absolute;padding:5px;background-color:#fff;border:1px solid #fff;border-radius:3px;white-space:nowrap;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;pointer-events:none}.leaflet-tooltip.leaflet-clickable{cursor:pointer;pointer-events:auto}.leaflet-tooltip-bottom:before,.leaflet-tooltip-left:before,.leaflet-tooltip-right:before,.leaflet-tooltip-top:before{content:'';position:absolute;pointer-events:none;border:5px solid transparent;background:0 0}.leaflet-tooltip-bottom{margin-top:5px}.leaflet-tooltip-top{margin-top:-5px}.leaflet-tooltip-bottom:before,.leaflet-tooltip-top:before{left:50%;margin-left:-5px}.leaflet-tooltip-top:before{bottom:0;margin-bottom:-10px;border-top-color:#fff}.leaflet-tooltip-bottom:before{top:0;margin-top:-15px;margin-left:-5px;border-bottom-color:#fff}.leaflet-tooltip-left{margin-left:-5px}.leaflet-tooltip-right{margin-left:5px}.leaflet-tooltip-left:before,.leaflet-tooltip-right:before{top:50%;margin-top:-5px}.leaflet-tooltip-left:before{right:0;margin-right:-10px;border-left-color:#fff}.leaflet-tooltip-right:before{left:0;margin-left:-10px;border-right-color:#fff}.leaflet-bar a,.leaflet-control-layers-toggle,.leaflet-popup-close-button,.map-tooltip.closable .close,.mapbox-button-icon:before,.mapbox-icon{content:'';display:inline-block;width:26px;height:26px;vertical-align:middle;background-repeat:no-repeat}.leaflet-bar a{display:block}.leaflet-container.dark .map-tooltip .close,.leaflet-control-attribution:after,.leaflet-control-layers-toggle,.leaflet-control-zoom-in,.leaflet-control-zoom-out,.leaflet-popup-close-button,.map-tooltip .close,.mapbox-icon{opacity:.75;background-image:url(images/icons-000000@2x.png);background-image:linear-gradient(transparent,transparent),url(images/icons.svg);background-repeat:no-repeat;background-size:26px 260px}.leaflet-container.dark .leaflet-control-attribution:after,.leaflet-container.dark .leaflet-control-layers-toggle,.leaflet-container.dark .leaflet-control-zoom-in,.leaflet-container.dark .leaflet-control-zoom-out,.leaflet-container.dark .mapbox-icon,.mapbox-button-icon:before{opacity:1;background-image:url(images/icons-ffffff@2x.png);background-image:linear-gradient(transparent,transparent),url(images/icons-ffffff.svg);background-size:26px 260px}.leaflet-bar .leaflet-control-zoom-in{background-position:0 0}.leaflet-bar .leaflet-control-zoom-out{background-position:0 -26px}.leaflet-popup-close-button,.map-tooltip.closable .close{background-position:-3px -55px;width:20px;height:20px;border-radius:0 3px 0 0}.mapbox-icon-info{background-position:0 -78px}.leaflet-control-layers-toggle{background-position:0 -104px}.mapbox-icon.mapbox-icon-share,.mapbox-icon.mapbox-icon-share:before{background-position:0 -130px}.mapbox-icon.mapbox-icon-geocoder,.mapbox-icon.mapbox-icon-geocoder:before{background-position:0 -156px}.mapbox-icon-facebook,.mapbox-icon-facebook:before{background-position:0 -182px}.mapbox-icon-twitter,.mapbox-icon-twitter:before{background-position:0 -208px}.mapbox-icon-pinterest,.mapbox-icon-pinterest:before{background-position:0 -234px}.leaflet-popup-content-wrapper,.map-legends,.map-tooltip{background:#fff;border-radius:3px;box-shadow:0 1px 2px rgba(0,0,0,.1)}.map-legends,.map-tooltip{max-width:300px}.map-legends .map-legend{padding:10px}.map-tooltip{z-index:999999;padding:10px;min-width:180px;max-height:400px;overflow:auto;opacity:1;-webkit-transition:opacity 150ms;-moz-transition:opacity 150ms;-o-transition:opacity 150ms;transition:opacity 150ms}.map-tooltip .close{text-indent:-999em;overflow:hidden;display:none}.map-tooltip.closable .close{position:absolute;top:0;right:0;border-radius:3px}.map-tooltip.closable .close:active{background-color:#f8f8f8}.leaflet-control-interaction{position:absolute;top:10px;right:10px;width:300px}.leaflet-popup-content .marker-title{font-weight:700}.leaflet-control .mapbox-button{background-color:#fff;border:1px solid #ddd;border-color:rgba(0,0,0,.1);padding:5px 10px;border-radius:3px}.mapbox-modal>div{position:absolute;top:0;left:0;width:100%;height:100%;z-index:-1;overflow-y:auto}.mapbox-modal.active>div{z-index:99999;transition:all .2s,z-index 0 0}.mapbox-modal .mapbox-modal-mask{background:rgba(0,0,0,.5);opacity:0}.mapbox-modal.active .mapbox-modal-mask{opacity:1}.mapbox-modal .mapbox-modal-content{-webkit-transform:translateY(-100%);-moz-transform:translateY(-100%);-ms-transform:translateY(-100%);transform:translateY(-100%)}.mapbox-modal.active .mapbox-modal-content{-webkit-transform:translateY(0);-moz-transform:translateY(0);-ms-transform:translateY(0);transform:translateY(0)}.mapbox-modal-body{position:relative;background:#fff;padding:20px;z-index:1000;width:50%;margin:20px 0 20px 25%}.mapbox-share-buttons{margin:0 0 20px}.mapbox-share-buttons a{width:33.3333%;border-left:1px solid #fff;text-align:center;border-radius:0}.mapbox-share-buttons a:last-child{border-radius:0 3px 3px 0}.mapbox-share-buttons a:first-child{border:none;border-radius:3px 0 0 3px}.mapbox-modal input{width:100%;height:40px;padding:10px;border:1px solid #ddd;border-color:rgba(0,0,0,.1);color:rgba(0,0,0,.5)}.mapbox-modal label{display:block;margin-top:5px}.leaflet-control-mapbox-geocoder{position:relative}.leaflet-control-mapbox-geocoder.searching{opacity:.75}.leaflet-control-mapbox-geocoder .leaflet-control-mapbox-geocoder-wrap{background:#fff;position:absolute;border:1px solid #999;border-color:rgba(0,0,0,.4);overflow:hidden;left:26px;height:28px;width:0;top:-1px;border-radius:0 3px 3px 0;opacity:0;-webkit-transition:opacity .1s;-moz-transition:opacity .1s;-o-transition:opacity .1s;transition:opacity .1s}.leaflet-control-mapbox-geocoder.active .leaflet-control-mapbox-geocoder-wrap{width:180px;opacity:1}.leaflet-bar .leaflet-control-mapbox-geocoder-toggle,.leaflet-bar .leaflet-control-mapbox-geocoder-toggle:hover{border-bottom:none}.leaflet-control-mapbox-geocoder-toggle{border-radius:3px}.leaflet-control-mapbox-geocoder.active,.leaflet-control-mapbox-geocoder.active .leaflet-control-mapbox-geocoder-toggle{border-top-right-radius:0;border-bottom-right-radius:0}.leaflet-control-mapbox-geocoder .leaflet-control-mapbox-geocoder-form input{background:0 0;border:0;width:180px;padding:0 0 0 10px;height:26px;outline:0}.leaflet-control-mapbox-geocoder-results{width:180px;position:absolute;left:26px;top:25px;border-radius:0 0 3px 3px}.leaflet-control-mapbox-geocoder.active .leaflet-control-mapbox-geocoder-results{background:#fff;border:1px solid #999;border-color:rgba(0,0,0,.4)}.leaflet-control-mapbox-geocoder-results a,.leaflet-control-mapbox-geocoder-results span{padding:0 10px;text-overflow:ellipsis;white-space:nowrap;display:block;width:100%;font-size:12px;line-height:26px;text-align:left;overflow:hidden}.leaflet-container.dark .leaflet-control .leaflet-control-mapbox-geocoder-results a:hover,.leaflet-control-mapbox-geocoder-results a:hover{background:#f8f8f8;opacity:1}.leaflet-right .leaflet-control-mapbox-geocoder-results,.leaflet-right .leaflet-control-mapbox-geocoder-wrap{left:auto;right:26px}.leaflet-right .leaflet-control-mapbox-geocoder-wrap{border-radius:3px 0 0 3px}.leaflet-right .leaflet-control-mapbox-geocoder.active,.leaflet-right .leaflet-control-mapbox-geocoder.active .leaflet-control-mapbox-geocoder-toggle{border-radius:0 3px 3px 0}.leaflet-bottom .leaflet-control-mapbox-geocoder-results{top:auto;bottom:25px;border-radius:3px 3px 0 0}.mapbox-logo.mapbox-logo-true{margin:0 0 5px 5px}.mapbox-logo-true:before{content:'';display:inline-block;width:85px;height:21px;vertical-align:middle}.mapbox-logo.mapbox-logo-true{background-repeat:no-repeat;background-size:85px 21px;background-image:url(\"data:image/svg+xml;charset=utf-8,%3C?xml%20version='1.0'%20encoding='utf-8'?%3E%3Csvg%20version='1.1'%20id='Layer_1'%20xmlns='http://www.w3.org/2000/svg'%20xmlns:xlink='http://www.w3.org/1999/xlink'%20x='0px'%20y='0px'%20viewBox='0%200%2084.49%2021'%20style='enable-background:new%200%200%2084.49%2021;'%20xml:space='preserve'%3E%3Cg%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M83.25,14.26c0,0.12-0.09,0.21-0.21,0.21h-1.61c-0.13,0-0.24-0.06-0.3-0.17l-1.44-2.39l-1.44,2.39%20c-0.06,0.11-0.18,0.17-0.3,0.17h-1.61c-0.04,0-0.08-0.01-0.12-0.03c-0.09-0.06-0.13-0.19-0.06-0.28l0,0l2.43-3.68L76.2,6.84%20c-0.02-0.03-0.03-0.07-0.03-0.12c0-0.12,0.09-0.21,0.21-0.21h1.61c0.13,0,0.24,0.06,0.3,0.17l1.41,2.36l1.4-2.35%20c0.06-0.11,0.18-0.17,0.3-0.17H83c0.04,0,0.08,0.01,0.12,0.03c0.09,0.06,0.13,0.19,0.06,0.28l0,0l-2.37,3.63l2.43,3.67%20C83.24,14.18,83.25,14.22,83.25,14.26z'/%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M66.24,9.59c-0.39-1.88-1.96-3.28-3.84-3.28c-1.03,0-2.03,0.42-2.73,1.18V3.51c0-0.13-0.1-0.23-0.23-0.23h-1.4%20c-0.13,0-0.23,0.11-0.23,0.23v10.72c0,0.13,0.1,0.23,0.23,0.23h1.4c0.13,0,0.23-0.11,0.23-0.23V13.5c0.71,0.75,1.7,1.18,2.73,1.18%20c1.88,0,3.45-1.41,3.84-3.29C66.37,10.79,66.37,10.18,66.24,9.59L66.24,9.59z%20M62.08,13c-1.32,0-2.39-1.11-2.41-2.48v-0.06%20c0.02-1.38,1.09-2.48,2.41-2.48s2.42,1.12,2.42,2.51S63.41,13,62.08,13z'/%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M71.67,6.32c-1.98-0.01-3.72,1.35-4.16,3.29c-0.13,0.59-0.13,1.19,0,1.77c0.44,1.94,2.17,3.32,4.17,3.3%20c2.35,0,4.26-1.87,4.26-4.19S74.04,6.32,71.67,6.32z%20M71.65,13.01c-1.33,0-2.42-1.12-2.42-2.51s1.08-2.52,2.42-2.52%20c1.33,0,2.42,1.12,2.42,2.51S72.99,13,71.65,13.01L71.65,13.01z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M62.08,7.98c-1.32,0-2.39,1.11-2.41,2.48v0.06C59.68,11.9,60.75,13,62.08,13s2.42-1.12,2.42-2.51%20S63.41,7.98,62.08,7.98z%20M62.08,11.76c-0.63,0-1.14-0.56-1.17-1.25v-0.04c0.01-0.69,0.54-1.25,1.17-1.25%20c0.63,0,1.17,0.57,1.17,1.27C63.24,11.2,62.73,11.76,62.08,11.76z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M71.65,7.98c-1.33,0-2.42,1.12-2.42,2.51S70.32,13,71.65,13s2.42-1.12,2.42-2.51S72.99,7.98,71.65,7.98z%20M71.65,11.76c-0.64,0-1.17-0.57-1.17-1.27c0-0.7,0.53-1.26,1.17-1.26s1.17,0.57,1.17,1.27C72.82,11.21,72.29,11.76,71.65,11.76z'/%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M45.74,6.53h-1.4c-0.13,0-0.23,0.11-0.23,0.23v0.73c-0.71-0.75-1.7-1.18-2.73-1.18%20c-2.17,0-3.94,1.87-3.94,4.19s1.77,4.19,3.94,4.19c1.04,0,2.03-0.43,2.73-1.19v0.73c0,0.13,0.1,0.23,0.23,0.23h1.4%20c0.13,0,0.23-0.11,0.23-0.23V6.74c0-0.12-0.09-0.22-0.22-0.22C45.75,6.53,45.75,6.53,45.74,6.53z%20M44.12,10.53%20C44.11,11.9,43.03,13,41.71,13s-2.42-1.12-2.42-2.51s1.08-2.52,2.4-2.52c1.33,0,2.39,1.11,2.41,2.48L44.12,10.53z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M41.71,7.98c-1.33,0-2.42,1.12-2.42,2.51S40.37,13,41.71,13s2.39-1.11,2.41-2.48v-0.06%20C44.1,9.09,43.03,7.98,41.71,7.98z%20M40.55,10.49c0-0.7,0.52-1.27,1.17-1.27c0.64,0,1.14,0.56,1.17,1.25v0.04%20c-0.01,0.68-0.53,1.24-1.17,1.24C41.08,11.75,40.55,11.19,40.55,10.49z'/%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M52.41,6.32c-1.03,0-2.03,0.42-2.73,1.18V6.75c0-0.13-0.1-0.23-0.23-0.23h-1.4c-0.13,0-0.23,0.11-0.23,0.23%20v10.72c0,0.13,0.1,0.23,0.23,0.23h1.4c0.13,0,0.23-0.1,0.23-0.23V13.5c0.71,0.75,1.7,1.18,2.74,1.18c2.17,0,3.94-1.87,3.94-4.19%20S54.58,6.32,52.41,6.32z%20M52.08,13.01c-1.32,0-2.39-1.11-2.42-2.48v-0.07c0.02-1.38,1.09-2.49,2.4-2.49c1.32,0,2.41,1.12,2.41,2.51%20S53.4,13,52.08,13.01L52.08,13.01z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M52.08,7.98c-1.32,0-2.39,1.11-2.42,2.48v0.06c0.03,1.38,1.1,2.48,2.42,2.48s2.41-1.12,2.41-2.51%20S53.4,7.98,52.08,7.98z%20M52.08,11.76c-0.63,0-1.14-0.56-1.17-1.25v-0.04c0.01-0.69,0.54-1.25,1.17-1.25c0.63,0,1.17,0.58,1.17,1.27%20S52.72,11.76,52.08,11.76z'/%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M36.08,14.24c0,0.13-0.1,0.23-0.23,0.23h-1.41c-0.13,0-0.23-0.11-0.23-0.23V9.68c0-0.98-0.74-1.71-1.62-1.71%20c-0.8,0-1.46,0.7-1.59,1.62l0.01,4.66c0,0.13-0.11,0.23-0.23,0.23h-1.41c-0.13,0-0.23-0.11-0.23-0.23V9.68%20c0-0.98-0.74-1.71-1.62-1.71c-0.85,0-1.54,0.79-1.6,1.8v4.48c0,0.13-0.1,0.23-0.23,0.23h-1.4c-0.13,0-0.23-0.11-0.23-0.23V6.74%20c0.01-0.13,0.1-0.22,0.23-0.22h1.4c0.13,0,0.22,0.11,0.23,0.22V7.4c0.5-0.68,1.3-1.09,2.16-1.1h0.03c1.09,0,2.09,0.6,2.6,1.55%20c0.45-0.95,1.4-1.55,2.44-1.56c1.62,0,2.93,1.25,2.9,2.78L36.08,14.24z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M84.34,13.59l-0.07-0.13l-1.96-2.99l1.94-2.95c0.44-0.67,0.26-1.56-0.41-2.02c-0.02,0-0.03,0-0.04-0.01%20c-0.23-0.15-0.5-0.22-0.78-0.22h-1.61c-0.56,0-1.08,0.29-1.37,0.78L79.72,6.6l-0.34-0.56C79.09,5.56,78.57,5.27,78,5.27h-1.6%20c-0.6,0-1.13,0.37-1.35,0.92c-2.19-1.66-5.28-1.47-7.26,0.45c-0.35,0.34-0.65,0.72-0.89,1.14c-0.9-1.62-2.58-2.72-4.5-2.72%20c-0.5,0-1.01,0.07-1.48,0.23V3.51c0-0.82-0.66-1.48-1.47-1.48h-1.4c-0.81,0-1.47,0.66-1.47,1.47v3.75%20c-0.95-1.36-2.5-2.18-4.17-2.19c-0.74,0-1.46,0.16-2.12,0.47c-0.24-0.17-0.54-0.26-0.84-0.26h-1.4c-0.45,0-0.87,0.21-1.15,0.56%20c-0.02-0.03-0.04-0.05-0.07-0.08c-0.28-0.3-0.68-0.47-1.09-0.47h-1.39c-0.3,0-0.6,0.09-0.84,0.26c-0.67-0.3-1.39-0.46-2.12-0.46%20c-1.83,0-3.43,1-4.37,2.5c-0.2-0.46-0.48-0.89-0.83-1.25c-0.8-0.81-1.89-1.25-3.02-1.25h-0.01c-0.89,0.01-1.75,0.33-2.46,0.88%20c-0.74-0.57-1.64-0.88-2.57-0.88H28.1c-0.29,0-0.58,0.03-0.86,0.11c-0.28,0.06-0.56,0.16-0.82,0.28c-0.21-0.12-0.45-0.18-0.7-0.18%20h-1.4c-0.82,0-1.47,0.66-1.47,1.47v7.5c0,0.82,0.66,1.47,1.47,1.47h1.4c0.82,0,1.48-0.66,1.48-1.48l0,0V9.79%20c0.03-0.36,0.23-0.59,0.36-0.59c0.18,0,0.38,0.18,0.38,0.47v4.57c0,0.82,0.66,1.47,1.47,1.47h1.41c0.82,0,1.47-0.66,1.47-1.47%20l-0.01-4.57c0.06-0.32,0.25-0.47,0.35-0.47c0.18,0,0.38,0.18,0.38,0.47v4.57c0,0.82,0.66,1.47,1.47,1.47h1.41%20c0.82,0,1.47-0.66,1.47-1.47v-0.38c0.96,1.29,2.46,2.06,4.06,2.06c0.74,0,1.46-0.16,2.12-0.47c0.24,0.17,0.54,0.26,0.84,0.26h1.39%20c0.3,0,0.6-0.09,0.84-0.26v2.01c0,0.82,0.66,1.47,1.47,1.47h1.4c0.82,0,1.47-0.66,1.47-1.47v-1.77c0.48,0.15,0.99,0.23,1.49,0.22%20c1.7,0,3.22-0.87,4.17-2.2v0.52c0,0.82,0.66,1.47,1.47,1.47h1.4c0.3,0,0.6-0.09,0.84-0.26c0.66,0.31,1.39,0.47,2.12,0.47%20c1.92,0,3.6-1.1,4.49-2.73c1.54,2.65,4.95,3.53,7.58,1.98c0.18-0.11,0.36-0.22,0.53-0.36c0.22,0.55,0.76,0.91,1.35,0.9H78%20c0.56,0,1.08-0.29,1.37-0.78l0.37-0.61l0.37,0.61c0.29,0.48,0.81,0.78,1.38,0.78h1.6c0.81,0,1.46-0.66,1.45-1.46%20C84.49,14.02,84.44,13.8,84.34,13.59L84.34,13.59z%20M35.86,14.47h-1.41c-0.13,0-0.23-0.11-0.23-0.23V9.68%20c0-0.98-0.74-1.71-1.62-1.71c-0.8,0-1.46,0.7-1.59,1.62l0.01,4.66c0,0.13-0.1,0.23-0.23,0.23h-1.41c-0.13,0-0.23-0.11-0.23-0.23%20V9.68c0-0.98-0.74-1.71-1.62-1.71c-0.85,0-1.54,0.79-1.6,1.8v4.48c0,0.13-0.1,0.23-0.23,0.23h-1.4c-0.13,0-0.23-0.11-0.23-0.23%20V6.74c0.01-0.13,0.11-0.22,0.23-0.22h1.4c0.13,0,0.22,0.11,0.23,0.22V7.4c0.5-0.68,1.3-1.09,2.16-1.1h0.03%20c1.09,0,2.09,0.6,2.6,1.55c0.45-0.95,1.4-1.55,2.44-1.56c1.62,0,2.93,1.25,2.9,2.78l0.01,5.16C36.09,14.36,35.98,14.46,35.86,14.47%20L35.86,14.47z%20M45.97,14.24c0,0.13-0.1,0.23-0.23,0.23h-1.4c-0.13,0-0.23-0.11-0.23-0.23V13.5c-0.7,0.76-1.69,1.18-2.72,1.18%20c-2.17,0-3.94-1.87-3.94-4.19s1.77-4.19,3.94-4.19c1.03,0,2.02,0.43,2.73,1.18V6.74c0-0.13,0.1-0.23,0.23-0.23h1.4%20c0.12-0.01,0.22,0.08,0.23,0.21c0,0.01,0,0.01,0,0.02v7.51h-0.01V14.24z%20M52.41,14.67c-1.03,0-2.02-0.43-2.73-1.18v3.97%20c0,0.13-0.1,0.23-0.23,0.23h-1.4c-0.13,0-0.23-0.1-0.23-0.23V6.75c0-0.13,0.1-0.22,0.23-0.22h1.4c0.13,0,0.23,0.11,0.23,0.23v0.73%20c0.71-0.76,1.7-1.18,2.73-1.18c2.17,0,3.94,1.86,3.94,4.18S54.58,14.67,52.41,14.67z%20M66.24,11.39c-0.39,1.87-1.96,3.29-3.84,3.29%20c-1.03,0-2.02-0.43-2.73-1.18v0.73c0,0.13-0.1,0.23-0.23,0.23h-1.4c-0.13,0-0.23-0.11-0.23-0.23V3.51c0-0.13,0.1-0.23,0.23-0.23%20h1.4c0.13,0,0.23,0.11,0.23,0.23v3.97c0.71-0.75,1.7-1.18,2.73-1.17c1.88,0,3.45,1.4,3.84,3.28C66.37,10.19,66.37,10.8,66.24,11.39%20L66.24,11.39L66.24,11.39z%20M71.67,14.68c-2,0.01-3.73-1.35-4.17-3.3c-0.13-0.59-0.13-1.19,0-1.77c0.44-1.94,2.17-3.31,4.17-3.3%20c2.36,0,4.26,1.87,4.26,4.19S74.03,14.68,71.67,14.68L71.67,14.68z%20M83.04,14.47h-1.61c-0.13,0-0.24-0.06-0.3-0.17l-1.44-2.39%20l-1.44,2.39c-0.06,0.11-0.18,0.17-0.3,0.17h-1.61c-0.04,0-0.08-0.01-0.12-0.03c-0.09-0.06-0.13-0.19-0.06-0.28l0,0l2.43-3.68%20L76.2,6.84c-0.02-0.03-0.03-0.07-0.03-0.12c0-0.12,0.09-0.21,0.21-0.21h1.61c0.13,0,0.24,0.06,0.3,0.17l1.41,2.36l1.41-2.36%20c0.06-0.11,0.18-0.17,0.3-0.17h1.61c0.04,0,0.08,0.01,0.12,0.03c0.09,0.06,0.13,0.19,0.06,0.28l0,0l-2.38,3.64l2.43,3.67%20c0.02,0.03,0.03,0.07,0.03,0.12C83.25,14.38,83.16,14.47,83.04,14.47L83.04,14.47L83.04,14.47z'/%3E%20%3Cpath%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20d='M10.5,1.24c-5.11,0-9.25,4.15-9.25,9.25s4.15,9.25,9.25,9.25s9.25-4.15,9.25-9.25%20C19.75,5.38,15.61,1.24,10.5,1.24z%20M14.89,12.77c-1.93,1.93-4.78,2.31-6.7,2.31c-0.7,0-1.41-0.05-2.1-0.16c0,0-1.02-5.64,2.14-8.81%20c0.83-0.83,1.95-1.28,3.13-1.28c1.27,0,2.49,0.51,3.39,1.42C16.59,8.09,16.64,11,14.89,12.77z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M10.5-0.01C4.7-0.01,0,4.7,0,10.49s4.7,10.5,10.5,10.5S21,16.29,21,10.49C20.99,4.7,16.3-0.01,10.5-0.01z%20M10.5,19.74c-5.11,0-9.25-4.15-9.25-9.25s4.14-9.26,9.25-9.26s9.25,4.15,9.25,9.25C19.75,15.61,15.61,19.74,10.5,19.74z'/%3E%20%3Cpath%20class='st1'%20style='opacity:0.35;%20enable-background:new;'%20d='M14.74,6.25C12.9,4.41,9.98,4.35,8.23,6.1c-3.16,3.17-2.14,8.81-2.14,8.81s5.64,1.02,8.81-2.14%20C16.64,11,16.59,8.09,14.74,6.25z%20M12.47,10.34l-0.91,1.87l-0.9-1.87L8.8,9.43l1.86-0.9l0.9-1.87l0.91,1.87l1.86,0.9L12.47,10.34z'/%3E%20%3Cpolygon%20class='st0'%20style='opacity:0.9;%20fill:%20%23FFFFFF;%20enable-background:%20new;'%20points='14.33,9.43%2012.47,10.34%2011.56,12.21%2010.66,10.34%208.8,9.43%2010.66,8.53%2011.56,6.66%2012.47,8.53%20'/%3E%3C/g%3E%3C/svg%3E\")}.leaflet-container.dark .leaflet-bar{background-color:#404040;border-color:#202020;border-color:rgba(0,0,0,.75)}.leaflet-container.dark .leaflet-bar a{color:#404040;border-color:rgba(0,0,0,.5)}.leaflet-container.dark .leaflet-bar a:active,.leaflet-container.dark .leaflet-bar a:hover{background-color:#505050}.leaflet-container.dark .leaflet-control-attribution,.leaflet-container.dark .leaflet-control-attribution:after,.leaflet-container.dark .map-info-container,.leaflet-container.dark .mapbox-info-toggle{background-color:rgba(0,0,0,.5);color:#f8f8f8}.leaflet-container.dark .leaflet-control-attribution a,.leaflet-container.dark .leaflet-control-attribution a:hover,.leaflet-container.dark .map-info-container a,.leaflet-container.dark .map-info-container a:hover{color:#fff}.leaflet-container.dark .leaflet-control-attribution:hover:after{background-color:#000}.leaflet-container.dark .leaflet-control-layers-list span{color:#f8f8f8}.leaflet-container.dark .leaflet-control-layers-separator{border-top-color:rgba(255,255,255,.1)}.leaflet-container.dark .leaflet-bar a.leaflet-disabled,.leaflet-container.dark .leaflet-control .mapbox-button.disabled{background-color:#252525;color:#404040}.leaflet-container.dark .leaflet-control-mapbox-geocoder>div{border-color:#202020;border-color:rgba(0,0,0,.75)}.leaflet-container.dark .leaflet-control .leaflet-control-mapbox-geocoder-results a{border-color:#ddd #202020;border-color:rgba(0,0,0,.1) rgba(0,0,0,.75)}.leaflet-container.dark .leaflet-control .leaflet-control-mapbox-geocoder-results span{border-color:#202020;border-color:rgba(0,0,0,.75)}@media only screen and (max-width:800px){.mapbox-modal-body{width:83.3333%;margin-left:8.3333%}}@media only screen and (max-width:640px){.mapbox-modal-body{width:100%;height:100%;margin:0}}@media print{.mapbox-improve-map{display:none}}.leaflet-vml-shape{width:1px;height:1px}.lvml{behavior:url(#default#VML);display:inline-block;position:absolute}.leaflet-container img.leaflet-tile{max-width:none!important}.leaflet-container img.leaflet-marker-icon{max-width:none}.leaflet-container img.leaflet-image-layer{max-width:15000px!important}.leaflet-overlay-pane svg{-moz-user-select:none}.leaflet-oldie .mapbox-modal .mapbox-modal-content{display:none}.leaflet-oldie .mapbox-modal.active .mapbox-modal-content{display:block}.leaflet-oldie .leaflet-container.dark .map-tooltip .close,.leaflet-oldie .leaflet-control-layers-toggle,.leaflet-oldie .leaflet-control-zoom-in,.leaflet-oldie .leaflet-control-zoom-out,.leaflet-oldie .leaflet-popup-close-button,.leaflet-oldie .map-tooltip .close,.leaflet-oldie .mapbox-icon{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAEECAYAAAA24SSRAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAXnSURBVHic7ZxfiFVFGMB/33pRUQsKto002DY3McJ6yBYkESQxpYTypaB66KEXYRWLYOlhr9RTRGWRUkk9RyEU+Y9ClECJVTKlPybWBilqkYuWrqBOD/NdPV7PmTPn3NPtat/AcO6ZP9/vfN/Mmfl2Zs6Kc452hK62UAxkIANdEURkVERGC9crOjKIiANwzkmRep1lOjWXa2ijaU7jaGWgKsL110a1EnV+LQMqbLqyobO6t4EMZCADGchABrqmQUlPNSWOVgaqIpi7ZSADGchABjKQga49kIjURaQem14apGE4KVR/D0fXds5FRaAOOL1e+h1dP7ZgE6wQxDnXvs7QWaZLE1wUVmRNdY1zrp6wRF0kfqHYnHwDGchABjJQIETNRyIyFVgBzAPmavIIsAt4xzn3d66QiNl1PnCYy05JczwMzG9pKlfIhQCkES/kwUKQqRma9GpM02xqGXdrBdCXZm2NzaFP66SGUGeYl5E+WqJO0HRHSG+PXtJN54AjVbhbjQcbBSjiakH4hR0p+hChOiHQrhKg7Drt6t7//Qtb9RAU5XtXMaiak28gAxnIQO0Gicg0EXlMRDaIyFGNGzRtWhQpMA/1A6uAL4BzZM9H57TMKqC/8HyUPFhZJLiMI4sh0/UDK4FtwHig3LiWWal1UkPsDDsFWAgsBZZo8hZgM7DdOXcmV0igjQ4Ba4HFwORAuclaZi1wqNU2OgNsVw22aNoS1XAhMCXx4OkubOBJZwKDwFbgLNm97qyWGQRmtuoFWRsV0ujabCPzVA1kIAMZqBNAIjIgImPNRxUzK+SsmtRJn4Pqmj8AjCXzsmTlaTSck/8zcDRX/QiNMp8S6Ab2a5nvG5plyioDaoLs1/sBYKwyUBokkTdQJeiVZgi6UR+UVQI0QWHdoXKFvKDYz7RiynXctk7LPlmeRmsKyAqWNQfSQAYykIGuS5CI1ERkSET2ishpvQ6JSLE93ByfoQbsRHeNgfe4vOO8E6iF6hdxToZU6OqGUIWv1vShqkB7VYNaU3pN0/fGgvLa6C5gk3PufJO5zwObgDuraqM8jbZWpdEnwG3AYKOX6XVQ07+sSqNQr3P4QxS9LXeGBGxIzTiGXwR8QSHRsCj7ZjxAbxFYaVAKbMe/BkrAduRpZJ6qgQxkoP8DKDRY1sk/s5W6YFhoUG3nFnZeOIJfxLgXWB7zBFmmyzPT44my9zXSC098OZCTwCQttzOZVzVoX1a5LHmdtYyWDM29yjknItKF3xSelFWvKo1mhCClQLo1sC95T8T/ebr+xrqOABVZT82tY56qgQxkIAN1CkhEulsGiUi3iCzKyJsjIpuBYyLyo4isFpHXReTuTFLAr1sOnAeeT8nbzNW+3rfAM2UcyAcSQj4FngR68Ot0F1NA24CuMqBu4PMUgYdS0hzwYqlFJ+AeNV3s30aLSoEUtjEScoHE3nkZ0Ay1fR7o3ZCcGNAEYHcO5A/g5pZACpsMPEf6UexTwCN5MvI6w2zgaeBt4HQK5BsC57ubY+jPll/wHzn1Ayc07QD+u6MR4GPn3LlA/SuCOZAGMpCBDFRhiF50EpFl+PP49wOzgIPAHmCLc+6zXAERE18P+b7DRqAnJCfvfF0P/mTgLZr0l97vB27CL3HO0rwTwBzn3PHCGiU0uQisA6bhzT0T/T4ZeAr4s6FZmal8WcI0LwETgdfwHzY1XKz3teyjibLLioLWa8UDeG/oZbxD+QHwdULwg1r+K71fXxQ0ohXfAgS/Mvyh5i1MgNZp2qt6P5ImL/QezdbrSeAG4EbVJJkH8LteJ+p1FikhBPpNr3Odc6fUNHdo2oJEucbX8Y2zDQeLgr7T62IReRb4AX9mGGC6Xo8Bu0VkOvCQpu1JlRZoo6Vc/WL2ad4C4A28CWvAR5TtdU0dwqH/ewHvHi8HbgUexh+euDRCFH6PVOh0/FKzw3um4M8zpA1DxwkMQzFjXR9+d/9N1WI8BZI71kU56Aq8HXgC+Ak/5o3gX+rUNmmO5nsbqP2gfwCyvJzPNoKXiAAAAABJRU5ErkJggg==)}.leaflet-oldie .leaflet-container.dark .leaflet-control-layers-toggle,.leaflet-oldie .leaflet-container.dark .leaflet-control-zoom-in,.leaflet-oldie .leaflet-container.dark .leaflet-control-zoom-out,.leaflet-oldie .leaflet-container.dark .mapbox-icon,.leaflet-oldie .mapbox-button-icon:before{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAEECAYAAAA24SSRAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAXYSURBVHic7ZxfiFVFHMc/a4uKWtDDtqJGZprYgwX5ByTdkkLbSgghCiKih14EBYtg6aEr9RRREKRUUs9hGEVtChKaYMkq2VqWmnUX2tKiNDNZY/Xbw/wue7x7zsw559626zY/GM6df7/P+c3MPfO7M3NumyTGQiaMCSWCIiiC6qVqoZC0lXgy1Cq0FanUck1XxVmSNL8WrzYT1LCMvz5qL1FnoAyoTNOVkpYb3hEUQREUQREUQRF0RYOqjHim9aHaTFDDEt2tCIqgCIqgCIqgCLoiQRULedNLgwCeq1NasbR8IilvqMhJpe5zrvpFQElYIYiksRsMLdd0aYoLwYqsqW5i9KjLLdHJj6AIiqAIiiCP5J2PpgLrgGXAYkvrA/YBrwF/BTXkmB2XSzqhbDlhZRqaypdLuuiB1ORiCOaDTM2wZLaFNMumZunzDYZ1wJy01ubyPfOazLE6qeIbDMsy0qsl6ngtWpyRfqOFInVKbWFXS9TxWtRXQl9mHR9oXwlQdp2xGt4t8YVt6iMor+/d8EM1OvkRFEERFEH/AWga8CCwFfjJwlZLm5ZHge/pPQ+4z8IKYGJGub+BT4GPLBwvCio7f6QeWfQ13TxgA7ATGPKUG7IyG6xOOj3nxDcFWAl0A/da2sdAL/AJcD6kwAc6bop6gT1kWzUZ6LKb6CbDqrx9dB535704S8BZ1o2zdEpSZ1HQ3MRddtmdp8kQzuKa9d8VBSUl9lEh0Pjro6ZKy00TERRBERRBLQZaCpxh9FHFUqBKiiJZ+n5gFfBHnrsKgUKb7t/j/PCwBNZwapKW1yGp3/KPSDrjKVsalIT0W3ypwZoGSoPU8pY2E/RCCqSiwJ55GdBVBusIlCu0Xpf3Na1guZbb1mnYJwtZtKmALm/Z6EBGUARFUASNV1A70AMcBP60aw9F93ADPkO7pD3mDwxKesOusvT2QP3czkmPKd2YUNpucVl+LlBo4jsITAduAIbrmnMAOAncnqflQn10M26JebgufdjSb8oDyQM6hlv3ru/4dkv/vFmgd4EZwPoErN3iM4BdeUGNjDpJqsrtmzc86mqwHkkH5X4t7JD0tEFyw3INzYwwuwisEVA9bPe/CarBdocsip5qBEVQBP3fQRWyX4jOCpUsZS2xhR2SQdwixq3A2lDhMkcTa7Ie2G6fwzfsmax8clrSJCu3py4vVV/ZphsALtjnFXkqtNwyWlLqR1Ub7obPA5OyKjXLolk+SFmQgEN18eD/PLXEI2j8gYqspwbrRE81giIogiKohUAdzQB1APdk5C3Ends6CXwLbAReBm7J1OZxINdKGpb0VEpeb4pT+aWkx8os0SxJKHlf0iOSOiXNkHQpBbRT0oQyoA5JH6YoPJ6SJknPeHR5+6gTWJ2SPjej/BceXV7QV8AHvsoJucTlvt5o8ZkraZa1fUheD+gJfo9+Bq4JlPkNt4Xgl9CdSJos6UlJF1IsOSvp/hw6vL8mFgCLgCXA44w+730IeIiM89314gP9ACzHHXD9xdIO49476gO2MfJjLCjRgYygCIqgCGqiFFl0WoM7j78ImA8cBQ7gzuaHp/wck1anpO2BqXy7lSu9I9YJ9APXWfycxfuBa4HbzDpwc9ZC4FQZi2qWXJK0WdI0ue3SuRp5P/lRSb8nLCvsQK5JNM2zkiZKeknSkKVdlPSmlX0gUXZNUdAWq3hY7tzj83K++FuS9icU32Hl91p8S1FQn1V8VVKb3Mrw25a3MgHabGkvWrwvTZ/ve7TArqeBq3H+3f66PIBf7VrzkuaTIj7Qj3ZdDJwF9jLy5wJdiXK1t+NrZxuOFgV9bddVwBPAN8ARS5tp15PAZxa/29IOpGrz9FG3Rsscy+uS9IqkBXLD/Z1GRl1yQEjuHANy7vFaSdMlrZa0K1Gm1PcISTMlDZiSbZa2I8VSSTolz2Mo9PQeBO7CvTE1iDtRc2dKuffwPX4CfVQfrpf0sKRjks5Zs27J6pP6EH3vCBp70D8db2VXFPfIagAAAABJRU5ErkJggg==)}.leaflet-oldie .mapbox-logo-true{background-image:none}";
addCSS(styleDatkccs);
addCSS(styleDatkSimpleccs);
addCSS(mapcss);
addCSS(datePickercss);
controlsType = [{
    id: "IdentityType",
    label: "نوع شخصیت",
    type: "radio",
    enable: !1,
    required: !1,
    events: [{event: "myformready", handler: onIdentityTypeInserted}],
    options: "identityTypeOptions",
    separator: "specifications",
    parent: "",
    serverValidation: !1
}, {
    id: "OrganType",
    label: "نوع سازمان",
    type: "radio",
    enable: !1,
    required: !1,
    options: "organTypeOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "CompanyType",
    label: "نوع شرکت",
    type: "radio",
    enable: !1,
    required: !1,
    options: "companyTypeOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "FirstName",
    label: "نام",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 15,
    parent: "",
    serverValidation: !1,
    separator: "specifications",
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "LastName",
    label: "نام خانوادگی",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 25,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "Gender",
    label: "جنسیت",
    type: "radio",
    enable: !1,
    required: !1,
    options: "genderOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "myformready", handler: onGenderTypeInserted}]
}, {
    id: "CompanyName",
    label: "نام شرکت/سازمان/موسسه/دانشگاه / وزارتخانه",
    type: "text",
    enable: !1,
    required: !1,
    "class": "w100",
    minLength: 3,
    maxLength: 25,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "BirthDate",
    label: "تاریخ تولد",
    type: "date",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 10,
    "class": "ltr",
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "RegNumber",
    label: "شماره شناسنامه",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 8,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "Marriage",
    label: "وضعیت تاهل",
    type: "radio",
    enable: !1,
    required: !1,
    "class": "w50",
    options: "marriageOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "myformready", handler: onMarriageTypeInserted}]
}, {
    id: "SpouseName",
    label: "نام و نام خانوادگی همسر",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 25,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "MilitaryService",
    label: "وضعیت سربازی",
    type: "radio",
    enable: !1,
    required: !1,
    options: "militaryServiceOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "Children",
    label: "تعداد فرزندان",
    type: "select",
    enable: !1,
    required: !1,
    options: "childrenOptions",
    events: [{event: "change", handler: childrenChanged}],
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "NationalID",
    label: "کد ملی",
    type: "text",
    enable: !1,
    required: !0,
    events: [{event: "validation", handler: tempNationalIdValidation}],
    "class": "ltr",
    minLength: 10,
    maxLength: 10,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "NationalIDCode",
    label: "کد شناسه ملی",
    type: "masktext",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: nationalIDCodeValidation}],
    "class": "ltr",
    minLength: 11,
    maxLength: 11,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "PassportNumber",
    label: "شماره پاسپورت",
    type: "masktext",
    enable: !1,
    required: !1,
    "class": "ltr",
    minLength: 3,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: passportNumberValidation}]
}, {
    id: "DrivingLicence",
    label: "شماره گواهینامه",
    type: "masktext",
    enable: !1,
    "class": "ltr",
    required: !1,
    minLength: 4,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "FatherName",
    label: "نام پدر",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 20,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "BirthPlace",
    label: "محل تولد",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 2,
    maxLength: 15,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "RegIssuePlace",
    label: "محل صدور شناسنامه",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 15,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "DirectorFirstName",
    label: "نام مدیرعامل /رئیس",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 15,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "DirectorLastName",
    label: "نام خانوادگی مدیر عامل / رئیس",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 25,
    parent: "",
    separator: "specifications",
    lengthStatus: !1,
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "AgentFirstName",
    label: "نام شخص مسوول",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 15,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "AgentLastName",
    label: "نام خانوادگی شخص مسوول",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 25,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "PhoneNumber",
    label: "تلفن ثابت",
    type: "phone",
    enable: !1,
    required: !1,
    minLength: 10,
    maxLength: "15 - No CountryCode",
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "CellNumber",
    label: "شماره همراه",
    description: "مثال: 09123456789",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: phoneValidation}],
    "class": "ltr",
    minLength: 10,
    maxLength: 12,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "AgentCellNumber",
    label: "شماره همراه شخص مسوول",
    description: "مثال: 09123456789",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: phoneValidation}],
    "class": "ltr",
    minLength: 10,
    maxLength: 12,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "Email",
    label: "ایمیل ",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: emailValidation}],
    "class": "ltr",
    minLength: 10,
    maxLength: 30,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "AgentEmail",
    label: "ایمیل شخص مسوول",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: emailValidation}],
    minLength: 10,
    maxLength: 30,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "PREmail",
    label: "ایمیل روابط عمومی ",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: emailValidation}],
    minLength: 10,
    maxLength: 30,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "AddressStatus",
    label: "وضعیت نشانی",
    type: "select",
    enable: !1,
    required: !1,
    options: "addressStatusOptions",
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "PostCode",
    label: "کد پستی",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: postCodeValidation}],
    "class": "ltr",
    minLength: 10,
    maxLength: 10,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "Province",
    label: "استان",
    type: "select",
    enable: !1,
    required: !1,
    options: "provinceOptions",
    events: [{event: "change", handler: provinceChanged}],
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "City",
    label: "شهرستان",
    type: "select",
    enable: !1,
    required: !1,
    options: [],
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "RegionType",
    label: "نوع بخش / محله/ شهرک",
    type: "select",
    enable: !1,
    required: !1,
    options: "regionTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "RegionName",
    label: "نام بخش / محله / شهرک",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: regionValidation}],
    minLength: 3,
    maxLength: 15,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "MainRoadType",
    label: "نوع معبر اصلی",
    type: "select",
    enable: !1,
    required: !1,
    options: "roadTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "MainRoadName",
    label: "نام معبر اصلی",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: roadValidation}],
    minLength: 3,
    maxLength: 20,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "PrimaryRoadType",
    label: "نوع معبر فرعی ۱",
    type: "select",
    enable: !1,
    required: !1,
    options: "roadTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "PrimaryRoadName",
    label: "نام معبر فرعی ۱",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: roadValidation}],
    minLength: 3,
    maxLength: 20,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "SecondaryRoadType",
    label: "نوع معبر فرعی ۲",
    type: "select",
    enable: !1,
    required: !1,
    options: "roadTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "SecondaryRoadName",
    label: "نام معبر فرعی ۲",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: roadValidation}],
    minLength: 3,
    maxLength: 20,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "HouseNumber",
    label: "شماره پلاک",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: houseNumberValidation}, {
        event: "change",
        target: prefex + "noHouseNumber",
        handler: onNoHouseNumberChange
    }],
    append: "noHouseNumber",
    minLength: 1,
    maxLength: 6,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0
}, {
    id: "Floor",
    label: "طبقه",
    type: "select",
    enable: !1,
    required: !1,
    options: "floorOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1
}, {
    id: "Unit",
    label: "واحد",
    description: "در صورت وجود حتما وارد کنید",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: unitValidation}],
    minLength: 1,
    maxLength: 10,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0
}, {
    id: "BuildingName",
    label: "نام ساختمان",
    description: "در صورت عدم وجود پلاک، تکمیل این قسمت الزامی‌ست",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation", handler: buildingNameValidation}],
    minLength: 2,
    maxLength: 25,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "DoorColor",
    label: "رنگ درب ساختمان",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "address",
    serverValidation: !0,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "Location",
    label: "مختصات ساختمان",
    description: "لطفا موقعیت دقیق خود را بر روی نقشه تعیین کنید",
    type: "map",
    enable: !1,
    required: !1,
    parent: "",
    separator: "Location",
    serverValidation: !1
}, {
    id: "PresentDays",
    label: "روزهای حضور",
    type: "checkbox",
    multiline: !0,
    enable: !1,
    required: !1,
    options: "presentDaysOptions",
    parent: "",
    separator: "submit",
    serverValidation: !1
}, {
    id: "PresentHours",
    label: "ساعتهای حضور",
    type: "checkbox",
    multiline: !0,
    enable: !1,
    required: !1,
    options: "presentHoursOptions",
    parent: "",
    separator: "submit",
    serverValidation: !1
}, {
    id: "DeliveryToSecuirty",
    label: "تحویل نگهبانی شود؟",
    type: "radio",
    checked: !0,
    enable: !1,
    required: !1,
    options: "deliveryToSecuirtyOptions",
    parent: "",
    separator: "submit",
    serverValidation: !1
}, {
    id: "NoticeNationalID",
    label: "هشدار در دست داشتن کارت ملی (تنها هشدار در فرم نشان داده شود)",
    type: "warning",
    enable: !1,
    required: !1,
    minLength: 10,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "ExtraDeliveryNotes",
    label: "توضیحات جهت تحویل",
    type: "textarea",
    enable: !1,
    required: !1,
    minLength: 1,
    maxLength: 256,
    lengthStatus: !1,
    parent: "",
    separator: "submit",
    serverValidation: !1,
    events: [{event: "validation", handler: persianRegexValidation}]
}, {
    id: "DiscountCode",
    label: "کد تخفیف",
    description: "در صورت در اختیار داشتن کد تخفیف وارد کنید",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 1,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "submit",
    serverValidation: !1
}, {
    id: "BankCardNumber",
    label: "شماره کارت بانکی",
    type: "cardnumber",
    enable: !1,
    required: !1,
    minLength: 16,
    maxLength: 16,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1
}, {
    id: "PersonalPhoto",
    label: "عکس پرسنلی",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "submit",
    serverValidation: !1
}, {
    id: "OrganizationLogo",
    label: "لوگوی سازمان",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1
}, {
    id: "IdentityCardImage",
    label: "تصویر کارت ملی",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1
}, {
    id: "BirthCertificateImage",
    label: "تصویر شناسنامه",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1
}, {
    id: "DrivingLicenceImage",
    label: "تصویر گواهینامه",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1
}, {
    id: "PassportImage",
    label: "تصویر پاسپورت",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1
}, {
    id: "SelfPortraitImage",
    label: "تصویر احراز  هویت",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    append: "selfPortraitDescription",
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1
},];
fields = controlsType, function (n) {
    function i(n) {
        var t = document.createElement("FORM"), i, f, u, h, c, e, o, s;
        for (t.setAttribute("method", "post"), t.setAttribute("action", n), t.setAttribute("class", prefex + "form"), t.setAttribute("autocomplete", "off"), i = [], f = 0; f < selectedfields.length; f++) u = selectedfields[f], i == null ? i.push({
            separator: u.separator,
            separatorName: u.separatorName
        }) : (h = !1, i.forEach(n => {
            n.separator == u.separator && (h = !0)
        }), h == !1 && i.push({separator: u.separator, separatorName: u.separatorName}));
        pages = [];
        i.forEach(n => {
            for (var i, r, t, u, f = [], e = 0; e < selectedfields.length; e++) if (i = selectedfields[e], n.separator == i.separator) for (r = 0; r < controlsType.length; r++) t = controlsType[r], t.id == i.id && (t.required = i.Required, t.type == "text" ? (t.minLength = i.min, t.maxLength = i.max) : t.type == "image", f.push(t));
            n.separatorName != "" ? (u = {
                title: n.separatorName,
                fields: f
            }, pages.push(u)) : (u = {title: "گام شماره " + n, fields: f}, pages.push(u))
        });
        theme.template == "paginated" && (t.style.width = 100 * pages.length + "%");
        for (c in pages) t.appendChild(r(pages[c])), theme.template == "paginated" && (pages[c].el.style.width = 100 / pages.length + "%");
        return e = document.createElement("INPUT"), e.setAttribute("type", "submit"), e.setAttribute("value", "ثبت"), o = document.createElement("DIV"), o.setAttribute("class", prefex + "submit-simple"), o.appendChild(e), t.appendChild(o), s = document.createElement("DIV"), s.setAttribute("class", prefex + "form-wrapper " + theme.template), s.appendChild(t), s
    }

    function r(n) {
        var t = document.createElement("DIV"), i, r, o, s, e, h, c;
        t.setAttribute("class", prefex + "page");
        n.classList && t.classList.add(n.classList);
        i = document.createElement("DIV");
        i.setAttribute("class", prefex + "page-title");
        i.innerHTML = n.title;
        t.appendChild(i);
        r = document.createElement("DIV");
        r.classList.add(prefex + "page-body");
        o = !1;
        for (s in n.fields) e = n.fields[s], h = selectedfields.find(n => n.id == e.id), h != undefined && (o = !0, c = f(e), r.appendChild(c));
        return t.appendChild(r), t.appendChild(u(pages.indexOf(n) + 1)), n.el = t, t
    }

    function u(n) {
        var r = document.createElement("BUTTON"), i, u, f, t;
        r.setAttribute("class", prefex + "next-button");
        r.innerHTML = "ادامه";
        i = document.createElement("BUTTON");
        i.setAttribute("class", prefex + "prev-button");
        i.innerHTML = "قبل";
        u = document.createElement("INPUT");
        u.setAttribute("type", "submit");
        u.setAttribute("value", "ثبت");
        f = document.createElement("LABEL");
        f.setAttribute("style", "color:red; float:left; font-size: 10px;");
        f.innerHTML = versionInfo;
        t = document.createElement("DIV");
        t.setAttribute("class", prefex + "form-pagination");
        switch (n) {
            case pages.length:
                t.innerHTML = (pages.length > 1 ? i.outerHTML : "") + u.outerHTML;
                break;
            case 1:
                t.innerHTML = r.outerHTML;
                break;
            default:
                t.innerHTML = i.outerHTML + r.outerHTML
        }
        return t.innerHTML = t.innerHTML + f.outerHTML, t
    }

    function f(n) {
        switch (n.type) {
            case"text":
                n.el = e(n);
                break;
            case"masktext":
                n.el = o(n);
                break;
            case"date":
                n.el = s(n);
                break;
            case"select":
                n.el = h(n);
                break;
            case"checkbox":
                n.el = c(n);
                break;
            case"radio":
                n.el = l(n);
                break;
            case"textarea":
                n.el = a(n);
                break;
            case"image":
                n.el = v(n);
                break;
            case"cardnumber":
                n.el = y(n);
                break;
            case"phone":
                n.el = p(n);
                break;
            case"map":
                n.el = w(n)
        }
        return n.class && n.el.classList.add(n.class), n.el
    }

    function e(n) {
        var i = document.createElement("INPUT");
        console.log(n)
        return i.setAttribute("type", "text") ,i.setAttribute("maxlength",n.maxLength), i.setAttribute("id", prefex + n.id), i.setAttribute("name", n.id), i.setAttribute("class", prefex + "input"), i.setAttribute("placeholder", n.label), n.required && i.setAttribute("required", ""), t(i, n, "", n.prepend ? n.prepend.outerHTML : "", castFieldAppends(n.id) ? castFieldAppends(n.id).outerHTML : "")
    }

    function o(n) {
        var r = document.createElement("DIV"), f, e, u, i;
        for (r.setAttribute("id", prefex + n.id), r.setAttribute("dir", "ltr"), r.setAttribute("class", prefex + "input"), r.setAttribute("style", "width:90%  !important;  display: inline-block !important;"), f = document.createElement("LABEL"), f.style = " float: right !important; display: inline !important; padding-right: 1px  !important; color: #c7c7c7;", f.innerHTML = n.label, e = 70 / n.maxLength, u = 0; u < n.maxLength; u++) i = document.createElement("INPUT"), i.setAttribute("type", "text"), i.setAttribute("id", prefex + n.id + u), i.setAttribute("name", n.id + u), i.setAttribute("class", prefex + "inputTooth"), i.setAttribute("maxlength", "1"), i.setAttribute("style", "width:" + e + "% !important;  margin-top: 8px; padding:5px !important; display: inline !important;"), n.required && i.setAttribute("required", ""), r.appendChild(i);
        return r.appendChild(f), t(r, n, "", n.prepend ? n.prepend.outerHTML : "", castFieldAppends(n.id) ? castFieldAppends(n.id).outerHTML : "")
    }

    function s(n) {
        var i = document.createElement("INPUT");
        return i.setAttribute("type", "text"), i.setAttribute("id", prefex + n.id), i.setAttribute("name", n.id), i.setAttribute("class", prefex + "input"), i.setAttribute("placeholder", n.label), n.required && i.setAttribute("required", ""), t(i, n, "", n.prepend ? n.prepend.outerHTML : "", castFieldAppends(n.id) ? castFieldAppends(n.id).outerHTML : "")
    }

    function h(n) {
        var o = n.multiple ? [] : [{label: n.label, value: "", disabled: n.required ? !0 : !1, selected: !0}], e = "",
            f = castFieldOptions(n.options), i, r, u;
        return o.concat(f).map(function (n) {
            if (n !== null) {
                var t = document.createElement("OPTION");
                t.setAttribute("value", n.value);
                n.disabled && (t.disabled = !0);
                n.selected && t.setAttribute("selected", !0);
                t.innerHTML = n.label;
                e += t.outerHTML
            }
        }), i = document.createElement("SELECT"), i.setAttribute("id", prefex + n.id), i.setAttribute("name", n.id), i.setAttribute("class", prefex + "input"), n.required && i.setAttribute("required", ""), n.multiple && (i.setAttribute("multiple", !0), f != null && i.setAttribute("size", f.length)), i.innerHTML = e, r = t(i, n), n.multiple && (u = document.createElement("LABEL"), u.setAttribute("for", i.id), u.innerHTML = n.label, r.innerHTML = u.outerHTML + r.innerHTML), n.el = r, r
    }

    function c(n) {
        var r = document.createElement("DIV"), o, u, s, l, f, i, e, h, c;
        r.className = prefex + "checkboxes-field";
        r.id = prefex + n.id;
        o = document.createElement("LABEL");
        o.innerHTML = n.label;
        u = document.createElement("DIV");
        u.className = prefex + "checkboxes-group" + (n.multiline ? " multiline" : "");
        s = castFieldOptions(n.options);
        for (l in s) f = s[l], i = document.createElement("INPUT"), i.id = n.id + "-" + f.value, i.name = n.id, i.value = f.value, i.type = "checkbox", n.required && i.setAttribute("required", ""), e = document.createElement("LABEL"), e.setAttribute("for", i.id), e.innerHTML = f.label, h = document.createElement("SPAN"), h.setAttribute("class", "check"), u.innerHTML += "<span>" + i.outerHTML + e.outerHTML + h.outerHTML + "<\/span>";
        return r.innerHTML = o.outerHTML + u.outerHTML, c = t(r, n, prefex + "form-checkbox"), n.el = c, c
    }

    function l(n) {
        var r = document.createElement("DIV"), o, u, s, c, f, i, e, h;
        r.className = prefex + "radios-field";
        r.id = prefex + n.id;
        o = document.createElement("LABEL");
        o.innerHTML = n.label;
        u = document.createElement("DIV");
        u.className = prefex + "radios-group";
        n.required && (r.setAttribute("required", ""), r.setAttribute("style", "background-color:#696969;"));
        s = castFieldOptions(n.options);
        for (c in s) f = s[c], i = document.createElement("INPUT"), i.id = n.id + "-" + f.value, i.name = n.id, i.value = f.value, i.type = "radio", n.required && i.setAttribute("required", ""), e = document.createElement("LABEL"), e.setAttribute("for", i.id), e.innerHTML = f.label, u.innerHTML += i.outerHTML + e.outerHTML;
        return r.innerHTML = o.outerHTML + u.outerHTML, h = t(r, n, prefex + "form-radio"), n.el = h, h
    }

    function a(n) {
        var i = document.createElement("TEXTAREA");
        return i.setAttribute("id", prefex + n.id), i.setAttribute("name", n.id), i.setAttribute("class", prefex + "input"), i.setAttribute("placeholder", n.label), n.required && i.setAttribute("required", ""), t(i, n)
    }

    function v(n) {
        var i = document.createElement("INPUT"), r, f, e, u, o;
        return i.type = "file", i.id = prefex + n.id, i.name = n.id, i.style.display = "none", i.setAttribute("accept", ".jpg,.png,.pdf"), n.required && i.setAttribute("required", ""), r = document.createElement("DIV"), r.className = prefex + "image", r.id = prefex + n.id, f = document.createElement("LABEL"), f.innerHTML = n.label, e = document.createElement("SPAN"), e.classList.add(prefex + "file-name"), u = document.createElement("BUTTON"), u.id = r.id + "-btn", u.innerHTML = "انتخاب فایل", u.setAttribute("onclick", "return false"), r.innerHTML = f.outerHTML + e.outerHTML + u.outerHTML + (castFieldAppends(n.id) ? castFieldAppends(n.id).outerHTML : ""), o = t(i, n, prefex + "form-radio", null, r.outerHTML), n.el = o, o
    }

    function y(n) {
        var i = document.createElement("DIV"), f, r, u, e;
        for (i.className = prefex + "card-field", i.id = prefex + n.id, f = document.createElement("LABEL"), f.innerHTML = n.label, r = document.createElement("DIV"), r.className = prefex + "card-group", u = 1; u < 5; u++) {
            let t = document.createElement("INPUT");
            t.id = prefex + n.id + "-" + u;
            t.setAttribute("size", "4");
            t.setAttribute("maxLength", "4");
            n.required && t.setAttribute("required", "");
            r.appendChild(t)
        }
        return i.innerHTML = f.outerHTML + r.outerHTML, e = t(i, n, prefex + "form-card"), n.el = e, e
    }

    function p(n) {
        var e = document.createElement("DIV"), s, i, o, r, u, f, h;
        return e.className = prefex + "phone-field", e.id = prefex + n.id, s = document.createElement("LABEL"), s.innerHTML = n.label, i = document.createElement("DIV"), i.className = prefex + "phone-group", o = document.createElement("SELECT"), o.className = prefex + "country-code", o.innerHTML = "<option>+98<\/option>", i.appendChild(o), r = document.createElement("INPUT"), r.className = prefex + "area-code", r.setAttribute("size", "4"), r.setAttribute("maxLength", "4"), r.setAttribute("placeholder", "پیش شماره"), i.appendChild(r), u = document.createElement("INPUT"), u.className = prefex + "phone-number", u.setAttribute("size", "8"), u.setAttribute("maxLength", "8"), u.setAttribute("placeholder", "شماره تلفن"), i.appendChild(u), f = document.createElement("INPUT"), f.className = prefex + "internal-code", f.setAttribute("size", "4"), f.setAttribute("maxLength", "4"), f.setAttribute("placeholder", "داخلی"), i.appendChild(f), e.innerHTML = s.outerHTML + i.outerHTML, h = t(e, n, prefex + "form-phone"), n.el = h, h
    }

    function w(n) {
        var r = document.createElement("DIV"), i;
        return r.setAttribute("id", prefex + "map"), i = t(r, n), n.el = i, i
    }

    function t(n, t, i = "", r = null, u = null) {
        var o = document.createElement("SMALL"), s, e, h, f;
        return o.setAttribute("class", prefex + "form-description"), t.description && (o.innerHTML = t.description), s = document.createElement("SMALL"), s.setAttribute("class", prefex + "form-validation"), e = document.createElement("DIV"), e.setAttribute("class", prefex + "form-bottom"), e.innerHTML = o.outerHTML + s.outerHTML, h = document.createElement("DIV"), h.setAttribute("class", prefex + "form-help"), f = document.createElement("DIV"), f.setAttribute("id", n.id + "-wrapper"), f.setAttribute("class", prefex + "form-group " + t.type + " " + i + (t.required ? " form-required" : "")), f.innerHTML = (r ? r : "") + n.outerHTML + (u ? u : "") + e.outerHTML + h.outerHTML, f
    }

    Object.defineProperty(n, "parentId", {
        get: function () {
            return parentId
        }, set: function (n) {
            parentId = n
        }
    });
    n.createForm = i;
    n.addEvents = addEvents;
    Object.defineProperty(n, "prefex", {
        get: function () {
            return prefex
        }, set: function (n) {
            prefex = n
        }
    });
    Object.defineProperty(n, "pages", {
        get: function () {
            return pages
        }, set: function (n) {
            pages = n
        }
    });
    Object.defineProperty(n, "currPage", {
        get: function () {
            return currPage
        }, set: function (n) {
            currPage = n
        }
    });
    Object.defineProperty(n, "theme", {
        get: function () {
            return theme
        }, set: function (n) {
            theme = n
        }
    });
    Object.defineProperty(n, "fields", {
        get: function () {
            return fields
        }, set: function (n) {
            fields = n
        }
    })
}($myformInstance = {});
window.myForm = {
    init: function (n, t) {
        var i = $myformInstance, r;
        i.parentId = n;
        document.getElementById(n).innerHTML = "";
        document.getElementById(n).appendChild(i.createForm(t));
        r = new CustomEvent("myformready");
        i.addEvents();
        document.getElementById(n).dispatchEvent(r)
    }
};
typeof jQuery != "undefined" && function (n) {
    n.fn.myForm = function (n = "") {
        var t = $myformInstance, i;
        return t.parentId = el, this.html(""), this.append(t.createForm(n)), i = new CustomEvent("myformready"), t.addEvents(), document.getElementById(el).dispatchEvent(i), this
    }
}(jQuery);
$myformInstance.theme.template = "paginated";
myForm.init("myform", "");
setDatePicker();
startRegister();