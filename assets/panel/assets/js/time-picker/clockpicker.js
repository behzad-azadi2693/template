'use strict';
$('.clockpicker').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'
    })
            .find('input').change(function(){
        $(".popover-title").attr("dir", "ltr")
    });
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': '20:48'
    });
