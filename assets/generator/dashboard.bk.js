function GetLastSelectedItems() {
    $.ajax({
        url: "http://melrio.com/api/v1/listOfForms?id=" + formID,
        type: "GET",
        headers: {
            Authorization: "Token 0b5ed24810a9ea2fca2a798b1c4a3d574600691e",
            "Content-Type": "application/x-www-form-urlencoded"
        },
        success: function (n) {
            lastSelectefieldtemp = n.json_form;
            createLastSelectefield()
        },
        error: function () {
        }
    })
}

function createStep() {
    var t, u, o, h, f, e, i, c, n, a, s, l, r;
    if (list != null) {
        stepCount++;
        t = document.createElement("DIV");
        t.className = "col-12 col-lg-6";
        t.id = "step" + stepCount;
        u = document.createElement("DIV");
        u.className = "title col-12";
        u.innerHTML = "گام شماره " + stepCount;
        o = document.createElement("I");
        o.className = "fas fa-chevron-left";
        o.setAttribute("aria-hidden", !0);
        u.appendChild(o);
        h = "stepName" + stepCount;
        f = document.createElement("LABEL");
        f.setAttribute("for", h);
        f.className = "mfl-input col-2 col-sm-1 col-md-1";
        f.style = "display: inline !important; margin-right: 5px !important;";
        f.innerHTML = "       نام گام :";
        e = document.createElement("INPUT");
        e.id = h;
        e.type = "text";
        e.className = "mfl-input col-6 col-sm-5 col-md-5";
        e.style = "display: inline !important; margin-right: 5px !important;";
        u.appendChild(f);
        u.appendChild(e);
        t.appendChild(u);
        i = document.createElement("select");
        i.id = "dropdown" + stepCount;
        i.className = "col-6 col-sm-5 col-md-5 mfl-input";
        i.setAttribute("data-stepIndex", stepCount);
        i.setAttribute("style", "font-size: small !important;");
        i.addEventListener("change", function () {
            var n = i.getAttribute("data-stepIndex"), t = $(this).children("option:selected").val();
            checkExistObject(n, t) != !0 && createSelectedObject(n, t)
        });
        c = '<option  value="firstIten"> لطفاً انتخاب نمایید<\/option>';
        for (x in list) {
            let n = list[x];
            n.isUse == !1 && (c += '<option  value="' + n.id + '">' + n.label + "<\/option>")
        }
        i.innerHTML += c;
        t.appendChild(i);
        stepCount != 1 && (n = document.createElement("INPUT"), n.id = "button" + stepCount, n.type = "button", n.className = "deleteButton btn btn-danger", n.style = "padding: 5px 10 5px 22px !important;margin-right: 5px !important;", n.setAttribute("value", "حذف این گام"), n.innerHTML = '<i class="fas fa-window-close" aria-hidden="true"><\/i>', n.setAttribute("data-stepIndex", stepCount), n.addEventListener("click", function () {
            var r = n.getAttribute("data-stepIndex"), t = document.getElementById("childWrapper" + r), i, u, f, e;
            if (t.childElementCount != 0) {
                for (i = 0; i < t.childElementCount; i++) {
                    u = t.children[i];
                    f = u.getAttribute("data-fieldId");
                    for (x in list) {
                        let n = list[x];
                        if (n.id == f) {
                            n.isUse = !1;
                            break
                        }
                    }
                }
                refreshDropdowns()
            }
            t.remove();
            e = document.getElementById("step" + r);
            e.remove();
            stepCount--
        }), t.appendChild(n));
        a = document.createElement("br");
        t.appendChild(a);
        s = document.createElement("DIV");
        s.id = "childWrapper" + stepCount;
        s.className = "row";
        t.appendChild(s);
        t.className = "row optionalFields";
        $("#bodyMainPage").append(t);
        l = document.getElementById("addButton");
        l != undefined && l.remove();
        r = document.createElement("INPUT");
        r.id = "addButton";
        r.type = "button";
        r.className = "btn btn-success";
        r.setAttribute("value", "ایجاد گام جدید");
        r.setAttribute("style", "margin-top: 20px !important;");
        r.addEventListener("click", checkCreateStep);
        $("#bodyMainPage").append(r)
    }
}

function checkExistObject(n, t) {
    var i = document.getElementById(t);
    return i == undefined ? !1 : !0
}

function checkCreateStep() {
    var n = document.getElementById("childWrapper" + stepCount);
    n.childElementCount == 0 || createStep()
}

function createSelectedObject(n, t) {
    var l, w, u, e, i, b, o, f, r, s, k, d, h, a, v, y, it, c, p, rt;
    for (l in optionalFields) {
        let rt = optionalFields[l];
        if (rt.id == t) {
            if (rt.parent != "" && (w = document.getElementById(rt.parent), w == undefined)) return !1;
            if (rt.groupName != "") {
                createSelectedGroup(n, rt.groupName);
                return
            }
            let ut = list[l];
            if (ut.isUse = !0, u = document.createElement("DIV"), u.id = "wrapper" + rt.id, u.className = "col-12 col-lg-5", u.setAttribute("data-fieldId", rt.id), u.setAttribute("data-stepIndex", n), u.setAttribute("style", "margin-right: 10px !important;"), e = document.createElement("DIV"), e.className = "field", i = document.createElement("INPUT"), i.id = rt.id, i.type = "checkbox", i.setAttribute("checked", ""), b = '<i class="checked far fa-check-square"><\/i><i class="not-checked far fa-square"><\/i>', o = document.createElement("LABEL"), o.setAttribute("for", i.id), o.innerHTML = rt.label, f = document.createElement("A"), f.setAttribute("href", "#" + i.id + "Settings"), f.setAttribute("data-toggle", "collapse"), f.className = "field-settings-btn", f.innerHTML = '<small><i class="fas fa-cog"><\/i>تنظیمات<\/small>', r = document.createElement("DIV"), r.id = i.id + "Settings", r.className = "collapse field-settings", s = document.createElement("DIV"), s.className = "custom-control custom-switch field-required", k = '<input id="' + (i.id + "Required") + '" type="checkbox" class="custom-control-input">', d = '<label for="' + (i.id + "Required") + '" class="custom-control-label">اجباری<\/label>', s.innerHTML = k + d, r.appendChild(s), h = rt.fixed ? " readonly" : "", rt.type == "text") a = document.createElement("DIV"), a.innerHTML = '<span class="field-min"><label>حداقل تعداد کاراکتر:<\/label><input id="' + rt.id + 'min" "type="number" value="' + rt.minLength + '"' + h + "><\/span>", v = document.createElement("DIV"), v.innerHTML = '<span class="field-max"><label>حداکثر تعداد کاراکتر:<\/label><input id="' + rt.id + 'max" type="number" value="' + rt.maxLength + '"' + h + "><\/span>", r.appendChild(a), r.appendChild(v); else if (rt.type == "image") {
                y = document.createElement("DIV");
                y.innerHTML = '<span class="field-max"><label>حداکثرحجم فایل:<\/label><input id="' + rt.id + 'size" type="number" value="2"> <label class="descriptionLabel" style="color:red; text-align:left;">MB<\/label><\/span>';
                var g = rt.formats.split("/"), nt = document.createElement("DIV"), tt = "";
                for (it in g) {
                    let n = g[it];
                    c = rt.id + "fileFormat" + n;
                    tt += '<input id="' + c + '" name="' + c + '" style="color:white" type="checkbox"  checked="checked"  value="' + n + '"> <label  class="descriptionLabel"  for="' + c + '"> ' + n + "<\/label>"
                }
                nt.innerHTML = '<span class="field-max"><label>فرمت فایل:<\/label>' + tt + "<\/span>";
                r.appendChild(y);
                r.appendChild(nt)
            } else rt.type == "masktext" && (p = document.createElement("DIV"), p.innerHTML = '<span class="field-min"><label> تعداد کاراکتر:<\/label><input id="' + rt.id + 'len" "type="number" value="' + rt.length + '"' + h + "><\/span>", r.appendChild(p));
            e.innerHTML = i.outerHTML + b + o.outerHTML + f.outerHTML + r.outerHTML;
            u.appendChild(e);
            rt = $("#" + rt.separator);
            $("#childWrapper" + n).append(u);
            rt.parent == "" ? $("#" + rt.id).attr("data-parent", "_") : $("#" + rt.id).attr("data-parent", rt.parent);
            rt.groupName == "" ? $("#" + rt.id).attr("data-groupname", "_") : $("#" + rt.id).attr("data-groupname", rt.groupName);
            $("#" + rt.id).change(function (n) {
                checkchanged(n)
            })
        }
    }
    refreshDropdowns()
}

function createSelectedGroup(n, t) {
    var l, w, u, e, i, b, o, f, r, s, k, d, h, a, v, y, it, c, p, rt;
    for (l in optionalFields) {
        let rt = optionalFields[l];
        if (rt.groupName == t) {
            if (rt.parent != "" && (w = document.getElementById(rt.parent), w == undefined)) return !1;
            let ut = list[l];
            if (ut.isUse = !0, u = document.createElement("DIV"), u.id = "wrapper" + rt.id, u.className = "col-12 col-lg-6", u.setAttribute("data-fieldId", rt.id), u.setAttribute("data-stepIndex", n), e = document.createElement("DIV"), e.className = "field", i = document.createElement("INPUT"), i.id = rt.id, i.type = "checkbox", i.setAttribute("checked", ""), b = '<i class="checked far fa-check-square"><\/i><i class="not-checked far fa-square"><\/i>', o = document.createElement("LABEL"), o.setAttribute("for", i.id), o.innerHTML = rt.label, f = document.createElement("A"), f.setAttribute("href", "#" + i.id + "Settings"), f.setAttribute("data-toggle", "collapse"), f.className = "field-settings-btn", f.innerHTML = '<small><i class="fas fa-cog"><\/i>تنظیمات<\/small>', r = document.createElement("DIV"), r.id = i.id + "Settings", r.className = "collapse field-settings", s = document.createElement("DIV"), s.className = "custom-control custom-switch field-required", k = '<input id="' + (i.id + "Required") + '" type="checkbox" class="custom-control-input">', d = '<label for="' + (i.id + "Required") + '" class="custom-control-label">اجباری<\/label>', s.innerHTML = k + d, r.appendChild(s), h = rt.fixed ? " readonly" : "", rt.type == "text") a = document.createElement("DIV"), a.innerHTML = '<span class="field-min"><label>حداقل تعداد کاراکتر:<\/label><input id="' + rt.id + 'min" "type="number" value="' + rt.minLength + '"' + h + "><\/span>", v = document.createElement("DIV"), v.innerHTML = '<span class="field-max"><label>حداکثر تعداد کاراکتر:<\/label><input id="' + rt.id + 'max" type="number" value="' + rt.maxLength + '"' + h + "><\/span>", r.appendChild(a), r.appendChild(v); else if (rt.type == "image") {
                y = document.createElement("DIV");
                y.innerHTML = '<span class="field-max"><label>حداکثرحجم فایل:<\/label><input id="' + rt.id + 'size" type="number" value="2"> <label class="descriptionLabel" style="color:red; text-align:left;">MB<\/label><\/span>';
                var g = rt.formats.split("/"), nt = document.createElement("DIV"), tt = "";
                for (it in g) {
                    let n = g[it];
                    c = rt.id + "fileFormat" + n;
                    tt += '<input id="' + c + '" name="' + c + '" style="color:white" type="checkbox"  checked="checked"  value="' + n + '"> <label  class="descriptionLabel"  for="' + c + '"> ' + n + "<\/label>"
                }
                nt.innerHTML = '<span class="field-max"><label>فرمت فایل:<\/label>' + tt + "<\/span>";
                r.appendChild(y);
                r.appendChild(nt)
            } else rt.type == "masktext" && (p = document.createElement("DIV"), p.innerHTML = '<span class="field-min"><label> تعداد کاراکتر:<\/label><input id="' + rt.id + 'len" "type="number" value="' + rt.length + '"' + h + "><\/span>", r.appendChild(p));
            e.innerHTML = i.outerHTML + b + o.outerHTML + f.outerHTML + r.outerHTML;
            u.appendChild(e);
            rt = $("#" + rt.separator);
            $("#childWrapper" + n).append(u);
            rt.parent == "" ? $("#" + rt.id).attr("data-parent", "_") : $("#" + rt.id).attr("data-parent", rt.parent);
            rt.groupName == "" ? $("#" + rt.id).attr("data-groupname", "_") : $("#" + rt.id).attr("data-groupname", rt.groupName);
            $("#" + rt.id).change(function (n) {
                checkchanged(n)
            })
        }
    }
    refreshDropdowns()
}

function checkchanged(n) {
    var r, u, t, i;
    if (n.target.dataset.groupname != "_") {
        var t = n.target.id, i = $("#" + t).is(":checked"), f = n.target.dataset.groupname;
        for (r in optionalFields) {
            let n = optionalFields[r];
            if (n.groupName == f && ($("#" + n.id).prop("checked", i == !0), i == !1)) {
                $("#wrapper" + n.id).remove();
                for (x in list) {
                    let t = list[x];
                    if (t.id == n.id) {
                        t.isUse = !1;
                        break
                    }
                }
            }
        }
    }
    if (n.target.parent != "") if (t = n.target.id, i = $("#" + t).is(":checked"), i == !1) for (r in optionalFields) {
        let n = optionalFields[r];
        if (n.parent == t) {
            $("#" + n.id).prop("checked", !1);
            for (x in list) {
                let t = list[x];
                if (t.id == n.id) {
                    t.isUse = !1;
                    break
                }
            }
        }
    } else if (u = $("#" + t).data("parent"), u != "_" && $("#" + u).is(":checked") != !0) {
        $("#" + t).prop("checked", !1);
        $("#wrapper" + t).remove();
        for (x in list) {
            let n = list[x];
            if (n.id == t) {
                n.isUse = !1;
                break
            }
        }
    }
    if (t = n.target.id, i = $("#" + t).is(":checked"), i == !1) if (t != "IdentityType") {
        for (x in list) {
            let n = list[x];
            if (n.id == t) {
                n.isUse = !1;
                break
            }
        }
        $("#wrapper" + t).remove()
    } else $("#" + t).prop("checked", !0);
    refreshDropdowns()
}

function refreshDropdowns() {
    var t = '<option  value="firstIten"> لطفاً انتخاب نمایید<\/option>', n, i;
    for (x in list) {
        let n = list[x];
        n.isUse == !1 && (t += '<option  value="' + n.id + '">' + n.label + "<\/option>")
    }
    for (n = 1; n <= stepCount; n++) i = document.getElementById("dropdown" + n), i.innerHTML = t
}

function getActiveFields() {
    var e = [], o, b, s, h, c, u, f, l, a, v, y, p, w;
    for (o in optionalFields) {
        let p = optionalFields[o];
        if (b = document.getElementById(p.id), s = $("#" + p.id).is(":checked"), s == !0) {
            var n = {}, i = document.getElementById(p.id + "Required"), k = document.getElementById("wrapper" + p.id),
                t = k.getAttribute("data-stepIndex"), r = document.getElementById("stepName" + t).value;
            if (p.type == "text") h = document.getElementById(p.id + "min").value, c = document.getElementById(p.id + "max").value, n = {
                id: p.id,
                Required: i.checked,
                min: h,
                max: c,
                separator: t,
                separatorName: r
            }; else if (p.type == "image") {
                u = p.formats.split("/");
                f = [];
                for (l in u) {
                    let n = u[l];
                    a = p.id + "fileFormat" + n;
                    v = $("#" + a).is(":checked");
                    v == !0 && f.push(n)
                }
                n = {id: p.id, Required: i.checked, formats: f, separator: t, separatorName: r}
            } else p.type == "masktext" ? (y = document.getElementById(p.id + "len").value, n = {
                id: p.id,
                Required: i.checked,
                length: y,
                separator: t,
                separatorName: r
            }) : n = {id: p.id, Required: i.checked, separator: t, separatorName: r};
            e.push(n)
        }
    }
    p = document.getElementById("formNameTextbox").value;
    w = document.getElementById("formDescriptionTextbox").value;
    $.ajax({
        url: "http://melrio.com/api/v1/saveForm",
        type: "POST",
        headers: {
            Authorization: "Token 0b5ed24810a9ea2fca2a798b1c4a3d574600691e",
            "X-CSRFToken": getCookie("csrftoken"),
            "Content-Type": "application/x-www-form-urlencoded"
        },
        data: {
            name: p,
            description: w,
            form_type: "0",
            font_type: "0",
            background_display: "0",
            json_form: JSON.stringify(e)
        },
        success: function () {
        },
        error: function () {
        }
    })
}

function getCookie(n) {
    let t = null;
    if (document.cookie && document.cookie !== "") {
        const i = document.cookie.split(";");
        for (let r = 0; r < i.length; r++) {
            const u = i[r].trim();
            if (u.substring(0, n.length + 1) === n + "=") {
                t = decodeURIComponent(u.substring(n.length + 1));
                break
            }
        }
    }
    return t
}

function createForm(n) {
    var t = document.createElement("SCRIPT");
    t.id = "form-script";
    t.type = "text/javascript";
    t.text = n;
    document.getElementsByTagName("iframe")[0].contentWindow.document.getElementsByTagName("body")[0].appendChild(t);
    document.getElementsByTagName("iframe")[0].contentWindow.myForm.init("myform", "")
}

function createFormScript() {
    var n, t, i, r;
    formEdited = form;
    n = inputs.split(",");
    for (t in n) i = n[t].slice(1), $(n[t]).is(":checked") ? (r = "", fields[i].isEnabled = !0) : (r = " $myformInstance.fields." + n[t].slice(1) + ".enabled = false;", fields[i].isEnabled = !1), fields[i].required = $(n[t] + "Required").is(":checked"), fields[i].required && fields[i].isEnabled && (r += " $myformInstance.fields." + n[t].slice(1) + ".required = true;"), formEdited += r;
    $("#template").val() == "simple" && (formEdited += " $myformInstance.theme.template='simple'; ")
}

function createLastSelectefield() {
    var i = JSON.parse(lastSelectefieldtemp), r, n, u, t, f;
    for (r in i) n = i[r], u = document.getElementById("step" + n.separator), u == undefined && createStep(), t = document.getElementById("stepName" + n.separator), t != undefined && (t.value = n.separatorName), f = document.getElementById(n.id), f == undefined && createSelectedObject(n.separator, n.id)
}

function selfPortraitDescription() {
    var t = document.createElement("DIV"), n, r;
    t.className = prefex + "selfportrait-desc";
    var u = document.createElement("DIV"), f = document.createElement("DIV"), i = document.createElement("DIV");
    return i.className = "code", i.innerHTML = "4542", n = document.createElement("SPAN"), n.className = "help", n.innerHTML = "لطفا کد 4 رقمی را بصورت بزرگ و خوانا روی یک برگه سفید نوشته و در کنار صورت خود نگهدارید. سپس یک عکس سلفی از خود به همراه این برگه بگیرید و در اینجا آپلود نمایید. دقت نمایید صورت شما شفاف، بدون عینک و یا گریم و کلاه باشد.", r = document.createElement("DIV"), r.className = "image", u.innerHTML = i.outerHTML + n.outerHTML, f.innerHTML = r.outerHTML, t.innerHTML = u.outerHTML + f.outerHTML, t
}

function addressStatusOptions() {
    return [{
        label: "محل دائمی سکونت | مالک هستم | بیش از دو سال در این مکان هستم",
        value: 1
    }, {
        label: "محل دائمی سکونت | مالک هستم | بیش از یک سال و کمتر از دو سال در این مکان هستم",
        value: 2
    }, {
        label: "محل دائمی سکونت | مالک هستم | به زودی تغییر مکان می‌دهم",
        value: 3
    }, {
        label: "محل دائمی سکونت | مستاجر هستم | بیش از یک سال در این مکان هستم",
        value: 4
    }, {
        label: "محل دائمی سکونت | مستاجر هستم | کمتر از یک سال در این مکان هستم",
        value: 5
    }, {
        label: "محل موقت سکونت | در ایران ساکن نیستم و ساکن کشور دیگری هستم",
        value: 6
    }, {
        label: "محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم",
        value: 7
    }, {
        label: "محل موقت سکونت | نه مالک و نه مستاجر هستم | به زودری تغییر مکان می‌دهم",
        value: 8
    }, {
        label: "محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم",
        value: 9
    }, {
        label: "محل دائمی کار و تجارت | مالک یا مستاجر هستم | به تازگی در این مکان هستم (کمتر از دو ماه)",
        value: 10
    }, {
        label: "محل دائمی کار و تجارت | مالک هستم | بیش از یک سال در این مکان هستم",
        value: 11
    }, {
        label: "محل دائمی کار و تجارت | مالک هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)",
        value: 12
    }, {
        label: "محل دائمی کار و تجارت | مستاجر هستم | بیش از یک سال در این مکان هستم",
        value: 13
    }, {label: "محل دائمی کار و تجارت | مستاجر هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)", value: 14}]
}

function identityTypeOptions() {
    return [{label: "حقیقی", value: "natural"}, {label: "حقوقی", value: "legal"}]
}

function genderOptions() {
    return [{label: "زن", value: "female"}, {label: "مرد", value: "male"}]
}

function provinceOptions() {
    return [{id: 3, label: "آذربایجان شرقی", value: "آذربایجان شرقی"}, {
        id: 16,
        label: "آذربایجان غربی",
        value: "آذربایجان غربی"
    }, {id: 15, label: "اردبیل", value: "اردبیل"}, {id: 6, label: "اصفهان", value: "اصفهان"}, {
        id: 31,
        label: "البرز",
        value: "البرز"
    }, {id: 27, label: "ایلام", value: "ایلام"}, {id: 21, label: "بوشهر", value: "بوشهر"}, {
        id: 1,
        label: "تهران",
        value: "تهران"
    }, {id: 24, label: "چهارمحال و بختیاری", value: "چهارمحال و بختیاری"}, {
        id: 30,
        label: "خراسان جنوبی",
        value: "خراسان جنوبی"
    }, {id: 7, label: "خراسان رضوی", value: "خراسان رضوی"}, {
        id: 29,
        label: "خراسان شمالی",
        value: "خراسان شمالی"
    }, {id: 4, label: "خوزستان", value: "خوزستان"}, {id: 12, label: "زنجان", value: "زنجان"}, {
        id: 9,
        label: "سمنان",
        value: "سمنان"
    }, {id: 26, label: "سیستان و بلوچستان", value: "سیستان و بلوچستان"}, {id: 5, label: "فارس", value: "فارس"}, {
        id: 8,
        label: "قزوین",
        value: "قزوین"
    }, {id: 10, label: "قم", value: "قم"}, {id: 18, label: "کردستان", value: "کردستان"}, {
        id: 22,
        label: "کرمان",
        value: "کرمان"
    }, {id: 19, label: "کرمانشاه", value: "کرمانشاه"}, {id: 0, label: "کشورهای خارجه", value: "کشورهای خارجه"}, {
        id: 28,
        label: "کهگیلویه و بویراحمد",
        value: "کهگیلویه و بویراحمد"
    }, {id: 14, label: "گلستان", value: "گلستان"}, {id: 2, label: "گیلان", value: "گیلان"}, {
        id: 20,
        label: "لرستان",
        value: "لرستان"
    }, {id: 13, label: "مازندران", value: "مازندران"}, {id: 11, label: "مرکزی", value: "مرکزی"}, {
        id: 23,
        label: "هرمزگان",
        value: "هرمزگان"
    }, {id: 17, label: "همدان", value: "همدان"}, {id: 25, label: "یزد", value: "یزد"}]
}

function regionTypeOptions() {
    return [{id: "1", label: "منطقه", value: "district"}, {id: "2", label: "محله", value: "neighborhood"}, {
        id: "3",
        label: "دهستان",
        value: "rural district"
    }, {id: "4", label: "شهرک", value: "township"}, {id: "5", label: "منطقه شهرداری", value: "municipality region"}]
}

function roadTypeOptions() {
    return [{id: "1", label: "خیابان", value: "street"}, {id: "2", label: "کوچه", value: "alley"}, {
        id: "3",
        label: "بنبست",
        value: "dead end"
    }, {id: "4", label: "بلوار", value: "boulvard"}, {id: "5", label: " بزرگراه", value: "highway"}, {
        id: "6",
        label: "آزادراه",
        value: "freeway"
    }, {id: "7", label: "میدان", value: "square"}, {id: "8", label: "پل", value: "bridge"}, {
        id: "9",
        label: "زیرگذر",
        value: "underpass"
    }, {id: "10", label: "کنارگذر", value: "Bypass"}, {id: "11", label: "جاده", value: "road"}, {
        id: "12",
        label: "مسیر اختصاصی",
        value: "assigned route"
    }, {id: "13", label: "کیلومتر", value: "kilometer"}, {id: "14", label: "چهارراه", value: "fourway"}, {
        id: "15",
        label: "سه راه",
        value: "threeway"
    }, {id: "16", label: "دو راه", value: "dualway"}]
}

function floorOptions() {
    for (var t = [], n = 1; n < 41; n++) t.push({id: n, label: "طبقه " + n, value: n});
    return t
}

function presentDaysOptions() {
    return [{label: "شنبه", value: "SAT"}, {label: "یکشنبه", value: "SUN"}, {
        label: "دوشنبه",
        value: "MON"
    }, {label: "سه‌شنبه", value: "TUE"}, {label: "چهارشنبه", value: "WED"}, {
        label: "پنج‌شنبه",
        value: "THU"
    }, {label: "جمعه", value: "FRI"}]
}

function presentHoursOptions() {
    return [{label: "۹ الی ۱۲", value: "09-12"}, {label: "۱۲ الی ۱۵", value: "12-15"}, {
        label: "۱۵ الی ۱۸",
        value: "15-18"
    }, {label: "۱۸ الی ۲۱", value: "18-21"}]
}

function deliveryToSecuirtyOptions() {
    return [{label: "بله", value: "1"}, {label: "خیر", value: "0"}]
}

function organTypeOptions() {
    return [{label: "دولتی", value: "government"}, {label: "خصوصی", value: "private"}, {
        label: "نیمه‌خصوصی",
        value: "semiprivate"
    }]
}

function companyTypeOptions() {
    return [{label: "مسئولیت محدود", value: "limited"}, {label: "سهامی خاص", value: "private"}, {
        label: "سهامی عام",
        value: "public"
    }, {label: "غیر انتفاعی", value: "nonprofit"}]
}

function marriageOptions() {
    return [{label: "مجرد", value: "single"}, {label: "متاهل", value: "married"}, {label: "جداشده", value: "divorced"}]
}

function militaryServiceOptions() {
    return [{label: "معافیت دائم", value: "1"}, {label: "مشمول", value: "2"}, {
        label: "مشغول خدمت",
        value: "3"
    }, {label: "غایب", value: "4"}]
}

function noHouseNumber() {
    var i = document.createElement("DIV"), t, n;
    return i.classList.add(prefex + "group-append", prefex + "form-checkbox"), t = document.createElement("LABEL"), t.classList.add(prefex + "group-text"), n = document.createElement("INPUT"), n.setAttribute("type", "checkbox"), n.name = "noHouseNumber", n.id = prefex + "noHouseNumber", t.innerHTML = n.outerHTML + '<span class="check"><\/span>بدون پلاک', i.appendChild(t), i
}

function castFieldOptions(n) {
    switch (n) {
        case "identityTypeOptions":
            return identityTypeOptions();
        case "organTypeOptions":
            return organTypeOptions();
        case "companyTypeOptions":
            return companyTypeOptions();
        case "genderOptions":
            return genderOptions();
        case "marriageOptions":
            return marriageOptions();
        case "militaryServiceOptions":
            return militaryServiceOptions();
        case "addressStatusOptions":
            return addressStatusOptions();
        case "provinceOptions":
            return provinceOptions();
        case "regionTypeOptions":
            return regionTypeOptions();
        case "roadTypeOptions":
            return roadTypeOptions();
        case "floorOptions":
            return floorOptions();
        case "presentDaysOptions":
            return presentDaysOptions();
        case "presentHoursOptions":
            return presentHoursOptions();
        case "deliveryToSecuirtyOptions":
            return deliveryToSecuirtyOptions();
        default:
            return null
    }
}



var controlsType = [{
    id: "IdentityType",
    label: "نوع شخصیت",
    type: "radio",
    enable: !1,
    required: !1,
    events: [{event: "myformready"}],
    options: "identityTypeOptions",
    separator: "specifications",
    parent: "",
    serverValidation: !1,
    groupName: ""
}, {
    id: "OrganType",
    label: "نوع سازمان",
    type: "radio",
    enable: !1,
    required: !1,
    options: "organTypeOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "CompanyType",
    label: "نوع شرکت",
    type: "radio",
    enable: !1,
    required: !1,
    options: "companyTypeOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "FirstName",
    label: "نام",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    parent: "",
    serverValidation: !1,
    separator: "specifications",
    groupName: "",
    fixed: !1
}, {
    id: "LastName",
    label: "نام خانوادگی",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "Gender",
    label: "جنسیت",
    type: "radio",
    enable: !1,
    required: !1,
    options: "genderOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "CompanyName",
    label: "نام شرکت/سازمان/موسسه/دانشگاه / وزارتخانه",
    type: "text",
    enable: !1,
    required: !1,
    "class": "w100",
    minLength: 3,
    maxLength: 25,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "BirthDate",
    label: "تاریخ تولد",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 10,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "RegNumber",
    label: "شماره شناسنامه",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 1,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "Marriage",
    label: "وضعیت تاهل",
    type: "radio",
    enable: !1,
    required: !1,
    "class": "w50",
    options: "marriageOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "SpouseName",
    label: "نام و نام خانوادگی همسر",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "MilitaryService",
    label: "وضعیت سربازی",
    type: "radio",
    enable: !1,
    required: !1,
    options: "militaryServiceOptions",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "Children",
    label: "تعداد فرزندان",
    type: "select",
    enable: !1,
    required: !1,
    serverValidation: !1,
    options: "",
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "NationalID",
    label: "کد ملی",
    type: "masktext",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    "class": "ltr",
    length: 10,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "NationalIDCode",
    label: "کد شناسه ملی",
    type: "masktext",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    "class": "ltr",
    length: 11,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "PassportNumber",
    label: "شماره پاسپورت",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 10,
    maxLength: 11,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "DrivingLicence",
    label: "شماره گواهینامه",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 4,
    maxLength: 10,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "FatherName",
    label: "نام پدر",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "BirthPlace",
    label: "محل تولد",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 2,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "RegIssuePlace",
    label: "محل صدور شناسنامه",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 2,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "DirectorFirstName",
    label: "نام مدیرعامل /رئیس",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "DirectorLastName",
    label: "نام خانوادگی مدیر عامل / رئیس",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    parent: "",
    separator: "specifications",
    lengthStatus: !1,
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "AgentFirstName",
    label: "نام شخص مسوول",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "AgentLastName",
    label: "نام خانوادگی شخص مسوول",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 3,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "PhoneNumber",
    label: "تلفن ثابت",
    type: "phone",
    enable: !1,
    required: !1,
    maxLength: "15 - No CountryCode",
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "CellNumber",
    label: "شماره همراه",
    description: "مثال: 09123456789",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    "class": "ltr",
    minLength: 11,
    maxLength: 11,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "AgentCellNumber",
    label: "شماره همراه شخص مسوول",
    description: "مثال: 09123456789",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    "class": "ltr",
    minLength: 11,
    maxLength: 11,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "Email",
    label: "ایمیل ",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    "class": "ltr",
    minLength: 5,
    maxLength: 50,
    lengthStatus: !0,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "AgentEmail",
    label: "ایمیل شخص مسوول",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 5,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "PREmail",
    label: "ایمیل روابط عمومی ",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 5,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "AddressStatus",
    label: "وضعیت نشانی",
    type: "select",
    enable: !1,
    required: !1,
    options: "addressStatusOptions",
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: ""
}, {
    id: "PostCode",
    label: "کد پستی",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    "class": "ltr",
    minLength: 10,
    maxLength: 10,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: "",
    fixed: !0
}, {
    id: "Province",
    label: "استان",
    type: "select",
    enable: !1,
    required: !1,
    options: "provinceOptions",
    events: [{event: "change"}],
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: ""
}, {
    id: "City",
    label: "شهرستان",
    type: "select",
    enable: !1,
    required: !1,
    options: [],
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "Province",
    separator: "address",
    serverValidation: !1,
    groupName: ""
}, {
    id: "RegionType",
    label: "نوع منطقه / محله/ شهرک",
    type: "select",
    enable: !1,
    required: !1,
    options: "regionTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: "AddressDetails"
}, {
    id: "RegionName",
    label: "نام منطقه / محله / شهرک",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 2,
    maxLength: 50,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails",
    fixed: !1
}, {
    id: "MainRoadType",
    label: "نوع معبر اصلی",
    type: "select",
    enable: !1,
    required: !1,
    options: "roadTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: "AddressDetails"
}, {
    id: "MainRoadName",
    label: "نام معبر اصلی",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 3,
    maxLength: 80,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails",
    fixed: !1
}, {
    id: "PrimaryRoadType",
    label: "نوع معبر فرعی ۱",
    type: "select",
    enable: !1,
    required: !1,
    options: "roadTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: "AddressDetails"
}, {
    id: "PrimaryRoadName",
    label: "نام معبر فرعی ۱",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 3,
    maxLength: 80,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails"
}, {
    id: "SecondaryRoadType",
    label: "نوع معبر فرعی ۲",
    type: "select",
    enable: !1,
    required: !1,
    options: "roadTypeOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: "AddressDetails"
}, {
    id: "SecondaryRoadName",
    label: "نام معبر فرعی ۲",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 3,
    maxLength: 80,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails"
}, {
    id: "HouseNumber",
    label: "شماره پلاک",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}, {event: "change"}],
    append: "noHouseNumber",
    minLength: 1,
    maxLength: 6,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails",
    fixed: !1
}, {
    id: "Floor",
    label: "طبقه",
    type: "select",
    enable: !1,
    required: !1,
    options: "floorOptions",
    maxLength: "N/A",
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !1,
    groupName: "AddressDetails"
}, {
    id: "Unit",
    label: "واحد",
    description: "در صورت وجود حتما وارد کنید",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 1,
    maxLength: 10,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails",
    fixed: !1
}, {
    id: "BuildingName",
    label: "نام ساختمان",
    description: "در صورت عدم وجود پلاک، تکمیل این قسمت الزامی‌ست",
    type: "text",
    enable: !1,
    required: !1,
    events: [{event: "validation"}],
    minLength: 1,
    maxLength: 50,
    lengthStatus: !0,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails",
    fixed: !1
}, {
    id: "DoorColor",
    label: "رنگ درب ساختمان",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 2,
    maxLength: 20,
    lengthStatus: !1,
    parent: "",
    separator: "address",
    serverValidation: !0,
    groupName: "AddressDetails",
    fixed: !1
}, {
    id: "Location",
    label: "مختصات ساختمان",
    description: "لطفا موقعیت دقیق خود را بر روی نقشه تعیین کنید",
    type: "map",
    enable: !1,
    required: !1,
    parent: "",
    separator: "Location",
    serverValidation: !1,
    groupName: ""
}, {
    id: "PresentDays",
    label: "روزهای حضور",
    type: "checkbox",
    multiline: !0,
    enable: !1,
    required: !1,
    options: "presentDaysOptions",
    parent: "",
    separator: "submit",
    serverValidation: !1,
    groupName: ""
}, {
    id: "PresentHours",
    label: "ساعتهای حضور",
    type: "checkbox",
    multiline: !0,
    enable: !1,
    required: !1,
    options: "presentHoursOptions",
    parent: "",
    separator: "submit",
    serverValidation: !1,
    groupName: ""
}, {
    id: "DeliveryToSecuirty",
    label: "تحویل نگهبانی شود؟",
    type: "radio",
    checked: !0,
    enable: !1,
    required: !1,
    options: "deliveryToSecuirtyOptions",
    parent: "",
    separator: "submit",
    serverValidation: !1,
    groupName: ""
}, {
    id: "NoticeNationalID",
    label: "هشدار در دست داشتن کارت ملی (تنها هشدار در فرم نشان داده شود)",
    type: "warning",
    enable: !1,
    required: !1,
    maxLength: 50,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "ExtraDeliveryNotes",
    label: "توضیحات جهت تحویل",
    type: "textarea",
    enable: !1,
    required: !1,
    maxLength: 256,
    lengthStatus: !1,
    parent: "",
    separator: "submit",
    serverValidation: !1,
    groupName: ""
}, {
    id: "DiscountCode",
    label: "کد تخفیف",
    description: "در صورت در اختیار داشتن کد تخفیف وارد کنید",
    type: "text",
    enable: !1,
    required: !1,
    minLength: 4,
    maxLength: 20,
    lengthStatus: !1,
    parent: "",
    separator: "submit",
    serverValidation: !1,
    groupName: "",
    fixed: !1
}, {
    id: "BankCardNumber",
    label: "شماره کارت بانکی",
    type: "cardnumber",
    enable: !1,
    required: !1,
    maxLength: 16,
    lengthStatus: !1,
    parent: "",
    separator: "specifications",
    serverValidation: !1,
    groupName: ""
}, {
    id: "PersonalPhoto",
    label: "عکس پرسنلی",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "submit",
    serverValidation: !1,
    groupName: ""
}, {
    id: "OrganizationLogo",
    label: "لوگوی سازمان",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1,
    groupName: ""
}, {
    id: "IdentityCardImage",
    label: "تصویر کارت ملی",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1,
    groupName: ""
}, {
    id: "BirthCertificateImage",
    label: "تصویر شناسنامه",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1,
    groupName: ""
}, {
    id: "DrivingLicenceImage",
    label: "تصویر گواهینامه",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1,
    groupName: ""
}, {
    id: "PassportImage",
    label: "تصویر پاسپورت",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1,
    groupName: ""
}, {
    id: "SelfPortraitImage",
    label: "تصویر احراز  هویت",
    description: "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    type: "image",
    enable: !1,
    required: !1,
    append: "selfPortraitDescription",
    maxLength: 15e6,
    formats: "PDF/JPG/PNG",
    lengthStatus: !1,
    parent: "",
    separator: "authentication",
    serverValidation: !1,
    groupName: ""
},]




var lastSelectefieldtemp = "",
    iframeTag = '<iframe style="border: 0;width: 100%;height: calc(100% - 50px);"><\/iframe>',
    inputs = "#personType,#gender,#nationalCode,#addressStatus,#discount,#recipient,#deliveryTime,#deliveryDay", form,
    formEdited, list = [], stepCount = 0, optionalFields = controlsType, i, item;
GetLastSelectedItems();
for (i in optionalFields) {
    let n = optionalFields[i];
    item = {id: n.id, label: n.label, isUse: !1};
    list.push(item)
}
createStep();
createSelectedObject(1, "IdentityType");
$("#download").click(function () {
    var n = document.createElement("a");
    n.setAttribute("href", "data:text/javascript;charset=utf-8," + encodeURIComponent(formEdited));
    n.setAttribute("download", "myform.js");
    n.style.display = "none";
    document.body.appendChild(n);
    n.click();
    document.body.removeChild(n)
});
$("#template").change(function () {
    theme.template = this.value;
    createFormScript()
});
