var controlsType = [{
    "id": "IdentityType",
    "label": "نوع شخصیت",
    "type": "radio",
    "enable": false,
    "required": false,
    "events": [{
        "event": "myformready",
        // "handler": onIdentityTypeInserted,
        // get target() {
        //     return parentId;
        // }
    }],
    "options": "identityTypeOptions",
    "separator": "specifications",
    "parent": "",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "OrganType",
    "label": "نوع سازمان",
    "type": "radio",
    "enable": false,
    "required": false,
    "options": "organTypeOptions",
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "CompanyType",
    "label": "نوع شرکت",
    "type": "radio",
    "enable": false,
    "required": false,
    "options": "companyTypeOptions",
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "FirstName",
    "label": "نام",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "parent": "",
    "serverValidation": false,
    "separator": "specifications",
    "groupName": "",
    "fixed":false
},
{
    "id": "LastName",
    "label": "نام خانوادگی",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "Gender",
    "label": "جنسیت",
    "type": "radio",
    "enable": false,
    "required": false,
    "options": "genderOptions",
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "CompanyName",
    "label": "نام شرکت/سازمان/موسسه/دانشگاه / وزارتخانه",
    "type": "text",
    "enable": false,
    "required": false,
    "class": "w100",
    "minLength": 3,
    "maxLength": 25,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "BirthDate",
    "label": "تاریخ تولد",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 10,
    "maxLength": 10,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "RegNumber",
    "label": "شماره شناسنامه",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 1,
    "maxLength": 10,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "Marriage",
    "label": "وضعیت تاهل",
    "type": "radio",
    "enable": false,
    "required": false,
    "class": 'w50',
    "options": "marriageOptions",
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "SpouseName",
    "label": "نام و نام خانوادگی همسر",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "MilitaryService",
    "label": "وضعیت سربازی",
    "type": "radio",
    "enable": false,
    "required": false,
    "options": "militaryServiceOptions",
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{

    "id": "Children",
    "label": "تعداد فرزندان",
    "type": "select",
    "enable": false,
    "required": false,
    "serverValidation": false,
    "options": "",
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
},
{
    "id": "NationalID",
    "label": "کد ملی",
    "type": "masktext",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": nationalIDValidation
    }],
    "class": "ltr",
    "length": 10,
    "lengthStatus": true,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},

{
    "id": "NationalIDCode",
    "label": "کد شناسه ملی",
    "type": "masktext",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": nationalIDValidation
    }],
    "class": "ltr",
    "length": 11,
    "lengthStatus": true,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},
{
    "id": "PassportNumber",
    "label": "شماره پاسپورت",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 10,
    "maxLength": 11,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},
{
    "id": "DrivingLicence",
    "label": "شماره گواهینامه",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 4,
    "maxLength": 10,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},
{
    "id": "FatherName",
    "label": "نام پدر",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "BirthPlace",
    "label": "محل تولد",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 2,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "RegIssuePlace",
    "label": "محل صدور شناسنامه",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 2,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "DirectorFirstName",
    "label": "نام مدیرعامل /رئیس",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "DirectorLastName",
    "label": "نام خانوادگی مدیر عامل / رئیس",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "parent": "",
    "separator": "specifications",
    "lengthStatus": false,
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "AgentFirstName",
    "label": "نام شخص مسوول",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "AgentLastName",
    "label": "نام خانوادگی شخص مسوول",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 3,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "PhoneNumber",
    "label": "تلفن ثابت",
    "type": "phone",
    "enable": false,
    "required": false,
    "maxLength": "15 - No CountryCode",
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "CellNumber",
    "label": "شماره همراه",
    "description": "مثال: 09123456789",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": phoneValidation
    }],
    "class": "ltr",
    "minLength": 11,
    "maxLength": 11,
    "lengthStatus": true,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},
{
    "id": "AgentCellNumber",
    "label": "شماره همراه شخص مسوول",
    "description": "مثال: 09123456789",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": phoneValidation
    }],
    "class": "ltr",
    "minLength": 11,
    "maxLength": 11,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},
{
    "id": "Email",
    "label": "ایمیل ",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": emailValidation
    }],
    "class": 'ltr',
    "minLength": 5,
    "maxLength": 50,
    "lengthStatus": true,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "AgentEmail",
    "label": "ایمیل شخص مسوول",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": emailValidation
    }],
    "minLength": 5,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "PREmail",
    "label": "ایمیل روابط عمومی ",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": emailValidation
    }],
    "minLength": 5,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "AddressStatus",
    "label": "وضعیت نشانی",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "addressStatusOptions",
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "PostCode",
    "label": "کد پستی",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": postCodeValidation
    }],
    "class": "ltr",
    "minLength": 10,
    "maxLength": 10,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": "",
    "fixed":true
},
{
    "id": "Province",
    "label": "استان",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "provinceOptions",
    "events": [{
        "event": "change",
        // "handler": provinceChanged
    }],
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "City",
    "label": "شهرستان",
    "type": "select",
    "enable": false,
    "required": false,
    "options": [],
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "Province",
    "separator": "address",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "RegionType",
    "label": "نوع منطقه / محله/ شهرک",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "regionTypeOptions",
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": "AddressDetails"
},
{
    "id": "RegionName",
    "label": "نام منطقه / محله / شهرک",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": regionValidation
    }],
    "minLength": 2,
    "maxLength": 50,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails",
    "fixed":false
},
{
    "id": "MainRoadType",
    "label": "نوع معبر اصلی",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "roadTypeOptions",
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": "AddressDetails"
},
{
    "id": "MainRoadName",
    "label": "نام معبر اصلی",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": roadValidation
    }],
    "minLength": 3,
    "maxLength": 80,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails",
    "fixed":false
},
{
    "id": "PrimaryRoadType",
    "label": "نوع معبر فرعی ۱",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "roadTypeOptions",
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": "AddressDetails"
},
{
    "id": "PrimaryRoadName",
    "label": "نام معبر فرعی ۱",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": roadValidation
    }],
    "minLength": 3,
    "maxLength": 80,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails"
},
{
    "id": "SecondaryRoadType",
    "label": "نوع معبر فرعی ۲",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "roadTypeOptions",
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": "AddressDetails"
},
{
    "id": "SecondaryRoadName",
    "label": "نام معبر فرعی ۲",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": roadValidation
    }],
    "minLength": 3,
    "maxLength": 80,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails"
},
{
    "id": "HouseNumber",
    "label": "شماره پلاک",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": houseNumberValidation
    }, {
        "event": "change",
        // "target": prefex + "noHouseNumber",
        //     "handler": onNoHouseNumberChange
    }],
    "append": "noHouseNumber",
    "minLength": 1,
    "maxLength": 6,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails",
    "fixed":false
},
{
    "id": "Floor",
    "label": "طبقه",
    "type": "select",
    "enable": false,
    "required": false,
    "options": "floorOptions",
    "maxLength": "N/A",
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": false,
    "groupName": "AddressDetails"
},
{
    "id": "Unit",
    "label": "واحد",
    "description": "در صورت وجود حتما وارد کنید",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation",
        // "handler": unitValidation
    }],
    "minLength": 1,
    "maxLength": 10,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails",
    "fixed":false
},
{
    "id": "BuildingName",
    "label": "نام ساختمان",
    "description": "در صورت عدم وجود پلاک، تکمیل این قسمت الزامی‌ست",
    "type": "text",
    "enable": false,
    "required": false,
    "events": [{
        "event": "validation" //,
        // "handler": buildingNameValidation
    }],
    "minLength": 1,
    "maxLength": 50,
    "lengthStatus": true,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails",
    "fixed":false
},
{
    "id": "DoorColor",
    "label": "رنگ درب ساختمان",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 2,
    "maxLength": 20,
    "lengthStatus": false,
    "parent": "",
    "separator": "address",
    "serverValidation": true,
    "groupName": "AddressDetails",
    "fixed":false
},
{
    "id": "Location",
    "label": "مختصات ساختمان",
    "description": "لطفا موقعیت دقیق خود را بر روی نقشه تعیین کنید",
    "type": "map",
    "enable": false,
    "required": false,
    "parent": "",
    "separator": "Location",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "PresentDays",
    "label": "روزهای حضور",
    "type": "checkbox",
    "multiline": true,
    "enable": false,
    "required": false,
    "options": "presentDaysOptions",
    "parent": "",
    "separator": "submit",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "PresentHours",
    "label": "ساعتهای حضور",
    "type": "checkbox",
    "multiline": true,
    "enable": false,
    "required": false,
    "options": "presentHoursOptions",
    "parent": "",
    "separator": "submit",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "DeliveryToSecuirty",
    "label": "تحویل نگهبانی شود؟",
    "type": "radio",
    "checked": true,
    "enable": false,
    "required": false,
    "options": "deliveryToSecuirtyOptions",
    "parent": "",
    "separator": "submit",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "NoticeNationalID",
    "label": "هشدار در دست داشتن کارت ملی (تنها هشدار در فرم نشان داده شود)",
    "type": "warning",
    "enable": false,
    "required": false,
    "maxLength": 50,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "ExtraDeliveryNotes",
    "label": "توضیحات جهت تحویل",
    "type": "textarea",
    "enable": false,
    "required": false,
    "maxLength": 256,
    "lengthStatus": false,
    "parent": "",
    "separator": "submit",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "DiscountCode",
    "label": "کد تخفیف",
    "description": "در صورت در اختیار داشتن کد تخفیف وارد کنید",
    "type": "text",
    "enable": false,
    "required": false,
    "minLength": 4,
    "maxLength": 20,
    "lengthStatus": false,
    "parent": "",
    "separator": "submit",
    "serverValidation": false,
    "groupName": "",
    "fixed":false
},
{
    "id": "BankCardNumber",
    "label": "شماره کارت بانکی",
    "type": "cardnumber",
    "enable": false,
    "required": false,
    "maxLength": 16,
    "lengthStatus": false,
    "parent": "",
    "separator": "specifications",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "PersonalPhoto",
    "label": "عکس پرسنلی",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "submit",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "OrganizationLogo",
    "label": "لوگوی سازمان",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "authentication",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "IdentityCardImage",
    "label": "تصویر کارت ملی",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "authentication",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "BirthCertificateImage",
    "label": "تصویر شناسنامه",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "authentication",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "DrivingLicenceImage",
    "label": "تصویر گواهینامه",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "authentication",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "PassportImage",
    "label": "تصویر پاسپورت",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "authentication",
    "serverValidation": false,
    "groupName": ""
},
{
    "id": "SelfPortraitImage",
    "label": "تصویر احراز  هویت",
    "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
    "type": "image",
    "enable": false,
    "required": false,
    "append": "selfPortraitDescription",
    "maxLength": 15000000,
    "formats": "PDF/JPG/PNG",
    "lengthStatus": false,
    "parent": "",
    "separator": "authentication",
    "serverValidation": false,
    "groupName": ""
},

];




//  <Field Options>.........................................


function selfPortraitDescription() {
var desc = document.createElement("DIV");
desc.className = prefex + 'selfportrait-desc';

var div1 = document.createElement("DIV");
var div2 = document.createElement("DIV");

var code = document.createElement("DIV");
code.className = 'code';
code.innerHTML = '4542';

var help = document.createElement("SPAN");
help.className = 'help';
help.innerHTML = "لطفا کد 4 رقمی را بصورت بزرگ و خوانا روی یک برگه سفید نوشته و در کنار صورت خود نگهدارید. سپس یک عکس سلفی از خود به همراه این برگه بگیرید و در اینجا آپلود نمایید. دقت نمایید صورت شما شفاف، بدون عینک و یا گریم و کلاه باشد.";

var image = document.createElement("DIV");
image.className = 'image';

div1.innerHTML = code.outerHTML + help.outerHTML;
div2.innerHTML = image.outerHTML;
desc.innerHTML = div1.outerHTML + div2.outerHTML;

return desc;
}


function addressStatusOptions() {
return [{
        label: 'محل دائمی سکونت | مالک هستم | بیش از دو سال در این مکان هستم',
        value: 1
    },
    {
        label: 'محل دائمی سکونت | مالک هستم | بیش از یک سال و کمتر از دو سال در این مکان هستم',
        value: 2
    },
    {
        label: 'محل دائمی سکونت | مالک هستم | به زودی تغییر مکان می‌دهم',
        value: 3
    },
    {
        label: 'محل دائمی سکونت | مستاجر هستم | بیش از یک سال در این مکان هستم',
        value: 4
    },
    {
        label: 'محل دائمی سکونت | مستاجر هستم | کمتر از یک سال در این مکان هستم',
        value: 5
    },
    {
        label: 'محل موقت سکونت | در ایران ساکن نیستم و ساکن کشور دیگری هستم',
        value: 6
    },
    {
        label: 'محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم',
        value: 7
    },
    {
        label: 'محل موقت سکونت | نه مالک و نه مستاجر هستم | به زودری تغییر مکان می‌دهم',
        value: 8
    },
    {
        label: 'محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم',
        value: 9
    },
    {
        label: 'محل دائمی کار و تجارت | مالک یا مستاجر هستم | به تازگی در این مکان هستم (کمتر از دو ماه)',
        value: 10
    },
    {
        label: 'محل دائمی کار و تجارت | مالک هستم | بیش از یک سال در این مکان هستم',
        value: 11
    },
    {
        label: 'محل دائمی کار و تجارت | مالک هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)',
        value: 12
    },
    {
        label: 'محل دائمی کار و تجارت | مستاجر هستم | بیش از یک سال در این مکان هستم',
        value: 13
    },
    {
        label: 'محل دائمی کار و تجارت | مستاجر هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)',
        value: 14
    }
]
}

function identityTypeOptions() {
return [{
        label: 'حقیقی',
        value: 'natural'
    },
    {
        label: 'حقوقی',
        value: 'legal'
    }
]
}

function genderOptions() {
return [{
        label: 'زن',
        value: 'female'
    },
    {
        label: 'مرد',
        value: 'male'
    }
]
}

function provinceOptions() {
return [{
    "id": 3,
    "label": "آذربایجان شرقی",
    "value": "آذربایجان شرقی"
}, {
    "id": 16,
    "label": "آذربایجان غربی",
    "value": "آذربایجان غربی"
}, {
    "id": 15,
    "label": "اردبیل",
    "value": "اردبیل"
}, {
    "id": 6,
    "label": "اصفهان",
    "value": "اصفهان"
}, {
    "id": 31,
    "label": "البرز",
    "value": "البرز"
}, {
    "id": 27,
    "label": "ایلام",
    "value": "ایلام"
}, {
    "id": 21,
    "label": "بوشهر",
    "value": "بوشهر"
}, {
    "id": 1,
    "label": "تهران",
    "value": "تهران"
}, {
    "id": 24,
    "label": "چهارمحال و بختیاری",
    "value": "چهارمحال و بختیاری"
}, {
    "id": 30,
    "label": "خراسان جنوبی",
    "value": "خراسان جنوبی"
}, {
    "id": 7,
    "label": "خراسان رضوی",
    "value": "خراسان رضوی"
}, {
    "id": 29,
    "label": "خراسان شمالی",
    "value": "خراسان شمالی"
}, {
    "id": 4,
    "label": "خوزستان",
    "value": "خوزستان"
}, {
    "id": 12,
    "label": "زنجان",
    "value": "زنجان"
}, {
    "id": 9,
    "label": "سمنان",
    "value": "سمنان"
}, {
    "id": 26,
    "label": "سیستان و بلوچستان",
    "value": "سیستان و بلوچستان"
}, {
    "id": 5,
    "label": "فارس",
    "value": "فارس"
}, {
    "id": 8,
    "label": "قزوین",
    "value": "قزوین"
}, {
    "id": 10,
    "label": "قم",
    "value": "قم"
}, {
    "id": 18,
    "label": "کردستان",
    "value": "کردستان"
}, {
    "id": 22,
    "label": "کرمان",
    "value": "کرمان"
}, {
    "id": 19,
    "label": "کرمانشاه",
    "value": "کرمانشاه"
}, {
    "id": 0,
    "label": "کشورهای خارجه",
    "value": "کشورهای خارجه"
}, {
    "id": 28,
    "label": "کهگیلویه و بویراحمد",
    "value": "کهگیلویه و بویراحمد"
}, {
    "id": 14,
    "label": "گلستان",
    "value": "گلستان"
}, {
    "id": 2,
    "label": "گیلان",
    "value": "گیلان"
}, {
    "id": 20,
    "label": "لرستان",
    "value": "لرستان"
}, {
    "id": 13,
    "label": "مازندران",
    "value": "مازندران"
}, {
    "id": 11,
    "label": "مرکزی",
    "value": "مرکزی"
}, {
    "id": 23,
    "label": "هرمزگان",
    "value": "هرمزگان"
}, {
    "id": 17,
    "label": "همدان",
    "value": "همدان"
}, {
    "id": 25,
    "label": "یزد",
    "value": "یزد"
}];
}

function regionTypeOptions() {
return [{
        "id": "1",
        "label": "منطقه",
        "value": "district"
    },
    {
        "id": "2",
        "label": "محله",
        "value": "neighborhood"
    },
    {
        "id": "3",
        "label": "دهستان",
        "value": "rural district"
    },
    {
        "id": "4",
        "label": "شهرک",
        "value": "township"
    },
    {
        "id": "5",
        "label": "منطقه شهرداری",
        "value": "municipality region"
    }
]
}

function roadTypeOptions() {
return [{
        "id": "1",
        "label": "خیابان",
        "value": "street"
    },
    {
        "id": "2",
        "label": "کوچه",
        "value": "alley"
    },
    {
        "id": "3",
        "label": "بنبست",
        "value": "dead end"
    },
    {
        "id": "4",
        "label": "بلوار",
        "value": "boulvard"
    },
    {
        "id": "5",
        "label": " بزرگراه",
        "value": "highway"
    },
    {
        "id": "6",
        "label": "آزادراه",
        "value": "freeway"
    },
    {
        "id": "7",
        "label": "میدان",
        "value": "square"
    },
    {
        "id": "8",
        "label": "پل",
        "value": "bridge"
    },
    {
        "id": "9",
        "label": "زیرگذر",
        "value": "underpass"
    },
    {
        "id": "10",
        "label": "کنارگذر",
        "value": "Bypass"
    },
    {
        "id": "11",
        "label": "جاده",
        "value": "road"
    },
    {
        "id": "12",
        "label": "مسیر اختصاصی",
        "value": "assigned route"
    },
    {
        "id": "13",
        "label": "کیلومتر",
        "value": "kilometer"
    },
    {
        "id": "14",
        "label": "چهارراه",
        "value": "fourway"
    },
    {
        "id": "15",
        "label": "سه راه",
        "value": "threeway"
    },
    {
        "id": "16",
        "label": "دو راه",
        "value": "dualway"
    }
]
}

function floorOptions() {
var floors = [];
for (var i = 1; i < 41; i++) {
    floors.push({
        "id": i,
        "label": "طبقه " + i,
        "value": i
    });
}
return floors;
}

function presentDaysOptions() {
return [{
        label: 'شنبه',
        value: 'SAT'
    },
    {
        label: 'یکشنبه',
        value: 'SUN'
    },
    {
        label: 'دوشنبه',
        value: 'MON'
    },
    {
        label: 'سه‌شنبه',
        value: 'TUE'
    },
    {
        label: 'چهارشنبه',
        value: 'WED'
    },
    {
        label: 'پنج‌شنبه',
        value: 'THU'
    },
    {
        label: 'جمعه',
        value: 'FRI'
    }
]
}

function presentHoursOptions() {
return [{
        label: '۹ الی ۱۲',
        value: '09-12'
    },
    {
        label: '۱۲ الی ۱۵',
        value: '12-15'
    },
    {
        label: '۱۵ الی ۱۸',
        value: '15-18'
    },
    {
        label: '۱۸ الی ۲۱',
        value: '18-21'
    }
]
}

function deliveryToSecuirtyOptions() {
return [{
        label: 'بله',
        value: '1'
    },
    {
        label: 'خیر',
        value: '0'
    }
]
}

function organTypeOptions() {
return [{
        label: 'دولتی',
        value: 'government'
    },
    {
        label: 'خصوصی',
        value: 'private'
    },
    {
        label: 'نیمه‌خصوصی',
        value: 'semiprivate'
    }
]
}

function companyTypeOptions() {
return [{
        label: 'مسئولیت محدود',
        value: 'limited'
    },
    {
        label: 'سهامی خاص',
        value: 'private'
    },
    {
        label: 'سهامی عام',
        value: 'public'
    },
    {
        label: 'غیر انتفاعی',
        value: 'nonprofit'
    }
]
}

function marriageOptions() {
return [{
        label: 'مجرد',
        value: 'single'
    },
    {
        label: 'متاهل',
        value: 'married'
    },
    {
        label: 'جداشده',
        value: 'divorced'
    }
]
}

function militaryServiceOptions() {
return [{
        label: 'معافیت دائم',
        value: '1'
    },
    {
        label: 'مشمول',
        value: '2'
    },
    {
        label: 'مشغول خدمت',
        value: '3'
    },
    {
        label: 'غایب',
        value: '4'
    }
]
}
//  </Field Options>........................................

function noHouseNumber() {
var div = document.createElement('DIV');
div.classList.add(prefex + 'group-append', prefex + 'form-checkbox');

var label = document.createElement('LABEL');
label.classList.add(prefex + 'group-text');

var input = document.createElement('INPUT');
input.setAttribute('type', 'checkbox');
input.name = 'noHouseNumber';
input.id = prefex + 'noHouseNumber';

label.innerHTML = input.outerHTML + '<span class="check"></span>' + 'بدون پلاک';
div.appendChild(label);

return div;
}


function castFieldOptions(option) {

switch (option) {
    case "identityTypeOptions":
        return identityTypeOptions();
    case "organTypeOptions":
        return organTypeOptions();

    case "companyTypeOptions":
        return companyTypeOptions();

    case "genderOptions":
        return genderOptions();

    case "marriageOptions":
        return marriageOptions();

    case "militaryServiceOptions":
        return militaryServiceOptions();

    case "addressStatusOptions":
        return addressStatusOptions();

    case "provinceOptions":
        return provinceOptions();

    case "regionTypeOptions":
        return regionTypeOptions();

    case "roadTypeOptions":
        return roadTypeOptions();

    case "floorOptions":
        return floorOptions();

    case "presentDaysOptions":
        return presentDaysOptions();

    case "presentHoursOptions":
        return presentHoursOptions();

    case "deliveryToSecuirtyOptions":
        return deliveryToSecuirtyOptions();

    default:
        return null
        // code block
}
}




var lastSelectefieldtemp = '[{"id":"IdentityType","Required":false,"separator":"1","separatorName":"هویتی"},{"id":"OrganType","Required":false,"separator":"1","separatorName":"هویتی"},{"id":"CompanyType","Required":false,"separator":"1","separatorName":"هویتی"},{"id":"FirstName","Required":false,"min":"1","max":"32","separator":"1","separatorName":"هویتی"},{"id":"LastName","Required":false,"min":"1","max":"32","separator":"1","separatorName":"هویتی"},{"id":"Gender","Required":false,"separator":"1","separatorName":"هویتی"},{"id":"CompanyName","Required":false,"min":"1","max":"32","separator":"1","separatorName":"هویتی"},{"id":"BirthDate","Required":false,"min":"1","max":"32","separator":"1","separatorName":"هویتی"},{"id":"Marriage","Required":false,"separator":"1","separatorName":"هویتی"},{"id":"SpouseName","Required":false,"min":"1","max":"32","separator":"1","separatorName":"هویتی"},{"id":"MilitaryService","Required":false,"separator":"1","separatorName":"هویتی"},{"id":"NationalID","Required":false,"min":"1","max":"32","separator":"3","separatorName":"تصاویر"},{"id":"Province","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"City","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"RegionType","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"RegionName","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"MainRoadType","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"MainRoadName","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"PrimaryRoadType","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"PrimaryRoadName","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"SecondaryRoadType","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"SecondaryRoadName","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"HouseNumber","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"Floor","Required":false,"separator":"2","separatorName":"نشانی"},{"id":"Unit","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"BuildingName","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"DoorColor","Required":false,"min":"1","max":"32","separator":"2","separatorName":"نشانی"},{"id":"IdentityCardImage","Required":false,"formats":["PDF","JPG","PNG"],"separator":"3","separatorName":"تصاویر"},{"id":"BirthCertificateImage","Required":false,"formats":["PDF","JPG","PNG"],"separator":"3","separatorName":"تصاویر"},{"id":"DrivingLicenceImage","Required":false,"formats":["PDF","JPG","PNG"],"separator":"3","separatorName":"تصاویر"}]';




//iframeLoad();

var iframeTag = '<iframe style="border: 0;width: 100%;height: calc(100% - 50px);"></iframe>';
var inputs = "#personType,#gender,#nationalCode,#addressStatus,#discount,#recipient,#deliveryTime,#deliveryDay";

var form;
var formEdited;

var list = [];
var stepCount = 0;

var optionalFields = controlsType;
GetLastSelectedItems();



for (var i in optionalFields) {
    let f = optionalFields[i];
    var item = {
        id: f.id,
        label: f.label,
        isUse: false
    };
    list.push(item);

}


function GetLastSelectedItems() {
    $.ajax({
        url: '/api/v1/listOfForms?id=' + formID,

        type: 'GET',
        headers: {
            'Authorization': 'Token 0b5ed24810a9ea2fca2a798b1c4a3d574600691e',
            // 'X-CSRFToken': getCookie('csrftoken'),
            'Content-Type': 'application/x-www-form-urlencoded'

        },

        // data: {
        //     "name": "تست فرم",
        //     "description": "فرممم تستی",
        //     "form_type": "0",
        //     "font_type": "0",
        //     "background_display": "0",
        //     "json_form": JSON.stringify(selectedFields),
        // },

        success: function (data) {
            
            lastSelectefieldtemp = data.json_form;

            createLastSelectefield();

            
        },
        error: function (error) {
            // console.log(error.responseText)
        }
    });
}


function createStep() {

    if (list != null) {

        stepCount++;

        var wrapper = document.createElement('DIV');
        wrapper.className = "col-12 col-lg-6";
        wrapper.id = 'step' + stepCount;

        //<div class="title col-12"><i class="fas fa-chevron-left" aria-hidden="true"></i>مشخصات

        var line = document.createElement('DIV');
        line.className = 'title col-12';
        line.innerHTML = 'گام شماره ' + stepCount;

        var li = document.createElement('I');

        li.className = 'fas fa-chevron-left';
        li.setAttribute('aria-hidden', true);


        line.appendChild(li);


        var stepName = 'stepName' + stepCount;
        //        var textts = '<label for=\"' + stepName + '\'">نام گام:</label> <input type="text" id=\"' + stepName + '\'" name=\"' + stepName + '\">';


        var label = document.createElement('LABEL');
        label.setAttribute('for', stepName);
        label.className = 'mfl-input col-2 col-sm-1 col-md-1';
        label.style = 'display: inline !important; margin-right: 5px !important;';
        label.innerHTML = '       نام گام :';


        var stepTextBox = document.createElement('INPUT');
        stepTextBox.id = stepName;
        stepTextBox.type = 'text';
        stepTextBox.className = 'mfl-input col-6 col-sm-5 col-md-5';
        stepTextBox.style = 'display: inline !important; margin-right: 5px !important;';




        line.appendChild(label);
        line.appendChild(stepTextBox);



        // deleteButton.className = 'deleteButton btn btn-danger'; //btn btn-danger
        // deleteButton.setAttribute('value', 'حذف این گام');


        //line.innerHTML += textBoxName;
        wrapper.appendChild(line);


        var select = document.createElement('select');
        select.id = 'dropdown' + stepCount;
        // input.type = 'checkbox';
        select.className = "col-6 col-sm-5 col-md-5 mfl-input";
        select.setAttribute("data-stepIndex", stepCount); //setter
        select.setAttribute("style", "font-size: small !important;"); //setter


        select.addEventListener('change', function () {

            var stepindex = select.getAttribute("data-stepIndex"); //$(this).attr('data-stepIndex')
            var selectedvalue = $(this).children("option:selected").val();


            if (checkExistObject(stepindex, selectedvalue) != true) {
                createSelectedObject(stepindex, selectedvalue);
            }
        });

        var str = '<option  value="firstIten"> لطفاً انتخاب نمایید</option>';

        for (x in list) {
            //if (hop.call(list, x)) {
            let f = list[x];
            if (f.isUse == false) {

                str += '<option  value="' + f.id + '">' + f.label + '</option>';
            }


            // }
        }


        select.innerHTML += str;

        wrapper.appendChild(select);

        if (stepCount != 1) {

            var deleteButton = document.createElement('INPUT');
            deleteButton.id = 'button' + stepCount;
            deleteButton.type = 'button';
            deleteButton.className = 'deleteButton btn btn-danger'; //btn btn-danger
            //deleteButton.className = 'deleteButton btn btn-danger col-2'; //btn btn-danger
            deleteButton.style = 'padding: 5px 10 5px 22px !important;margin-right: 5px !important;';


            deleteButton.setAttribute('value', 'حذف این گام');

            deleteButton.innerHTML = '<i class="fas fa-window-close" aria-hidden="true"></i>';


            deleteButton.setAttribute("data-stepIndex", stepCount); //setter
            deleteButton.addEventListener('click', function () {

                var stepindex = deleteButton.getAttribute("data-stepIndex"); //$(this).attr('data-stepIndex')

                //get child

                var childsWrapper = document.getElementById('childWrapper' + stepindex);

                if (childsWrapper.childElementCount != 0) {


                    //set isused

                    for (var j = 0; j < childsWrapper.childElementCount; j++) {

                        var element = childsWrapper.children[j];

                        var elId = element.getAttribute("data-fieldId");

                        for (x in list) {
                            //if (hop.call(list, x)) {
                            let q = list[x];
                            if (q.id == elId) {
                                q.isUse = false;
                                break;
                            }
                        }
                    }




                    refreshDropdowns();
                }
                //remove

                childsWrapper.remove();

                var stepWrapper = document.getElementById('step' + stepindex);
                stepWrapper.remove();
                stepCount--;


            });

            wrapper.appendChild(deleteButton);
        }


        var br = document.createElement('br');

        wrapper.appendChild(br);


        var childWrapper = document.createElement('DIV');
        childWrapper.id = 'childWrapper' + stepCount;
        childWrapper.className = 'row'; //"col-12 col-lg-6";

        wrapper.appendChild(childWrapper);




        // wrapper.appendChild(field);

        wrapper.className = "row optionalFields"; //

        $('#' + "bodyMainPage").append(wrapper);

        var oldAddbutton = document.getElementById('addButton');
        if (oldAddbutton != undefined) {
            oldAddbutton.remove();
        }

        var addButton = document.createElement('INPUT');
        addButton.id = 'addButton';
        addButton.type = 'button';


        addButton.className = 'btn btn-success';
        addButton.setAttribute('value', 'ایجاد گام جدید');
        addButton.setAttribute("style", "margin-top: 20px !important;"); //setter
        addButton.addEventListener('click', checkCreateStep);
        $('#' + "bodyMainPage").append(addButton);


    }
}

function checkExistObject(stepindex, selectedvalue) {

    var object = document.getElementById(selectedvalue);

    if (object == undefined) {
        return false;
    } else {
        return true;
    }

}



function checkCreateStep() {

    var childWrapper = document.getElementById('childWrapper' + stepCount);

    if (childWrapper.childElementCount == 0) {
        // alert('لطفا فیلد های گام قبلی را انتخاب نمایید');

    } else {
        createStep();
    }

}

createStep();
createSelectedObject(1, "IdentityType");



function createSelectedObject(stepindex, selectedvalue) {


    for (var i in optionalFields) {
        let f = optionalFields[i];



        if (f.id != selectedvalue) {
            continue;
        }


        if (f.parent != "") {

            var parent = document.getElementById(f.parent);
            if (parent == undefined) {
                return false;
            }
        }


        if (f.groupName != "") {
            createSelectedGroup(stepindex, f.groupName);
            return;
        }



        // for (x in list) {
        //if (hop.call(list, x)) {
        let jk = list[i];

        jk.isUse = true;
        //}

        var wrapper = document.createElement('DIV');
        wrapper.id = 'wrapper' + f.id;
        wrapper.className = "col-12 col-lg-5";
        wrapper.setAttribute("data-fieldId", f.id); //setter
        wrapper.setAttribute("data-stepIndex", stepindex); //setter

        wrapper.setAttribute("style", "margin-right: 10px !important;"); //setter


        

        var field = document.createElement('DIV');
        field.className = 'field';
        var input = document.createElement('INPUT');
        input.id = f.id;
        input.type = 'checkbox';
        input.setAttribute('checked', '');
        //input.setAttribute('parent', f.parent);
        //$( input ).data( "parent", f.parent );

        var checkIcons = '<i class="checked far fa-check-square"></i><i class="not-checked far fa-square"></i>';
        var label = document.createElement('LABEL');
        label.setAttribute('for', input.id);
        label.innerHTML = f.label;
        var setBtn = document.createElement('A');
        setBtn.setAttribute('href', '#' + input.id + 'Settings');
        setBtn.setAttribute('data-toggle', 'collapse');
        setBtn.className = 'field-settings-btn';
        setBtn.innerHTML = '<small><i class="fas fa-cog"></i>تنظیمات</small>';
        var settings = document.createElement('DIV');
        settings.id = input.id + 'Settings';
        settings.className = 'collapse field-settings';
        var required = document.createElement('DIV');
        required.className = "custom-control custom-switch field-required";
        var rInput = '<input id="' + (input.id + 'Required') + '" type="checkbox" class="custom-control-input">';
        var rLabel = '<label for="' + (input.id + 'Required') + '" class="custom-control-label">اجباری</label>';
        required.innerHTML = rInput + rLabel;
        settings.appendChild(required);
        var isReadonly = f.fixed ? ' readonly' : '';
        if (f.type == 'text') {
            

            var min = document.createElement('DIV');
            min.innerHTML = '<span class="field-min"><label>حداقل تعداد کاراکتر:</label><input id="' + f.id + 'min' + '" "type="number" value="' + f.minLength + '"' + isReadonly + '></span>';
            var max = document.createElement('DIV');
            max.innerHTML = '<span class="field-max"><label>حداکثر تعداد کاراکتر:</label><input id="' + f.id + 'max' + '" type="number" value="' + f.maxLength + '"' + isReadonly + '></span>';
            settings.appendChild(min);
            settings.appendChild(max);
        } else if (f.type == 'image') {
            var size = document.createElement('DIV');
            size.innerHTML = '<span class="field-max"><label>حداکثرحجم فایل:</label><input id="' + f.id + 'size' + '" type="number" value="2"> <label class="descriptionLabel" style="color:red; text-align:left;">MB</label></span>';


            var formats = f.formats.split("/");
            var format = document.createElement('DIV');
            var temptext = "";
            for (var k in formats) {
                let formatField = formats[k];
                var mimeTypeName = f.id + 'fileFormat' + formatField;
                temptext += '<input id="' + mimeTypeName + '" name="' + mimeTypeName + '" style="color:white" type="checkbox"  checked="checked"  value="' + formatField + '"> <label  class="descriptionLabel"  for="' + mimeTypeName + '"> ' + formatField + '</label>';


            }

            format.innerHTML = '<span class="field-max"><label>فرمت فایل:</label>' + temptext + '</span>';


            settings.appendChild(size);
            settings.appendChild(format);


        } else if (f.type == 'masktext') {
            var len = document.createElement('DIV');
            len.innerHTML = '<span class="field-min"><label> تعداد کاراکتر:</label><input id="' + f.id + 'len' + '" "type="number" value="' + f.length + '"' + isReadonly + '></span>';

            settings.appendChild(len);


        }
        field.innerHTML = input.outerHTML + checkIcons + label.outerHTML + setBtn.outerHTML + settings.outerHTML;
        wrapper.appendChild(field);






        var sprator = $('#' + f.separator);




        $('#' + 'childWrapper' + stepindex).append(wrapper);


        if (f.parent == "") {
            $("#" + f.id).attr("data-parent", '_'); //setter

        } else {
            $("#" + f.id).attr("data-parent", f.parent); //setter
        }

        if (f.groupName == "") {
            $("#" + f.id).attr("data-groupname", '_'); //setter

        } else {
            $("#" + f.id).attr("data-groupname", f.groupName); //setter
        }


        $("#" + f.id).change(function (el) {


            checkchanged(el);



        });



    }


    refreshDropdowns();

}




function createSelectedGroup(stepindex, selectedvalue) {


    for (var i in optionalFields) {
        let f = optionalFields[i];



        if (f.groupName != selectedvalue) {
            continue;
        }


        if (f.parent != "") {

            var parent = document.getElementById(f.parent);
            if (parent == undefined) {
                return false;
            }
        }



        // for (x in list) {
        //if (hop.call(list, x)) {
        let jk = list[i];

        jk.isUse = true;
        //}

        var wrapper = document.createElement('DIV');
        wrapper.id = 'wrapper' + f.id;
        wrapper.className = "col-12 col-lg-6";
        wrapper.setAttribute("data-fieldId", f.id); //setter
        wrapper.setAttribute("data-stepIndex", stepindex); //setter
        var field = document.createElement('DIV');
        field.className = 'field';
        var input = document.createElement('INPUT');
        input.id = f.id;
        input.type = 'checkbox';
        input.setAttribute('checked', '');
        //input.setAttribute('parent', f.parent);
        //$( input ).data( "parent", f.parent );

        var checkIcons = '<i class="checked far fa-check-square"></i><i class="not-checked far fa-square"></i>';
        var label = document.createElement('LABEL');
        label.setAttribute('for', input.id);
        label.innerHTML = f.label;
        var setBtn = document.createElement('A');
        setBtn.setAttribute('href', '#' + input.id + 'Settings');
        setBtn.setAttribute('data-toggle', 'collapse');
        setBtn.className = 'field-settings-btn';
        setBtn.innerHTML = '<small><i class="fas fa-cog"></i>تنظیمات</small>';
        var settings = document.createElement('DIV');
        settings.id = input.id + 'Settings';
        settings.className = 'collapse field-settings';
        var required = document.createElement('DIV');
        required.className = "custom-control custom-switch field-required";
        var rInput = '<input id="' + (input.id + 'Required') + '" type="checkbox" class="custom-control-input">';
        var rLabel = '<label for="' + (input.id + 'Required') + '" class="custom-control-label">اجباری</label>';
        required.innerHTML = rInput + rLabel;
        settings.appendChild(required);       

        var isReadonly = f.fixed ? ' readonly' : '';
        if (f.type == 'text') {
            

            var min = document.createElement('DIV');
            min.innerHTML = '<span class="field-min"><label>حداقل تعداد کاراکتر:</label><input id="' + f.id + 'min' + '" "type="number" value="' + f.minLength + '"' + isReadonly + '></span>';
            var max = document.createElement('DIV');
            max.innerHTML = '<span class="field-max"><label>حداکثر تعداد کاراکتر:</label><input id="' + f.id + 'max' + '" type="number" value="' + f.maxLength + '"' + isReadonly + '></span>';
            settings.appendChild(min);
            settings.appendChild(max);
        } else if (f.type == 'image') {
            var size = document.createElement('DIV');
            size.innerHTML = '<span class="field-max"><label>حداکثرحجم فایل:</label><input id="' + f.id + 'size' + '" type="number" value="2"> <label class="descriptionLabel" style="color:red; text-align:left;">MB</label></span>';


            var formats = f.formats.split("/");
            var format = document.createElement('DIV');
            var temptext = "";
            for (var k in formats) {
                let formatField = formats[k];
                var mimeTypeName = f.id + 'fileFormat' + formatField;
                temptext += '<input id="' + mimeTypeName + '" name="' + mimeTypeName + '" style="color:white" type="checkbox"  checked="checked"  value="' + formatField + '"> <label  class="descriptionLabel"  for="' + mimeTypeName + '"> ' + formatField + '</label>';


            }

            format.innerHTML = '<span class="field-max"><label>فرمت فایل:</label>' + temptext + '</span>';


            settings.appendChild(size);
            settings.appendChild(format);


        } else if (f.type == 'masktext') {
            var len = document.createElement('DIV');
            len.innerHTML = '<span class="field-min"><label> تعداد کاراکتر:</label><input id="' + f.id + 'len' + '" "type="number" value="' + f.length + '"' + isReadonly + '></span>';

            settings.appendChild(len);


        }
        field.innerHTML = input.outerHTML + checkIcons + label.outerHTML + setBtn.outerHTML + settings.outerHTML;
        wrapper.appendChild(field);




        var sprator = $('#' + f.separator);




        $('#' + 'childWrapper' + stepindex).append(wrapper);


        if (f.parent == "") {
            $("#" + f.id).attr("data-parent", '_'); //setter

        } else {
            $("#" + f.id).attr("data-parent", f.parent); //setter
        }

        if (f.groupName == "") {
            $("#" + f.id).attr("data-groupname", '_'); //setter

        } else {
            $("#" + f.id).attr("data-groupname", f.groupName); //setter
        }


        $("#" + f.id).change(function (el) {


            checkchanged(el);
        });


    }


    refreshDropdowns();

}


function checkchanged(el) {

    if (el.target.dataset['groupname'] != '_') {

        var objectname = el.target.id;
        var check = $('#' + objectname).is(":checked");
        var groupname = el.target.dataset['groupname'];


        for (var k in optionalFields) {
            let j = optionalFields[k];


            if (j.groupName == groupname) {
                $("#" + j.id).prop('checked', check == true);

                if (check == false) {

                    $("#wrapper" + j.id).remove();

                    for (x in list) {

                        let q = list[x];
                        if (q.id == j.id) {
                            q.isUse = false;
                            break;
                        }
                    }



                }


            }
        }


    }

    // تغییر نکند چون امکان دارد خود والد انتخاب می شود
    if (el.target.parent != '') {


        var objectname = el.target.id;





        var check = $('#' + objectname).is(":checked");


        if (check == false) {
            for (var k in optionalFields) {
                let j = optionalFields[k];


                if (j.parent == objectname) {
                    $("#" + j.id).prop('checked', false);

                    for (x in list) {
                        //if (hop.call(list, x)) {
                        let q = list[x];
                        if (q.id == j.id) {
                            q.isUse = false;
                            break;
                        }
                    }





                }
            }
        } else {

            var parentObject = $("#" + objectname).data('parent');

            if (parentObject == "_" || $("#" + parentObject).is(":checked") == true) {


            } else {

                $("#" + objectname).prop('checked', false);
                $('#wrapper' + objectname).remove();

                for (x in list) {

                    let q = list[x];
                    if (q.id == objectname) {
                        q.isUse = false;
                        break;
                    }
                }



            }

        }
    }
    var objectname = el.target.id;
    var check = $('#' + objectname).is(":checked");
    if (check == false) {

        if (objectname != "IdentityType") {


            for (x in list) {

                let q = list[x];
                if (q.id == objectname) {
                    q.isUse = false;
                    break;
                }
            }
            $('#wrapper' + objectname).remove();
        } else {
            $("#" + objectname).prop('checked', true);
        }

    }


    refreshDropdowns();


}

function refreshDropdowns() {
    var str = '<option  value="firstIten"> لطفاً انتخاب نمایید</option>';
    // update dropdowns
    for (x in list) {

        let f = list[x];
        if (f.isUse == false) {
            str += '<option  value="' + f.id + '">' + f.label + '</option>';
        }
    }

    for (var i = 1; i <= stepCount; i++) {
        var select = document.getElementById("dropdown" + i);
        select.innerHTML = str;
    }
}

function getActiveFields() {
    var selectedFields = [];

    for (var i in optionalFields) {
        let f = optionalFields[i];



        var element = document.getElementById(f.id);


        var check = $('#' + f.id).is(":checked");
        if (check == true) {
            var selectedField = {};



            var required = document.getElementById(f.id + 'Required');
            var wrapper = document.getElementById('wrapper' + f.id);
            var stepindex = wrapper.getAttribute("data-stepIndex");

            var stepName = document.getElementById('stepName' + stepindex).value;



            if (f.type == 'text') {
                var min = document.getElementById(f.id + 'min').value;
                var max = document.getElementById(f.id + 'max').value;
                selectedField = {
                    id: f.id,
                    Required: required.checked,
                    min: min,
                    max: max,
                    separator: stepindex,
                    separatorName: stepName
                }
            } else if (f.type == 'image') {
                var formats = f.formats.split("/");

                var fileForats = [];
                for (var k in formats) {
                    let formatField = formats[k];
                    var mimeTypeName = f.id + 'fileFormat' + formatField;

                    var mimeTypeCheck = $('#' + mimeTypeName).is(":checked");
                    if (mimeTypeCheck == true) {
                        fileForats.push(formatField);

                    }

                }

                selectedField = {
                    id: f.id,
                    Required: required.checked,
                    formats: fileForats,
                    separator: stepindex,
                    separatorName: stepName
                };



            } else if (f.type == 'masktext') {

                var len = document.getElementById(f.id + 'len').value;

                selectedField = {
                    id: f.id,
                    Required: required.checked,
                    length: len,

                    separator: stepindex,
                    separatorName: stepName
                }

            } else {
                selectedField = {
                    id: f.id,
                    Required: required.checked,
                    separator: stepindex,
                    separatorName: stepName
                }
            }
            selectedFields.push(selectedField);
        }


    }

    // console.log(selectedField);






    var strFormName = document.getElementById('formNameTextbox').value;
    var strFormDescription = document.getElementById('formDescriptionTextbox').value;
    var formFontType = document.getElementById('formFontType').value;

    $.ajax({
        url: '/api/v1/saveForm',

        type: 'POST',
        headers: {
            'Authorization': 'Token 0b5ed24810a9ea2fca2a798b1c4a3d574600691e',
            'X-CSRFToken': getCookie('csrftoken'),
            'Content-Type': 'application/x-www-form-urlencoded'

        },

        data: {
            "name": strFormName,
            "description": strFormDescription,
            "form_type": "0",
            "font_type": formFontType,
            "background_display": "0",
            "json_form": JSON.stringify(selectedFields),
        },

        success: function (data) {
            
        },
        error: function (error) {
            // console.log(error.responseText)
        }
    });
}


function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function createForm(form) {
    var script = document.createElement('SCRIPT');
    script.id = 'form-script';
    script.type = "text/javascript";
    script.text = form;
    document.getElementsByTagName("iframe")[0].contentWindow.document.getElementsByTagName("body")[0].appendChild(script);

    document.getElementsByTagName("iframe")[0].contentWindow.myForm.init('myform', '');
}

// function iframeLoad() {
//     var iframe = '<html lang="en-US" dir="rtl" style="overflow: hidden;"><head><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link href="styles.dark.css" type="text/css" rel="stylesheet"><link href="styles.simple.dark.css" type="text/css" rel="stylesheet"><style>#myform{width:600px;max-width:90%;margin:auto}</style> </head><body><div id="myform"></div> </body></html>';
//     document.querySelector('iframe').contentDocument.write(iframe);
//     resizeIframe();

//     setTimeout(function () {
//         $.getScript('./form.preview.js')
//     }, 300);
// }



$("#download").click(function () {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/javascript;charset=utf-8,' + encodeURIComponent(formEdited));
    element.setAttribute('download', 'myform.js');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
});

$("#template").change(function () {
    theme.template = this.value;
    createFormScript();
    // resizeIframe();
});

// function resizeIframe() {
//     setTimeout(function () {
//         $('iframe').height(($('iframe').contents().find('body > div').height() + 50) + 'px');
//     }, 200);
// }

function createFormScript() {
    formEdited = form;

    var arr = inputs.split(',');

    for (var i in arr) {
        var k = arr[i].slice(1);
        if ($(arr[i]).is(':checked')) {
            var s = '';
            fields[k].isEnabled = true;
        } else {
            var s = " $myformInstance.fields." + arr[i].slice(1) + ".enabled = false;";
            fields[k].isEnabled = false;
        }

        fields[k].required = $(arr[i] + 'Required').is(':checked');
        if (fields[k].required && fields[k].isEnabled) {
            s += " $myformInstance.fields." + arr[i].slice(1) + ".required = true;";
        }

        formEdited += s;
    }

    if ($("#template").val() == 'simple') {
        formEdited += " $myformInstance.theme.template='simple'; ";
    }
}


// $(window).add($("iframe")[0].contentWindow).resize(function () {
//     if ($("iframe").height() < window.innerHeight && $(window).width() > 767) {
//         $("#iframe-wrapper").addClass('fixed');
//     } else {
//         $("#iframe-wrapper").removeClass('fixed');
//     }
// });


function createLastSelectefield() {

    var lastSelectefield = JSON.parse(lastSelectefieldtemp);

    for (var k in lastSelectefield) {
        var field = lastSelectefield[k];


        var step = document.getElementById('step' + field.separator);
        if (step == undefined) {
            createStep();


        }
        var stepName = document.getElementById('stepName' + field.separator);

        if (stepName != undefined) {
            stepName.value = field.separatorName;

        }

        var objectName = document.getElementById(field.id);
        if (objectName == undefined) {
            createSelectedObject(field.separator, field.id);
        }

    }

}


