function selfPortraitDescription(){var t=document.createElement("DIV"),n,r;t.className=prefex+"selfportrait-desc";var u=document.createElement("DIV"),f=document.createElement("DIV"),i=document.createElement("DIV");return i.className="code",i.innerHTML="4542",n=document.createElement("SPAN"),n.className="help",n.innerHTML="لطفا کد 4 رقمی را بصورت بزرگ و خوانا روی یک برگه سفید نوشته و در کنار صورت خود نگهدارید. سپس یک عکس سلفی از خود به همراه این برگه بگیرید و در اینجا آپلود نمایید. دقت نمایید صورت شما شفاف، بدون عینک و یا گریم و کلاه باشد.",r=document.createElement("DIV"),r.className="image",u.innerHTML=i.outerHTML+n.outerHTML,f.innerHTML=r.outerHTML,t.innerHTML=u.outerHTML+f.outerHTML,t}function addressStatusOptions(){return[{label:"محل دائمی سکونت | مالک هستم | بیش از دو سال در این مکان هستم",value:1},{label:"محل دائمی سکونت | مالک هستم | بیش از یک سال و کمتر از دو سال در این مکان هستم",value:2},{label:"محل دائمی سکونت | مالک هستم | به زودی تغییر مکان می‌دهم",value:3},{label:"محل دائمی سکونت | مستاجر هستم | بیش از یک سال در این مکان هستم",value:4},{label:"محل دائمی سکونت | مستاجر هستم | کمتر از یک سال در این مکان هستم",value:5},{label:"محل موقت سکونت | در ایران ساکن نیستم و ساکن کشور دیگری هستم",value:6},{label:"محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم",value:7},{label:"محل موقت سکونت | نه مالک و نه مستاجر هستم | به زودری تغییر مکان می‌دهم",value:8},{label:"محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم",value:9},{label:"محل دائمی کار و تجارت | مالک یا مستاجر هستم | به تازگی در این مکان هستم (کمتر از دو ماه)",value:10},{label:"محل دائمی کار و تجارت | مالک هستم | بیش از یک سال در این مکان هستم",value:11},{label:"محل دائمی کار و تجارت | مالک هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)",value:12},{label:"محل دائمی کار و تجارت | مستاجر هستم | بیش از یک سال در این مکان هستم",value:13},{label:"محل دائمی کار و تجارت | مستاجر هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)",value:14}]}function identityTypeOptions(){return[{label:"حقیقی",value:"natural"},{label:"حقوقی",value:"legal"}]}function genderOptions(){return[{label:"زن",value:"female"},{label:"مرد",value:"male"}]}function provinceOptions(){return[{id:3,label:"آذربایجان شرقی",value:"آذربایجان شرقی"},{id:16,label:"آذربایجان غربی",value:"آذربایجان غربی"},{id:15,label:"اردبیل",value:"اردبیل"},{id:6,label:"اصفهان",value:"اصفهان"},{id:31,label:"البرز",value:"البرز"},{id:27,label:"ایلام",value:"ایلام"},{id:21,label:"بوشهر",value:"بوشهر"},{id:1,label:"تهران",value:"تهران"},{id:24,label:"چهارمحال و بختیاری",value:"چهارمحال و بختیاری"},{id:30,label:"خراسان جنوبی",value:"خراسان جنوبی"},{id:7,label:"خراسان رضوی",value:"خراسان رضوی"},{id:29,label:"خراسان شمالی",value:"خراسان شمالی"},{id:4,label:"خوزستان",value:"خوزستان"},{id:12,label:"زنجان",value:"زنجان"},{id:9,label:"سمنان",value:"سمنان"},{id:26,label:"سیستان و بلوچستان",value:"سیستان و بلوچستان"},{id:5,label:"فارس",value:"فارس"},{id:8,label:"قزوین",value:"قزوین"},{id:10,label:"قم",value:"قم"},{id:18,label:"کردستان",value:"کردستان"},{id:22,label:"کرمان",value:"کرمان"},{id:19,label:"کرمانشاه",value:"کرمانشاه"},{id:0,label:"کشورهای خارجه",value:"کشورهای خارجه"},{id:28,label:"کهگیلویه و بویراحمد",value:"کهگیلویه و بویراحمد"},{id:14,label:"گلستان",value:"گلستان"},{id:2,label:"گیلان",value:"گیلان"},{id:20,label:"لرستان",value:"لرستان"},{id:13,label:"مازندران",value:"مازندران"},{id:11,label:"مرکزی",value:"مرکزی"},{id:23,label:"هرمزگان",value:"هرمزگان"},{id:17,label:"همدان",value:"همدان"},{id:25,label:"یزد",value:"یزد"}]}function regionTypeOptions(){return[{id:"1",label:"منطقه",value:"district"},{id:"2",label:"محله",value:"neighborhood"},{id:"3",label:"دهستان",value:"rural district"},{id:"4",label:"شهرک",value:"township"},{id:"5",label:"منطقه شهرداری",value:"municipality region"}]}function roadTypeOptions(){return[{id:"1",label:"خیابان",value:"street"},{id:"2",label:"کوچه",value:"alley"},{id:"3",label:"بنبست",value:"dead end"},{id:"4",label:"بلوار",value:"boulvard"},{id:"5",label:" بزرگراه",value:"highway"},{id:"6",label:"آزادراه",value:"freeway"},{id:"7",label:"میدان",value:"square"},{id:"8",label:"پل",value:"bridge"},{id:"9",label:"زیرگذر",value:"underpass"},{id:"10",label:"کنارگذر",value:"Bypass"},{id:"11",label:"جاده",value:"road"},{id:"12",label:"مسیر اختصاصی",value:"assigned route"},{id:"13",label:"کیلومتر",value:"kilometer"},{id:"14",label:"چهارراه",value:"fourway"},{id:"15",label:"سه راه",value:"threeway"},{id:"16",label:"دو راه",value:"dualway"}]}function floorOptions(){for(var t=[],n=1;n<41;n++)t.push({id:n,label:"طبقه "+n,value:n});return t}function presentDaysOptions(){return[{label:"شنبه",value:"SAT"},{label:"یکشنبه",value:"SUN"},{label:"دوشنبه",value:"MON"},{label:"سه‌شنبه",value:"TUE"},{label:"چهارشنبه",value:"WED"},{label:"پنج‌شنبه",value:"THU"},{label:"جمعه",value:"FRI"}]}function presentHoursOptions(){return[{label:"۹ الی ۱۲",value:"09-12"},{label:"۱۲ الی ۱۵",value:"12-15"},{label:"۱۵ الی ۱۸",value:"15-18"},{label:"۱۸ الی ۲۱",value:"18-21"}]}function deliveryToSecuirtyOptions(){return[{label:"بله",value:"1"},{label:"خیر",value:"0"}]}function organTypeOptions(){return[{label:"دولتی",value:"government"},{label:"خصوصی",value:"private"},{label:"نیمه‌خصوصی",value:"semiprivate"}]}function companyTypeOptions(){return[{label:"مسئولیت محدود",value:"limited"},{label:"سهامی خاص",value:"private"},{label:"سهامی عام",value:"public"},{label:"غیر انتفاعی",value:"nonprofit"}]}function marriageOptions(){return[{label:"مجرد",value:"single"},{label:"متاهل",value:"married"},{label:"جداشده",value:"divorced"}]}function militaryServiceOptions(){return[{label:"معافیت دائم",value:"1"},{label:"مشمول",value:"2"},{label:"مشغول خدمت",value:"3"},{label:"غایب",value:"4"}]}function noHouseNumber(){var i=document.createElement("DIV"),t,n;return i.classList.add(prefex+"group-append",prefex+"form-checkbox"),t=document.createElement("LABEL"),t.classList.add(prefex+"group-text"),n=document.createElement("INPUT"),n.setAttribute("type","checkbox"),n.name="noHouseNumber",n.id=prefex+"noHouseNumber",t.innerHTML=n.outerHTML+'<span class="check"><\/span>بدون پلاک',i.appendChild(t),i}function castFieldOptions(n){switch(n){case"identityTypeOptions":return identityTypeOptions();case"organTypeOptions":return organTypeOptions();case"companyTypeOptions":return companyTypeOptions();case"genderOptions":return genderOptions();case"marriageOptions":return marriageOptions();case"militaryServiceOptions":return militaryServiceOptions();case"addressStatusOptions":return addressStatusOptions();case"provinceOptions":return provinceOptions();case"regionTypeOptions":return regionTypeOptions();case"roadTypeOptions":return roadTypeOptions();case"floorOptions":return floorOptions();case"presentDaysOptions":return presentDaysOptions();case"presentHoursOptions":return presentHoursOptions();case"deliveryToSecuirtyOptions":return deliveryToSecuirtyOptions();default:return null}}var controlsType=[{id:"IdentityType",label:"نوع شخصیت",type:"radio",enable:!1,required:!1,events:[{event:"myformready"}],options:"identityTypeOptions",separator:"specifications",parent:"",serverValidation:!1,groupName:""},{id:"OrganType",label:"نوع سازمان",type:"radio",enable:!1,required:!1,options:"organTypeOptions",parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"CompanyType",label:"نوع شرکت",type:"radio",enable:!1,required:!1,options:"companyTypeOptions",parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"FirstName",label:"نام",type:"text",enable:!1,required:!1,maxLength:15,parent:"",serverValidation:!1,separator:"specifications",groupName:""},{id:"LastName",label:"نام خانوادگی",type:"text",enable:!1,required:!1,maxLength:25,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"Gender",label:"جنسیت",type:"radio",enable:!1,required:!1,options:"genderOptions",parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"CompanyName",label:"نام شرکت/سازمان/موسسه/دانشگاه / وزارتخانه",type:"text",enable:!1,required:!1,"class":"w100",maxLength:25,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"BirthDate",label:"تاریخ تولد",type:"text",enable:!1,required:!1,maxLength:10,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"RegNumber",label:"شماره شناسنامه",type:"text",enable:!1,required:!1,maxLength:8,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"Marriage",label:"وضعیت تاهل",type:"radio",enable:!1,required:!1,"class":"w50",options:"marriageOptions",parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"SpouseName",label:"نام و نام خانوادگی همسر",type:"text",enable:!1,required:!1,maxLength:25,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"MilitaryService",label:"وضعیت سربازی",type:"radio",enable:!1,required:!1,options:"militaryServiceOptions",parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"Children",label:"تعداد فرزندان",type:"text",enable:!1,required:!1,maxLength:2,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"NationalID",label:"کد ملی / کد شناسه ملی",type:"text",enable:!1,required:!1,events:[{event:"validation"}],"class":"ltr",maxLength:11,lengthStatus:!0,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"PassportNumber",label:"شماره پاسپورت",type:"text",enable:!1,required:!1,maxLength:11,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"DrivingLicence",label:"شماره گواهینامه",type:"text",enable:!1,required:!1,maxLength:11,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"FatherName",label:"نام پدر",type:"text",enable:!1,required:!1,maxLength:20,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"BirthPlace",label:"محل تولد",type:"text",enable:!1,required:!1,maxLength:15,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"RegIssuePlace",label:"محل صدور شناسنامه",type:"text",enable:!1,required:!1,maxLength:15,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"DirectorFirstName",label:"نام مدیرعامل /رئیس",type:"text",enable:!1,required:!1,maxLength:15,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"DirectorLastName",label:"نام خانوادگی مدیر عامل / رئیس",type:"text",enable:!1,required:!1,maxLength:25,parent:"",separator:"specifications",lengthStatus:!1,serverValidation:!1,groupName:""},{id:"AgentFirstName",label:"نام شخص مسوول",type:"text",enable:!1,required:!1,maxLength:15,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"AgentLastName",label:"نام خانوادگی شخص مسوول",type:"text",enable:!1,required:!1,maxLength:25,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"PhoneNumber",label:"تلفن ثابت",type:"phone",enable:!1,required:!1,maxLength:"15 - No CountryCode",lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"CellNumber",label:"شماره همراه",description:"مثال: 09123456789",type:"text",enable:!1,required:!1,events:[{event:"validation"}],"class":"ltr",maxLength:12,lengthStatus:!0,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"AgentCellNumber",label:"شماره همراه شخص مسوول",description:"مثال: 09123456789",type:"text",enable:!1,required:!1,events:[{event:"validation"}],"class":"ltr",maxLength:12,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"Email",label:"ایمیل ",type:"text",enable:!1,required:!1,events:[{event:"validation"}],"class":"ltr",maxLength:30,lengthStatus:!0,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"AgentEmail",label:"ایمیل شخص مسوول",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:30,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"PREmail",label:"ایمیل روابط عمومی ",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:30,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"AddressStatus",label:"وضعیت نشانی",type:"select",enable:!1,required:!1,options:"addressStatusOptions",parent:"",separator:"address",serverValidation:!1,groupName:""},{id:"PostCode",label:"کد پستی",type:"text",enable:!1,required:!1,events:[{event:"validation"}],"class":"ltr",maxLength:10,lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:""},{id:"Province",label:"استان",type:"select",enable:!1,required:!1,options:"provinceOptions",events:[{event:"change"}],maxLength:"N/A",lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:""},{id:"City",label:"شهرستان",type:"select",enable:!1,required:!1,options:[],maxLength:"N/A",lengthStatus:!0,parent:"Province",separator:"address",serverValidation:!1,groupName:""},{id:"RegionType",label:"نوع منطقه / محله/ شهرک",type:"select",enable:!1,required:!1,options:"regionTypeOptions",maxLength:"N/A",lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:"AddressDetails"},{id:"RegionName",label:"نام منطقه / محله / شهرک",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:15,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"MainRoadType",label:"نوع معبر اصلی",type:"select",enable:!1,required:!1,options:"roadTypeOptions",maxLength:"N/A",lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:"AddressDetails"},{id:"MainRoadName",label:"نام معبر اصلی",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:20,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"PrimaryRoadType",label:"نوع معبر فرعی ۱",type:"select",enable:!1,required:!1,options:"roadTypeOptions",maxLength:"N/A",lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:"AddressDetails"},{id:"PrimaryRoadName",label:"نام معبر فرعی ۱",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:20,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"SecondaryRoadType",label:"نوع معبر فرعی ۲",type:"select",enable:!1,required:!1,options:"roadTypeOptions",maxLength:"N/A",lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:"AddressDetails"},{id:"SecondaryRoadName",label:"نام معبر فرعی ۲",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:20,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"HouseNumber",label:"شماره پلاک",type:"text",enable:!1,required:!1,events:[{event:"validation"},{event:"change"}],append:"noHouseNumber",maxLength:6,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"Floor",label:"طبقه",type:"select",enable:!1,required:!1,options:"floorOptions",maxLength:"N/A",lengthStatus:!0,parent:"",separator:"address",serverValidation:!1,groupName:"AddressDetails"},{id:"Unit",label:"واحد",description:"در صورت وجود حتما وارد کنید",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:10,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"BuildingName",label:"نام ساختمان",description:"در صورت عدم وجود پلاک، تکمیل این قسمت الزامی‌ست",type:"text",enable:!1,required:!1,events:[{event:"validation"}],maxLength:25,lengthStatus:!0,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"DoorColor",label:"رنگ درب ساختمان",type:"text",enable:!1,required:!1,maxLength:10,lengthStatus:!1,parent:"",separator:"address",serverValidation:!0,groupName:"AddressDetails"},{id:"Location",label:"مختصات ساختمان",description:"لطفا موقعیت دقیق خود را بر روی نقشه تعیین کنید",type:"map",enable:!1,required:!1,parent:"",separator:"Location",serverValidation:!1,groupName:""},{id:"PresentDays",label:"روزهای حضور",type:"checkbox",multiline:!0,enable:!1,required:!1,options:"presentDaysOptions",parent:"",separator:"submit",serverValidation:!1,groupName:""},{id:"PresentHours",label:"ساعتهای حضور",type:"checkbox",multiline:!0,enable:!1,required:!1,options:"presentHoursOptions",parent:"",separator:"submit",serverValidation:!1,groupName:""},{id:"DeliveryToSecuirty",label:"تحویل نگهبانی شود؟",type:"radio",checked:!0,enable:!1,required:!1,options:"deliveryToSecuirtyOptions",parent:"",separator:"submit",serverValidation:!1,groupName:""},{id:"NoticeNationalID",label:"هشدار در دست داشتن کارت ملی (تنها هشدار در فرم نشان داده شود)",type:"warning",enable:!1,required:!1,maxLength:50,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"ExtraDeliveryNotes",label:"توضیحات جهت تحویل",type:"textarea",enable:!1,required:!1,maxLength:256,lengthStatus:!1,parent:"",separator:"submit",serverValidation:!1,groupName:""},{id:"DiscountCode",label:"کد تخفیف",description:"در صورت در اختیار داشتن کد تخفیف وارد کنید",type:"text",enable:!1,required:!1,maxLength:10,lengthStatus:!1,parent:"",separator:"submit",serverValidation:!1,groupName:""},{id:"BankCardNumber",label:"شماره کارت بانکی",type:"cardnumber",enable:!1,required:!1,maxLength:16,lengthStatus:!1,parent:"",separator:"specifications",serverValidation:!1,groupName:""},{id:"PersonalPhoto",label:"عکس پرسنلی",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"submit",serverValidation:!1,groupName:""},{id:"OrganizationLogo",label:"لوگوی سازمان",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"authentication",serverValidation:!1,groupName:""},{id:"IdentityCardImage",label:"تصویر کارت ملی",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"authentication",serverValidation:!1,groupName:""},{id:"BirthCertificateImage",label:"تصویر شناسنامه",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"authentication",serverValidation:!1,groupName:""},{id:"DrivingLicenceImage",label:"تصویر گواهینامه",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"authentication",serverValidation:!1,groupName:""},{id:"PassportImage",label:"تصویر پاسپورت",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"authentication",serverValidation:!1,groupName:""},{id:"SelfPortraitImage",label:"تصویر احراز  هویت",description:"فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",type:"image",enable:!1,required:!1,append:"selfPortraitDescription",maxLength:15e6,formats:"PDF/JPG/PNG",lengthStatus:!1,parent:"",separator:"authentication",serverValidation:!1,groupName:""},]