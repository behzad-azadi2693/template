/*
Name: My Form
Version: 1.0.0
Author: Nami Nabilzadeh
Date: April 2020
*/
(function(f){
//  <Properties>.............................................
    var parentId;
    Object.defineProperty(f, 'parentId', {
        get: function() { return parentId; },
        set: function(x) { parentId = x; }
    });

    f.createForm = createForm;
    f.addEvents = addEvents;

    var prefex = "mfl-";
    Object.defineProperty(f, 'prefex', {
        get: function() { return prefex; },
        set: function(x) { prefex = x; }
    });

    var apiUrl = "http://api.geocoding.ir/api/address_validation?";
    var apiToken = '09eb07718a37c49a0f761a890ee08f335bbc363f';

    var pages = [];
    Object.defineProperty(f, 'pages', {
        get: function() { return pages; },
        set: function(x) { pages = x; }
    });

    var currPage = 1;
    Object.defineProperty(f, 'currPage', {
        get: function() { return currPage; },
        set: function(x) { currPage = x; }
    });

    var theme = {
        template: 'paginated'
    }
    Object.defineProperty(f, 'theme', {
        get: function() { return theme; },
        set: function(x) { theme = x; }
    });

    var fields = [
        {
            "id": "IdentityType",
            "label": "نوع شخصیت",
            "type": "radio",
            "enable": false,
            "required": false,
            "events": [ { "event": "myformready", "handler": onIdentityTypeInserted, get target() { return parentId; } } ],
            "options": identityTypeOptions(),
            "serverValidation": false
        },
        {
            "id": "OrganType",
            "label": "نوع سازمان",
            "type": "radio",
            "enable": false,
            "required": false,
            "options": organTypeOptions(),
            "serverValidation": false
        },
        {
            "id": "CompanyType",
            "label": "نوع شرکت",
            "type": "radio",
            "enable": false,
            "required": false,
            "options": companyTypeOptions(),
            "serverValidation": false
        },
        {
            "id": "FirstName",
            "label": "نام",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 15,
            "serverValidation": false,
        },
        {
            "id": "LastName",
            "label": "نام خانوادگی",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 25,
            "serverValidation": false
        },
        {
            "id": "Gender",
            "label": "جنسیت",
            "type": "radio",
            "enable": false,
            "required": false,
            "options": genderOptions(),
            "serverValidation": false
        },
        {
            "id": "CompanyName",
            "label": "نام شرکت/سازمان/موسسه/دانشگاه / وزارتخانه",
            "type": "text",
            "enable": false,
            "required": false,
            "class": "w100",
            "maxLength": 25,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "BirthDate",
            "label": "تاریخ تولد",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 10,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "RegNumber",
            "label": "شماره شناسنامه",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 8,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "Marriage",
            "label": "وضعیت تاهل",
            "type": "radio",
            "enable": false,
            "required": false,
            "class": 'w50',
            "options": marriageOptions(),
            "serverValidation": false
        },
        {
            "id": "SpouseName",
            "label": "نام و نام خانوادگی همسر",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 25,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "MilitaryService",
            "label": "وضعیت سربازی",
            "type": "radio",
            "enable": false,
            "required": false,
            "options": militaryServiceOptions(),
            "serverValidation": false
        },
        {
            "id": "Children",
            "label": "تعداد فرزندان",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 2,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "NationalID",
            "label": "کد ملی / کد شناسه ملی",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": nationalIDValidation } ],
            "class": "ltr",
            "maxLength": 11,
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "PassportNumber",
            "label": "شماره پاسپورت",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 11,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "DrivingLicence",
            "label": "شماره گواهینامه",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 11,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "FatherName",
            "label": "نام پدر",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 20,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "BirthPlace",
            "label": "محل تولد",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 15,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "RegIssuePlace",
            "label": "محل صدور شناسنامه",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 15,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "DirectorFirstName",
            "label": "نام مدیرعامل /رئیس",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 15,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "DirectorLastName",
            "label": "نام خانوادگی مدیر عامل / رئیس",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 25,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "AgentFirstName",
            "label": "نام شخص مسوول",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 15,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "AgentLastName",
            "label": "نام خانوادگی شخص مسوول",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 25,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "PhoneNumber",
            "label": "تلفن ثابت",
            "type": "phone",
            "enable": false,
            "required": false,
            "maxLength": "15 - No CountryCode",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "CellNumber",
            "label": "شماره همراه",
            "description": "مثال: 09123456789",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": phoneValidation } ],
            "class": "ltr",
            "maxLength": 12,
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "AgentCellNumber",
            "label": "شماره همراه شخص مسوول",
            "description": "مثال: 09123456789",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": phoneValidation } ],
            "class": "ltr",
            "maxLength": 12,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "Email",
            "label": "ایمیل ",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": emailValidation } ],
            "class": 'ltr',
            "maxLength": 30,
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "AgentEmail",
            "label": "ایمیل شخص مسوول",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": emailValidation } ],
            "maxLength": 30,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "PREmail",
            "label": "ایمیل روابط عمومی ",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": emailValidation } ],
            "maxLength": 30,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "AddressStatus",
            "label": "وضعیت نشانی",
            "type": "select",
            "enable": false,
            "required": false,
            "options": addressStatusOptions(),
            "serverValidation": false
        },
        {
            "id": "PostCode",
            "label": "کد پستی",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": postCodeValidation } ],
            "class": "ltr",
            "maxLength": 10,
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "Province",
            "label": "استان",
            "type": "select",
            "enable": false,
            "required": false,
            "options": provinceOptions(),
            "events": [ { "event": "change", "handler": provinceChanged } ],
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "City",
            "label": "شهرستان",
            "type": "select",
            "enable": false,
            "required": false,
            "options": [],
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "RegionType",
            "label": "نوع منطقه / محله/ شهرک",
            "type": "select",
            "enable": false,
            "required": false,
            "options": regionTypeOptions(),
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "RegionName",
            "label": "نام منطقه / محله / شهرک",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": regionValidation } ],
            "maxLength": 15,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "MainRoadType",
            "label": "نوع معبر اصلی",
            "type": "select",
            "enable": false,
            "required": false,
            "options": roadTypeOptions(),
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "MainRoadName",
            "label": "نام معبر اصلی",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": roadValidation } ],
            "maxLength": 20,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "PrimaryRoadType",
            "label": "نوع معبر فرعی ۱",
            "type": "select",
            "enable": false,
            "required": false,
            "options": roadTypeOptions(),
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "PrimaryRoadName",
            "label": "نام معبر فرعی ۱",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": roadValidation } ],
            "maxLength": 20,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "SecondaryRoadType",
            "label": "نوع معبر فرعی ۲",
            "type": "select",
            "enable": false,
            "required": false,
            "options": roadTypeOptions(),
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "SecondaryRoadName",
            "label": "نام معبر فرعی ۲",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": roadValidation } ],
            "maxLength": 20,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "HouseNumber",
            "label": "شماره پلاک",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": houseNumberValidation }, { "event": "change", "target": prefex+"noHouseNumber", "handler": onNoHouseNumberChange } ],
            "append": noHouseNumber(),
            "maxLength": 6,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "Floor",
            "label": "طبقه",
            "type": "select",
            "enable": false,
            "required": false,
            "options": floorOptions(),
            "maxLength": "N/A",
            "lengthStatus": true,
            "serverValidation": false
        },
        {
            "id": "Unit",
            "label": "واحد",
            "description": "در صورت وجود حتما وارد کنید",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": unitValidation } ],
            "maxLength": 10,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "BuildingName",
            "label": "نام ساختمان",
            "description": "در صورت عدم وجود پلاک، تکمیل این قسمت الزامی‌ست",
            "type": "text",
            "enable": false,
            "required": false,
            "events": [ { "event": "validation", "handler": buildingNameValidation } ],
            "maxLength": 25,
            "lengthStatus": true,
            "serverValidation": true
        },
        {
            "id": "DoorColor",
            "label": "رنگ درب ساختمان",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 10,
            "lengthStatus": false,
            "serverValidation": true
        },
        {
            "id": "Location",
            "label": "مختصات ساختمان",
            "description": "لطفا موقعیت دقیق خود را بر روی نقشه تعیین کنید",
            "type": "map",
            "enable": false,
            "required": false,
            "serverValidation": false
        },
        {
            "id": "PresentDays",
            "label": "روزهای حضور",
            "type": "checkbox",
            "multiline": true,
            "enable": false,
            "required": false,
            "options": presentDaysOptions(),
            "serverValidation": false
        },
        {
            "id": "PresentHours",
            "label": "ساعتهای حضور",
            "type": "checkbox",
            "multiline": true,
            "enable": false,
            "required": false,
            "options": presentHoursOptions(),
            "serverValidation": false
        },
        {
            "id": "DeliveryToSecuirty",
            "label": "تحویل نگهبانی شود؟",
            "type": "radio",
            "checked": true,
            "enable": false,
            "required": false,
            "options": deliveryToSecuirtyOptions(),
            "serverValidation": false
        },
        {
            "id": "NoticeNationalID",
            "label": "هشدار در دست داشتن کارت ملی (تنها هشدار در فرم نشان داده شود)",
            "type": "warning",
            "enable": false,
            "required": false,
            "maxLength": 50,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "ExtraDeliveryNotes",
            "label": "توضیحات جهت تحویل",
            "type": "textarea",
            "enable": false,
            "required": false,
            "maxLength": 256,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "DiscountCode",
            "label": "کد تخفیف",
            "description": "در صورت در اختیار داشتن کد تخفیف وارد کنید",
            "type": "text",
            "enable": false,
            "required": false,
            "maxLength": 10,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "BankCardNumber",
            "label": "شماره کارت بانکی",
            "type": "cardnumber",
            "enable": false,
            "required": false,
            "maxLength": 16,
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "PersonalPhoto",
            "label": "عکس پرسنلی",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "OrganizationLogo",
            "label": "لوگوی سازمان",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "IdentityCardImage",
            "label": "تصویر کارت ملی",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "BirthCertificateImage",
            "label": "تصویر شناسنامه",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "DrivingLicenceImage",
            "label": "تصویر گواهینامه",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "PassportImage",
            "label": "تصویر پاسپورت",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        },
        {
            "id": "SelfPortraitImage",
            "label": "تصویر احراز  هویت",
            "description": "فرمت‌های قابل قبول: jpg, png, pdf - حداکثر حجم فایل: 15 مگابایت",
            "type": "image",
            "enable": false,
            "required": false,
            "append": selfPortraitDescription(),
            "maxLength": 15000000,
            "formats": "PDF/JPG/PNG",
            "lengthStatus": false,
            "serverValidation": false
        }
    ];
    Object.defineProperty(f, 'fields', {
        get: function() { return fields; },
        set: function(x) { fields = x; }
    });
//  </Properties>............................................

//  <Form>...................................................
    function createForm(action) {
        var form = document.createElement('FORM');
        form.setAttribute('method', 'post');
        form.setAttribute('action', action);
        form.setAttribute('class', prefex + 'form');
        form.setAttribute('autocomplete', 'off');

        if(theme.template == 'paginated') {
            form.style.width = (100*pages.length) + '%';
        }

        //Add each page to form
        for (var p in pages) {
            form.appendChild(createPageElement(pages[p]));
            if(theme.template == 'paginated') {
                pages[p].el.style.width = (100/pages.length) + '%';
            }
        }

        //Add Submit button for simple form (not paginated form)
        var submitSimple = document.createElement('INPUT');
        submitSimple.setAttribute('type', 'submit');
        submitSimple.setAttribute('value', 'ثبت');

        var submitSimpleWrapper = document.createElement('DIV');
        submitSimpleWrapper.setAttribute('class', prefex+'submit-simple');
        submitSimpleWrapper.appendChild(submitSimple);

        form.appendChild(submitSimpleWrapper);

        //Create wrapper for form
        var wrapper = document.createElement('DIV');
        wrapper.setAttribute('class', prefex + 'form-wrapper ' + theme.template);
        wrapper.appendChild(form);

        return wrapper;
    }
//  </Form>..................................................

//  <Pages>..................................................
    function createPageElement(page) {
        var el = document.createElement('DIV');
        el.setAttribute('class', prefex+'page');
        if(page.classList) el.classList.add(page.classList);

        //Add title
        var title = document.createElement("DIV");
        title.setAttribute('class', prefex+'page-title');
        title.innerHTML = page.title;
        el.appendChild(title);

        var body = document.createElement("DIV");
        body.classList.add(prefex+'page-body');

        //Add fields
        for (var k in page.fields) {
            var field = page.fields[k];
            var fieldEl = createFieldElement(field);
            body.appendChild(fieldEl);
        }
        el.appendChild(body);

        //Add Pagination Buttons
        el.appendChild(getPaginationButtons(pages.indexOf(page)+1));

        page.el = el;
        return el;
    }

    function getPaginationButtons (pos) {
        var next = document.createElement('BUTTON');
        next.setAttribute('class', prefex + 'next-button');
        next.innerHTML = 'ادامه';
    
        var prev = document.createElement('BUTTON');
        prev.setAttribute('class', prefex + 'prev-button');
        prev.innerHTML = 'قبل';
    
        var submit = document.createElement('INPUT');
        submit.setAttribute('type', 'submit');
        submit.setAttribute('value', 'ثبت');
    
        var pagination = document.createElement('DIV');
        pagination.setAttribute('class', prefex + 'form-pagination');
        switch(pos) {
            case pages.length: 
                pagination.innerHTML = (pages.length > 1 ? prev.outerHTML:'') + submit.outerHTML;
            break;
            case 1: 
                pagination.innerHTML = next.outerHTML;
            break;
            default:
                pagination.innerHTML = prev.outerHTML + next.outerHTML;
        }
    
        return pagination;
    }
//  </Pages>.................................................

//  <Create Inputs>..........................................
    function createFieldElement(field) {
        switch(field.type) {
            case 'text': field.el = textInput(field); break;
            case 'select': field.el = selectInput(field); break;
            case 'checkbox': field.el = checkboxInput(field); break;
            case 'radio': field.el = radioInput(field); break;
            case 'textarea': field.el = textareaInput(field); break;
            case 'image': field.el = imageInput(field); break;
            case 'cardnumber': field.el = cardnumberInput(field); break;
            case 'phone': field.el = phoneInput(field); break;
            case 'map': field.el = mapInput(field); break;
        }

        if(field.class) field.el.classList.add(field.class);

        return field.el;
    }

    function textInput(field) {
        var input = document.createElement("INPUT");
        input.setAttribute('type', 'text');
        input.setAttribute('id', prefex + field.id);
        input.setAttribute('name', field.id);
        input.setAttribute('class', prefex + 'input');
        input.setAttribute('placeholder', field.label);

        var el = createInputWrapper(input, field, '', field.prepend ? field.prepend.outerHTML:'', field.append ? field.append.outerHTML:'');

        return el;
    }

    function selectInput(field) {
        var option1 = field.multiple ? [] : [{ label: field.label, value: '', disabled: (field.required? true:false), selected: true }];
        var options = '';
        option1.concat(field.options).map(function(option, i){
            var o = document.createElement("OPTION");
            o.setAttribute('value', option.value);
            if(option.disabled) o.disabled = true;
            if(option.selected) o.setAttribute('selected', true);
            o.innerHTML = option.label;
             options += o.outerHTML;
        });

        var input = document.createElement("SELECT");
        input.setAttribute('id', prefex + field.id);
        input.setAttribute('name', field.id);
        input.setAttribute('class', prefex + 'input');
        if(field.multiple) {
            input.setAttribute('multiple', true);
            input.setAttribute('size', field.options.length);
        }
        input.innerHTML = options;
        

        var el = createInputWrapper(input, field);

        if(field.multiple) {
            var label = document.createElement("LABEL");
            label.setAttribute('for', input.id);
            label.innerHTML = field.label;
            el.innerHTML = label.outerHTML + el.innerHTML;
        }

        field.el = el;

        return el;
    }

    function checkboxInput(field) {
        var div = document.createElement("DIV");
        div.className = prefex+'checkboxes-field';
        div.id = prefex+field.id;

        var label = document.createElement("LABEL");
        label.innerHTML = field.label;
        
        var checkboxes = document.createElement("DIV");
        checkboxes.className = prefex+"checkboxes-group" + (field.multiline ? ' multiline':''); 

        var options = field.options;
        for(var i in options) {
            var option = options[i];
            var input = document.createElement("INPUT");
            input.id = field.id + '-' + option.value;
            input.name = field.id;
            input.value = option.value;
            input.type = 'checkbox';

            var l = document.createElement("LABEL");
            l.setAttribute('for', input.id);
            l.innerHTML = option.label;

            var c = document.createElement("SPAN");
            c.setAttribute('class', 'check');

            checkboxes.innerHTML += '<span>' + input.outerHTML + l.outerHTML + c.outerHTML + '</span>';
        }

        div.innerHTML = label.outerHTML + checkboxes.outerHTML;

        var el = createInputWrapper(div, field, prefex+'form-checkbox');
        field.el = el;

        return el;
    }

    function radioInput(field) {
        var div = document.createElement("DIV");
        div.className = prefex+'radios-field';
        div.id = prefex+field.id;

        var label = document.createElement("LABEL");
        label.innerHTML = field.label;
        
        var radios = document.createElement("DIV");
        radios.className = prefex+"radios-group";

        var options = field.options;
        for(var i in options) {
            var option = options[i];
            var input = document.createElement("INPUT");
            input.id = field.id + '-' + option.value;
            input.name = field.id;
            input.value = option.value;
            input.type = 'radio';

            var l = document.createElement("LABEL");
            l.setAttribute('for', input.id);
            l.innerHTML = option.label;

            radios.innerHTML += input.outerHTML + l.outerHTML;
        }

        div.innerHTML = label.outerHTML + radios.outerHTML;

        var el = createInputWrapper(div, field, prefex+'form-radio');
        field.el = el;

        return el;
    }

    function textareaInput(field) {
        var input = document.createElement("TEXTAREA");
        input.setAttribute('id', prefex + field.id);
        input.setAttribute('name', field.id);
        input.setAttribute('class', prefex + 'input');
        input.setAttribute('placeholder', field.label);

        var el = createInputWrapper(input, field);

        return el;
    }

    function imageInput(field) {
        var input = document.createElement("INPUT");
        input.type = "file";
        input.id = prefex+field.id;
        input.name = field.id;
        input.style.display = 'none';
        input.setAttribute('accept', '.jpg,.png,.pdf');
        
        var div = document.createElement("DIV");
        div.className = prefex+'image';
        div.id = prefex+field.id;

        var label = document.createElement("LABEL");
        label.innerHTML = field.label;
        
        var span = document.createElement("SPAN");
        span.classList.add(prefex+'file-name');

        var button = document.createElement("BUTTON");
        button.id = div.id + '-btn';
        button.innerHTML = 'انتخاب فایل';

        div.innerHTML = label.outerHTML + span.outerHTML + button.outerHTML + (field.append ? field.append.outerHTML : '');

        var el = createInputWrapper(input, field, prefex+'form-radio', null, div.outerHTML);
        field.el = el;

        return el;
    }

    function cardnumberInput(field) {
        var div = document.createElement("DIV");
        div.className = prefex+'card-field';
        div.id = prefex+field.id;

        var label = document.createElement("LABEL");
        label.innerHTML = field.label;
        
        var card = document.createElement("DIV");
        card.className = prefex+"card-group";

        for(var i=1; i<5; i++) {
            let input = document.createElement('INPUT');
            input.id = prefex+field.id+'-'+i;
            input.setAttribute('size', '4');
            input.setAttribute('maxLength', '4');
            card.appendChild(input);
        }

        div.innerHTML = label.outerHTML + card.outerHTML;

        var el = createInputWrapper(div, field, prefex+'form-card');
        field.el = el;

        return el;
    }

    function phoneInput(field) {
        var div = document.createElement("DIV");
        div.className = prefex+'phone-field';
        div.id = prefex+field.id;

        var label = document.createElement("LABEL");
        label.innerHTML = field.label;
        
        var phone = document.createElement("DIV");
        phone.className = prefex+"phone-group";

        var cnt = document.createElement("SELECT");
        cnt.className = prefex+'country-code';
        cnt.innerHTML = '<option>+98</option>'
        phone.appendChild(cnt);

        var area = document.createElement("INPUT");
        area.className = prefex+'area-code';
        area.setAttribute('size', '4');
        area.setAttribute('maxLength', '4');
        area.setAttribute('placeholder', 'پیش شماره');
        phone.appendChild(area);

        var number = document.createElement("INPUT");
        number.className = prefex+'phone-number';
        number.setAttribute('size', '8');
        number.setAttribute('maxLength', '8');
        number.setAttribute('placeholder', 'شماره تلفن');
        phone.appendChild(number);

        var internal = document.createElement("INPUT");
        internal.className = prefex+'internal-code';
        internal.setAttribute('size', '4');
        internal.setAttribute('maxLength', '4');
        internal.setAttribute('placeholder', 'داخلی');
        phone.appendChild(internal);

        div.innerHTML = label.outerHTML + phone.outerHTML;

        var el = createInputWrapper(div, field, prefex+'form-phone');
        field.el = el;

        return el;
    }

    function noHouseNumber() {
        var div = document.createElement('DIV');
        div.classList.add(prefex+'group-append', prefex+'form-checkbox');
        
        var label = document.createElement('LABEL');
        label.classList.add(prefex+'group-text');

        var input = document.createElement('INPUT');
        input.setAttribute('type', 'checkbox');
        input.name = 'noHouseNumber';
        input.id = prefex+'noHouseNumber';

        label.innerHTML = input.outerHTML + '<span class="check"></span>' + 'بدون پلاک';
        div.appendChild(label);

        return div;
    }

    function selfPortraitDescription() {
        var desc = document.createElement("DIV");
        desc.className = prefex+'selfportrait-desc';

        var div1 = document.createElement("DIV");
        var div2 = document.createElement("DIV");

        var code = document.createElement("DIV");
        code.className = 'code';
        code.innerHTML = '4542';

        var help = document.createElement("SPAN");
        help.className = 'help';
        help.innerHTML = "لطفا کد 4 رقمی را بصورت بزرگ و خوانا روی یک برگه سفید نوشته و در کنار صورت خود نگهدارید. سپس یک عکس سلفی از خود به همراه این برگه بگیرید و در اینجا آپلود نمایید. دقت نمایید صورت شما شفاف، بدون عینک و یا گریم و کلاه باشد.";

        var image = document.createElement("DIV");
        image.className = 'image';

        div1.innerHTML = code.outerHTML + help.outerHTML;
        div2.innerHTML = image.outerHTML;
        desc.innerHTML = div1.outerHTML + div2.outerHTML;

        return desc;
    }

    function mapInput(field) {
        var map = document.createElement('DIV');
        map.setAttribute('id', prefex+'map');

        var el = createInputWrapper(map, field);
        field.el = el;

        return el;        
    }

    function createInputWrapper(input, field, optionalClass='', prepend = null, append = null) {
        var desc = document.createElement("SMALL");
        desc.setAttribute('class', prefex+'form-description')
        if(field.description) desc.innerHTML = field.description;

        var validation = document.createElement("SMALL");
        validation.setAttribute('class', prefex + 'form-validation');

        var bottom = document.createElement("DIV");
        bottom.setAttribute('class', prefex+'form-bottom');
        bottom.innerHTML = desc.outerHTML + validation.outerHTML;

        var help = document.createElement("DIV");
        help.setAttribute('class', prefex+'form-help');

        var el = document.createElement("DIV");
        el.setAttribute('id', input.id + '-wrapper')
        el.setAttribute('class', prefex + 'form-group ' + field.type + ' ' + optionalClass + (field.required ? ' form-required' : ''));
        /*
        if(field.childCheckbox) {
            input.classList.add(prefex+'group-prepend');

            var checkbox = document.createElement("INPUT");
            checkbox.setAttribute('type', 'checkbox');
            checkbox.setAttribute('id', prefex+field.childCheckbox.id);

            var label2 = document.createElement("LABEL");
            label2.setAttribute('class', prefex+'group-text');

            label2.innerHTML = checkbox.outerHTML + field.childCheckbox.label;

            var append = document.createElement("DIV");
            append.setAttribute('class', prefex + 'group-append');

            append.innerHTML = label2.outerHTML;
        }
        */
        el.innerHTML = (prepend ? prepend:'') + input.outerHTML + (append ? append:'') + bottom.outerHTML + help.outerHTML;

        return el;
    }
//  </Create Inputs>........................................

//  <Field Options>.........................................
    function addressStatusOptions() {
        return [
            { label: 'محل دائمی سکونت | مالک هستم | بیش از دو سال در این مکان هستم', value: 1 },
            { label: 'محل دائمی سکونت | مالک هستم | بیش از یک سال و کمتر از دو سال در این مکان هستم', value: 2 },
            { label: 'محل دائمی سکونت | مالک هستم | به زودی تغییر مکان می‌دهم', value: 3 },
            { label: 'محل دائمی سکونت | مستاجر هستم | بیش از یک سال در این مکان هستم', value: 4 },
            { label: 'محل دائمی سکونت | مستاجر هستم | کمتر از یک سال در این مکان هستم', value: 5 },
            { label: 'محل موقت سکونت | در ایران ساکن نیستم و ساکن کشور دیگری هستم', value: 6 },
            { label: 'محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم', value: 7 },
            { label: 'محل موقت سکونت | نه مالک و نه مستاجر هستم | به زودری تغییر مکان می‌دهم', value: 8 },
            { label: 'محل موقت سکونت | مالک هستم | ساکن شهر دیگری هستم', value: 9 },
            { label: 'محل دائمی کار و تجارت | مالک یا مستاجر هستم | به تازگی در این مکان هستم (کمتر از دو ماه)', value: 10 },
            { label: 'محل دائمی کار و تجارت | مالک هستم | بیش از یک سال در این مکان هستم', value: 11 },
            { label: 'محل دائمی کار و تجارت | مالک هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)', value: 12 },
            { label: 'محل دائمی کار و تجارت | مستاجر هستم | بیش از یک سال در این مکان هستم', value: 13 },
            { label: 'محل دائمی کار و تجارت | مستاجر هستم | کمتر از یک سال در این مکان هستم (بیش از دو ماه)', value: 14 }
        ]
    }

    function identityTypeOptions() {
        return [
            { label: 'حقیقی', value: 'natural' },
            { label: 'حقوقی', value: 'legal' }
        ]
    }

    function genderOptions() {
        return [
            { label: 'زن', value: 'female' },
            { label: 'مرد', value: 'male' }
        ]
    }

    function provinceOptions() {
        return [ { "id": 3, "label": "آذربایجان شرقی", "value": "آذربایجان شرقی" }, { "id": 16, "label": "آذربایجان غربی", "value": "آذربایجان غربی" }, { "id": 15, "label": "اردبیل", "value": "اردبیل" }, { "id": 6, "label": "اصفهان", "value": "اصفهان" }, { "id": 31, "label": "البرز", "value": "البرز" }, { "id": 27, "label": "ایلام", "value": "ایلام" }, { "id": 21, "label": "بوشهر", "value": "بوشهر" }, { "id": 1, "label": "تهران", "value": "تهران" }, { "id": 24, "label": "چهارمحال و بختیاری", "value": "چهارمحال و بختیاری" }, { "id": 30, "label": "خراسان جنوبی", "value": "خراسان جنوبی" }, { "id": 7, "label": "خراسان رضوی", "value": "خراسان رضوی" }, { "id": 29, "label": "خراسان شمالی", "value": "خراسان شمالی" }, { "id": 4, "label": "خوزستان", "value": "خوزستان" }, { "id": 12, "label": "زنجان", "value": "زنجان" }, { "id": 9, "label": "سمنان", "value": "سمنان" }, { "id": 26, "label": "سیستان و بلوچستان", "value": "سیستان و بلوچستان" }, { "id": 5, "label": "فارس", "value": "فارس" }, { "id": 8, "label": "قزوین", "value": "قزوین" }, { "id": 10, "label": "قم", "value": "قم" }, { "id": 18, "label": "کردستان", "value": "کردستان" }, { "id": 22, "label": "کرمان", "value": "کرمان" }, { "id": 19, "label": "کرمانشاه", "value": "کرمانشاه" }, { "id": 0, "label": "کشورهای خارجه", "value": "کشورهای خارجه" }, { "id": 28, "label": "کهگیلویه و بویراحمد", "value": "کهگیلویه و بویراحمد" }, { "id": 14, "label": "گلستان", "value": "گلستان" }, { "id": 2, "label": "گیلان", "value": "گیلان" }, { "id": 20, "label": "لرستان", "value": "لرستان" }, { "id": 13, "label": "مازندران", "value": "مازندران" }, { "id": 11, "label": "مرکزی", "value": "مرکزی" }, { "id": 23, "label": "هرمزگان", "value": "هرمزگان" }, { "id": 17, "label": "همدان", "value": "همدان" }, { "id": 25, "label": "یزد", "value": "یزد" } ] ;
    }

    function regionTypeOptions() {
        return [
            { "id": "1", "label": "منطقه", "value": "district" },
            { "id": "2", "label": "محله", "value": "neighborhood" },
            { "id": "3", "label": "دهستان", "value": "rural district" },
            { "id": "4", "label": "شهرک", "value": "township" },
            { "id": "5", "label": "منطقه شهرداری", "value": "municipality region" }
         ]
    }

    function roadTypeOptions() {
        return [
            { "id": "1", "label": "خیابان", "value": "street" },
            { "id": "2", "label": "کوچه", "value": "alley" },
            { "id": "3", "label": "بنبست", "value": "dead end" },
            { "id": "4", "label": "بلوار", "value": "boulvard" },
            { "id": "5", "label": " بزرگراه", "value": "highway" },
            { "id": "6", "label": "آزادراه", "value": "freeway" },
            { "id": "7", "label": "میدان", "value": "square" },
            { "id": "8", "label": "پل", "value": "bridge" },
            { "id": "9", "label": "زیرگذر", "value": "underpass" },
            { "id": "10", "label": "کنارگذر", "value": "Bypass" },
            { "id": "11", "label": "جاده", "value": "road" },
            { "id": "12", "label": "مسیر اختصاصی", "value": "assigned route" },
            { "id": "13", "label": "کیلومتر", "value": "kilometer" },
            { "id": "14", "label": "چهارراه", "value": "fourway" },
            { "id": "15", "label": "سه راه", "value": "threeway" },
            { "id": "16", "label": "دو راه", "value": "dualway" }
         ]
    }

    function floorOptions() {
        var floors = [];
        for(var i=1; i<41; i++) {
            floors.push({ "id": i, "label": "طبقه "+i, "value": i });
        }
        return floors;
    }

    function presentDaysOptions() {
        return [
            { label: 'شنبه', value: 'SAT' },
            { label: 'یکشنبه', value: 'SUN' },
            { label: 'دوشنبه', value: 'MON' },
            { label: 'سه‌شنبه', value: 'TUE' },
            { label: 'چهارشنبه', value: 'WED' },
            { label: 'پنج‌شنبه', value: 'THU' },
            { label: 'جمعه', value: 'FRI' }
        ]
    }

    function presentHoursOptions() {
        return [
            { label: '۹ الی ۱۲', value: '09-12'},
            { label: '۱۲ الی ۱۵', value: '12-15'},
            { label: '۱۵ الی ۱۸', value: '15-18'},
            { label: '۱۸ الی ۲۱', value: '18-21'}
        ]
    }

    function deliveryToSecuirtyOptions() {
        return [
            { label: 'بله', value: '1'},
            { label: 'خیر', value: '0'}
        ]
    }

    function organTypeOptions() {
        return [
            { label: 'دولتی', value: 'government' },
            { label: 'خصوصی', value: 'private' },
            { label: 'نیمه‌خصوصی', value: 'semiprivate' }
        ]
    }

    function companyTypeOptions() {
        return [
            { label: 'مسئولیت محدود', value: 'limited' },
            { label: 'سهامی خاص', value: 'private' },
            { label: 'سهامی عام', value: 'public' },
            { label: 'غیر انتفاعی', value: 'nonprofit' }
        ]
    }

    function marriageOptions() {
        return [
            { label: 'مجرد', value: 'single' },
            { label: 'متاهل', value: 'married' },
            { label: 'جداشده', value: 'divorced' }
        ]
    }

    function militaryServiceOptions() {
        return [
            { label: 'معافیت دائم', value: '1' },
            { label: 'مشمول', value: '2' },
            { label: 'مشغول خدمت', value: '3' },
            { label: 'غایب', value: '4' }
        ]
    }
//  </Field Options>........................................

//  <Events>................................................
    function addEvents() {
        for (var p in pages) {
            var page = pages[p];
            var fields = page.fields;

            for (var i in fields) { 
                var field = fields[i];
                var wrapper = document.getElementById(prefex+field.id+'-wrapper');
                var input = document.getElementById(prefex+field.id);
                var events = field.events;
                var hasValidation = false;

                if(field.type == 'map') {
                    mapInit(prefex+'map');
                    continue;
                }

                //Add custom events
                if(events != null) {
                    for(var e in events) {
                        var event = '';
                        if(events[e].event == 'validation') {
                            event = 'input';
                            hasValidation = true;
                        }else{
                            event = events[e].event;
                        }

                        var target = input;
                        if(events[e].target) {
                            target = document.getElementById(events[e].target);
                        }
                        target.addEventListener(event, events[e].handler.bind(null, target, field.el, field, true, null));
                    }
                }

                input.addEventListener('focusout', function(wrapper, e){
                    wrapper.getElementsByClassName(prefex+'form-help')[0].classList.remove('show');
                }.bind(null, wrapper));


                if(field.type == 'image') {
                    document.getElementById(input.id+'-btn').addEventListener('click', onImageButtonClick.bind(null, input));
                    input.addEventListener('change', onImageChange.bind(null, input, field.el, field));
                }
                else if(field.type == 'cardnumber') {
                    Array.from(document.querySelectorAll('#'+prefex+field.id+' input'), function(el){
                        el.addEventListener('input', onCardnumberChange.bind(null, el, wrapper, field));
                        el.addEventListener('keydown', onCardnumberChange.bind(null, el, wrapper, field));
                    });
                }

                //Add validation events
                if(!hasValidation) {
                    switch (field.type) {
                        case 'text':
                            input.addEventListener('input', textInputValidation.bind(null, input, field.el, field, true, null));
                        break;
    
                        case 'select':
                            input.addEventListener('change', selectInputValidation.bind(null, input, field.el, field, true));
                        break;

                        case 'radio':
                            Array.from(input.getElementsByTagName('input')).map(function(i){
                                i.addEventListener('change', checkInputValidation.bind(null, field.el, field, false));
                            });
                        break;

                        case 'checkbox':
                            Array.from(input.getElementsByTagName('input')).map(function(i){
                                i.addEventListener('change', checkInputValidation.bind(null, field.el, field, false));
                            });
                        break;

                        case 'textarea':
                            input.addEventListener('input', textInputValidation.bind(null, input, field.el, field, true, null));
                        break;
                    }
                }
            }

            var nextBtn = document.querySelector('.'+prefex+'page:nth-child('+(parseInt(p)+1)+') .'+prefex+'next-button');
            var prevBtn = document.querySelector('.'+prefex+'page:nth-child('+(parseInt(p)+1)+') .'+prefex+'prev-button');
            if(nextBtn) nextBtn.addEventListener('click', nextHandler.bind(page));
            if(prevBtn) prevBtn.addEventListener('click', prevHandler.bind(page));
        }

        document.getElementsByClassName(prefex+'form')[0].addEventListener('submit', submitHandler);
        
        window.addEventListener('resize', onWindowResize);
    }

    function onIdentityTypeInserted(input, wrapper, field, delay, e) {
        var naturalInputs = [
            'FirstName', 'LastName', 'Gender', 'CellNumber', 'Email', 'BirthDate', 'RegNumber', 
            'Marriage', 'SpouseName', 'Children', 'NationalID', 'PassportNumber', 'DrivingLicence', 
            'FatherName', 'BirthPlace', 'RegIssuePlace'
        ];
        var legalInputs = [
            'OrganType', 'CompanyType', 'CompanyName', 'DirectorFirstName', 'DirectorLastName', 
            'AgentFirstName', 'AgentLastName', 'AgentCellNumber', 'AgentEmail', 'PREmail'
        ];
        var commonInputs = [ 'PhoneNumber', 'BankCardNumber' ];
        var all = naturalInputs.concat(legalInputs).concat(commonInputs);

        //Hide All IdentityType related inputs;
        for (var i in all){
            document.getElementById(prefex+all[i]+'-wrapper').style.display = 'none';
            let field = getFieldById(all[i]);
            field.orgRequired = field.required;
            field.required = false;
        }

        document.getElementById(prefex+field.id).addEventListener('change', function(){
            var val = this.querySelector('input:checked').value;
            if(val == 'natural'){
                setITRelatedInputsState(naturalInputs, true);
                setITRelatedInputsState(legalInputs, false);
                setITRelatedInputsState(commonInputs, true);
            }else{
                setITRelatedInputsState(legalInputs, true);
                setITRelatedInputsState(naturalInputs, false);
                setITRelatedInputsState(commonInputs, true);
            }
            document.querySelector('.'+prefex+'form-wrapper').style.height = pages[0].el.offsetHeight;
        });

        setPageWidth();
    }

    function onImageButtonClick(input, e) {
        input.click();
    }

    function onImageChange(input, wrapper, field, delay, e) {
        var fileName = '';
		fileName = input.value.split( "\\" ).pop();

		if(fileName)
			wrapper.querySelector('.'+prefex+'file-name').innerHTML = fileName;
		else
            wrapper.querySelector('.'+prefex+'file-name').innerHTML = '';

        imageInputValidation(wrapper, field);
    }

    function onCardnumberChange(input, wrapper, field, delay, e) {
        input.value = input.value.replace(/[^\d]/g, '');
        var n = input.id.slice(-1);
        if(input.value.length == 4 && n != '4') {
            document.getElementById(prefex+field.id+'-'+(parseInt(n)+1)).focus();
        }

        if(input.value.length == 0 && n != '1') {
            if(e.keyCode && e.keyCode == 8) {
                document.getElementById(prefex+field.id+'-'+(parseInt(n)-1)).focus();
            }
        }
    }

    function onNoHouseNumberChange(input, wrapper, field, delay, e) {
        if(input.checked) {
            document.getElementById(prefex+'HouseNumber').setAttribute('disabled', '');
        }else {
            document.getElementById(prefex+'HouseNumber').removeAttribute('disabled');
        }
    }

    function nextHandler(e) {
        e.preventDefault();

        //َAvoid changing page if page has error
        checkPageValidation(pages[currPage-1]);
        for(var n in pages[currPage-1].fields) {
            var field = pages[currPage-1].fields[n];
            if(field.hasError) return;
        }

        var wrapper = document.getElementsByClassName(prefex+'form-wrapper')[0];
        var wrapperWidth = wrapper.offsetWidth;
        wrapper.scrollLeft -= wrapperWidth;
        currPage++;

        for(var p in pages) {
            var page = pages[p];
            if(parseInt(p)+1 == currPage) {
                page.el.style.opacity = 1;
                page.el.style.transform = 'scale(1)';
            }
            else {
                page.el.style.opacity = 0;
                page.el.style.transform = 'scale(0.7)';
            }
        }

        wrapper.scrollIntoView();
        setPageWidth();
    }

    function prevHandler(e) {
        e.preventDefault();

        var wrapper = document.getElementsByClassName(prefex+'form-wrapper')[0];
        var wrapperWidth = wrapper.offsetWidth;
        wrapper.scrollLeft += wrapperWidth;
        currPage--;

        for(var p in pages) {
            var page = pages[p];
            if(parseInt(p)+1 == currPage) {
                page.el.style.opacity = 1;
                page.el.style.transform = 'scale(1)';
            }
            else {
                page.el.style.opacity = 0;
                page.el.style.transform = 'scale(0.7)';
            }
        }

        wrapper.scrollIntoView();
        setPageWidth();
    }

    function submitHandler(e) {
        e.preventDefault();
        
    }

    function onWindowResize(e) {
        var wrapper = document.getElementsByClassName(prefex+'form-wrapper')[0];
        wrapper.scrollLeft = wrapper.scrollWidth - ((wrapper.scrollWidth/pages.length) * f.currPage);

        setPageWidth();
    }

    function provinceChanged(input, wrapper, field, delay, e) {
        var province = field.options.find(function(p) { return p.value == input.value })

        if(!province.citiesLoaded) {
            ajax({
                url: "http://api.geocoding.ir/rest/cities/?province="+province.id,
                method: 'get',
                success: function(e){
                    var res = JSON.parse(e.responseText);
                    var cities = [];
                    res.results.map(function(c){ 
                        var city = {
                            label: c.normalized_name,
                            value: c.normalized_name,
                            'data-id': c.id 
                        }
                        if(c.is_province_center)cities.unshift(city);
                        else cities.push(city);
                    });
                    
                    var city = getFieldById('City');
                    city.options = cities;

                    var oldOption = document.querySelector('#' + prefex + 'City-wrapper option');
                    while(oldOption) {
                        oldOption.remove();
                        oldOption = document.querySelector('#' + prefex + 'City-wrapper option');
                    }
                    
                    for(var i=0; i< cities.length; i++) {
                        var option = document.createElement('option');
                        option.value = cities[i].value;
                        option.setAttribute('data-id', cities[i]['data-id']);
                        option.innerHTML = cities[i].label;
                        document.querySelector('#'+prefex + 'City-wrapper select').appendChild(option);
                    }

                    var evt = document.createEvent("HTMLEvents");
                    evt.initEvent("change", false, true);
                    document.getElementById(prefex+'City').dispatchEvent(evt);
                } 
            });
        }
    }
//  </Events>...............................................

//  <Validation>............................................
    function checkPageValidation(page) {
        return;
        for(var n in page.fields){
            var field = page.fields[n];
            if(typeof field.hasError != "undefined") {
                continue;
            }
            
            var input = document.getElementById(prefex + field.id);
            var wrapper = document.getElementById(prefex + field.id + '-wrapper');
            switch (field.type) {
                case 'text':
                    if(field.events) {
                        ev = field.events.find(function(e){ return e.event == 'validation' });
                        if(ev) ev.handler(input, wrapper, field, false);
                        else textInputValidation(input, wrapper, field, false);
                    }
                    else textInputValidation(input, wrapper, field, false);
                break;

                case 'select':
                    selectInputValidation(input, wrapper, field, false);
                break;

                case 'radio':
                    checkInputValidation(wrapper, field, false);
                break;

                case 'checkbox':
                    checkInputValidation(wrapper, field, false);
                break;

                case 'image':
                    imageInputValidation(wrapper, field);
                break;
            }
        }
    }
    
    function emailValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input.value) === false) {
                return 'ایمیل صحیح نیست';
            }
            return false;
        });
    }

    function phoneValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            if(/^09\d{9}$/.test(input.value) === false) {
                return 'شماره تلفن همراه صحیح نیست';
            }
            return false;
        });
    }

    function nationalIDValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            if(/\d{10}/.test(input.value) === false) {
                return 'کد ملی صحیح نیست';
            }
            return false;
        });
    }

    function regionValidation(input, wrapper, field, delay, e) {
        var type = document.getElementById(prefex+'RegionType').value;
        textInputValidation(input, wrapper, field, delay, function(){
            return addressValidation(input, wrapper, field, type);
        });
    }

    function roadValidation(input, wrapper, field, delay, e) {
        var type = document.getElementById(prefex+field.id.replace('Name', 'Type')).value;
        textInputValidation(input, wrapper, field, delay, function(){
            return addressValidation(input, wrapper, field, type);
        });
    }

    function unitValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            return addressValidation(input, wrapper, field, 'unit');
        });
    }

    function postCodeValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            if(/\b(?!(\d)\1{3})[13-9]{4}[1346-9][013-9]{5}\b/.test(input.value) === false) {
                return 'کد پستی صحیح نیست';
            }
            return false;
        });
    }

    function houseNumberValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            return addressValidation(input, wrapper, field, 'house_number');
        });
    }

    function buildingNameValidation(input, wrapper, field, delay, e) {
        textInputValidation(input, wrapper, field, delay, function(){
            var noBuildingNo = document.getElementById(prefex+'noHouseNumber').checked;
            if(noBuildingNo && isEmpty(input)) return field.label + ' نمی‌تواند خالی باشد';

            return addressValidation(input, wrapper, field, 'building');
        });
    }

    function addressValidation(input, wrapper, field, type) {
        if(input.value == '' && field.required) return field.label + ' نمی‌تواند خالی باشد';

        var typeField = getFieldById(field.id.replace('Name', 'Type'));
        var typeElement = typeField ? document.getElementById(prefex+typeField.id) : null;
        if(typeElement != null && field.required) {
            if(typeElement.value == '' || typeElement.value == null) {
                return 'لطفا ' + typeField.label + ' را انتخاب کنید';
            }
        }

        if(!input.required && input.value == '') return;

        var params = "type=" + type + "&q=" + input.value;
        params = encodeURI(params);

        ajax({
            url: apiUrl+params,
            method: 'get',
            token: apiToken,
            success: function(e) {
                var res = JSON.parse(e.responseText);
                var help = wrapper.querySelector('.'+prefex+'form-help');
                if(res.error == 0) {
                    setInputError('', wrapper, field, input.value);
                    input.value = res.final_text;
                    help.classList.remove('show');
                } else {
                    setInputError(res.message, wrapper, field, input.value);

                    //Show help
                    if (res.poison.length > 0) {
                        var txt = input.value;
                        for (var i in res.poison) {
                            txt = txt.replace(res.poison[i], "<span class='strike-word'>"+res.poison[i]+"</span>");
                        }
                        txt = 'راهنما: ' + txt;
                        help.innerHTML = txt;
                        help.classList.add('show');
                    }else{
                        help.classList.remove('show');
                    }
                }
            },
            error: function(e) {
                setInputError('', wrapper, field, value.input);
            }
        });

        return 'in_progress';
    }

    function textInputValidation(input, wrapper, field, delay, hasErrorFunc = null) {
        if(field.validationTimeout) {
            clearTimeout(field.validationTimeout);
        }

        if(delay){
            field.validationTimeout = setTimeout(function(){
                innerFunc();
            }, 900);
        }else{
            innerFunc();
        }
        

        function innerFunc () {
            var error = '';
            if(field.required && isEmpty(input)) error = field.label + ' نمی‌تواند خالی باشد.';
            else if(input.value.length > field.maxLength) error = field.label + ' نمی‌تواند بیشتر از ' + field.maxLength +  ' کاراکتر باشد';
            else {
                if(hasErrorFunc != null) error = hasErrorFunc();
            }

            if(error != '') {
                field.hasError = true;
                setInputError(error, wrapper, field, input.value);
            }
            else {
                field.hasError = false;
                setInputError(error, wrapper, field, input.value);
            }
        }
    }

    function selectInputValidation(input, wrapper, field) {
        var error = false;
        if(field.required && input.value == '')  error = field.label + ' را انتخاب کنید';

        if(error) {
            setInputError(error, wrapper, field, input.value);
        }
        else {
            setInputError(error, wrapper, field, input.value);
        }
    }

    function checkInputValidation(wrapper, field) {
        var error = false;
        var input = document.querySelector("input[name='"+field.id+"']:checked");
        if(field.required && !input)  error = field.label + ' را انتخاب کنید';

        if(error) {
            setInputError(error, wrapper, field);
        }
        else {
            setInputError(error, wrapper, field);
        }
    }

    function imageInputValidation(wrapper, field) {
        var error = false;
        var input = document.getElementById(prefex+field.id);
        if(field.required && input.files.length == 0)  error = field.label + ' را انتخاب کنید';
        else if(input.files[0].size > field.maxLength) error = 'حجم فایل انتخاب شده بیشتر از حد مجاز است';

        if(error) {
            setInputError(error, wrapper, field);
        }
        else {
            setInputError(error, wrapper, field);
        }
    }

    function isEmpty(input) {
        if (input.value.trim() == '') return true;
        return false;
    }

    function setInputError(error, wrapper, field, value) {
        var errorElement = getValidationElement(wrapper);
        errorElement.innerHTML = (error && error != 'in_progress') ? error:'';

        if(error == '') {
            field.hasError = false;
            if(field.required || value.trim()) wrapper.classList.add('validated');
            else wrapper.classList.remove('validated');
            document.getElementById(prefex+field.id).classList.remove('error');
        }
        else if(error != 'in_progress') {
            field.hasError = true;
            wrapper.classList.remove('validated');
            document.getElementById(prefex+field.id).classList.add('error');
        }
    }

    function getValidationElement(wrapper) {
        return document.getElementById(wrapper.id).getElementsByClassName(prefex + 'form-validation')[0];
    }
//  </Validation>...........................................

//  <Map>...................................................
    function mapInit(id) {
        L.mapbox.accessToken = 'pk.eyJ1IjoibmFtaW5hYmlsemFkZWgiLCJhIjoiY2s4b3Y3YWJyMDV2cTNmcHJrMHFiejl1ZCJ9.FTlM39MR4Ob3HUTX2CPbtw';
        var map = L.mapbox.map(id)
            .setView([35.7148197, 51.3411997], 15)
            .addLayer(L.mapbox.styleLayer('mapbox://styles/mapbox/streets-v11'));

        L.marker([35.7148197, 51.3411997], {
            icon: L.mapbox.marker.icon({
                'marker-size': 'large',
                'marker-symbol': 'star',
                'marker-color': '#ff5b5c',
            }),
            draggable: true
        }).addTo(map);
    } 
//  </Map>..................................................

//  <Helpers>...............................................
    function ajax(options) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4) {
                if(this.status == 200) {
                    if(options.success) options.success(this);
                } else {
                    if(options.error) options.error(this);
                }
                
            }
        };
        xhttp.open(options.method, options.url, true);
        xhttp.setRequestHeader("Content-type", "application/json");
        if(options.token) {
            xhttp.setRequestHeader("Authorization", "Token "+options.token);
        }
        
        if(options.method == 'post') {
            xhttp.send(JSON.stringify(options.data));
        }else{
            xhttp.send();
        }
    }

    function getFieldById(id) {
        return fields.find(function(x){ return x.id == id; });
    }

    function setPageWidth() {
        if(theme.template == 'paginated') {
            document.querySelector('.'+prefex+'form-wrapper').style.height = pages[currPage-1].el.offsetHeight;
        } else {
            document.querySelector('.'+prefex+'form-wrapper').style.height = 'auto';
        }
    }

    //Set IdentityType Related Inputs visible or hide
    function setITRelatedInputsState(inputs, visible) {
        inputs.forEach(function(id){
            document.getElementById(prefex+id+'-wrapper').style.display = visible ? '' : 'none';
            var field = getFieldById(id);
            field.required = visible ? field.orgRequired : false;
        });
    }
//  </Helpers>..............................................


}($myformInstance = {}))



//  <init>..................................................
window.myForm = {
    init: function(el, action) {
        var f = $myformInstance;
        f.parentId = el;
        document.getElementById(el).innerHTML = '';
        document.getElementById(el).appendChild(f.createForm(action));
        var evt = new CustomEvent('myformready');
        f.addEvents();
        document.getElementById(el).dispatchEvent(evt);
    }
}


if(typeof jQuery != "undefined"){
    (function($){
        $.fn.myForm = function(action = ''){
            var f = $myformInstance;
            f.parentId = el;
            this.html('');
            this.append(f.createForm(action));
            var evt = new CustomEvent('myformready');
            f.addEvents();
            document.getElementById(el).dispatchEvent(evt);
            return this;
        }
    }(jQuery))
}
//  </init>.................................................


